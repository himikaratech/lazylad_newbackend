from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from serializers import MerchantRatingSerializer
from utils import saveRatingWrapper
from account2.utils import process_cashback


class Rating(ViewSet):
    def save_merchant_rating(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.data.copy()
        _serlz = MerchantRatingSerializer(data=data)
        try:
            if _serlz.is_valid():
                ratings = data.get('rating')
                urid = data.get('urid')
                urid_type = data.get('urid_type')
                feedback = data.get('feedback')
                response = saveRatingWrapper(ratings, feedback, urid, urid_type)
                update_dict = dict()
                update_dict['user_code'] = 1
                update_dict['owner_type'] = 0
                update_dict['t_type'] = 'CASHBACK'
                update_dict['urid'] = urid
                update_dict['urid_type'] = urid_type
                update_dict['amount'] = 10.00
                cash_back = process_cashback(update_dict)
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
            return Response(response, api_response_status)
        except Exception as inst:
            print inst
