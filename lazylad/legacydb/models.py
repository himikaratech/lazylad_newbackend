# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
from django.contrib.gis.db import models as model_1


class AboutUsSeller(models.Model):
    info_id = models.AutoField(primary_key=True)
    info_name = models.CharField(max_length=61)
    shall_show = models.BooleanField(null=True, blank=True)
    info_position = models.IntegerField()
    info_text = models.TextField()

    class Meta:
        db_table = 'about_us_seller'
        app_label = 'lazylad_server'


class AccountDetails(models.Model):
    account_id = models.AutoField(primary_key=True)
    account_name = models.CharField(max_length=255, null=True, blank=True)
    account_number = models.CharField(max_length=255, null=True, blank=True)
    ifsc_code = models.CharField(max_length=255, null=True, blank=True)
    branch_details = models.CharField(max_length=255, null=True, blank=True)
    owner_type = models.CharField(max_length=8)
    owner_id = models.CharField(max_length=25)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'account_details'
        app_label = 'lazylad_server'


class AreaDetails(models.Model):
    id = models.AutoField(primary_key=True)
    area_code = models.CharField(unique=True, max_length=45)
    area_name = models.CharField(max_length=25, blank=True, null=True)
    city_code = models.CharField(max_length=45, blank=True, null=True)
    area_launched_for_buyer = models.BooleanField(null=True, blank=True)
    area_launched_for_seller = models.BooleanField(null=True, blank=True)
    area_geocode = model_1.PointField()  # This field type is a guess.

    class Meta:
        db_table = 'area_details'
        unique_together = (('id', 'area_code'),)
        app_label = 'lazylad_server'


class CityDetails(models.Model):
    id = models.AutoField(primary_key=True)
    city_code = models.CharField(unique=True, max_length=45)
    city_name = models.CharField(max_length=25, blank=True, null=True)
    country_code = models.CharField(max_length=25, unique=False)
    country_name = models.CharField(max_length=25, blank=True, null=True)
    launched_for_buyer = models.BooleanField(null=True, blank=True)
    launched_for_seller = models.BooleanField(null=True, blank=True)
    city_image_add = models.CharField(max_length=255, blank=True, null=True)
    city_image_flag = models.BooleanField(null=True, blank=True)
    city_alias_name = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'city_details'
        unique_together = (('id', 'city_code', 'country_code'),)
        app_label = 'lazylad_server'



class CountryDetails(models.Model):
    id = models.AutoField(primary_key=True)
    country_code = models.CharField(unique=True, max_length=45)
    country_name = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'country_details'
        unique_together = (('id', 'country_code'),)
        app_label = 'lazylad_server'


class CouponCityMapping(models.Model):
    id = models.AutoField(primary_key=True)
    coupon_id = models.IntegerField(blank=True, null=True)
    city_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'coupon_city_mapping'
        app_label = 'lazylad_server'


class CouponsByLazylad(models.Model):
    coupon_code = models.CharField(max_length=25, blank=True, null=True)
    coupon_type_id = models.CharField(max_length=25, blank=True, null=True)
    coupon_name = models.CharField(max_length=25, blank=True, null=True)
    discount_amount = models.FloatField(blank=True, null=True)
    number_of_coupons = models.IntegerField(blank=True, null=True)
    minimum_order_amount = models.FloatField(blank=True, null=True)
    coupons_applied = models.IntegerField(blank=True, null=True)
    apply_type = models.CharField(max_length=25, blank=True, null=True)
    max_discount_amount = models.IntegerField(blank=True, null=True)
    num_coupons_per_user = models.IntegerField(blank=True, null=True)
    coupon_active = models.IntegerField(blank=True, null=True)
    city_specific = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'coupons_by_lazylad'
        app_label = 'lazylad_server'


class CsCouponArea(models.Model):
    coupon_id = models.IntegerField(blank=True, null=True)
    city_code = models.IntegerField(blank=True, null=True)
    area_code = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'cs_coupon_area'
        app_label = 'lazylad_server'


class CsCouponCity(models.Model):
    coupon_id = models.IntegerField(blank=True, null=True)
    city_code = models.IntegerField(blank=True, null=True)
    b_citywide = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'cs_coupon_city'
        app_label = 'lazylad_server'


class CsCoupons(models.Model):
    coupon_id = models.AutoField(primary_key=True)
    client_code = models.IntegerField()
    coupon_name = models.CharField(max_length=21)
    coupon_desc = models.CharField(max_length=500)
    coupon_short_desc = models.CharField(max_length=60)
    terms = models.TextField()
    coupon_img_flag = models.IntegerField()
    coupon_img_add = models.TextField()
    lazy_points = models.IntegerField()
    launch_time = models.DateTimeField()
    end_time = models.DateTimeField()
    shall_show = models.IntegerField()
    validity_type = models.CharField(max_length=18)
    validity_time = models.DateTimeField()
    validity_days = models.IntegerField()
    num_coupons_used = models.IntegerField()
    num_coupons = models.IntegerField()
    b_group_coupon = models.IntegerField()
    coupon_code = models.CharField(max_length=12)

    class Meta:
        db_table = 'cs_coupons'
        app_label = 'lazylad_server'


class CsCouponsSold(models.Model):
    sale_id = models.AutoField(primary_key=True)
    buyer_type = models.CharField(max_length=21)
    buyer_urid = models.CharField(max_length=21)
    coupon_id = models.IntegerField()
    coupon_code = models.CharField(max_length=12)
    buy_time = models.DateTimeField()
    validity_time = models.DateTimeField()
    b_redeemed = models.IntegerField()
    lazy_points_spent = models.IntegerField()
    b_reported = models.IntegerField()
    report_stmt = models.TextField()
    b_replied = models.IntegerField()
    replied_stmt = models.TextField()

    class Meta:
        db_table = 'cs_coupons_sold'
        app_label = 'lazylad_server'


class CsGroupCoupons(models.Model):
    single_c_id = models.AutoField(primary_key=True)
    group_coupon_id = models.IntegerField()
    coupon_num = models.IntegerField()
    coupon_code = models.IntegerField()
    b_sold = models.IntegerField()

    class Meta:
        db_table = 'cs_group_coupons'
        app_label = 'lazylad_server'


class ItemDetails(models.Model):
    id = models.AutoField(primary_key=True)
    item_code = models.CharField(max_length=25, blank=True, null=True)
    st_code = models.CharField(max_length=25, blank=True, null=True)
    item_name = models.CharField(max_length=255, blank=True, null=True)
    item_type_code = models.CharField(max_length=45, blank=True, null=True)
    item_unit = models.CharField(max_length=25, blank=True, null=True)
    item_quantity = models.CharField(max_length=25, blank=True, null=True)
    item_cost = models.FloatField(blank=True, null=True)
    item_img_flag = models.IntegerField(blank=True, null=True)
    item_img_add = models.CharField(max_length=255, blank=True, null=True)
    item_short_desc = models.CharField(max_length=60, blank=True, null=True)
    item_desc = models.CharField(max_length=60, blank=True, null=True)
    availability_class = models.CharField(max_length=25, blank=True, null=True)
    sc_code = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'item_details'
        unique_together = (('id', 'item_code'),)
        app_label = 'lazylad_server'


class ItemTypes(models.Model):
    id = models.AutoField(primary_key=True)
    item_type_code = models.CharField(unique=True, max_length=45)
    item_type_name = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        db_table = 'item_types'
        unique_together = (('id', 'item_type_code'),)
        app_label = 'lazylad_server'


class ServiceProviders(models.Model):
    id = models.AutoField(primary_key=True)
    sp_code = models.CharField(unique=True, max_length=45)
    sp_name = models.TextField(blank=True, null=True)
    sp_city_code = models.CharField(max_length=25, unique=False)
    sp_area_code = models.CharField(max_length=25, unique=False)
    sp_latitude = models.FloatField(blank=True, null=True)
    sp_longitude = models.FloatField(blank=True, null=True)
    sp_email = models.TextField(blank=True, null=True)
    sp_password = models.CharField(max_length=45, blank=True, null=True)
    sp_number = models.CharField(max_length=45)
    sp_number_val_flag = models.IntegerField()
    sp_service_type_code = models.CharField(max_length=25, unique=False)
    sp_api_key = models.TextField()
    sp_delivery_time = models.TextField()
    sp_min_order = models.TextField()
    reg_id = models.TextField()
    flag_open = models.IntegerField()
    shop_address = models.TextField(blank=True, null=True)
    shop_open_time = models.IntegerField()
    shop_close_time = models.IntegerField()
    shop_approval_status = models.CharField(max_length=24)
    seller_name = models.TextField()
    shop_open_bits = models.IntegerField()
    inventory_by_us = models.IntegerField()
    logistics_services = models.IntegerField()
    sodexo_coupons = models.IntegerField()
    midnight_delivery = models.IntegerField()
    referral_code_used = models.TextField()
    seller_registration_time = models.DateTimeField()
    comment_text = models.TextField()
    incentive_model = models.IntegerField()
    images_available = models.IntegerField()
    tax_display_1 = models.CharField(max_length=41, blank=True, null=True)
    tax_flag_1 = models.IntegerField(blank=True, null=True)
    tax_number_1 = models.FloatField(blank=True, null=True)
    tax_display_2 = models.CharField(max_length=41, blank=True, null=True)
    tax_display_3 = models.CharField(max_length=41, blank=True, null=True)
    tax_flag_2 = models.IntegerField(blank=True, null=True)
    tax_flag_3 = models.IntegerField(blank=True, null=True)
    tax_number_2 = models.FloatField(blank=True, null=True)
    tax_number_3 = models.FloatField(blank=True, null=True)
    self_del_flat_cut = models.FloatField()
    lazy_del_perc_cut = models.FloatField()
    lazy_del_flat_cut = models.FloatField()
    self_del_perc_cut = models.FloatField()
    delivery_radius = models.FloatField()
    lazylad_store = models.IntegerField()
    lazylad_charge_type = models.IntegerField()
    using_pos = models.IntegerField()

    class Meta:
        db_table = 'service_providers'
        unique_together = (('id', 'sp_service_type_code', 'sp_code'),)
        app_label = 'lazylad_server'


class ItemsOfferedByServiceProvider(models.Model):
    item_code = models.CharField(max_length=25, unique=False)
    sp_code = models.CharField(max_length=25, unique=False)
    item_type_code = models.CharField(max_length=25, unique=False)
    item_name = models.TextField(blank=True, null=True)
    item_unit = models.CharField(max_length=45)
    item_quantity = models.CharField(max_length=45)
    item_cost = models.FloatField(blank=True, null=True)
    item_img_flag = models.IntegerField()
    item_img_add = models.TextField()
    item_short_desc = models.CharField(max_length=60)
    item_desc = models.CharField(max_length=60, blank=True, null=True)
    item_status = models.IntegerField()
    sc_code = models.CharField(max_length=45)
    sp_bfr_offer_price = models.FloatField()
    b_in_offer = models.IntegerField()

    class Meta:
        db_table = 'items_offered_by_service_provider'
        app_label = 'lazylad_server'


class LazyladConfig(models.Model):
    config_name = models.CharField(max_length=40)
    config_value_integer = models.IntegerField()
    config_value_string = models.CharField(max_length=40)

    class Meta:
        db_table = 'lazylad_config'
        app_label = 'lazylad_server'


class OfferDetails(models.Model):
    offer_code = models.AutoField(primary_key=True)
    offer_tag = models.TextField()
    offer_desc = models.TextField()
    offer_launch_time = models.DateTimeField()
    offer_validity = models.IntegerField()
    offer_img_flag = models.IntegerField()
    offer_img_add = models.TextField()
    shall_show = models.IntegerField()

    class Meta:
        db_table = 'offer_details'
        app_label = 'lazylad_server'


class OrderDetails(models.Model):
    id = models.AutoField(primary_key=True)
    order_code = models.CharField(unique=True, max_length=45)
    user_code = models.CharField(max_length=25, unique=False)
    expected_del_time = models.DateTimeField()
    del_address_code = models.CharField(max_length=45, blank=True, null=True)
    del_address_user_code = models.CharField(max_length=45)
    order_date = models.CharField(max_length=90, blank=True, null=True)
    order_tot_amount = models.FloatField()
    order_total_cost = models.FloatField()
    order_delivery_cost = models.FloatField()
    order_discount_cost = models.FloatField()
    order_orig_amount = models.FloatField()
    coupon_code = models.TextField()
    taxes = models.FloatField()
    tax_percentage = models.FloatField()
    logistics_services_used = models.IntegerField()
    rider_assigned = models.IntegerField()
    rider_accepted = models.IntegerField()

    class Meta:
        db_table = 'order_details'
        unique_together = (('id', 'order_code'),)
        app_label = 'lazylad_server'


class OrderInProgress(models.Model):
    order_code = models.CharField(max_length=25, unique=False)
    sp_code = models.CharField(max_length=25, unique=False)
    status = models.CharField(max_length=45, blank=True, null=True)
    max_no_of_items = models.CharField(max_length=45, blank=True, null=True)
    user_code = models.CharField(max_length=25, unique=False)

    class Meta:
        db_table = 'order_in_progress'
        app_label = 'lazylad_server'


class OrderItemDetails(models.Model):
    item_code = models.CharField(max_length=25, unique=False)
    item_quantity = models.CharField(max_length=45, blank=True, null=True)
    user_code = models.CharField(max_length=25, unique=False)
    order_code = models.CharField(max_length=25, unique=False)
    item_cost = models.FloatField()

    class Meta:
        db_table = 'order_item_details'
        app_label = 'lazylad_server'


class OrderItemServedBySp(models.Model):
    order_code = models.CharField(max_length=45)
    user_code = models.CharField(max_length=45)
    sp_code = models.CharField(max_length=45)
    item_code = models.CharField(max_length=45)
    item_cost = models.FloatField()

    class Meta:
        db_table = 'order_item_served_by_sp'
        app_label = 'lazylad_server'


class OrderPaymentDetails(models.Model):
    pid = models.AutoField(primary_key=True)
    order_code = models.IntegerField()
    p_mode = models.CharField(max_length=7)
    amount = models.FloatField()
    p_status = models.CharField(max_length=7)
    p_tid = models.IntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()

    class Meta:
        db_table = 'order_payment_details'
        app_label = 'lazylad_server'


class OrderStrings(models.Model):
    o_id = models.AutoField(primary_key=True)
    order_string = models.CharField(unique=True, max_length=12)

    class Meta:
        db_table = 'order_strings'
        app_label = 'lazylad_server'


class ServiceProvidersDeliveryDetails(models.Model):
    id = models.AutoField(primary_key=True)
    sp_code = models.CharField(max_length=45)
    area_code = models.CharField(max_length=45)
    sp_delivery_time = models.TextField()
    sp_min_order = models.TextField()
    score = models.FloatField()

    class Meta:
        db_table = 'service_providers_delivery_details'
        unique_together = (('id', 'sp_code', 'area_code'),)
        app_label = 'lazylad_server'


class ServiceTypeCategories(models.Model):
    id = models.AutoField(primary_key=True)
    st_code = models.CharField(max_length=45)
    sc_code = models.CharField(max_length=45)
    sc_name = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'service_type_categories'
        unique_together = (('id', 'st_code', 'sc_code'),)
        app_label = 'lazylad_server'


class ServiceTypeStatus(models.Model):
    st_code = models.IntegerField()
    city_code = models.IntegerField()
    launched_for_buyer = models.IntegerField()
    launched_for_seller = models.IntegerField()
    seller_automated = models.IntegerField()

    class Meta:
        db_table = 'service_type_status'
        app_label = 'lazylad_server'


class ServiceTypeSubCategories(models.Model):
    id = models.AutoField(primary_key=True)
    st_code = models.CharField(max_length=45)
    sc_code = models.CharField(max_length=45)
    ssc_code = models.CharField(max_length=45)
    ssc_name = models.TextField()

    class Meta:
        db_table = 'service_type_sub_categories'
        unique_together = (('id', 'st_code', 'sc_code', 'ssc_code'),)
        app_label = 'lazylad_server'


class ServiceTypes(models.Model):
    id = models.AutoField(primary_key=True)
    st_code = models.CharField(unique=True, max_length=45)
    st_name = models.TextField(blank=True, null=True)
    st_img_flag = models.IntegerField()
    st_img_add = models.TextField()
    sort_order = models.IntegerField()
    items_common = models.IntegerField()

    class Meta:
        db_table = 'service_types'
        unique_together = (('id', 'st_code'),)
        app_label = 'lazylad_server'


class UserAddressDetails(models.Model):
    id = models.AutoField(primary_key=True)
    user_code = models.IntegerField()
    user_address_code = models.IntegerField()
    user_flat = models.TextField()
    user_subarea = models.TextField()
    user_area = models.TextField()
    user_city = models.TextField()
    user_address = models.TextField()
    user_name = models.TextField()
    user_email_id = models.TextField()
    user_phone_number = models.TextField()
    shall_show = models.IntegerField()
    address_lat = models.FloatField(blank=True, null=True)
    address_long = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'user_address_details'
        unique_together = (('id', 'user_code', 'user_address_code'),)
        app_label = 'lazylad_server'


class UserDetails(models.Model):
    id = models.AutoField(primary_key=True)
    created = models.DateTimeField()
    user_code = models.CharField(unique=True, max_length=45)
    reg_id = models.TextField(blank=True, null=True)
    phone_number = models.TextField(blank=True, null=True)
    user_current_city = models.CharField(max_length=30, blank=True, null=True)
    user_current_area = models.CharField(max_length=30, blank=True, null=True)
    lazylad_version_code = models.IntegerField()
    imei_number = models.CharField(max_length=21)
    mac_address = models.CharField(max_length=21)
    os_type = models.IntegerField()
    model_name = models.CharField(max_length=21)
    phone_manufacturer = models.CharField(max_length=21)
    last_update_time = models.DateTimeField()
    user_delivery_lat = models.FloatField(blank=True, null=True)
    user_delivery_long = models.FloatField(blank=True, null=True)
    user_delivery_city = models.IntegerField()

    class Meta:
        db_table = 'user_details'
        unique_together = (('id', 'user_code'),)
        app_label = 'lazylad_server'


class UserProfile(models.Model):
    profile_id = models.AutoField(primary_key=True)
    phone_number = models.CharField(max_length=16)
    first_name = models.CharField(max_length=41)
    last_name = models.CharField(max_length=41)
    user_email = models.CharField(max_length=255)
    lazy_points_balance = models.IntegerField()

    class Meta:
        db_table = 'user_profile'
        app_label = 'lazylad_server'
