
from rest_framework import fields, serializers


class AddUserSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    phone_number = serializers.CharField(required=True)
    origin = serializers.CharField(required=False)


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)


class SampleOrderSerializer(serializers.Serializer):
    items_amount = serializers.CharField(required=True)
    amount_payable = serializers.CharField(required=True)
    delivery_charge = serializers.CharField(required=False)
    discount = serializers.CharField(required=False)
    taxes = serializers.CharField(required=False)
    coupon_code = serializers.CharField(required=False)


class SampleItemSerializer(serializers.Serializer):
    item_code = serializers.CharField(required=True)
    item_quantity = serializers.CharField(required=True)
    item_cost = serializers.CharField(required=True)


class SampleAddressSerializer(serializers.Serializer):
    address_code = serializers.CharField(required=True)
    address_user_code = serializers.CharField(required=True)


class SendOrdersSerializer(serializers.Serializer):
    order = serializers.DictField(required=True)
    items_list = serializers.ListField(required=True)
    orders_list = serializers.ListField(required=False)
    address_key = serializers.CharField(required=True)
    city_code = serializers.CharField(required=False)
    user_code = serializers.CharField(required=True)


class SendOrdersPOSSerializer(serializers.Serializer):
    order = serializers.CharField(required=True)
    items_list = serializers.CharField(required=True)
    city_name = serializers.CharField(required=False)
    sp_code = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    phone_number = serializers.CharField(required=True)


class GetItemSerializer(serializers.Serializer):
    st_code = serializers.CharField(required=True)
    sc_code = serializers.CharField(required=True)
    prev_item_code = serializers.CharField(required=False)


class SyncSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)


class ServiceCatSerlz(serializers.Serializer):
    user_code = serializers.CharField(required=True)


class AddAddressSerializer(serializers.Serializer):
    city_id = serializers.IntegerField(required=True)
    user_code = serializers.CharField(required=True)
    flat = serializers.CharField(required=False)
    subarea = serializers.CharField(required=False)
    area = serializers.CharField(required=False)
    pincode = serializers.CharField(required=False)
    latitude = serializers.CharField(required=False)
    longitude = serializers.CharField(required=False)


class OrderSerializer(serializers.Serializer):
    order_code = serializers.CharField(required=True)
    logistics_id = serializers.IntegerField(required=False)


class GetOrdersSerializer(serializers.Serializer):
    vendor_id = serializers.CharField(required=True)
    prev_order_code = serializers.CharField(required=False)
    status = serializers.CharField(required=False)


class DummySerializer(serializers.Serializer):
    dummy = serializers.CharField(required=False)


class BrandSerializer(serializers.Serializer):
    brand_name = serializers.CharField(required=True)
    prev_item_code = serializers.CharField(required=False)


class VendorAccountSerializer(serializers.Serializer):
    user_code = serializers.CharField(required=True)
    status = serializers.CharField(required=True)