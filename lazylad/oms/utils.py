import linecache
import sys
import operator
import json
import math
from account.utils import return_wallet_details
from account.constant.UserType import CUSTOMER
from .LazyladConnection import LazyladConnection
from django.db import connections
import lazyprime.models as lpm
from lazyprime.LazyPrimeConnection import LazyPrime as Prime


def group_list_by_sp_code(unsorted_list):
    """

    :param unsorted_list: [{
                            'sp_code': 'qqqqq'
                            'price': 100,
                            'qty': 5,
                            'item_code': 'qqqq'
                            },
                            {},
                            {}]
    :return:
        {
            'sp_code_1': [
                            {'qty': 5,
                            'price': 100.0,
                            'item_code': 'qqqqq'},

                          ],
            'sp_code_2': [....
        }
    """
    return_dict = {}
    v5_flag = False
    for item in unsorted_list:
        try:
            temp_dict = item.copy()
            temp_dict.pop('sp_code')
            if item.get('sp_code') in return_dict.keys():
                return_dict[item.get('sp_code')].append(temp_dict)
            else:
                return_dict[item.get('sp_code')] = [temp_dict]
            if 'itemOfferCost' and 'itemOfferFlag' in\
                    return_dict[item.get('sp_code')][0].keys():
                v5_flag = True
        except:
            pass
    return return_dict, v5_flag


def group_by_sp_code(unsorted_list):
    """

    :param unsorted_list: [{
                            'sp_code': 'qqqqq'
                            'price': 100,
                            'qty': 5,
                            'item_code': 'qqqq'
                            },
                            {},
                            {}]
    :return:
        {
            'sp_code_1': {
                            'qty': 5,
                            'price': 100.0,
                            'item_code': 'qqqqq',
                          },
            'sp_code_2': [....
        }
    """
    return_dict = {}
    for item in unsorted_list:
        temp_dict = item.copy()
        sp_code = temp_dict.get('sp_code')
        temp_dict.pop('sp_code')
        return_dict[sp_code] = temp_dict
    return return_dict


def compute_total_items_amount(
        sorted_shopping_cart, sorted_seller_details):
    """

    :param sorted_shopping_cart: output from above
    :param sorted_seller_details: output from above
    :return:
        total_items_amount : 7735.0
        return_dict :   {
                        'sp_code_1': {
                                    'total_items_amount': 7000.0,
                                    'total_amount': 7777.77,
                                    'JsonDataArray': [{}, {},....],
                                    'delivery_charges': 77.0,
                                    'taxes': 101,
                                 },
                        ...........
                        }
    """
    try:
        grand_total_items_amount = 0.0
        return_dict = {}
        for sp_code, items in sorted_shopping_cart.items():
            return_dict[sp_code] = \
                {
                    'JsonDataArray': [],
                    'total_items_amount': 0.0,
                    'tot_amount': 0.0
                }
            for item in items:
                if item.get('itemOfferFlag') == 1:
                    item_cost = float(item.get('itemOfferCost'))
                else:
                    item_cost = item.get('itemCost')
                return_dict[sp_code]['total_items_amount'] += (
                    item.get('itemQuantitySelected') * item_cost)
            return_dict[sp_code]['tot_amount'] =\
                round(return_dict[sp_code]['total_items_amount'] +
                          sorted_seller_details[sp_code]['delivery_charges'] +
                          sorted_seller_details[sp_code]['taxes'])
            return_dict[sp_code]['JsonDataArray'] = json.dumps(items)
            return_dict[sp_code]['delivery_charges'] = \
                sorted_seller_details[sp_code]['delivery_charges']
            return_dict[sp_code]['taxes'] = sorted_seller_details[sp_code]['taxes']
            grand_total_items_amount += return_dict[sp_code]['total_items_amount']
        return grand_total_items_amount, return_dict

    except KeyError:
        return None, None


def sort_cart_by_amount(processed_shopping_cart):
    """
    returns sorted tuple in ascending order of total items amount
    :param processed_shopping_cart:
    :return:
    """
    return_dict = {}
    for sp_code, value in processed_shopping_cart.items():
        return_dict[sp_code] = value.get('tot_amount')
    sorted_list = sorted(return_dict.items(), key=operator.itemgetter(1))
    return sorted_list


def process_discount(user_code, sorted_list, order_payment_details,
                     processed_shopping_cart, discount_amount=0.0):
    """

    :param sorted_tuple:
    :param order_payment_details:
    :param processed_shopping_cart:
    :param discount_amount:
    :return: processed_shopping_cart -->
    {
        'sp_code_1': {
                        'sp_code': 'seller_Abc'
                        'total_items_amount': 63.0,
                        'total_amount': 50.0,
                        'JsonDataArray': [{}, {},....],
                        'delivery_charges': 10.0,
                        'taxes': 3.0,
                        'discount': 26.0,
                        'order_payment_details': {
                                'COD': 22.0
                                'online': 0.0
                                'wallet': 28.0
                        }
                     },
    }
    """
    try:
        connection  = LazyladConnection()
        user_phone_no = connection.get_user_phone_no_from_user_code(user_code)
        wallet_details = return_wallet_details(user_phone_no, CUSTOMER)
        usage_entry = None
        for each in wallet_details:
            if each.get('urid_type') == 'Order':
                usage_entry = each
                break

        online_paid = False
        discount = discount_amount
        wallet = order_payment_details.get('WALLET')
        total_amount = float(0)
        discount_dict = dict()
        for payment in sorted_list:
            total_amount += payment[1]
        for payment in sorted_list:
            discount_dict[payment[0]] = (payment[1]/total_amount) * discount
        for order_tuple in sorted_list:
            order = list(order_tuple)
            processed_shopping_cart[order[0]]['sp_code'] = order[0]
            online = 0.0
            cod = order[1]
            if order_payment_details.get('ONLINE') and not order_payment_details.get('ONLINE') == 0.0:
                cod = 0.0
                online = order[1]
                online_paid = True
            if order_payment_details.get(
                    'PROMOTIONAL') and not order_payment_details.get('PROMOTIONAL') == 0.0:
                processed_shopping_cart[order[0]].update({
                    'discount_amount': 0.0,
                    'order_payment_details': {
                        'ONLINE': online,
                        'COD': cod,
                        'WALLET': 0.0,
                        'PROMOTIONAL': order_payment_details.get('PROMOTIONAL')}
                })
            else:
                processed_shopping_cart[order[0]].update({
                    'discount_amount': 0.0,
                    'order_payment_details': {
                        'ONLINE': online,
                        'COD': cod,
                        'WALLET': 0.0}
                })
            if order_payment_details.get(
                    'OFFER') and not order_payment_details.get('OFFER') == 0.0:
                processed_shopping_cart[order[0]]['order_payment_details'].update({
                        'OFFER': order_payment_details.get('OFFER')}
                )
            if discount_dict[order[0]] != 0:
                order[1] -= discount_dict[order[0]]
                processed_shopping_cart[order[0]]['tot_amount'] = round(order[1])
                processed_shopping_cart[order[0]]['discount_amount'] = round(discount_dict[order[0]])
                discount_dict[order[0]] = 0.0

            if order[1] != 0 and wallet != 0:
                wallet_amount_usable = order[1]
                if usage_entry :
                    if usage_entry.get('usage_type') == 1:
                        wallet_amount_usable = \
                            wallet_amount_usable*usage_entry.get('usage_number')/100
                        wallet_amount_usable = math.floor(min(
                            wallet_amount_usable, usage_entry.get('usage_cap'), wallet))
                else:
                    wallet_amount_usable = math.floor(min(wallet_amount_usable, wallet))

                processed_shopping_cart[order[0]][
                        'order_payment_details']['WALLET'] = wallet_amount_usable
                if online_paid:
                    processed_shopping_cart[order[0]][
                        'order_payment_details']['ONLINE'] = order[1] - wallet_amount_usable
                else:
                    processed_shopping_cart[order[0]][
                        'order_payment_details']['COD'] = order[1] - wallet_amount_usable

            processed_shopping_cart[order[0]]['order_payment_details'] = \
                json.dumps(processed_shopping_cart[order[0]]['order_payment_details'])

        return processed_shopping_cart
    except:
        None


def check_coupon_code_validity(data):
    try:
        response = dict()
        coupon_name = data['coupon_code']
        user_code = data['user_code']
        total_amount = data['total_amount']
        cursor = connections['lazylad_server'].cursor()
        query = "select * from coupons_by_lazylad where coupon_name='"+str(coupon_name)+"' and coupon_active = 1"
        rows_fetched = cursor.execute(query)
        if int(rows_fetched) == 1:
            columns = [column[0] for column in cursor.description]
            coupon_details = dict(zip(columns, cursor.fetchone()))
            if int(coupon_details["coupons_applied"]) < int(coupon_details["number_of_coupons"]):
                discount = coupon_details["discount_amount"]
                response["error"] = 0
                response["coupon_type_id"] = coupon_details["coupon_type_id"]
                if int(coupon_details["coupon_type_id"]) == 1 or int(coupon_details["coupon_type_id"]) == 2:
                    response["coupon_amount"] = discount
                    response["minimum_order_amount"] = coupon_details["minimum_order_amount"]
                    response["success"] = True
                elif int(coupon_details["coupon_type_id"]) == 3:
                    if (0 != coupon_details["minimum_order_amount"]) and (float(total_amount) < coupon_details["minimum_order_amount"]):
                        response = dict()
                        response["coupon_type_id"] = coupon_details["coupon_type_id"]
                        response["error"] = 0
                        response["message"] = "Please add more items to your cart to avail this coupon"
                        response["success"] = False
                    else:
                        query = "select count(*)  as num_coupon_used from order_details where user_code in \
                         (select user_code from user_details where phone_number = ( select phone_number from \
                         user_details where user_code = '" + str(user_code)+"' ) ) and coupon_code = lower('" \
                                + str(coupon_name) + "')"
                        cursor.execute(query)
                        coupon_usage_number = cursor.fetchone()[0]
                        if (0 != coupon_details["num_coupons_per_user"]) and (coupon_details["num_coupons_per_user"] <= \
                            coupon_usage_number):
                            response = dict()
                            response["coupon_type_id"] = coupon_details["coupon_type_id"]
                            response["error"] = 1
                            response["message"] = "Coupon usage maximum limit reached for you"
                            response["success"] = False
                        else:
                            valid = True
                            max_discount_amount = coupon_details['max_discount_amount']
                            if int(coupon_details["city_specific"]):
                                connection = LazyladConnection()
                                city_code = connection.get_user_delivery_city(user_code)
                                if not int(city_code):
                                    city_code = connection.get_user_city_code_from_user_code(user_code)
                                if city_code:
                                    query = "select count(*) from coupon_city_mapping where coupon_id ='" + \
                                            str(coupon_details["id"]) + "' and city_id = " + str(city_code)
                                    cursor.execute(query)
                                    city_allowed_for_coupon = cursor.fetchone()[0]
                                    if not int(city_allowed_for_coupon):
                                        valid = False
                            if valid:
                                if coupon_details['apply_type'] == 'FLAT':
                                    if 0 != int(discount):
                                        if float(total_amount) < float(discount):
                                            final_discount = float(total_amount)
                                        else:
                                            final_discount = discount
                                    else:
                                        if(0 == int(max_discount_amount)) or (float(total_amount) < max_discount_amount):
                                            final_discount = float(total_amount)
                                        else:
                                            final_discount = max_discount_amount
                                    response["coupon_amount"] = float(round(final_discount))
                                    response["minimum_order_amount"] = coupon_details["minimum_order_amount"]
                                    response["success"] = True
                                elif coupon_details['apply_type'] == 'Percentage':
                                    if 0 == int(discount):
                                        percentage_amount = float(total_amount)
                                    else:
                                        percentage_amount = ((float(discount) * float(total_amount))/100)
                                    if 0 == max_discount_amount or int(percentage_amount) < max_discount_amount:
                                        final_discount = percentage_amount
                                    else:
                                        final_discount = max_discount_amount
                                    response["coupon_amount"] = float(round(final_discount))
                                    response["minimum_order_amount"] = coupon_details["minimum_order_amount"]
                                    response['success'] = True
                                else:
                                    response = dict()
                                    response["coupon_type_id"] = coupon_details["coupon_type_id"]
                                    response["error"] = 1
                                    response["message"] = "Invalid coupon"
                                    response["success"] = False
                            else:
                                response = dict()
                                response["coupon_type_id"] = coupon_details["coupon_type_id"]
                                response["error"] = 1
                                response["message"] = "coupon Invalid for your city"
                                response["success"] = False
                else:
                    response = dict()
                    response["error"] = 1
                    response["message"] = "Coupon does not exist"
                    response["success"] = False
            else:
                response["error"] = 1
                response["message"] = "Coupon expired"
                response["success"] = False

        else:
            response["error"] = 1
            response["message"] = "Coupon does not exist"
            response["success"] = False

    except TypeError:
        response = dict()
        response["error"] = 1
        response["message"] = "Type Error : " + print_exception()
        response["success"] = False
    except Exception:
        response = dict()
        response["error"] = 1
        response["message"] = "Exception Error : " + print_exception()
        response["success"] = False
    except:
        response = dict()
        response["error"] = 1
        response["message"] = print_exception()
        response["success"] = False
    return response


def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    line_no = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, line_no, f.f_globals)
    return str('EXCEPTION IN ({}, LINE {} :: "{}"): {}'.format(filename, line_no, line.strip(), exc_obj))


def get_lpm_orders(pen_last_order, con_last_order):
    resp = dict()
    try:
        pen_orders = lpm.Order.objects.filter(
            status='PEN').order_by(
            "-post_time")[int(pen_last_order):int(pen_last_order)+5]
        con_orders = lpm.Order.objects.filter(
            status='CNF').order_by(
            "-post_time")[int(con_last_order):int(con_last_order)+5]
        conn = Prime()
        pen_orders_list = list()
        con_orders_list = list()
        for order in pen_orders:
            order_dict = conn.view_order(order.order_code)
            user_add = conn.get_address(order.user_code, order.address_id)
            order_dict.update(user_add)
            pen_orders_list.append(order_dict)
        for order in con_orders:
            order_dict = conn.view_order(order.order_code)
            user_add = conn.get_address(order.user_code, order.address_id)
            order_dict.update(user_add)
            con_orders_list.append(order_dict)
        resp = {
            "success": True,
            "pending": True if len(pen_orders_list) else False,
            "confirmed": True if len(con_orders_list) else False,
            "pen_orders": pen_orders_list,
            "con_orders": con_orders_list
        }
    except:
        resp['success'] = False
        resp['message'] = print_exception()
    return resp



