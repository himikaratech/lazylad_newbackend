
import json
import string
import random
import linecache
import sys
from decimal import Decimal
from django.core.exceptions import MultipleObjectsReturned
from django.core.mail import send_mail
from lazylad.settings import EMAIL_HOST_USER


from .models import Order, Item, SellerItems


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def post_order_items_to_server(order_code, item_list):
    """
    sample order:
        {
            'order_code': [
                {
                    'order_code':
                    'local_flag':
                    'item_code':
                    ..
                    ..
                    ..
                    ..
                },
            ]
        }
    :return:
    """
    order_item_list = list()
    try:
        for item_dict in item_list:
            order_item_list.append(
                {'item_code': item_dict.get('item_code'),
                 'item_cost': item_dict.get('item_cost'),
                 'quantity': item_dict.get('quantity')})
        order = Order.objects.get(order_code=order_code)
        order.items = json.dumps(order_item_list)
        order.save()
    except:
        return False
    return True


def generate_order_code():
    new_order_code = None
    while 1:
        try:
            new_order_code = id_generator()
            order = Order.objects.get(order_number=new_order_code)
        except Order.DoesNotExist:
            return new_order_code


def generate_item_code():
    new_item_code = Item.objects.filter(item_code__isnull=False).extra(
        select={'char_code': 'CAST(item_code AS UNSIGNED)'}
    ).order_by('char_code').last().item_code
    return str(int(new_item_code)+1)


# def generate_barcode():
#     new_item_barcode = 100000000001
#     try:
#         new_item_barcode = Item.objects.filter(
#             item_barcode__isnull=False).extra(
#             select={'char_code': 'CAST(item_barcode AS UNSIGNED)'}
#         ).order_by('char_code').last().item_barcode
#         new_item_barcode = str(int(new_item_barcode)+1)
#     except TypeError:
#         while True:
#             item_exist = Item.objects.get(item_barcode=str(new_item_barcode))
#             new_item_barcode += 1
#     except Item.DoesNotExist:
#         return new_item_barcode
#     except:
#         return None
#     return new_item_barcode

def generate_barcode():
    most_recent_barcode = 100000000001
    while True:
        try:
            most_recent_barcode = str(int(Item.objects.filter(
                item_barcode__isnull=False).last().item_barcode) + 1)
            item_exist = Item.objects.get(item_barcode=most_recent_barcode)
        except MultipleObjectsReturned:
            pass
        except Item.DoesNotExist:
            return most_recent_barcode
        except:
            return None
    return most_recent_barcode


def calculate_revenue(revenue_model, order_cost):
    try:
        revenue = 0.00
        perc_cut = (revenue_model.perc_cut * Decimal(order_cost))/100
        flat_cut = revenue_model.flat_cut
        revenue = Decimal(revenue) + perc_cut + flat_cut
        if revenue < revenue_model.max_cut:
            return revenue
        else:
            return revenue_model.max_cut
    except:
        return None


def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    line_no = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, line_no, f.f_globals)
    return str('EXCEPTION IN ({}, LINE {} :: "{}"): {}'.format(
        filename, line_no, line.strip(), exc_obj))


def email_customer(message, to_email):
    try:
        send_mail('New Order From Lazylad', message,
                  EMAIL_HOST_USER, [to_email], fail_silently=False)
        return True
    except:
        return False

