ANNOUNCEMENT

.idea directory removed from server and it may cause conflict when you pull, then you can use this command 
git rm -r --cached .idea
and commit the merge commit

Also when you are working on other local branches you need to merge this change in your other branch too  
and then push it to your branch on bitbucket otherwise when you simply merge your other bitbucket branch
into master, these .idea files will again come to master. So please follow the steps.


README

Install First ->(Run Commands on root or superuser)
(Either Manually or using requirements.txt file you can install required packages)

A) Manually Installation:

1. Django (1.8.4 or higher) -> pip install Django==1.8.4
2. Django rest framework -> pip install djangorestframework
3. Markdown -> pip install markdown 
4. Django filter -> pip install django-filter
5. MySQL-server -> For Fedora: yum install mysql-server,
                   For Ubuntu: apt-get install mysql-server
6. MySQL-python -> For Fedora: yum install MySQL-python,
                   For Ubuntu: 1. apt-get install python-dev libmysqlclient-dev, 2. pip install MySQL-python
7. phpMyadmin (optional for simplicity)

B) Using requirements.txt file:

Run on terminal:  pip install -r /path/to/requirements.txt

Steps :
1. start mysql service
2. create a database with name "lazylad"
3. move to directory containing manage.py
4. Run on terminal: python manage.py syncdb
5. Run on terminal: python manage.py runserver


super user:
username : root
password : lazylad

//api links available
admin-pannel like phpmyadmin
url: /admin

//testing of api
url: /test-api




##Endpoints

/apiv1.0/getWalletDetails  [POST]
show the wallet details
require owner_id,owner_type




/apiv1.0/doesWalletExists  [POST]
returns if wallet exists
require owner_id,owner_type



/apiv1.0/getAllTransactions  [POST]
show the wallet details
require owner_id,owner_type
optional previous_tid   //previous trasaction id


/apiv1.0/getTransactionsForURID  [POST]
show the wallet details
require owner_id,owner_type,URID


/apiv1.0/getTransactionForURID  [POST]
show the wallet details
require t_id,URID,URID_TYPE


UML Links:

Wallet-Version v12
http://creately.com/diagram/example/iev9pnmt1

Transaction_relation v6
http://creately.com/diagram/example/ievbvft61

transaction_uml_1 v15
http://creately.com/diagram/example/ievbvft62

transaction_uml_1 v1
http://creately.com/diagram/example/ievbvft63

