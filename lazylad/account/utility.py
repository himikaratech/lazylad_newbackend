from rest_framework import serializers, viewsets
from django.contrib.auth.models import User

from database.models import *


# wallet serializer and class
class WalletSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Wallet
        fields = ('w_id', 'owner_id', 'owner_type', 'balance', 'neg_allowed', 'max_neg_balance')


class WalletSet(viewsets.ModelViewSet):
    def post(self, request):
        print request.get_data();

    queryset = Wallet.objects.filter(w_id='33')
    serializer_class = WalletSerializer


class Posting(viewsets.ModelViewSet):
    queryset = Wallet.objects.all()
    serializer_class = WalletSerializer


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
