from django.conf.urls import url, include

from . import views

# Routers provide an easy way of automatically determining the URL conf.
from rest_framework import routers
from account.utility import UserViewSet, WalletSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'wallets', WalletSet)

urlpatterns = [
    url(r'^createWallet', views.createWallet, name='createWallet'),
    url(r'^doesWalletExists', views.doesWalletExists, name='doesWalletExists'),
    url(r'^getWalletDetails', views.getWalletDetails, name='getWalletDetails'),
    url(r'^getAllTransactions', views.getAllTransactions, name='getAllTransactions'),
    url(r'^updateStatusofTransaction', views.updateStatusofTransaction, name='updateStatusofTransaction'),
    url(r'^getTransactionsForURID', views.getTransactionsForURID, name='getTransactionsForURID'),
    url(r'^getTransactionForURID', views.getTransactionForURID, name='getTransactionForURID'),
    url(r'^getTransactionURIDTTYPE', views.getTransactionURIDTTYPE, name='getTransactionURIDTTYPE'),
    url(r'^netValueForT_TYPE_AND_URID', views.netValueForT_TYPE_AND_URID, name='netValueForT_TYPE_AND_URID'),
    url(r'^netValueForURID', views.netValueForURID, name='netValueForURID'),
    url(r'^makeTransaction', views.makeTransaction, name='makeTransaction'),
    url(r'^orderPaymentDetails', views.orderPaymentDetails, name='orderPaymentDetails'),
    url(r'^walletCashbackByCoupon', views.walletCashbackByCoupon, name='walletCashbackByCoupon'),
    url(r'^test', views.test, name='test'),
    url(r'^', include(router.urls)),
]