
from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import Customer, Merchant

api_urlpatterns = patterns(
    'lazyad.apiviews',
    url(
        r'^customer/get_all_coupon_codes',
        Customer.as_view({'get': 'get_all_coupon_codes'})),
    url(
        r'^customer/get_cities',
        Customer.as_view({'get': 'get_cities'})),
    url(
        r'^customer/coupons_clicks',
        Customer.as_view({'post': 'coupons_clicks'})),
    url(
        r'^customer/coupons_views',
        Customer.as_view({'post': 'coupons_views'})),
    url(
        r'^customer/user_signup',
        Customer.as_view({'post': 'user_signup'})),
    url(
        r'^customer/user_signin',
        Customer.as_view({'get': 'user_signin'})),
    url(
        r'^customer/user_login',
        Customer.as_view({'post': 'user_login'})),
    url(
        r'^customer/get_coupon_code_details',
        Customer.as_view({'get': 'get_coupon_code_details'})),
    url(
        r'^customer/get_coupon_details',
        Customer.as_view({'get': 'get_coupon_details'})),
    url(
        r'^customer/buy_coupon',
        Customer.as_view({'post': 'buy_coupon'})),
    url(
        r'^customer/get_coupons',
        Customer.as_view({'get': 'get_coupons'})),
    url(
        r'^merchant/redeem_coupon',
        Merchant.as_view({'post': 'redeem_coupon'})),
    url(
        r'^merchant/sign_in_merchant',
        Merchant.as_view({'post': 'sign_in_merchant'})),
    url(
        r'^merchant/coupon_verification',
        Merchant.as_view({'post': 'merchant_coupon_verification'})),
    url(
        r'^merchant/get_coupons',
        Merchant.as_view({'post': 'merchant_coupons'})),
    url(
        r'^merchant/coupon_details',
        Merchant.as_view({'post': 'merchant_coupon_details'})),
    url(
        r'^merchant/coupon_analytics',
        Merchant.as_view({'post': 'merchant_coupon_analytics'})),
    url(
        r'^merchant/lazylad_coupon_analytics',
        Merchant.as_view({'get': 'lazylad_coupon_analytics'})),
    url(
        r'^merchant/lazylad_item_coupon_analytics$',
        Merchant.as_view({'get': 'lazylad_item_coupon_analytics'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)