from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from .Connections import LazyladConnection
from .serializers import OrderDetailsSerlz, ConfigAPISerlz


class UserOrders(ViewSet):

    def get_user_previous_orders(self, request, user_code):
        """
        API to send user previous orders
        :param request: GET

        :return:
        """
        response = dict()
        connection = LazyladConnection()
        try:
            api_response_status = status.HTTP_400_BAD_REQUEST
            data = self.request.data.copy()
            if not data:
                data = self.request.GET.copy()
            if len(str(user_code)):
                prev_order_code = data.get('previous_order_code', None)
                response = connection.get_user_previous_orders(user_code, prev_order_code)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error_code': 1})
            else:
                response['error_details'] = 'Bad Input'
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_user_previous_orders_v2(self, request, user_code, page_number):
        """
        API to send user previous orders
        :param request: GET

        :return:
        """
        response = dict()
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            api_response_status = status.HTTP_400_BAD_REQUEST
            data = self.request.data.copy()
            if not data:
                data = self.request.GET.copy()
            if len(str(user_code)):
                prev_order_code = data.get('previous_order_code', None)
                response = connection.get_user_previous_orders_v2(
                    user_code, prev_order_code, int(page_number))
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error_code': 1})
            else:
                response['error_details'] = 'Bad Input'
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_order_details_user(self, request):
        """
        API To Send Order details of a particular order

        :param request:
        {"order_code":"C7M83X","user_code":"63798"}

        :return:
        {
            "error": 1
            "order_delivery_details": {
            "user_name": "paresh"
            "user_phone_number": "9818698736"
            "user_address": "tower 8; 902,unitech fresco,Sector 49,Gurgaon"
            }
            "order_code": "C7M83X"
            "order_payment_details": {
            "paid": 74
            "to_be_paid": 136
            }
        }

        """
        response = dict()
        connection = LazyladConnection()
        try:
            api_response_status = status.HTTP_400_BAD_REQUEST
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = OrderDetailsSerlz(data=data)
            if _serlz.is_valid():
                api_response_status = status.HTTP_200_OK
                response = connection.get_order_details(
                    data['user_code'], data['order_code'])
                if response['success']:
                    response.update({'error_code': 1})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)


class Config(ViewSet):

    def get_user_config(self, request):
        """
        API To store basic configuration of user
        :param request:

        {"area_code_selected":"Sector 49","city_code_selected":"1",
        "imei_number":"355490061334631","lazylad_version_code":35,
        "mac_address":"02:00:00:00:00:00","model_name":"MotoG3",
        "os_type":23,"phone_manufacturer":"Motorola","user_code":4}

        :return:

        {
            "error_code": 1
            "lazylad_contact": {
            "call_us_number": "9509479438"
            }-
            "number_verification": {
            "shall_number_verify": 1
            }-
            "success": true
        }
        """
        response = dict()
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = ConfigAPISerlz(data=data)
            if _serlz.is_valid():
                response = connection.save_user_config(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error_code': 1})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

