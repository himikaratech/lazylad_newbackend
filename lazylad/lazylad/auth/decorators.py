from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User


def login_required(f):
    def wrapper(request, **kwargs):
        try:
            if request.session['_auth_user_id']:
                user = User.objects.get(id=request.session['_auth_user_id'])
                if user.is_staff:
                    return f(request, **kwargs)
                else:
                    return HttpResponseRedirect('/?msg=invalid user')
            else:
                return HttpResponseRedirect('/newserver/admin/login/?next='+request.path)
        except KeyError as e:
            return HttpResponseRedirect('/newserver/admin/login/?next='+request.path)
        except Exception as e:
            return HttpResponseRedirect('/newserver/admin/login/?next='+request.path)
    return wrapper


def login_required_vs(f):
    def wrapper(viewset, request, **kwargs):
        try:
            if request.session['_auth_user_id']:
                user = User.objects.get(id=request.session['_auth_user_id'])
                request.user = user
                if user.is_staff:
                    return f(viewset, request, **kwargs)
                else:
                    return HttpResponseRedirect('/?msg=invalid user')
            else:
                return HttpResponseRedirect('/newserver/admin/login/?next='+request.path)
        except KeyError as e:
            return HttpResponseRedirect('/newserver/admin/login/?next='+request.path)
        except Exception as e:
            return HttpResponseRedirect('/newserver/admin/login/?next='+request.path)
    return wrapper

