
from rest_framework import fields, serializers
from app_settings import *


class LocationSerializer(serializers.Serializer):
    latitude = serializers.CharField(required=False)
    longitude = serializers.CharField(required=False)


class AddRiderSerializer(serializers.Serializer):
    rider_name = serializers.CharField(required=True)
    phone_no = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    address = serializers.CharField(required=False)
    license_no = serializers.CharField(required=True)
    city_code = serializers.CharField(required=True)
    checkin_time = serializers.CharField(required=False)
    duration_time = serializers.CharField(required=False)
    account_name = serializers.CharField(required=False)
    account_no = serializers.CharField(required=False)
    ifsc_code = serializers.CharField(required=False)


class GetRidersSerializer(serializers.Serializer):
    city_code = serializers.CharField(required=False)
    order_code = serializers.CharField(required=True)


class RiderLoginSerializer(serializers.Serializer):
    phone_no = serializers.CharField(required=True)
    password = serializers.CharField(required=True)


class RiderCheckInOutSerializer(LocationSerializer):
    rider_id = serializers.CharField(required=True)
    bike_reading = serializers.CharField(required=True)
    bike_number = serializers.CharField(required=True)


class RiderDetailSerializer(LocationSerializer):
    rider_id = serializers.CharField(required=True)
    status = serializers.ChoiceField(
        choices=LOGISTICS_ORDER_STATUS, required=False)


class RiderEditSerializer(serializers.Serializer):
    rider_id = serializers.CharField(required=True)
    rider_name = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    license_no = serializers.CharField(required=False)
    city_code = serializers.CharField(required=False)
    checkin_time = serializers.CharField(required=False)
    duration_time = serializers.CharField(required=False)
    account_name = serializers.CharField(required=False)
    account_no = serializers.CharField(required=False)
    ifsc_code = serializers.CharField(required=False)


class OrderSerializer(LocationSerializer):
    order_code = serializers.CharField(required=True)
    rider_id = serializers.CharField(required=False)


class OrderCreateSerializer(serializers.Serializer):
    order_code = serializers.CharField(required=True)
    city_code = serializers.CharField(required=False)


class OrderStatusSerializer(serializers.Serializer):
    status = serializers.ChoiceField(
        choices=LOGISTICS_ORDER_STATUS, required=True)


class AssignRiderSerializer(LocationSerializer):
    rider_uid = serializers.CharField(required=True)
    order_code = serializers.CharField(required=True)


class AcceptRejectOrdersSerializer(AssignRiderSerializer):
    action = serializers.ChoiceField(choices=['ACCEPT', 'REJECT'], required=True)


class RiderSerializer(serializers.Serializer):
    rider_id = serializers.CharField(required=True)

class OrderDeliveredSerializer(OrderSerializer):
    amount_collected = serializers.CharField(required=True)
    rider_id = serializers.CharField(required=False)
