
from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt


from .services import success as suc, failure as fail
from .models import *

import json

@csrf_exempt
def success(request):
    if request.method == 'POST':
        data = request.POST.copy()
        if not data:
            data = json.loads(request.body)
        s_log = SuccessLog(
            request_json=data
        )
        s_log.save()
        if not data:
            ret = dict()
            ret['message'] = 'Values missing in requests'
            ret['success'] = False
            return HttpResponse\
                (json.dumps(ret), content_type="application/json")
        response = suc(data)
        if response.get('success'):
            return render_to_response(
                'success.html',
                {'input': 'Success'},
                context_instance=RequestContext(request))
        else:
            ret_method = dict()
            ret_method['message'] = 'Unsuccessful'
            ret_method['success'] = False
            return HttpResponse(
                json.dumps(ret_method), content_type="application/json")
    else:
        ret_method = dict()
        ret_method['message'] = 'Method not allowed'
        ret_method['success'] = False
        return HttpResponse(
            json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def failure(request):
    if request.method == 'POST':
        data = request.POST.copy()
        if not data:
            data = json.loads(request.body)
        if not data:
            ret = dict()
            ret['message'] = 'Values missing in requests'
            ret['success'] = False
            return HttpResponse\
                (json.dumps(ret), content_type="application/json")
        resp = fail(data)
        if resp.get('success'):
            return render_to_response(
                'failure.html',
                {'input': "Transaction Failed"},
                context_instance=RequestContext(request))
        else:
            ret_method = dict()
            ret_method['message'] = 'Unsuccessful'
            ret_method['success'] = False
            return HttpResponse(
                json.dumps(ret_method), content_type="application/json")
    else:
        ret_method = dict()
        ret_method['message'] = 'Method not allowed'
        ret_method['success'] = False
        return HttpResponse(
            json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def cancel(request):
    pass
