
from django.db import connections
import datetime
from .models import *
from .utils import *
from django.core.exceptions import ValidationError

from utility import print_exception


class LazyServices:
    def __init__(self):
        pass

    def post_order(self, post_dict):
        response = {
            'success': False,
            'message': None,
            'order_code': None
        }
        try:
            user_code = post_dict.get('user_code')
            address_code = post_dict.get('address_code')
            service_requests = post_dict.get('services_requested')
            address_user_code = post_dict.get('address_user_code')
            phone_number = post_dict.get('phone')
            coupon_code = post_dict.get('coupon_code')
            lazy_con = LazyladConnect()
            if not (lazy_con.check_user(user_code)
                    and lazy_con.check_address_code(
                    address_user_code, address_code)):
                response['message'] = 'Invalid User/Address'
                return response
            try:
                coupon = Coupon.objects.get(coupon_code=coupon_code)
                coupon.coupons_applied += 1
                coupon.save()
                coupon_type = coupon.coupon_type
                coupon_discount = coupon.discount_amount
                coupon_max_discount = coupon.max_discount
            except:
                coupon_code = ''
                coupon_type = ''
                coupon_discount = Decimal(0)
                coupon_max_discount = Decimal(0)
            order = Order(
                order_code=generate_order_code(),
                user_code=user_code,
                address_code=address_code,
                address_user_code=address_user_code,
                coupon_code=coupon_code,
                coupon_type=coupon_type,
                coupon_discount=coupon_discount,
                coupon_max_discount=coupon_max_discount,
                phone_number=phone_number
            )
            order.save()

            pick_time = 0
            delivery_time = 0

            for service_dict in service_requests:
                resp = self.add_order_service(order, service_dict)
                # if service_dict.get('pickup_time') > pick_time and resp:
                #     pick_time = service_dict.get('pickup_time')
                # if service_dict.get('delivery_time') > delivery_time and resp:
                #     delivery_time = service_dict.get('delivery_time')

            if service_requests:
                pick_time = service_requests[0].get('pickup_time')
                delivery_time = service_requests[0].get('delivery_time')

            order.pickup_time = epoch_to_datetime(pick_time)
            order.delivery_time = epoch_to_datetime(delivery_time)
            order.save()

            user_details = lazy_con.get_address(user_code, address_user_code)
            message = 'Order Code: {0} \n ' \
                      '{1} \n ' \
                      'Pickup Timestamp : {2} \n ' \
                      'Delivery Timestamp : {3} \n ' \
                      'Discount Coupon Used : {4} \n ' \
                      'Discount Amount: {5} \n ' \
                      'Max Discount: {6} \n ' \
                      'Services Requested: {7}'.format(
                                order.order_code,
                                user_details,
                                order.pickup_time,
                                order.delivery_time,
                                coupon_code,
                                coupon_max_discount,
                                coupon_discount,
                                ''.join(x[0]+' | ' for x in
                                        OrderServices.objects.filter(
                                    order=order).values_list(
                                    'service__service_name'))
                                )

            send_email(message)
            response['success'] = True
            response['order_code'] = order.order_code
            response['pickup_time'] = order.pickup_time
            response['delivery_time'] = order.delivery_time
        except:
            response['message'] = print_exception()
        return response

    def get_services(self, user_code):
        response = {
            'success': False,
            'message': None,
            'services': []
        }
        try:
            lazy_con = LazyladConnect()
            if not lazy_con.check_user(user_code):
                response['message'] = 'Invalid User'
                return response
            services = Service.objects.all()
            for service in services:
                response['services'].append({
                    'st_code': service.st_code,
                    'st_name': service.st_name,
                    'service_code': service.service_code,
                    'service_name': service.service_name,
                    'score': service.score,
                    'desc': service.desc,
                    'open_time': service.open_time,
                    'close_time': service.close_time,
                    'gap_time': service.gap_time,
                    'interval': service.interval,
                    'image_url': service.image_url,
                    'image_flag': service.image_flag,
                })
                response['success'] = True
        except:
            response['message'] = print_exception()
        return response

    def add_order_service(self, order, service_dict):
        try:
            service = Service.objects.get(
                    st_code=service_dict.get('st_code'),
                    service_code=service_dict.get('service_code')
                )
            order_service = OrderServices(
                order=order,
                service=service,
                pick_up_time=epoch_to_datetime(service_dict.get('pickup_time')),
                delivery_time=epoch_to_datetime(service_dict.get(
                    'delivery_time')),
            )
            order_service.save()
            return True
        except:
            return False

    def get_service_categories(self, st_code):
        response = {
            'success': False,
            'message': None,
            'categories': []
        }
        try:
            categories = ServiceCategory.objects.all()
            for category in categories:
                item_dict = CategoryItems.objects.filter(
                    item_category=category).values(
                    'item_name', 'item_cost', 'item_img',
                    'group', 'unit', 'item_desc')
                category_dict = {
                    'category_name': category.category_name,
                    'category_code': category.category_code,
                    'category_desc': category.category_desc,
                    'items': item_dict
                }
                response['categories'].append(category_dict)
            response['success'] = True
        except:
            response['message'] = print_exception()
        return response

    def post_kart(self, data):
        response = {
            'success': False,
            'message': None,
            'categories': []
        }
        user_code = data.get('user_code')
        item_kart = data.get('kart_items')
        try:
            lazy_con = LazyladConnect()
            if not lazy_con.check_user(user_code):
                response['message'] = 'Invalid User'
                return response
            kart = EstimateCart(
                user_code=user_code,
                estimated_cost=Decimal(data.get('estimated_cost'))
            )
            kart.save()
            for item in item_kart:
                try:
                    cart_item = CategoryItems(
                        cart=kart,
                        item=CategoryItems.objects.get(
                            item_name=item.get('item_name')),
                        quantity=int(item.get('item_quantity')))
                    cart_item.save()
                except:
                    pass
        except:
            response['message'] = print_exception()
        return response

    def coupon_validity(self, coupon_data):
        try:
            response = dict()
            coupon_code = coupon_data['coupon_code']
            user_code = coupon_data['user_code']
            date = datetime.datetime.now().date()
            coupon = Coupon.objects.filter(
                coupon_code=coupon_code, active=True, expiry_date__gte=date)
            if len(coupon):
                coupon = coupon[0]
                if int(coupon.coupons_applied) < int(coupon.total_coupons):
                    response["error"] = 0
                    discount = coupon.discount_amount
                    max_discount = coupon.max_discount
                    coupon_usage_number = len(Order.objects.filter(
                        user_code=user_code, coupon_code=coupon_code))
                    if (0 != coupon.coupons_per_user) and\
                            (coupon.coupons_per_user <= coupon_usage_number):
                        response = dict()
                        response["error"] = 1
                        response["message"] = \
                            "Coupon usage maximum limit reached for you"
                        response["success"] = False
                    else:
                        if coupon.coupon_type == 'FLAT':
                            response["discount_amount"] = discount
                            response["max_discount"] = max_discount
                            response["coupon_type"] = 'FLAT'
                            response["success"] = True
                        elif coupon.coupon_type == 'PERCENTAGE':
                            response["max_discount"] = max_discount
                            response["discount_amount"] = discount
                            response["coupon_type"] = 'PERCENTAGE'
                            response['success'] = True
                        else:
                            response = dict()
                            response["error"] = 1
                            response["message"] = "Invalid coupon"
                            response["success"] = False
                else:
                    response["error"] = 1
                    response["message"] = "Coupon expired"
                    response["success"] = False

            else:
                response["error"] = 1
                response["message"] = "Coupon does not exist"
                response["success"] = False
        except:
            response = dict()
            response["error"] = 1
            response["message"] = print_exception()
            response["success"] = False
        return response


class LazyladConnect:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def check_user(self, user_code):
        try:
            query = 'select * from user_details where user_code = ' +\
                    user_code
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)[0]
            return True
        except:
            return False

    def check_address_code(self, user_code, address_code):
        try:
            query = 'select * from user_address_details where user_code' \
                    ' = {0} and user_address_code = {1}'.format(
                user_code, address_code)
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)[0]
            return True
        except:
            return False

    def get_user(self, user_code):
        return_dict = dict()
        try:
            query = 'select phone_number from user_details where user_code = ' +\
                    user_code
            self.cursor.execute(query)
            phone_no = dictfetchall(self.cursor)[0]

            query = 'select first_name, last_name from user_profile where' \
                    ' phone_number = ' + str(phone_no.get('phone_number'))
            self.cursor.execute(query)
            return_dict = dictfetchall(self.cursor)[0]
            return_dict.update(phone_no)
            return return_dict
        except:
            return False

    def get_address(self, user_code, address_code):
        address = ''
        try:
            query = 'select * from user_address_details where user_code' \
                    ' = {0}'.format(address_code)
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)[0]
            address = 'Customer Name {0} \n Customer Number {1}' \
                      ' \n Pickup Address {2}'.format(
                data.get('user_name'),
                data.get('user_phone_number'),
                data.get('user_address'),
            )
        except:
            pass
        return address


def dictfetchall(cursor):
    " Return all rows from a cursor as a dict "
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def order_logs(request_json, response_json, successful):
    order_log = OrderLogs(request_json=request_json,
                          response_json=response_json,
                          successful=successful)
    order_log.save()
    return True
