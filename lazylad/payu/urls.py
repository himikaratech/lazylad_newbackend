from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import PayU


urlpatterns = patterns('',
                       url(r'^success/$', 'payu.views.success', name='success'),
                       url(r'^failure/$', 'payu.views.failure', name='failure'),
                       url(r'^cancel/$', 'payu.views.cancel', name='cancel'),
                       )

api_urlpatterns = patterns(
    'payu.apiviews',
    url(
        r'^pay_online',
        PayU.as_view({'post': 'create_hash'})),
    url(
        r'^post_order',
        PayU.as_view({'post': 'post_order'})),
)

urlpatterns += format_suffix_patterns(api_urlpatterns)
