import decimal
import json
import math

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .makeTransactions import redeem_wallet_cashback_coupon
from constant import RequestConstant, STATUS, ResponseConstant, ErrorCode, TransactionType, RelationColumnName
from database.LazyladConnection import LazyladConnection
from database.models import Wallet, Transactions, PaymentDetails
from .utils import return_wallet_details
from account2.apiviews import CashBack
import makeTransactions, helper
import utils


def test(request):
    ret_method = dict()
    connection = LazyladConnection()
    ret_method["adas"] = connection.get_order_wallet_component(1)
    connection.close_connection()
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def createWallet(request):
    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                user_code = request.POST[RequestConstant.USER_CODE]
                owner_type = request.POST[RequestConstant.OWNER_TYPE]
                balance = request.POST[RequestConstant.BALANCE]
                neg_allowed = request.POST[RequestConstant.NEGATIVE_ALLOWED]
                max_neg_balance = request.POST[RequestConstant.MAXIMUM_NEGATIVE_ALLOWED]
            else:
                data = json.loads(request.body)
                user_code = data[RequestConstant.USER_CODE]
                owner_type = data[RequestConstant.OWNER_TYPE]
                balance = data[RequestConstant.BALANCE]
                neg_allowed = data[RequestConstant.NEGATIVE_ALLOWED]
                max_neg_balance = data[RequestConstant.MAXIMUM_NEGATIVE_ALLOWED]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connect=LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(user_code, owner_type)
        if owner_id is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")


        try:
            userWallet, created = Wallet.objects.get_or_create(owner_id=owner_id, owner_type=owner_type,
                                                               balance=balance, neg_allowed=neg_allowed,
                                                               max_neg_balance=max_neg_balance)
            if not created:
                userWallet.save()
        except:
            userWallet = Wallet.objects.get(owner_id=owner_id, owner_type=owner_type)
        ret = dict()
        wallet = dict()
        wallet[RelationColumnName.WALLET_WALLET_ID] = userWallet.w_id
        wallet[RelationColumnName.WALLET_OWNER_ID] = userWallet.owner_id
        wallet[RelationColumnName.WALLET_OWNER_TYPE] = userWallet.owner_type
        ret[ResponseConstant.WALLET] = wallet
        ret[ResponseConstant.SUCCESS] = True
        ret[ResponseConstant.MESSAGE] = 'Wallet already exsists'
        ret[ResponseConstant.IS_PRESENT] = True
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")

@csrf_exempt
def getWalletDetails(request):
    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                user_code = request.POST[RequestConstant.USER_CODE]
                o_type = request.POST[RequestConstant.OWNER_TYPE]
            else:
                data = json.loads(request.body)
                user_code = data[RequestConstant.USER_CODE]
                o_type = data[RequestConstant.OWNER_TYPE]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connect = LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
        if owner_id is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            Wallet_details = connect.create_and_get_wallet(owner_id=owner_id, owner_type=o_type)
            ret = dict()
            wallet = dict()
            wallet[RelationColumnName.WALLET_WALLET_ID] = Wallet_details.w_id
            wallet[RelationColumnName.WALLET_OWNER_ID] = Wallet_details.owner_id
            wallet[RelationColumnName.WALLET_OWNER_TYPE] = Wallet_details.owner_type
            wallet[RelationColumnName.WALLET_BALANCE] = str(math.floor(Wallet_details.balance))
            wallet[RelationColumnName.WALLET_NEGATIVE_ALLOWED] = Wallet_details.neg_allowed
            wallet[RelationColumnName.WALLET_MAXIMUM_NEGATIVE_BALANCE] = str(Wallet_details.max_neg_balance)

            wallet["wallet_usage"] = return_wallet_details(owner_id, o_type)

            ret[ResponseConstant.WALLET] = wallet
            ret[ResponseConstant.SUCCESS] = True
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        except Wallet.DoesNotExist:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
            ret[ResponseConstant.SUCCESS] = True
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def doesWalletExists(request):
    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                user_code = request.POST[RequestConstant.USER_CODE]
                o_type = request.POST[RequestConstant.OWNER_TYPE]
            else:
                data = json.loads(request.body)
                user_code = data[RequestConstant.USER_CODE]
                o_type = data[RequestConstant.OWNER_TYPE]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connect = LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
        if owner_id is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            Wallet_details = Wallet.objects.get(owner_id=owner_id, owner_type=o_type)
            ret = dict()
            ret[ResponseConstant.WALLET_EXISTS] = True
            ret[ResponseConstant.MESSAGE] = 'Wallet exists with this owner_id and owner_type'
            ret[ResponseConstant.SUCCESS] = True
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        except Wallet.DoesNotExist:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
            ret[ResponseConstant.SUCCESS] = True
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def getAllTransactions(request):
    num_transactions_to_send = 10

    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                user_code = request.POST[RequestConstant.USER_CODE]
                o_type = request.POST[RequestConstant.OWNER_TYPE]
                if RequestConstant.PREVIOUS_TRANSACTION_ID in request.POST:
                    previous_tid = request.POST[RequestConstant.PREVIOUS_TRANSACTION_ID]
                    previous_tid = int(previous_tid)
                else:
                    previous_tid = -1

            else:
                data = json.loads(request.body)
                user_code = data[RequestConstant.USER_CODE]
                o_type = data[RequestConstant.OWNER_TYPE]
                if RequestConstant.PREVIOUS_TRANSACTION_ID in data.keys():
                    previous_tid = data[RequestConstant.PREVIOUS_TRANSACTION_ID]
                    previous_tid = int(previous_tid)
                else:
                    previous_tid = -1
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connect = LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
        if owner_id is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            walletdetails = connect.create_and_get_wallet(owner_id=owner_id, owner_type=o_type)
            w_id = walletdetails.w_id
        except Wallet.DoesNotExist:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        if previous_tid == -1:
            t_details = Transactions.objects.filter(w_id__w_id=w_id).order_by('-t_id')
        elif previous_tid == 0:
            t_details = Transactions.objects.filter(w_id__w_id=w_id).order_by('-t_id')[:num_transactions_to_send]
        else:
            t_details = Transactions.objects.filter(w_id__w_id=w_id, t_id__lt=previous_tid).order_by('-t_id')[:num_transactions_to_send]

        tr = list()
        for e in t_details:
            trans = dict()
            if e.CorD == 1:
                rt = "Credit"
            else:
                rt = "Debit"
            trans[RelationColumnName.TRANSACTION_ID] = str(e.t_id)
            trans[RelationColumnName.TRANSACTION_TYPE] = str(e.t_type)
            trans[RelationColumnName.TRANSACTION_URID] = str(e.urid)
            trans[RelationColumnName.TRANSACTION_URID_TYPE] = str(e.urid_type)
            trans[RelationColumnName.TRANSACTION_DESCRIPTION] = str(e.description)
            trans[RelationColumnName.TRANSACTION_CREDIT_DEBIT] = str(rt)
            trans[RelationColumnName.TRANSACTION_AMOUNT] = float(e.amount)
            trans[RelationColumnName.TRANSACTION_STATUS] = str(e.status)
            trans[RelationColumnName.TRANSACTION_TIME_TRANSACTION] = helper.datetime_to_timestamp(e.created)
            tr.append(trans)

        ret = dict()
        ret[ResponseConstant.TRANSACTION] = tr
        ret[ResponseConstant.SUCCESS] = True
        ret[ResponseConstant.WALLET_ID] = w_id
        ret[ResponseConstant.TOTAL_COUNT] = len(tr)
        if len(tr) == 0:
            ret[ResponseConstant.MESSAGE] = 'No transaction for this wallet details'
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    else:
        ret_method = dict()
        ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
        ret_method[ResponseConstant.SUCCESS] = False
        ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
        return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def updateStatusofTransaction(request):
    if request.method == 'POST':
        try:
            if RequestConstant.TRANSACTION_ID in request.POST:
                t_id = request.POST[RequestConstant.TRANSACTION_ID]
                status = request.POST[RequestConstant.STATUS]

            else:
                data = json.loads(request.body)
                t_id = data[RequestConstant.TRANSACTION_ID]
                status = data[RequestConstant.STATUS]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            Transactions.objects.filter(t_id=t_id).update(status=status)
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Transaction status updated'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
            ret[ResponseConstant.SUCCESS] = True
            ret[ResponseConstant.TRANSACTION] = t_id
            ret[ResponseConstant.STATUS] = status
            return HttpResponse(json.dumps(ret), content_type="application/json")
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Error while Updating you request'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def getTransactionsForURID(request):
    if request.method == 'POST':
        try:
            if RequestConstant.URID in request.POST:
                urid = request.POST[RequestConstant.URID]
                urid_type = request.POST[RequestConstant.URID_TYPE]
                user_code = request.POST[RequestConstant.USER_CODE]
                o_type = request.POST[RequestConstant.OWNER_TYPE]

            else:
                data = json.loads(request.body)
                urid = data[RequestConstant.URID]
                urid_type = data[RequestConstant.URID_TYPE]
                user_code = data[RequestConstant.USER_CODE]
                o_type = data[RequestConstant.OWNER_TYPE]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connect = LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
        if owner_id is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            wallet = Wallet.objects.filter(owner_id=owner_id, owner_type=o_type)
            w_id = wallet[0].w_id
            try:
                t_details = Transactions.objects.filter(w_id__w_id=w_id, urid=urid,urid_type=urid_type)
                ret = dict()
                tr = list()
                for e in t_details:
                    trans = dict()
                    if e.CorD == 1:
                        rt = "Credit"
                    else:
                        rt = "Debit"
                    trans[RelationColumnName.TRANSACTION_ID] = str(e.t_id)
                    trans[RelationColumnName.TRANSACTION_TYPE] = str(e.t_type)
                    trans[RelationColumnName.TRANSACTION_URID] = str(e.urid)
                    trans[RelationColumnName.TRANSACTION_URID_TYPE] = str(e.urid_type)
                    trans[RelationColumnName.TRANSACTION_DESCRIPTION] = str(e.description)
                    trans[RelationColumnName.TRANSACTION_CREDIT_DEBIT] = str(rt)
                    trans[RelationColumnName.TRANSACTION_AMOUNT] = float(e.amount)
                    trans[RelationColumnName.TRANSACTION_STATUS] = str(e.status)
                    trans[RelationColumnName.TRANSACTION_TIME_TRANSACTION] = helper.datetime_to_timestamp(e.created)
                    tr.append(trans)

                if len(tr) == 0:
                    raise ValueError('Nothing found')
                ret[ResponseConstant.TRANSACTION] = tr
                ret[ResponseConstant.SUCCESS] = True
                ret[ResponseConstant.WALLET_ID] = w_id
                ret[ResponseConstant.TOTAL_COUNT] = len(tr)
                connect.close_connection()
                return HttpResponse(json.dumps(ret), content_type="application/json")
            except:
                ret = dict()
                ret[ResponseConstant.MESSAGE] = 'No transactions for this wallet and urid'
                ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
                ret[ResponseConstant.SUCCESS] = True
                ret[ResponseConstant.TOTAL_COUNT] = 0
                connect.close_connection()
                return HttpResponse(json.dumps(ret), content_type="application/json")
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner with this  o_id does not exist'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def getTransactionForURID(request):
    if request.method == 'POST':
        try:
            if RequestConstant.URID in request.POST:
                t_id = request.POST[RequestConstant.TRANSACTION_ID]
                urid = request.POST[RequestConstant.URID]
                urid_type = request.POST[RequestConstant.URID_TYPE]

            else:
                data = json.loads(request.body)
                t_id = data[RequestConstant.TRANSACTION_ID]
                urid = data[RequestConstant.URID]
                urid_type = data[RequestConstant.URID_TYPE]

        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            t_details = Transactions.objects.filter(t_id=t_id, urid=urid, urid_type=urid_type)
            ret = dict()
            tr = list()

            for e in t_details:
                trans = dict()
                if e.CorD == 1:
                    rt = "Credit"
                else:
                    rt = "Debit"
                trans[RelationColumnName.TRANSACTION_ID] = str(e.t_id)
                trans[RelationColumnName.TRANSACTION_TYPE] = str(e.t_type)
                trans[RelationColumnName.TRANSACTION_URID] = str(e.urid)
                trans[RelationColumnName.TRANSACTION_URID_TYPE] = str(e.urid_type)
                trans[RelationColumnName.TRANSACTION_DESCRIPTION] = str(e.description)
                trans[RelationColumnName.TRANSACTION_CREDIT_DEBIT] = str(rt)
                trans[RelationColumnName.TRANSACTION_AMOUNT] = float(e.amount)
                trans[RelationColumnName.TRANSACTION_STATUS] = str(e.status)
                trans[RelationColumnName.TRANSACTION_TIME_TRANSACTION] = helper.datetime_to_timestamp(e.created)
                tr.append(trans)

            if len(tr) == 0:
                raise ValueError('Nothing Found')
            ret[ResponseConstant.TRANSACTION] = tr
            ret[ResponseConstant.SUCCESS] = True
            ret[ResponseConstant.TOTAL_COUNT] = len(tr)
            return HttpResponse(json.dumps(ret), content_type="application/json")
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'No data with this t_id, URID ,URID_TYPE'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.NOT_FOUND
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def getTransactionURIDTTYPE(request):
    if request.method == 'POST':
        try:
            if RequestConstant.URID in request.POST:
                t_type = request.POST[RequestConstant.TRANSACTION_TYPE]
                urid = request.POST[RequestConstant.URID]
                urid_type = request.POST[RequestConstant.URID_TYPE]
            else:
                data = json.loads(request.body)
                t_type = data[RequestConstant.TRANSACTION_TYPE]
                urid = data[RequestConstant.URID]
                urid_type = data[RequestConstant.URID_TYPE]

        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            t_details = Transactions.objects.filter(t_type=t_type, urid=urid, urid_type=urid_type)
            ret = dict()
            tr = list()

            for e in t_details:
                trans = dict()
                if e.CorD == 1:
                    rt = "Credit"
                else:
                    rt = "Debit"
                trans[RelationColumnName.TRANSACTION_ID] = str(e.t_id)
                trans[RelationColumnName.TRANSACTION_TYPE] = str(e.t_type)
                trans[RelationColumnName.TRANSACTION_URID] = str(e.urid)
                trans[RelationColumnName.TRANSACTION_URID_TYPE] = str(e.urid_type)
                trans[RelationColumnName.TRANSACTION_DESCRIPTION] = str(e.description)
                trans[RelationColumnName.TRANSACTION_CREDIT_DEBIT] = str(rt)
                trans[RelationColumnName.TRANSACTION_AMOUNT] = float(e.amount)
                trans[RelationColumnName.TRANSACTION_STATUS] = str(e.status)
                trans[RelationColumnName.TRANSACTION_TIME_TRANSACTION] = helper.datetime_to_timestamp(e.created)
                tr.append(trans)

            if len(tr) == 0:
                raise ValueError('Nothing Found')
            ret[ResponseConstant.TRANSACTION] = tr
            ret[ResponseConstant.SUCCESS] = True
            ret[ResponseConstant.TOTAL_COUNT] = len(tr)
            return HttpResponse(json.dumps(ret), content_type="application/json")
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'No data with this t_type, URID ,URID_TYPE'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.NOT_FOUND
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def makeTransaction(request):
    if request.method == 'POST':
        try:
            if RequestConstant.TRANSACTION_TYPE in request.POST:
                t_type = request.POST[RequestConstant.TRANSACTION_TYPE]
                data = request.POST
            else:
                data = json.loads(request.body)
                t_type = data[RequestConstant.TRANSACTION_TYPE]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        if t_type == TransactionType.ORDER_PAY_WALLET_CUSTOMER:
            return makeTransactions.order_pay_wallet_customer(data)
        elif t_type == TransactionType.ORDER_PAY_ONLINE_CUSTOMER:
            return makeTransactions.order_pay_online_customer(data)
        elif t_type == TransactionType.ORDER_CANCEL_CUSTOMER:
            return makeTransactions.order_cancel_customer(data)
        elif t_type == TransactionType.ORDER_FINISH_CUSTOMER:
            return makeTransactions.order_finish_customer(data)
        elif t_type == TransactionType.ORDER_FINISH_SELLER:
            return makeTransactions.order_finish_seller(data)
        elif t_type == TransactionType.ORDER_CANCEL_SELLER:
            return makeTransactions.order_cancel_seller(data)
        elif t_type == TransactionType.ORDER_RETURN_CUSTOMER:
            return makeTransactions.order_return_customer(data)
        elif t_type == TransactionType.ORDER_RETURN_SELLER:
            return makeTransactions.order_return_seller(data)
        elif t_type == TransactionType.REDEEM_CASH_COUPON:
            return makeTransactions.redeem_cash_coupon(data)
        elif t_type == TransactionType.RECHARGE:
            return makeTransactions.recharge(data)
        elif t_type == TransactionType.SETTLE_UP:
            return makeTransactions.settle_up(data)
        elif t_type == TransactionType.REFFERAL_BONUS:
            return makeTransactions.refferal_bonus(data)
        elif t_type == TransactionType.CASHBACK:
            return CashBack().cash_back(request)
        elif t_type == TransactionType.LAZYPOS_CHARGE:
            return makeTransactions.remote_wallet_transaction(data)
        elif t_type == TransactionType.LAZYPOS_REDEEM:
            return makeTransactions.remote_wallet_redeem_transaction(data)
        elif t_type == TransactionType.LAZYPOS_SMS_CHARGE:
            return makeTransactions.remote_wallet_sms_transaction(data)
        else:
            ret_method = dict()
            ret_method[ResponseConstant.MESSAGE] = 'No Such transaction'
            ret_method[ResponseConstant.SUCCESS] = False
            ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
            return HttpResponse(json.dumps(ret_method), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def netValueForURID(request):
    if request.method == 'POST':
        try:
            if RequestConstant.URID in request.POST:
                urid = request.POST[RequestConstant.URID]
                urid_type = request.POST[RequestConstant.URID_TYPE]
            else:
                data = json.loads(request.body)
                urid = data[RequestConstant.URID]
                urid_type = data[RequestConstant.URID_TYPE]

        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            t_details = Transactions.objects.filter( urid=urid, urid_type=urid_type)
            ret = dict()
            tr = list()
            total_amount=decimal.Decimal('0')
            for e in t_details:
                trans = dict()
                if e.status==STATUS.SUCCESS:
                    if e.CorD==1:
                        rt="Credit"
                        total_amount = total_amount- e.amount
                    else:
                        rt='Debit'
                        total_amount = total_amount + e.amount
                else:
                    if e.CorD==1:
                        rt="Credit"
                    else:
                        rt='Debit'
                trans[RelationColumnName.TRANSACTION_ID] = str(e.t_id)
                trans[RelationColumnName.TRANSACTION_CREDIT_DEBIT] = str(rt)
                trans[RelationColumnName.TRANSACTION_STATUS]=e.status
                tr.append(trans)

            if len(tr) == 0:
                raise ValueError('Nothing Found')
            ret[ResponseConstant.TRANSACTION] = tr
            ret[ResponseConstant.SUCCESS] = True
            ret['total_transactions'] = len(tr)
            ret[ResponseConstant.TOTAL_AMOUNT]=str(total_amount)
            ret[ResponseConstant.MESSAGE]='Net worth is sum of successful transactions'
            return HttpResponse(json.dumps(ret), content_type="application/json")
        except Exception,e:
            print e
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'No data with this URID ,URID_TYPE'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.NOT_FOUND
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def netValueForT_TYPE_AND_URID(request):
    if request.method == 'POST':
        try:
            if RequestConstant.URID in request.POST:
                urid = request.POST[RequestConstant.URID]
                t_type = request.POST[RequestConstant.TRANSACTION_TYPE]
                urid_type = request.POST[RequestConstant.URID_TYPE]
            else:
                data = json.loads(request.body)
                urid = data[RequestConstant.URID]
                t_type = data[RequestConstant.TRANSACTION_TYPE]
                urid_type = data[RequestConstant.URID_TYPE]

        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try:
            t_details = Transactions.objects.filter( urid=urid,urid_type=urid_type, t_type=t_type)
            ret = dict()
            tr = list()
            total_amount=decimal.Decimal('0')
            for e in t_details:
                trans = dict()
                if e.status==STATUS.SUCCESS:
                    if e.CorD==1:
                        rt="Credit"
                        total_amount = total_amount- e.amount
                    else:
                        rt='Debit'
                        total_amount = total_amount + e.amount
                else:
                    if e.CorD==1:
                        rt="Credit"
                    else:
                        rt='Debit'
                trans[RelationColumnName.TRANSACTION_ID] = str(e.t_id)
                trans[RelationColumnName.TRANSACTION_CREDIT_DEBIT] = str(rt)
                trans[RelationColumnName.TRANSACTION_STATUS]=e.status
                tr.append(trans)

            if len(tr) == 0:
                raise ValueError('Nothing Found')
            ret[ResponseConstant.TRANSACTION] = tr
            ret[ResponseConstant.SUCCESS] = True
            ret['total_transactions'] = len(tr)
            ret[ResponseConstant.TOTAL_AMOUNT]=str(total_amount)
            ret[ResponseConstant.MESSAGE]='Net worth is sum of successful transactions'
            return HttpResponse(json.dumps(ret), content_type="application/json")
        except Exception,e:
            print e
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'No data with this URID ,URID_TYPE'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.NOT_FOUND
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

    ret_method = dict()
    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")

@csrf_exempt
def orderPaymentDetails(request):
    ret_method = dict()
    if request.method == 'POST':
        try:
            if RequestConstant.URID in request.POST:
                urid = request.POST[RequestConstant.URID]
            else:
                data = json.loads(request.body)
                urid = data[RequestConstant.URID]

        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        payment_data = utils.get_order_payment_details(urid)

        ret_method[ResponseConstant.ORDER_CODE] = urid
        ret_method[ResponseConstant.SUCCESS] = True
        ret_method.update(payment_data)
        return HttpResponse(json.dumps(ret_method), content_type="application/json")

    ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
    ret_method[ResponseConstant.SUCCESS] = False
    ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
    return HttpResponse(json.dumps(ret_method), content_type="application/json")


@csrf_exempt
def walletCashbackByCoupon(request):
    try:
        if request.method == 'POST':
            data = dict()
            data['user_code'] = request.POST.get('user_code', '')
            data['coupon_code'] = request.POST.get('coupon_code', '')
            if len(str(data['user_code'])) and len(str(data['coupon_code'])):
                response = redeem_wallet_cashback_coupon(data)
                if response['success']:
                    return HttpResponse(json.dumps(response), content_type="application/json")
                return HttpResponse(json.dumps(response), content_type="application/json")
            return HttpResponse(json.dumps({'success': False, 'message': 'Bad Input'}), content_type="application/json")
    except Exception as inst:
        print inst
