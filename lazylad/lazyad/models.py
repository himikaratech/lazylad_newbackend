
from django.db import models
from decimal import Decimal
import datetime
from django.utils import timezone

from .app_settings import USER_SOURCE, REDEEM_TRANSACTION_TYPE

class City(models.Model):
    name = models.CharField(max_length=25, unique=True)
    country = models.CharField(max_length=25, default='India')

    class Meta:
        app_label = 'lazyad'


class Area(models.Model):
    city = models.ForeignKey(City, to_field='name')
    area_code = models.CharField(max_length=25, unique=True)
    area_name = models.CharField(max_length=100, null=True, blank=True)
    pincode = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        app_label = 'lazyad'


class User(models.Model):
    user_code = models.CharField(max_length=25, unique=True)
    username = models.CharField(max_length=25, null=True, blank=True)
    password = models.CharField(max_length=25, null=True, blank=True)
    phone_number = models.CharField(max_length=25, unique=True)
    number_verified = models.BooleanField(default=False)

    class Meta:
        app_label = 'lazyad'


class Merchant(models.Model):
    merchant_code = models.CharField(max_length=25, primary_key=True)
    name = models.CharField(max_length=25)
    phone_no = models.CharField(max_length=25, unique=True)
    password = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        app_label = 'lazyad'


class Coupon(models.Model):
    name = models.CharField(max_length=25, unique=True)
    clicks = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    conversions = models.IntegerField(default=0)
    number_of_coupons = models.IntegerField(default=0)
    redeemable_coupons = models.IntegerField(default=0)
    coupons_redeemed = models.IntegerField(default=0)
    desc = models.CharField(max_length=25, null=True, blank=True)
    short_desc = models.CharField(
        max_length=25, null=True, blank=True)
    terms = models.TextField(null=True, blank=True)
    img_flag = models.BooleanField(default=False)
    img_address = models.CharField\
        (max_length=100, null=True, blank=True)
    merchant = models.ForeignKey(Merchant, to_field='merchant_code')
    coupon_redirect_link = models.CharField(
        max_length=255, null=True, blank=True)
    active = models.BooleanField(default=True)
    city = models.ForeignKey(City, to_field='name', null=True, blank=True)

    class Meta:
        app_label = 'lazyad'


class CouponCodes(models.Model):
    coupon = models.ForeignKey(Coupon, to_field='name')
    coupon_code = models.CharField(max_length=25, unique=True)
    user = models.ForeignKey(User, to_field='user_code')
    created_at = models.DateTimeField()
    used_at = models.DateTimeField(null=True, blank=True)
    redeemed_at = models.DateTimeField(null=True, blank=True)
    city = models.ForeignKey(City, to_field='name', null=True, blank=True)
    area = models.ForeignKey(Area, to_field='area_code', null=True, blank=True)
    redeem = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now() +\
                               datetime.timedelta(hours=5, minutes=30)
        super(CouponCodes, self).save(*args, **kwargs)

    class Meta:
        app_label = 'lazyad'


class CouponViewLog(models.Model):
    coupon = models.ForeignKey(Coupon, to_field='name')
    user = models.ForeignKey(User, to_field='user_code')
    counter = models.IntegerField()
    view_time = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.view_time = datetime.datetime.now() +\
                               datetime.timedelta(hours=5, minutes=30)
        super(CouponViewLog, self).save(*args, **kwargs)

    class Meta:
        app_label = 'lazyad'


class CouponClickLog(models.Model):
    coupon = models.ForeignKey(Coupon, to_field='name')
    user = models.ForeignKey(User, to_field='user_code')
    counter = models.IntegerField()
    click_time = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.click_time = datetime.datetime.now() +\
                               datetime.timedelta(hours=5, minutes=30)
        super(CouponClickLog, self).save(*args, **kwargs)

    class Meta:
        app_label = 'lazyad'


class CouponUseLog(models.Model):
    coupon_code = models.ForeignKey(CouponCodes, to_field='coupon_code')
    use_time = models.DateTimeField()
    user_code = models.CharField(max_length=25)

    def save(self, *args, **kwargs):
        self.use_time = datetime.datetime.now() +\
                               datetime.timedelta(hours=5, minutes=30)
        super(CouponUseLog, self).save(*args, **kwargs)

    class Meta:
        app_label = 'lazyad'


class CouponUseFailedLog(models.Model):
    coupon = models.ForeignKey(Coupon, to_field='name')
    use_time = models.DateTimeField()
    user_code = models.CharField(max_length=25)

    def save(self, *args, **kwargs):
        self.use_time = datetime.datetime.now() +\
                               datetime.timedelta(hours=5, minutes=30)
        super(CouponUseFailedLog, self).save(*args, **kwargs)

    class Meta:
        app_label = 'lazyad'


class CouponRedeemLog(models.Model):
    coupon_code = models.ForeignKey(CouponCodes, to_field='coupon_code')
    redeem_time = models.DateTimeField()
    user_code = models.CharField(max_length=25)
    trans_type = models.CharField(
        max_length=5, choices=REDEEM_TRANSACTION_TYPE, default='SUC')

    def save(self, *args, **kwargs):
        self.redeem_time = datetime.datetime.now() +\
                               datetime.timedelta(hours=5, minutes=30)
        super(CouponRedeemLog, self).save(*args, **kwargs)

    class Meta:
        app_label = 'lazyad'
