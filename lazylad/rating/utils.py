from models import RatingModel


def saveRatingWrapper(ratings, feedback, urid, urid_type):
    try:
        for rating_type, rating in ratings.items():
            rating_object = RatingModel(
                feedback='',
                urid=urid,
                urid_type=urid_type,
                score=rating['score'],
                out_of=rating['out_of'],
                rating_type=rating_type
            )
            rating_object.save()
        rating_object = RatingModel(
            feedback=feedback,
            urid=urid,
            urid_type=urid_type,
            rating_type='feedback'
        )
        rating_object.save()
        return {'success': True,'error_code': 1}
    except Exception as inst:
        print inst
