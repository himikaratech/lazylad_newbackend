
from rest_framework import fields, serializers

class SendSMSSerializer(serializers.Serializer):
    message = serializers.CharField(required=True)
    send_to = serializers.CharField(required=True)


class OfferDiffSerializer(serializers.Serializer):
    order_code = serializers.CharField(required=True)
