from django.db import connections
from revenue.models import *
from oms.utils import print_exception
from decimal import Decimal
import datetime
from django.utils import timezone
from database.models import Transactions
from database.LazyladConnection import LazyladConnection
from .models import RunningAccountItemOffer


class RunningSellerTransactions:

    def __init__(self):
        self.ncomdb = connections['lazylad_server'].cursor()

    def running_wallet_charges_logs(self, amount, sp_code):
        try:
            try:
                seller_log = RunningAccountWallet.objects.get(sp_code=sp_code)
                seller_log.unsettled_amount += Decimal(amount)
                seller_log.save()
            except RunningAccountWallet.DoesNotExist:
                new_seller_log = RunningAccountWallet(
                    sp_code=sp_code,
                    unsettled_amount=Decimal(amount),
                )
                new_seller_log.save()
        except:
            print print_exception()

    def running_lazylad_charges_logs(self, amount, sp_code):
        try:
            try:
                seller_log = RunningAccountCut.objects.get(sp_code=sp_code)
                seller_log.unsettled_amount += Decimal(amount)
                seller_log.save()
            except RunningAccountCut.DoesNotExist:
                new_seller_log = RunningAccountCut(
                    sp_code=sp_code,
                    unsettled_amount=Decimal(amount),
                )
                new_seller_log.save()
        except:
            print print_exception()

    def running_coupon_charges_logs(self, amount, sp_code):
        try:
            try:
                seller_log = RunningAccountCoupons.objects.get(sp_code=sp_code)
                seller_log.unsettled_amount += Decimal(amount)
                seller_log.save()
            except RunningAccountCoupons.DoesNotExist:
                new_seller_log = RunningAccountCoupons(
                    sp_code=sp_code,
                    unsettled_amount=Decimal(amount),
                )
                new_seller_log.save()
        except:
            print print_exception()

    def running_item_offer_logs(self, amount, sp_code):
        try:
            try:
                seller_log = RunningAccountItemOffer.objects.get(sp_code=sp_code)
                seller_log.unsettled_amount += Decimal(amount)
                seller_log.save()
                return seller_log.id
            except RunningAccountItemOffer.DoesNotExist:
                new_seller_log = RunningAccountItemOffer(
                    sp_code=sp_code,
                    unsettled_amount=Decimal(amount),
                )
                new_seller_log.save()
                return new_seller_log.id
        except:
            print print_exception()
            return None

    def running_price_diff_logs(self, amount, sp_code):
        try:
            try:
                seller_log = RunningAccountPriceDiff.objects.get(sp_code=sp_code)
                seller_log.unsettled_amount += Decimal(amount)
                seller_log.save()
            except RunningAccountPriceDiff.DoesNotExist:
                new_seller_log = RunningAccountPriceDiff(
                    sp_code=sp_code,
                    unsettled_amount=Decimal(amount),
                )
                new_seller_log.save()
        except:
            print print_exception()


class SettleSellerTransactions:

    def __init__(self):
        self.ncomdb = connections['lazylad_server'].cursor()

    def settle_wallet_logs(self, settled_by, wallet_unsettled,
                           coupons_unsettled, cut_unsettled,
                           pricediff_unsettled, wallet_order_no,
                           wallet_last_settled, sp_code, total,
                           tid):
        try:
            try:
                last_settlement = SettleUpAccountWallet.objects.\
                    filter(sp_code=sp_code).latest('to_time')
                amount_settled_till_now = last_settlement.amount_settled_till_now \
                                          + wallet_unsettled
            except SettleUpAccountWallet.DoesNotExist:
                amount_settled_till_now = wallet_unsettled
                pass
            wallet_settlement = SettleUpAccountWallet(
                settled_by=settled_by,
                sp_code=sp_code,
                from_time=wallet_last_settled,
                amount_for_this_settlement=total,
                no_of_orders_settled=wallet_order_no,
                wallet_settlement=wallet_unsettled,
                discount_settlement=coupons_unsettled,
                price_difference_settlement=pricediff_unsettled,
                cut_settlement=cut_unsettled,
                amount_settled_till_now=amount_settled_till_now,
                lazy_wallet_t_id=tid
            )
            wallet_settlement.save()
        except:
            print print_exception()

    def settle_coupons_logs(self, settled_by, wallet_unsettled,
                           coupons_unsettled, cut_unsettled,
                           pricediff_unsettled, coupons_order_no,
                           coupons_last_settled, sp_code, total,
                            tid):
        try:
            try:
                last_settlement = SettleUpAccountCoupons.objects.\
                    filter(sp_code=sp_code).latest('to_time')
                amount_settled_till_now = last_settlement.amount_settled_till_now \
                                          + coupons_unsettled
            except SettleUpAccountCoupons.DoesNotExist:
                amount_settled_till_now = coupons_unsettled
                pass
            wallet_settlement = SettleUpAccountCoupons(
                settled_by=settled_by,
                sp_code=sp_code,
                from_time=coupons_last_settled,
                amount_for_this_settlement=total,
                no_of_orders_settled=coupons_order_no,
                wallet_settlement=wallet_unsettled,
                discount_settlement=coupons_unsettled,
                price_difference_settlement=pricediff_unsettled,
                cut_settlement=cut_unsettled,
                amount_settled_till_now=amount_settled_till_now,
                lazy_wallet_t_id=tid
            )
            wallet_settlement.save()
        except:
            print print_exception()

    def settle_pricediff_logs(self, settled_by, wallet_unsettled,
                           coupons_unsettled, cut_unsettled,
                           pricediff_unsettled, pricediff_order_no,
                           pricediff_last_settled, sp_code, total,
                              tid):
        try:
            try:
                last_settlement = SettleUpAccountPriceDiff.objects.\
                    filter(sp_code=sp_code).latest('to_time')
                amount_settled_till_now = last_settlement.amount_settled_till_now \
                                          + pricediff_unsettled
            except SettleUpAccountPriceDiff.DoesNotExist:
                amount_settled_till_now = pricediff_unsettled
                pass
            pricediff_settlement = SettleUpAccountPriceDiff(
                settled_by=settled_by,
                sp_code=sp_code,
                from_time=pricediff_last_settled,
                amount_for_this_settlement=total,
                no_of_orders_settled=pricediff_order_no,
                wallet_settlement=wallet_unsettled,
                discount_settlement=coupons_unsettled,
                price_difference_settlement=pricediff_unsettled,
                cut_settlement=cut_unsettled,
                amount_settled_till_now=amount_settled_till_now,
                lazy_wallet_t_id=tid
            )
            pricediff_settlement.save()
        except:
            print print_exception()

    def settle_cut_logs(self, settled_by, wallet_unsettled,
                           coupons_unsettled, cut_unsettled,
                           pricediff_unsettled, cut_order_no,
                           cut_last_settled, sp_code, total,
                            tid):
        try:
            try:
                last_settlement = SettleUpAccountCut.objects.\
                    filter(sp_code=sp_code).latest('to_time')
                amount_settled_till_now = last_settlement.amount_settled_till_now \
                                          + cut_unsettled
            except SettleUpAccountCut.DoesNotExist:
                amount_settled_till_now = cut_unsettled
                pass
            cut_settlement = SettleUpAccountCut(
                settled_by=settled_by,
                sp_code=sp_code,
                from_time=cut_last_settled,
                amount_for_this_settlement=total,
                no_of_orders_settled=cut_order_no,
                wallet_settlement=wallet_unsettled,
                discount_settlement=coupons_unsettled,
                price_difference_settlement=pricediff_unsettled,
                cut_settlement=cut_unsettled,
                amount_settled_till_now=amount_settled_till_now,
                lazy_wallet_t_id=tid
            )
            cut_settlement.save()
        except:
            print print_exception()


def settle_up_with_merchant(data, settled_by):
    response = dict()
    try:
        sp_code = data["sp_code"]
        wallet_set = int(data["wallet"])
        coupons_set = int(data["coupons"])
        pricediff_set = int(data["pricediff"])
        settlement = dict()
        settlement["total_amt"] = Decimal(0)
        wallet_unsettled = coupons_unsettled = \
            cut_unsettled = pricediff_unsettled = Decimal(0)
        settle_logs = SettleSellerTransactions()
        settled_for = []
        firstjan = datetime.datetime.strptime("1/1/16", "%m/%d/%y")
        if wallet_set:
            settled_for.append("Wallet")
            wallet_running = RunningAccountWallet.objects.get(sp_code=sp_code)
            wallet_unsettled = wallet_running.unsettled_amount
            wallet_order_no = wallet_running.no_of_orders_from_last_settlement
            wallet_last_settled = wallet_running.last_settled_at \
                if wallet_running.last_settled_at else firstjan
            wallet_running.last_settled_at = timezone.now() + \
                datetime.timedelta(hours=5, minutes=30)
            wallet_running.unsettled_amount = Decimal(0)
            wallet_running.no_of_orders_from_last_settlement = -1
            if wallet_running.settlement_requested:
                wallet_running.settlement_requested = False
            wallet_running.save()
        if coupons_set:
            settled_for.append("Coupons")
            coupons_running = RunningAccountCoupons.objects.get(sp_code=sp_code)
            coupons_unsettled = coupons_running.unsettled_amount
            coupons_order_no = coupons_running.no_of_orders_from_last_settlement
            coupons_last_settled = coupons_running.last_settled_at \
                if coupons_running.last_settled_at else firstjan
            coupons_running.last_settled_at = timezone.now() + \
                datetime.timedelta(hours=5, minutes=30)
            coupons_running.unsettled_amount = Decimal(0)
            coupons_running.no_of_orders_from_last_settlement = -1
            if coupons_running.settlement_requested:
                coupons_running.settlement_requested = False
            coupons_running.save()
        if pricediff_set:
            settled_for.append("Price Difference")
            pricediff_running = RunningAccountPriceDiff.objects.get(sp_code=sp_code)
            pricediff_unsettled = pricediff_running.unsettled_amount
            pricediff_order_no = pricediff_running.no_of_orders_from_last_settlement
            pricediff_last_settled = pricediff_running.last_settled_at \
                if pricediff_running.last_settled_at else firstjan
            pricediff_running.last_settled_at = timezone.now() + \
                datetime.timedelta(hours=5, minutes=30)
            pricediff_running.unsettled_amount = Decimal(0)
            pricediff_running.no_of_orders_from_last_settlement = -1
            if pricediff_running.settlement_requested:
                pricediff_running.settlement_requested = False
            pricediff_running.save()
        settlement["total_amt"] = wallet_unsettled + coupons_unsettled + \
                                  cut_unsettled + pricediff_unsettled
        settlement["settled_for"] = "For settlement of " + ', '.join(str(s) for s in settled_for)
        connect = LazyladConnection()
        wallet = connect.create_and_get_wallet(owner_id=sp_code, owner_type=1)
        transaction = Transactions(w_id=wallet,
                                   t_type='SETTLE_UP',
                                   description=settlement["settled_for"],
                                   CorD=0,
                                   amount=settlement["total_amt"],
                                   status="Success")
        wallet.balance -= settlement["total_amt"]
        wallet.save()
        transaction.save()
        tid = transaction.t_id
        if wallet_set:
            settle_logs.settle_wallet_logs(settled_by, wallet_unsettled,
                                           coupons_unsettled, cut_unsettled,
                                           pricediff_unsettled, wallet_order_no,
                                           wallet_last_settled, sp_code,
                                           settlement["total_amt"], tid)
        if coupons_set:
            settle_logs.settle_coupons_logs(settled_by, wallet_unsettled,
                                            coupons_unsettled, cut_unsettled,
                                            pricediff_unsettled, coupons_order_no,
                                            coupons_last_settled, sp_code,
                                            settlement["total_amt"], tid)
        if pricediff_set:
            settle_logs.settle_pricediff_logs(settled_by, wallet_unsettled,
                                              coupons_unsettled, cut_unsettled,
                                              pricediff_unsettled, pricediff_order_no,
                                              pricediff_last_settled, sp_code,
                                              settlement["total_amt"], tid)
        response["success"] = True
        response["transaction_id"] = tid
        response["wallet_amount"] = wallet_unsettled
        response["coupons_amount"] = coupons_unsettled
        response["pricediff"] = pricediff_unsettled
        response["total_amount"] = settlement["total_amt"]
    except:
        response["success"] = False
        response["message"] = print_exception()
    return response


def merchant_request_settlement(data):
    response = dict()
    try:
        sp_code = data["sp_code"]
        wallet_settle = int(data["wallet"])
        coupons_settle = int(data["coupons"])
        pricediff_settle = int(data["pricediff"])
        response["total_amount"] = Decimal(0)
        try:
            RunningAccountCut.objects.get(sp_code=sp_code)
        except:
            response["success"] = False
            response["message"] = "Seller Does Not Exists In Logs"
            return response
        if wallet_settle:
            wallet_running = RunningAccountWallet.objects.get(sp_code=sp_code)
            wallet_running.settlement_requested = True
            wallet_running.save()
            response["wallet_amount"] = wallet_running.unsettled_amount
            response["wallet_order_nos"] = wallet_running.no_of_orders_from_last_settlement
            if wallet_running.last_settled_at:
                response["wallet_last_settled"] = wallet_running.last_settled_at.\
                    strftime("%B %d, %Y   %I:%M %p")
            response["total_amount"] += wallet_running.unsettled_amount
        if coupons_settle:
            coupons_running = RunningAccountCoupons.objects.get(sp_code=sp_code)
            coupons_running.settlement_requested = True
            coupons_running.save()
            response["coupons_amount"] = coupons_running.unsettled_amount
            response["coupons_order_nos"] = coupons_running.no_of_orders_from_last_settlement
            if coupons_running.last_settled_at:
                response["coupons_last_settled"] = coupons_running.last_settled_at.\
                    strftime("%B %d, %Y   %I:%M %p")
            response["total_amount"] += coupons_running.unsettled_amount
        if pricediff_settle:
            pricediff_running = RunningAccountPriceDiff.objects.get(sp_code=sp_code)
            pricediff_running.settlement_requested = True
            pricediff_running.save()
            response["pricediff_amount"] = pricediff_running.unsettled_amount
            response["pricediff_order_nos"] = pricediff_running.no_of_orders_from_last_settlement
            if pricediff_running.last_settled_at:
                response["pricediff_last_settled"] = pricediff_running.last_settled_at.\
                    strftime("%B %d, %Y   %I:%M %p")
            response["total_amount"] += pricediff_running.unsettled_amount
        response["success"] = True
    except:
        response["success"] = False
        response["message"] = print_exception()
    return response


def merchant_request_balance(data):
    response = dict()
    try:
        sp_code = data["sp_code"]
        wallet_settle = int(data["wallet"])
        coupons_settle = int(data["coupons"])
        pricediff_settle = int(data["pricediff"])
        response["total_amount"] = Decimal(0)
        try:
            RunningAccountCut.objects.get(sp_code=sp_code)
        except:
            response["success"] = False
            response["message"] = "Seller Does Not Exists In Logs"
            return response
        if wallet_settle:
            try:
                wallet_running = RunningAccountWallet.objects.get(sp_code=sp_code)
                response["wallet_amount"] = wallet_running.unsettled_amount
                response["wallet_order_nos"] = wallet_running.no_of_orders_from_last_settlement
                if wallet_running.last_settled_at:
                    response["wallet_last_settled"] = wallet_running.last_settled_at.\
                        strftime("%B %d, %Y   %I:%M %p")
                response["total_amount"] += wallet_running.unsettled_amount
            except RunningAccountWallet.DoesNotExist:
                 response["wallet_amount"] = 0
                 response["wallet_order_nos"] = 0
        if coupons_settle:
            try:
                coupons_running = RunningAccountCoupons.objects.get(sp_code=sp_code)
                response["coupons_amount"] = coupons_running.unsettled_amount
                response["coupons_order_nos"] = coupons_running.no_of_orders_from_last_settlement
                if coupons_running.last_settled_at:
                    response["coupons_last_settled"] = coupons_running.last_settled_at.\
                        strftime("%B %d, %Y   %I:%M %p")
                response["total_amount"] += coupons_running.unsettled_amount
            except RunningAccountCoupons.DoesNotExist:
                 response["coupons_amount"] = 0
                 response["pricediff_order_nos"] = 0
        if pricediff_settle:
            try:
                pricediff_running = RunningAccountPriceDiff.objects.get(sp_code=sp_code)
                response["pricediff_amount"] = pricediff_running.unsettled_amount
                response["pricediff_order_nos"] = pricediff_running.no_of_orders_from_last_settlement
                if pricediff_running.last_settled_at:
                    response["pricediff_last_settled"] = pricediff_running.last_settled_at.\
                        strftime("%B %d, %Y   %I:%M %p")
                response["total_amount"] += pricediff_running.unsettled_amount
            except RunningAccountPriceDiff.DoesNotExist:
                 response["pricediff_amount"] = 0
                 response["pricediff_order_nos"] = 0
        cursor = connections["lazylad_server"].cursor()
        query = "select charge_tax_percentage from " \
                "service_providers_service_tax where sp_code = "+str(sp_code)
        rows_fetched = cursor.execute(query)
        if rows_fetched:
            seller_tax_percentage = cursor.fetchone()[0]
        charge_running = RunningAccountCut.objects.get(sp_code=sp_code)
        response["charge_amount"] = abs(charge_running.unsettled_amount)
        response["charge_tax_amount"] = abs(charge_running.unsettled_amount * seller_tax_percentage/100)
        response["charge_order_nos"] = charge_running.no_of_orders_from_last_settlement
        response["total_charge_amount"] = response["charge_tax_amount"] + response["charge_amount"]
        if charge_running.last_settled_at:
                response["charge_last_settled"] = charge_running.last_settled_at.\
                    strftime("%B %d, %Y   %I:%M %p")
        response["success"] = True
    except:
        response["success"] = False
        response["message"] = print_exception()
    return response


def merchant_cut_settlement(settled_by):
    response = dict()
    try:
        firstfeb = datetime.datetime.strptime("2/1/16", "%m/%d/%y")
        wallet_unsettled = coupons_unsettled = \
            cut_unsettled = pricediff_unsettled = Decimal(0)
        settle = SettleSellerTransactions()
        cut_accounts=RunningAccountCut.objects.all()
        sellers_settled = list()
        sellers_not_settled = list()
        for cut_account in cut_accounts:
            try:
                cut_unsettled = cut_account.unsettled_amount
                cut_order_no = cut_account.no_of_orders_from_last_settlement
                cut_last_settled = cut_account.last_settled_at \
                    if cut_account.last_settled_at else firstfeb
                cut_account.last_settled_at = timezone.now() + \
                    datetime.timedelta(hours=5, minutes=30)
                cut_account.unsettled_amount = Decimal(0)
                cut_account.no_of_orders_from_last_settlement = -1
                cut_account.save()
                connect = LazyladConnection()
                wallet = connect.create_and_get_wallet(owner_id=
                    cut_account.sp_code, owner_type=1)
                settlement_des = "Settlement for Cut for month "+\
                    str(cut_last_settled) + " for sp_code "+\
                    str(cut_account.sp_code)
                transaction = Transactions(
                    w_id=wallet,
                    t_type='SETTLE_UP',
                    description=settlement_des,
                    CorD=1, amount=cut_unsettled,
                    status="Success")
                wallet.balance -= cut_unsettled
                wallet.save()
                transaction.save()
                tid = transaction.t_id
                settle.settle_cut_logs(settled_by, wallet_unsettled,
                                       coupons_unsettled, cut_unsettled,
                                       pricediff_unsettled, cut_order_no,
                                       cut_last_settled, cut_account.sp_code,
                                       cut_unsettled, tid)
                sellers_settled.append(str(cut_account.sp_code))
            except:
                sellers_not_settled.append(str(cut_account.sp_code))
        response["settled_sellers"] = ', '.join(str(s) for
                                                s in sellers_settled)
        response["unsettled_sellers"] = ', '.join(str(s) for
                                                  s in sellers_not_settled)
        response["success"] = True
    except:
        response["success"] = False
        response["message"] = print_exception()
    return response


def item_offer_balance(sp_code):
    try:
        balance = RunningAccountItemOffer.objects.get(
            sp_code=sp_code).unsettled_amount
        return balance
    except:
        return None
