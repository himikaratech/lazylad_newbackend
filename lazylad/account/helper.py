from  decimal import *
from datetime import datetime


def is_amount_payable_by_wallet(wallet, amount):
    if amount <= wallet.balance:
        return True
    return False


def pay_amount_from_wallet(wallet, amount):
    if is_amount_payable_by_wallet(wallet, amount):
        wallet.balance -= Decimal(amount)
        wallet.save()
        return True
    else:
        return False


def pay_amount_from_seller_wallet(wallet, amount):
    wallet.balance -= Decimal(amount)
    wallet.save()
    return True


def datetime_to_timestamp(dt):
    return (dt.replace(tzinfo=None) - datetime(1970,1,1)).total_seconds()
