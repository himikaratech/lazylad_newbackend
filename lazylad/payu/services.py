
import requests
import urllib
import json
from .api_settings import lazylad_payu_path, headers


def send_hash(post_dict):
    try:
        res = requests.post(lazylad_payu_path +
                            '/payu/pay_online',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return json.loads(res.text)
    else:
        return {'error': 0, 'message': 'payment unsuccessful'}


def post_order(post_dict):
    try:
        res = requests.post(lazylad_payu_path +
                            '/payu/post_order',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return json.loads(res.text).get('return_response')
    else:
        return {'error': 0, 'message': 'payment unsuccessful'}


def success(post_dict):
    try:
        res = requests.post(lazylad_payu_path +
                            '/payu/success',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    return json.loads(res.text)


def failure(post_dict):
    res = {'success': False}
    try:
        res = requests.post(lazylad_payu_path +
                            '/payu/failure',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return json.loads(res.text)
    return res


def cancel(post_dict):
    try:
        res = requests.post(lazylad_payu_path +
                            '/cancel',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return json.loads(res.text).get('response')
    return res
