__author__ = 'AnkitAtreja'

from account import utils
import LazyladConnection
from django.db import connections

def get_order_payment_details(order_code, owner_type, order_details = None):
    if not order_details:
        order_details = get_order_basic_details(order_code, owner_type)

    order_total_amount = order_details['order_details']["total_amount"]
    order_discount_cost = order_details['order_details']["discount_cost"]

    pay_details = dict()
    pay_details['paid'] = 0
    pay_details['to_be_paid'] = order_total_amount

    payment_dict = utils.get_order_payment_details(order_code)
    if payment_dict:

        order_payment_details = payment_dict['order_payments']

        paid_amount = 0
        for payment in order_payment_details:
            if "Success" == payment["p_status"]:
                paid_amount += payment["amount"]

        if paid_amount < order_total_amount:
            to_be_paid = order_total_amount - paid_amount
        else:
            to_be_paid = 0

        if 1 == owner_type:
            urid_specific_paid_amount = paid_amount + order_discount_cost
        else:
            urid_specific_paid_amount = paid_amount

        pay_details['paid'] = urid_specific_paid_amount
        pay_details['to_be_paid'] = to_be_paid

    return {'order_payment_details' : pay_details}


def get_order_basic_details(order_code, owner_type):
    connection = LazyladConnection.LazyladConnection()
    return connection.getOrderBasicDetails(order_code, owner_type)


def get_order_details(order_code, owner_type):
    connection = LazyladConnection.LazyladConnection()
    order_details = dict()
    order_basic_details = get_order_basic_details(order_code, owner_type)
    order_user_item_details = connection.getOrderUserItemDetails(order_code, owner_type)
    order_confirmed_item_details = connection.getOrderConfirmedItemDetails(order_code, owner_type)
    order_payment_details = get_order_payment_details(order_code, owner_type, order_basic_details)
    order_details.update(order_basic_details)
    order_details.update(order_user_item_details)
    order_details.update(order_confirmed_item_details)
    order_details.update(order_payment_details)
    return order_details


def get_order_city_code(o_str):
    cursor = connections['lazylad_server'].cursor()
    query = "select cd.city_code from order_details od, user_address_details uad, " \
            "city_details cd, order_strings os where os.order_string='" + \
            str(o_str) + "' and od.id=os.o_id and uad.user_code=od.del_address_user_code" \
                         " and uad.user_address_code=od.del_address_code and " \
                         "cd.city_name=uad.user_city"
    cursor.execute(query)
    city = cursor.fetchone()[0]
    return city


def does_order_exists(o_str):
    cursor = connections['lazylad_server'].cursor()
    query = "SELECT COUNT( * ) FROM order_strings os, " \
            "order_details od WHERE os.order_string =  '" + \
            str(o_str) + "' AND od.id = os.o_id"
    cursor.execute(query)
    count = cursor.fetchone()[0]
    return False if int(count) == 0 else True
