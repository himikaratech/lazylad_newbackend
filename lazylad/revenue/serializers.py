from rest_framework import fields, serializers


class SettleUpSerializer(serializers.Serializer):
    wallet = serializers.CharField(required=True, allow_blank=False)
    coupons = serializers.CharField(required=True, allow_blank=False)
    pricediff = serializers.CharField(required=True, allow_blank=False)
    sp_code = serializers.CharField(required=True, allow_blank=False)


class RequestSettlementSerializer(serializers.Serializer):
    wallet = serializers.CharField(required=True, allow_blank=False)
    coupons = serializers.CharField(required=True, allow_blank=False)
    pricediff = serializers.CharField(required=True, allow_blank=False)
    sp_code = serializers.CharField(required=True, allow_blank=False)


class SellerSerializer(serializers.Serializer):
    sp_code = serializers.CharField(required=True)
