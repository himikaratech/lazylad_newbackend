from django.db import connections
from oms.utils import print_exception
from .utils import *


class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def get_seller_details(self, seller_id):
        try:
            query = "select sp_name, sp_number from service_providers where sp_code=%s"
            self.cursor.execute(query, [seller_id])
            data = self.cursor.fetchone()
            return data[0], data[1]
        except (IndexError, TypeError) as e:
            return None

    def get_seller_account_details(self, seller_id):

        get_start = self.check_or_create(seller_id)
        if not get_start:
            return None
        query = "select account_name, account_number, ifsc_code, \
                  branch_details, owner_type, account_id from  \
                 account_details where owner_id=%s and owner_type=%s"
        try:
            self.cursor.execute(query, [seller_id, "SELLER"])
        except Exception as e:
            return None
        data = self.cursor.fetchone()
        if data:
            return_dict = {
                    'account_name': data[0],
                    'account_number': data[1],
                    'ifsc_code': data[2],
                    'branch_details': data[3]
            }
            return return_dict
        else:
            return None


    def update_seller_account_details(self, seller_id, update_dict):
        try:
            query = "update account_details SET "
            for key, value in update_dict.items():
                query += (' ' + key+' = "'+value+'",')
            query = query[:-1]
            query += ' '
            query += 'where owner_id={0} and owner_type="{1}"'.format(
                seller_id, "SELLER")
            self.cursor.execute(query)
            return True
        except (IndexError, TypeError) as e:
            return None

    def create_seller_account_details(self, create_dict):
        try:
            query = "insert into account_details ("
            for key in create_dict.keys():
                query += (' "' + key + '",')
            query = query[:-1]
            query += ') VALUES ('
            for value in create_dict.values():
                query += (' "' + value + '",')
            query = query[:-1]
            query += ')'
            self.cursor.execute(query)
            return True
        except (IndexError, TypeError) as e:
            return None

    def check_or_create(self, seller_id):
        query = "select account_id from account_details where " \
                "owner_id=%s and owner_type=%s"
        try:
            self.cursor.execute(query, [seller_id, "SELLER"])
        except Exception as e:
            return False
        data = self.cursor.fetchone()
        if data:
            pass
        else:
            try:
                query = "insert into account_details (owner_id) VALUES (%s)"
                self.cursor.execute(query, [seller_id])
            except Exception as e:
                return False
        return True

    def create_or_update_seller_account_details(self, seller_id, update_dict):
        flag = False
        try:
            account = self.check_or_create(seller_id)
            if account:
                self.update_seller_account_details(seller_id, update_dict)
                flag = True
        except:
            pass
        return flag

    def getSellersdataforSpcodeList(self, sellers_list):

        service_providers = list()
        if len(sellers_list) > 0:
            list_specifier = ','.join(['%s']*len(sellers_list))

            query = "SELECT sp.sp_code, sp.sp_name, sp.sp_number, sp.sp_service_type_code, \
             sp.shop_open_time, sp.shop_close_time, sp.sp_delivery_time, sp.sp_min_order, sp.images_available, \
              sp.tax_flag_1, sp.tax_display_1, sp.tax_number_1, sp.tax_flag_2, sp.tax_display_2, sp.tax_number_2, \
               sp.tax_flag_3, sp.tax_display_3, sp.tax_number_3 FROM service_providers sp \
               where sp.sp_code in ("+list_specifier+") and \
               sp.shop_approval_status = 'SHOP_APPROVED' "

            self.cursor.execute(query, sellers_list)
            i = 0
            for row in self.cursor :
                (spcode, spname,  spnumber, sptype, shop_open_time, shop_close_time, spdeltime, spminorder, images_available,
                 tax_flag_1, tax_display_1, tax_number_1, tax_flag_2, tax_display_2,
                 tax_number_2, tax_flag_3, tax_display_3, tax_number_3) = row

                tmp = dict();

                tmp["id"] = i
                tmp["sp_code"] = spcode
                tmp["sp_name"] = spname
                tmp["sp_number"] = spnumber
                tmp["sp_type"] = sptype
                tmp["shop_open_time"] = shop_open_time
                tmp["shop_close_time"] = shop_close_time
                tmp["sp_del_time"] = spdeltime
                tmp["sp_min_order"] = spminorder
                tmp["images_available"] = images_available
                tmp["seller_taxes"] = {
                    "tax_flag_1" : tax_flag_1,
                    "tax_display_1" : tax_display_1,
                    "tax_number_1" : tax_number_1,
                    "tax_flag_2" : tax_flag_2,
                    "tax_display_2" : tax_display_2,
                    "tax_number_2" : tax_number_2,
                    "tax_flag_3" : tax_flag_3,
                    "tax_display_3" : tax_display_3,
                    "tax_number_3" : tax_number_3,
                }
                service_providers.append(tmp)
                i=i+1

        return {"service_providers" : service_providers}

    def confirm_items_service_provider(self, data):
        response = dict()
        update_count = 0
        response["error"] = 0
        try:
            if is_service_provider_valid(data['sp_code']):
                response["error"] = 1
                for item in data["JsonDataArray"]:
                    query = "select item_code from " \
                            "items_offered_by_service_provider where " \
                            "item_code='" + str(item["item_code"]) + "' and " \
                            "sp_code='" + str(data["sp_code"]) + "'"
                    rows_fetched = self.cursor.execute(query)
                    if rows_fetched == 0:
                        if int(item["item_status"]) == 1 or int(item["item_status"]) == 3:
                            query = "insert into items_offered_by_service_provider" \
                                    " (item_code, sp_code, item_cost, item_status) " \
                                    "values ('"+str(item["item_code"])+"', '" +\
                                    str(data["sp_code"])+"', '" +\
                                    str(item["item_cost"])+"','" +\
                                    str(item["item_status"])+"')"
                            rows_affected = self.cursor.execute(query)
                            if rows_affected:
                                response["error"] = 1
                                response["message"] = "Inserted Successfully"
                            else:
                                response["error"] = 0
                                response["message"] = "Insert Unsuccessful"
                        else:
                            response["error"] = 0
                    elif rows_fetched == 1:
                        if int(item["item_status"]) == 1 or int(item["item_status"] == 3):
                            query = "update items_offered_by_service_provider " \
                                    "set item_cost = "+str(item["item_cost"]) +\
                                    " , item_status = "+str(item["item_status"]) +\
                                    " where item_code = "+str(item["item_code"]) +\
                                    " and sp_code = "+str(data["sp_code"])
                            rows_affected = self.cursor.execute(query)
                            if rows_affected:
                                response["error"] = 1
                                response["message"] = "Updated Successfully"
                            else:
                                response["error"] = 0
                                response["message"] = "Updated Unsuccessfully"
                        elif int(item["item_status"]) == 2:
                            query = "delete from  items_offered_by_service_provider " \
                                    "where item_code = " + str(item["item_code"]) +\
                                    " and sp_code = " + str(data["sp_code"])
                            result_delete = self.cursor.execute(query)
                            if result_delete:
                                response["error"] = 1
                                response["message"] = "Removed Successfully"
                            else:
                                response["error"] = 0
                                response["message"] = "Remove Unsuccessfully"
                    else:
                        response["error"] = 0
                        response["message"] = "Multiple Entries"
                    if response["error"] == 1:
                        update_count += 1
            else:
                response["error"] = 0
                response["message"] = "Service Provider Not Available"
            response["Update_Count"] = update_count
            response["success"] = True
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def seller_call_us(self, sp_code):
        """
        sp_code: Not using for now.

        """
        response = dict()
        try:
            if int(sp_code) == 0:
                cursor = self.get_cursor()
                query = "select config_value_string from lazylad_config where config_value_integer = 0"
                cursor.execute(query)
                number = cursor.fetchone()[0]
                response["customer_care"] = number
                response["success"] = True
            else:
                cursor = self.get_cursor()
                query = "select config_value_string from lazylad_config where config_value_integer = 0"
                cursor.execute(query)
                number = cursor.fetchone()[0]
                response["customer_care"] = number
                response["success"] = True
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response



