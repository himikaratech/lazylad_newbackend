
from django.db import connections

class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    __order_details_query = "SELECT os.order_string, UNIX_TIMESTAMP(od.expected_del_time)," \
            " uad.user_address, ud.user_code, ud.phone_number, od.order_date," \
            " od.order_tot_amount, od.order_total_cost, od.order_delivery_cost,\
             od.order_discount_cost, oin.status, uad.user_name, uad.user_phone_number," \
            " sps.sp_name, sps.sp_number, sps.shop_address, od.coupon_code, " \
            "od.logistics_services_used, od.rider_assigned, od.rider_accepted "\
            "FROM "   \
            "order_in_progress oin, order_details od, user_address_details uad,  \
            service_providers sps, user_details ud, order_strings os    \
            where   \
            os.o_id = od.id and \
            od.order_code=oin.order_code and    \
            uad.user_code=od.del_address_user_code and  \
            uad.user_address_code=od.del_address_code and   \
            od.user_code = ud.user_code and \
            sps.sp_code=oin.sp_code "

    __order_user_details_query = "SELECT iobsp.item_code, iobsp.item_name, iobsp.item_desc, iobsp.item_img_flag, \
            iobsp.item_img_add, iobsp.item_short_desc, oid.item_cost, oid.item_quantity \
            from order_item_details oid, items_offered_by_service_provider iobsp \
            where \
            oid.order_code= %s and \
            iobsp.sp_code= %s and \
            oid.item_code=iobsp.item_code "

    __order_confirmed_details_query = "SELECT iobsp.item_code, iobsp.item_name, iobsp.item_desc, iobsp.item_img_flag, \
            iobsp.item_img_add, iobsp.item_short_desc, oisbsp.item_cost, oid.item_quantity \
            from \
            order_item_details oid, order_item_served_by_sp oisbsp, items_offered_by_service_provider iobsp \
            where \
            oisbsp.order_code= %s and \
            iobsp.sp_code=%s and \
            oisbsp.item_code=iobsp.item_code and \
            oid.order_code=oisbsp.order_code and \
            oid.item_code=oisbsp.item_code "

    def getOrdersforList(self, order_codes, owner_type):
        order_list = {"orders" : []}
        if order_codes :

            list_specifier = ','.join(['%s']*len(order_codes))
            order_condition = " and od.order_code in ("+list_specifier+") "
            final_query = self.__order_details_query + order_condition
            self.cursor.execute(final_query, order_codes)

            for row in self.cursor:
                order_detail_dict = self.rowToOrderDetails(order_tuple=row)

                order_list['orders'].append(order_detail_dict)

        return order_list

    def getOrderBasicDetails(self, order_code, owner_type):
        order_detail_dict = None
        if order_code :
            order_condition = " and od.order_code = %s "
            final_query = self.__order_details_query + order_condition
            self.cursor.execute(final_query, [order_code])

            row = self.cursor.fetchone()
            if row:
                order_detail_dict = self.rowToOrderDetails(order_tuple=row)

        return order_detail_dict

    def rowToOrderDetails(self, order_tuple):
        (ordercode, expecteddeltime, deladdress, usercode, phonenumber,
         orderdate, ordertotamount, ordertotalcost, orderdeliverycost,
         orderdiscountcost, orderstatus, username, userphonenumber,
          serv_prov_name, sp_number, shop_address, coupon_code, rider_requested, rider_assigned,
           rider_accepted) = order_tuple

        tmp = dict()
        tmp["order_code"] = ordercode
        tmp["order_time"] = orderdate
        tmp["order_status"] = orderstatus
        tmp["delivery_address"] = deladdress
        tmp["user_code"] = usercode
        tmp["phone_number"] = phonenumber
        tmp["total_amount"] = ordertotamount
        tmp["total_items_amount"] = ordertotalcost
        tmp["delivery_cost"] = orderdeliverycost
        tmp["discount_cost"] = orderdiscountcost
        tmp["expected_del_time"] = expecteddeltime
        tmp["sp_name"] = serv_prov_name
        tmp["sp_number"] = sp_number
        tmp["shop_address"] = shop_address
        tmp["user_name"] = username
        tmp["user_phone_number"] = userphonenumber
        tmp["coupon_code"] = coupon_code
        tmp["rider_requested"] = rider_requested
        tmp["rider_assigned"] = rider_assigned
        tmp["rider_accepted"] = rider_accepted

        return {"order_details":tmp}

    def getOrderSpCode(self, order_code):
        sp_code = None

        stmt_sp = "SELECT sp_code from order_in_progress where order_code = %s"
        self.cursor.execute(stmt_sp, [order_code])
        data = self.cursor.fetchone()
        if data:
            sp_code = data[0]

        return sp_code

    def getOrderUserItemDetails(self, order_code, owner_type):
       sp_code = self.getOrderSpCode(order_code)

       query = self.__order_user_details_query
       self.cursor.execute(query, [order_code, sp_code])

       item_details = []
       for row in self.cursor:
           item_detail = self.rowToOrderItemDetails(row)
           item_details.append(item_detail)

       return {"user_item_details" : item_details}


    def getOrderConfirmedItemDetails(self, order_code, owner_type):
       sp_code = self.getOrderSpCode(order_code)

       query = self.__order_confirmed_details_query
       self.cursor.execute(query, [order_code, sp_code])

       item_details = []
       for row in self.cursor:
           item_detail = self.rowToOrderItemDetails(row)
           item_details.append(item_detail)

       return {"confirmed_item_details" : item_details}


    def rowToOrderItemDetails(self, order_item_tuple):
        order_item_details = None
        if order_item_tuple:
            (itemcode, itemname, itemdesc, itemimgflag, itemimgadd,
             itemshortdesc, itemcost, itemquantity) = order_item_tuple

            tmp = dict()
            tmp["item_code"] = itemcode
            tmp["item_name"] = itemname
            tmp["item_img_flag"] = itemimgflag
            tmp["item_img_address"] = itemimgadd
            tmp["item_short_desc"] = itemshortdesc
            tmp["item_quantity"] = itemquantity
            tmp["item_cost"] = itemcost
            tmp["item_desc"] = itemdesc

            order_item_details = tmp

        return order_item_details

    def get_user_phone_no_from_user_code(self, user_code):
        try:
            query = 'select phone_number from user_details where user_code = ' \
                    + user_code
            self.cursor.execute(query)
            phone_no = self.cursor.fetchone()[0]
            return phone_no
        except:
            return None

    def get_user_city_code_from_user_code(self, user_code):
        try:
            query = "SELECT cd.city_code FROM user_address_details uad, city_details cd WHERE\
            uad.user_phone_number = (SELECT phone_number FROM user_details WHERE \
            user_code = '" + str(user_code) + "') AND cd.city_name = uad.user_city \
            ORDER BY user_address_code DESC LIMIT 1"
            self.cursor.execute(query)
            return self.cursor.fetchone()[0]
        except:
            return None

    def update_logistics(self, sp_code, order_code):
        try:
            query1 = 'select logistics_services from service_providers' \
                     ' where sp_code = ' + sp_code
            self.cursor.execute(query1)
            logistics_flag = self.cursor.fetchone()[0]
            query2 = 'update order_details set logistics_services_used = '\
                     + str(logistics_flag) + ' where order_code = ' + order_code
            self.cursor.execute(query2)
            return True
        except:
            return False

    def get_user_delivery_city(self, user_code):
        try:
            query = "SELECT user_delivery_city FROM user_details WHERE user_code = "+str(user_code)
            self.cursor.execute(query)
            return self.cursor.fetchone()[0]
        except:
            return None

    def get_offer_diff(self, order_code):
        offer_diff = 0
        try:
            query = 'select offer_diff from order_item_offer_details where order_code = '+str(order_code)
            self.cursor.execute(query)
            offer_diff = self.cursor.fetchone()[0]
        except:
            pass
        return offer_diff





