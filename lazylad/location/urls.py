from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import SaveUser, SaveSeller, SellerList

api_urlpatterns = patterns(
    'location.apiviews',
    url(
        r'^save_user_cordinates',
        SaveUser.as_view({'post': 'save_user_cordinates'}),
        name='send-sms'),
    url(r'^update_user_delivery_location',
        SaveUser.as_view({'post': 'update_user_delivery_location'}),
        name='update_user_delivery_location'),
    url(r'^v2_update_user_delivery_location',
        SaveUser.as_view({'post': 'update_user_delivery_location_v2'}),
        name='update_user_delivery_location'),
    url(
        r'^save_seller_cordinates',
        SaveSeller.as_view({'post': 'save_seller_cordinates'}),
        name='send-sms'),
    url(
        r'^update_seller_shop_cordinates',
        SaveSeller.as_view({'post': 'update_seller_shop_cordinates'}),
        name='update_seller_shop_cordinates'),
    url(
        r'^get_seller_list',
        SellerList.as_view({'post': 'get_seller_list'}),    
        name='send-sms'),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)