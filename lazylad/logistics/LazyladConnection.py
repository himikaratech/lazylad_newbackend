
from django.db import connections
from database.LazyladConnection import LazyladConnection as common_connection


class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def assign_rider(self, order_code):
        try:
            connect = common_connection()
            order_id = connect.get_urid_original_mapping(order_code, 'ORDER')
            query = "update order_details SET rider_assigned = 1 where order_code=%s"
            self.cursor.execute(query, [order_id])
            return True
        except (IndexError, TypeError) as e:
            return None

    def rider_accept(self, order_code):
        try:
            connect = common_connection()
            order_id = connect.get_urid_original_mapping(order_code, 'ORDER')
            query = "update order_details SET rider_accepted = 1 where order_code=%s"
            self.cursor.execute(query, [order_id])
            return True
        except (IndexError, TypeError) as e:
            return None

    def rider_reject(self, order_code):
        try:
            connect = common_connection()
            order_id = connect.get_urid_original_mapping(order_code, 'ORDER')
            query = "update order_details SET rider_assigned = 0 where order_code=%s"
            self.cursor.execute(query, [order_id])
            return True
        except (IndexError, TypeError) as e:
            return None

    def update_order_delivered(self, order_code):
        try:
            connect = common_connection()
            order_id = connect.get_urid_original_mapping(order_code, 'ORDER')
            query = "update order_details SET rider_accepted = 1 where order_code=%s"
            self.cursor.execute(query, [order_id])
            return True
        except (IndexError, TypeError) as e:
            return None

    def get_city_code(self, order_string):
        response = {'success': False, 'city_code': None}
        try:
            connect = common_connection()
            order_code = connect.get_urid_original_mapping(
                order_string, 'ORDER')
            query1 = 'select sp_code from order_in_progress where order_code ='\
                     + order_code
            self.cursor.execute(query1)
            data = self.cursor.fetchone()
            sp_code = data[0]
            query2 = 'select sp_city_code from service_providers where sp_code = '\
                     + sp_code
            self.cursor.execute(query2)
            data = self.cursor.fetchone()
            city_code = data[0]
            response['success'] = True
            response['city_code'] = city_code

        except (IndexError, TypeError) as e:
            response['message'] = 'City Code Not Found/Exists'
        return response

    def update_logistics_used(self, order_code):
        try:
            connect = common_connection()
            order_id = connect.get_urid_original_mapping(order_code, 'ORDER')
            query = "update order_details SET logistics_services_used = 1 where order_code=%s"
            self.cursor.execute(query, [order_id])
            return True
        except (IndexError, TypeError) as e:
            return None
