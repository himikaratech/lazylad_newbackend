import linecache
import sys
import json
import string
import random
from decimal import Decimal


from .models import CouponCodes


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    line_no = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, line_no, f.f_globals)
    return str('EXCEPTION IN ({}, LINE {} :: "{}"): {}'.format(
        filename, line_no, line.strip(), exc_obj))


def generate_coupon_code():
    new_coupon_code = None
    while 1:
        try:
            new_coupon_code = id_generator()
            coupon = CouponCodes.objects.get(coupon_code=new_coupon_code)
        except CouponCodes.DoesNotExist:
            return new_coupon_code
