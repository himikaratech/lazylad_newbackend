
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from .serializers import *
from .services import *
import json


class PayU(ViewSet):

    def create_hash(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = HashCreateSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                resp = send_hash(data)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
                    response.update(resp)
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def post_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = PostOrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                resp = post_order(data)
                if resp.get('success'):
                    response['error_code'] = 1
                    response.update(**resp)
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)
