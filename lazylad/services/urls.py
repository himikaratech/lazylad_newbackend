from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import Services

api_urlpatterns = patterns(
    'services.apiviews',
    url(
        r'^get_services',
        Services.as_view({'get': 'get_services'})),
    url(
        r'^post_order',
        Services.as_view({'post': 'post_order'})),
    url(
        r'^post_cart',
        Services.as_view({'post': 'post_cart'})),
    url(
        r'^get_categories',
        Services.as_view({'get': 'get_categories'})),
    url(
        r'^coupon_validity',
        Services.as_view({'post': 'coupon_validity'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)