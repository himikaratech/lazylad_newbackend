
from rest_framework import fields, serializers


class PostOrderSerializer(serializers.Serializer):
    user_code = serializers.CharField()
    address_code = serializers.CharField()
    services_requested = serializers.ListField()
    address_user_code = serializers.CharField()
    coupon_code = serializers.CharField(allow_blank=True, allow_null=True)


class GetServicesSerializer(serializers.Serializer):
    user_code = serializers.CharField()


class CouponValidity(serializers.Serializer):
    user_code = serializers.CharField()
    coupon_code = serializers.CharField()


class PostKartSerializer(serializers.Serializer):
    user_code = serializers.CharField()
    estimated_cost = serializers.CharField()
    kart_items = serializers.ListField()
