
from rest_framework import fields, serializers
from .app_settings import KHATA_TRANSACTION_TYPE
import datetime


class FetchAllItemsSerializer(serializers.Serializer):
    st_code = serializers.CharField(required=True)
    prev_item_code = serializers.CharField(required=False)


class SendOrdersSerializer(serializers.Serializer):
    orders_list = serializers.ListField(
        required=True, child=serializers.DictField())
    sp_code = serializers.CharField(required=True)


class UpdateInventorySerializer(serializers.Serializer):
    items_list = serializers.ListField(
        required=True, child=serializers.DictField())
    sp_code = serializers.CharField(required=True)


class AddUsersSerializer(serializers.Serializer):
    users_list = serializers.ListField(
        required=True, child=serializers.DictField())
    sp_code = serializers.CharField(required=True)


class AddSellersSerializer(serializers.Serializer):
    sellers_list = serializers.ListField(
        required=True, child=serializers.DictField())


class UserSerializer(serializers.Serializer):
    sp_code = serializers.CharField(required=True)
    user_name = serializers.CharField(required=True)
    phone_number = serializers.CharField(required=True)


class SyncSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)


class KhataSerializer(serializers.Serializer):
    sp_code = serializers.CharField(required=True)
    khata_transactions = serializers.ListField(
        required=True, child=serializers.DictField())


class SellerSerializer(serializers.Serializer):
    sp_code = serializers.CharField(required=True)


class ViewKhataSerializer(serializers.Serializer):
    sp_code = serializers.CharField(required=True)
    user_name = serializers.CharField(required=True)
    phone_number = serializers.CharField(required=True)


class SellerLoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)


class ImageInventorySerializer(SellerSerializer):
    inventory_image = serializers.ImageField()


class SettleUpSerializer(SellerSerializer):
    settle_up_payment = serializers.CharField(required=True)


class SellerSMSSerializers(SellerSerializer):
    recipients_list = serializers.ListField(
        required=True, child=serializers.CharField())
    message = serializers.CharField()


class SellerMonthlySerializer(SellerSerializer):
    month = serializers.IntegerField(default=datetime.datetime.now().month)
    year = serializers.IntegerField(default=datetime.datetime.now().year)


class SellerReportSerializer(SellerSerializer):
    from_day = serializers.IntegerField(default=datetime.datetime.now().day)
    from_month = serializers.IntegerField(default=datetime.datetime.now().month)
    from_year = serializers.IntegerField(default=datetime.datetime.now().year)
    to_day = serializers.IntegerField(default=datetime.datetime.now().day)
    to_month = serializers.IntegerField(default=datetime.datetime.now().month)
    to_year = serializers.IntegerField(default=datetime.datetime.now().year)


class SellerDailyReportSerializer(SellerSerializer):
    day = serializers.IntegerField(default=datetime.datetime.now().day)
    month = serializers.IntegerField(default=datetime.datetime.now().month)
    year = serializers.IntegerField(default=datetime.datetime.now().year)


class OrderSerializer(serializers.Serializer):
    order_code = serializers.CharField(required=True)


class GenerateOrderSerializer(serializers.Serializer):
    no_of_times = serializers.CharField(required=False)

