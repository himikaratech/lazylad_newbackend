from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import ItemSystem

api_urlpatterns = patterns(
    'ims.apiviews',
    url(
        r'^save_item_clicks',
        ItemSystem.as_view({'post': 'save_item_clicks'})),
    url(
        r'^save_item_views',
        ItemSystem.as_view({'post': 'save_item_views'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)