
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from .serializers import *
from .LazyPrimeConnection import LazyPrime, LazyladConnection, PrimeVendor
import json


class Prime(ViewSet):

    def add_user(self, request):
        """

        """

        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddUserSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.add_user(data)
                if resp.get('success'):
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def user_login(self, request):
        """

        """

        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = UserLoginSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.user_login(data)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['user_code'] = resp.get('user_code')
                else:
                    response['error_details'] = resp.get('message')
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_items(self, request):
        """

        """

        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GetItemSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.get_items(data)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['items_list'] = resp.get('items_list')
                else:
                    response['error_details'] = resp.get('message')
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def place_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SendOrdersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.post_order(data)
                if resp.get('success'):
                    response['success'] = True
                    response['order_code'] = resp.get('order_code')
                    response['address'] = resp.get('address')
                    if resp.get('sub_orders'):
                        response['sub_orders'] = resp.get('sub_orders')
                    response['total_amount'] = resp.get('total_amount')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def pos_to_prime(self, request):
        """

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        _serlz = SendOrdersPOSSerializer(data=data)
        if data:
            data['items_list'] = json.loads(data['items_list'])
            data['order'] = json.loads(data['order'])
        try:
            if _serlz.is_valid():
                resp = connection.post_order_POS(data)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['order_code'] = resp.get('order_code')
                    response['address'] = resp.get('address')
                    response['total_amount'] = resp.get('total_amount')
                else:
                    response['error_details'] = resp.get('message')
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def initial_sync_items(self, request):
        """
        API to sync items initially
        from lazylad to lazypos

        request:
            password
        return:
            success
            error_code
            error_details

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SyncSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                password = data.get('password')
                if password == 'chicharito':
                    resp = connection.initial_sync_items()
                else:
                    response['error_message'] = 'Invalid Password'
                    return Response(response, api_response_status)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_service_categories(self, request):
        """
        API TO SEND SERVICE CATEGORIES OF SERVICE TYPE 6

        :param request:
            user_code
            password
        :return:
            {   success:
                error_code:
                types:{ st_name:
                        st_code:
                        categories:[{sc_name:
                                    sc_code:}...]
                      }
            }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = ServiceCatSerlz(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                response = connection.get_service_types_categories()
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_addresses(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = ServiceCatSerlz(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.get_addresses(data.get('user_code'))
                if resp.get('success'):
                    response['addresses'] = resp.get('addresses')
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def add_address(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddAddressSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.add_address(
                    data, data.get('user_code'), data.get('city_id'))
                if resp.get('success'):
                    response['success'] = True
                    response['address_key'] = resp.get('address_key')
                    response['address_id'] = resp.get('address_id')
                    response['user_code'] = resp.get('user_code')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def confirm_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.confirm_order(data['order_code'])
                if resp.get('success'):
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def vendor_account(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = VendorAccountSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPrime()
        try:
            if _serlz.is_valid():
                resp = connection.vendor_accounts(data)
                if resp.get('success'):
                    response['success'] = True
                    response['order'] = resp.get('orders')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)


class Vendor(ViewSet):

    def get_brands(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = DummySerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.get_brands(data)
                if resp.get('success'):
                    response['success'] = True
                    response['brands'] = resp.get('brands')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_items(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = BrandSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.get_items(data)
                if resp.get('success'):
                    response['success'] = True
                    response['items_list'] = resp.get('items_list')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def vendor_login(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = UserLoginSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.vendor_login(data)
                if resp.get('success'):
                    response['success'] = True
                    response['vendor_id'] = resp.get('vendor_id')
                    response['message'] = resp.get('message')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_orders(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = GetOrdersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.get_orders(data)
                if resp.get('success'):
                    response['success'] = True
                    response['order_list'] = resp.get('order_list')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def view_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.view_order(data)
                if resp.get('success'):
                    response['success'] = True
                    response['order'] = resp.get('order')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def cnf_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.cnf_order(data)
                response['message'] = resp.get('message')
                if resp.get('success'):
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def pay_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = PrimeVendor()
        try:
            if _serlz.is_valid():
                resp = connection.pay_order(data)
                response['message'] = resp.get('message')
                if resp.get('success'):
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)