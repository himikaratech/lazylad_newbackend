

LOGISTICS_OWNER_CODE = 4

LOGISTICS_ORDER_STATUS = (('UNA', 'unassigned'),
                          ('ASS', 'assigned'),
                          ('TBP', 'to_be_picked'),
                          ('PIK', 'picked'),
                          ('DLV', 'delivered'))

DRIVER_LOGGING = (('IN', 'checked_in'),
                  ('OUT', 'checked_out'),
                  ('DLV', 'order_delivered'),
                  ('PIK', 'order_picked'),
                  ('LB', 'owner_type'))

DRIVER_STATUS = (('0', 'BUSY'),
                 ('1', 'AVAILABLE'))

ORDER_BY_STATUS_MAPPING = (('UNA', 'created_at'),
                    ('ASS', 'assigned_at'),
                    ('TBP', 'to_be_picked_at'),
                    ('PIK', 'picked_at'),
                    ('DLV', 'delivered_at'))

