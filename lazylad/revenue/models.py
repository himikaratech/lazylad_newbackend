from django.db import models
from decimal import Decimal
import datetime
from django.utils import timezone
import decimal

PAYMENT_TYPE_CHOICES = (('C', 'Credit'),
                        ('D', 'Debit'))


class RunningAccountWallet(models.Model):
    sp_code = models.CharField(max_length=25, unique=True)
    unsettled_amount = models.DecimalField(max_digits=20, decimal_places=2)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)
    settlement_requested = models.BooleanField(default=False)
    amount_requested = models.DecimalField(max_digits=20,
                                           decimal_places=2,
                                           default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountWallet, self).save(*args, **kwargs)


class SettleUpAccountWallet(models.Model):
    settled_by = models.CharField(max_length=50, default=None)
    sp_code = models.CharField(max_length=25)
    lazy_wallet_t_id = models.CharField(max_length=25, null=True, blank=True)
    payment_type = models.CharField(
        max_length=1, choices=PAYMENT_TYPE_CHOICES)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    amount_for_this_settlement = models.DecimalField(max_digits=20, decimal_places=2)
    no_of_orders_settled = models.IntegerField()
    wallet_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    discount_settlement = models.DecimalField(max_digits=20,
                                              decimal_places=2,
                                              default=decimal.Decimal(0.000))
    cut_settlement = models.DecimalField(max_digits=20,
                                         decimal_places=2,
                                         default=decimal.Decimal(0.000))
    price_difference_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    amount_settled_till_now = models.DecimalField(max_digits=20,
                                                  decimal_places=2,
                                                  default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        if self.amount_for_this_settlement > 0:
            self.payment_type = 'D'
        else:
            self.payment_type = 'C'
        super(SettleUpAccountWallet, self).save(*args, **kwargs)


class RunningAccountCut(models.Model):
    sp_code = models.CharField(max_length=25, unique=True)
    unsettled_amount = models.DecimalField(max_digits=20, decimal_places=2)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)
    settlement_requested = models.BooleanField(default=False)
    amount_requested = models.DecimalField(max_digits=20,
                                           decimal_places=2,
                                           default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        self.last_edited_at = timezone.now() + \
                          datetime.timedelta(hours=5, minutes=30)
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountCut, self).save(*args, **kwargs)


class SettleUpAccountCut(models.Model):
    settled_by = models.CharField(max_length=50, default=None)
    lazy_wallet_t_id = models.CharField(max_length=25, null=True, blank=True)
    sp_code = models.CharField(max_length=25)
    payment_type = models.CharField(
        max_length=1, choices=PAYMENT_TYPE_CHOICES)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    amount_for_this_settlement = models.DecimalField(max_digits=20, decimal_places=2)
    no_of_orders_settled = models.IntegerField()
    wallet_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    discount_settlement = models.DecimalField(max_digits=20,
                                              decimal_places=2,
                                              default=decimal.Decimal(0.000))
    cut_settlement = models.DecimalField(max_digits=20,
                                         decimal_places=2,
                                         default=decimal.Decimal(0.000))
    price_difference_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    amount_settled_till_now = models.DecimalField(max_digits=20,
                                                  decimal_places=2,
                                                  default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        if self.amount_for_this_settlement > 0:
            self.payment_type = 'D'
        else:
            self.payment_type = 'C'
        super(SettleUpAccountCut, self).save(*args, **kwargs)


class RunningAccountCoupons(models.Model):
    sp_code = models.CharField(max_length=25, unique=True)
    unsettled_amount = models.DecimalField(max_digits=20, decimal_places=2)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)
    settlement_requested = models.BooleanField(default=False)
    amount_requested = models.DecimalField(max_digits=20,
                                           decimal_places=2,
                                           default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountCoupons, self).save(*args, **kwargs)


class SettleUpAccountCoupons(models.Model):
    settled_by = models.CharField(max_length=50, default=None)
    lazy_wallet_t_id = models.CharField(max_length=25, null=True, blank=True)
    sp_code = models.CharField(max_length=25)
    payment_type = models.CharField(
        max_length=1, choices=PAYMENT_TYPE_CHOICES)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    amount_for_this_settlement = models.DecimalField(max_digits=20, decimal_places=2)
    no_of_orders_settled = models.IntegerField()
    wallet_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    discount_settlement = models.DecimalField(max_digits=20,
                                              decimal_places=2,
                                              default=decimal.Decimal(0.000))
    cut_settlement = models.DecimalField(max_digits=20,
                                         decimal_places=2,
                                         default=decimal.Decimal(0.000))
    price_difference_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    amount_settled_till_now = models.DecimalField(max_digits=20,
                                                  decimal_places=2,
                                                  default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        if self.amount_for_this_settlement > 0:
            self.payment_type = 'D'
        else:
            self.payment_type = 'C'
        super(SettleUpAccountCoupons, self).save(*args, **kwargs)


class RunningAccountPriceDiff(models.Model):
    sp_code = models.CharField(max_length=25, unique=True)
    unsettled_amount = models.DecimalField(max_digits=20, decimal_places=2)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)
    settlement_requested = models.BooleanField(default=False)
    amount_requested = models.DecimalField(max_digits=20,
                                           decimal_places=2,
                                           default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountPriceDiff, self).save(*args, **kwargs)


class SettleUpAccountPriceDiff(models.Model):
    settled_by = models.CharField(max_length=50, default=None)
    lazy_wallet_t_id = models.CharField(max_length=25, null=True, blank=True)
    sp_code = models.CharField(max_length=25)
    payment_type = models.CharField(
        max_length=1, choices=PAYMENT_TYPE_CHOICES)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    amount_for_this_settlement = models.DecimalField(
        max_digits=20, decimal_places=2)
    no_of_orders_settled = models.IntegerField()
    wallet_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    discount_settlement = models.DecimalField(max_digits=20,
                                              decimal_places=2,
                                              default=decimal.Decimal(0.000))
    cut_settlement = models.DecimalField(max_digits=20,
                                         decimal_places=2,
                                         default=decimal.Decimal(0.000))
    price_difference_settlement = models.DecimalField(max_digits=20,
                                            decimal_places=2,
                                            default=decimal.Decimal(0.000))
    amount_settled_till_now = models.DecimalField(max_digits=20,
                                                  decimal_places=2,
                                                  default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        super(SettleUpAccountPriceDiff, self).save(*args, **kwargs)


class RunningAccountItemOffer(models.Model):
    sp_code = models.CharField(max_length=25, unique=True)
    unsettled_amount = models.DecimalField(max_digits=20, decimal_places=2)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)
    settlement_requested = models.BooleanField(default=False)
    amount_requested = models.DecimalField(
        max_digits=20,
        decimal_places=2,
        default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountItemOffer, self).save(*args, **kwargs)


class SettleUpAccountItemOffer(models.Model):
    settled_by = models.CharField(max_length=50, default=None)
    lazy_wallet_t_id = models.CharField(max_length=25, null=True, blank=True)
    sp_code = models.CharField(max_length=25)
    payment_type = models.CharField(
        max_length=1, choices=PAYMENT_TYPE_CHOICES)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    amount_for_this_settlement = models.DecimalField(
        max_digits=20, decimal_places=2)
    no_of_orders_settled = models.IntegerField()
    offer_price_settlement = models.DecimalField(
        max_digits=20,
        decimal_places=2,
        default=decimal.Decimal(0.000))
    amount_settled_till_now = models.DecimalField(
        max_digits=20,
        decimal_places=2,
        default=decimal.Decimal(0.000))

    class Meta:
        app_label = 'revenue'

    def save(self, *args, **kwargs):
        super(SettleUpAccountItemOffer, self).save(*args, **kwargs)
