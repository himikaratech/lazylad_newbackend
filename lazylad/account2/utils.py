from django.db import IntegrityError
from decimal import Decimal
from database.LazyladConnection import LazyladConnection
from database.models import Wallet, Transactions, OrderCompletionDetails, PaymentDetails
from cs.utils import print_exception
from django.db import connections

def process_cashback(update_dict):
    try:
        connect = LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(
            update_dict.get('user_code'), update_dict.get('owner_type'))
        wallet = connect.create_and_get_wallet(
            owner_id=owner_id, owner_type=update_dict.get('owner_type'))
        amount = Decimal(update_dict.get('amount'))
        urid_visible = connect.get_urid_visible_mapping(
            update_dict.get('urid'), update_dict.get('urid_type'))
        t_details = Transactions(w_id=wallet,
                                 t_type=update_dict.get('t_type'),urid=urid_visible,
                                 urid_type= update_dict.get('urid_type'),
                                 description="Cashback for Order no " + str(urid_visible),
                                 CorD=1, amount=amount, status='Pending')
        t_details.save()
        wallet.balance += amount
        wallet.save()
        t_details.status = 'Success'
        t_details.save()

    except (ValueError, IntegrityError) as e:
        return False

    return True


def order_payment_details(t_id):
    response = dict()
    try:
        trans = Transactions.objects.get(t_id=t_id)
        if trans.t_type == 'ORDER_FINISH_SELLER':
            order_string = trans.description.split(' ')[5]
            cursor = connections['lazylad_server'].cursor()
            query = "SELECT order_code ,order_discount_cost, " \
                    "order_tot_amount, order_date from " \
                    "order_details od, order_strings os where " \
                    "os.order_string = '"+str(order_string)+"' " \
                    "and od.id = os.o_id"
            rows_fetched = cursor.execute(query)
            if int(rows_fetched):
                row = cursor.fetchone()
                discount = row[1]
                code = row[0]
                total = row[2]
                order_date = row[3]
                order_comp = OrderCompletionDetails.objects.get(order_code=code)
                wallet_pay = PaymentDetails.objects.get(order_code=code)
                wallet = wallet_pay.amount
                price_diff = order_comp.price_difference
                cut = order_comp.lazy_lad_charges
                delivery = order_comp.lazylad_charge
                cod = float(total) - float(wallet)
                response["success"] = True
                response["transaction_details"] = {
                    "discount": str(discount),
                    "wallet": str(wallet),
                    "price_difference": str(price_diff),
                    "lazylad_charges": str(cut),
                    "delivery_charges": str(delivery),
                    "total_amount": str(total),
                    "COD": str(cod),
                    "order_date": str(order_date)
                }
                response["message"] = "Transaction details for order "+str(order_string)
            else:
                response["success"] = False
                response["message"] = "Transaction for this order not found"
        else:
            response["success"] = False
            response["message"] = "Incompatible transaction ID"
    except:
        response["success"] = False
        response["message"] = print_exception()
    return response
