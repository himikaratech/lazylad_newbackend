
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from .dbconnections import LazyServices, order_logs
from .serializers import *


class Services(ViewSet):

    def get_services(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GetServicesSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyServices()
        try:
            if _serlz.is_valid():
                resp = connection.get_services(data.get('user_code'))
                if resp.get('success'):
                    response['services'] = resp.get('services')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def post_order(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = PostOrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyServices()
        try:
            if _serlz.is_valid():
                resp = connection.post_order(data)
                if resp.get('success'):
                    response['order_code'] = resp.get('order_code')
                    response['pickup_time'] = resp.get('pickup_time')
                    response['delivery_time'] = resp.get('delivery_time')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        order_logs(data, response, response['success'])
        return Response(response, api_response_status)

    def post_cart(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = PostKartSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyServices()
        try:
            if _serlz.is_valid():
                resp = None
                if resp.get('success'):
                    response['xyz'] = resp.get('xyz')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_categories(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GetServicesSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyServices()
        try:
            if _serlz.is_valid():
                resp = connection.get_service_categories(data.get('user_code'))
                if resp.get('success'):
                    response['categories'] = resp.get('categories')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def coupon_validity(self, request):
        response = dict()
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = CouponValidity(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyServices()
        try:
            if _serlz.is_valid():
                resp = connection.coupon_validity(data)
                if resp.get('success'):
                    response = resp
                else:
                    response['error_details'] = resp.get('message')
                    response['success'] = False
                    response['error'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)
