from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^getReferralCodeForOwner', views.get_referral_code_for_owner, name='getReferralCodeForOwner'),
    url(r'^performReferral', views.perform_referral, name='performReferral'),
    url(r'^createReferralCodeForOwner', views.create_referral_code_for_owner, name='createReferralCodeForOwner'),
    url(r'^searchRefereeByCode$', views.search_referee_by_code, name='search_referee_by_code'),
    url(r'^searchRefereeByCodeResult$', views.search_referee_by_code_result, name='search_referee_by_code_result'),
    url(r'^searchOrderByPhone$', views.search_order_details_by_phone, name='search_order_details_by_phone'),
    url(r'^searchOrderByPhoneResult$', views.search_order_details_by_phone_result, name='search_order_details_by_phone_result'),
]