from django.db import models
from decimal import Decimal
import datetime
from django.utils import timezone
from .app_settings import ITEM_STATUS_CHOICES,\
        KHATA_TRANSACTION_TYPE, REMOTE_WALLET_TRANSACTION_TYPE


def upload_path_handler(instance, filename):
    return "inventory/{sp_code}/{file}".format(
        sp_code=instance.sp_code.sp_code, file=filename)


class RevenueModel(models.Model):
    name = models.CharField(max_length=25, unique=True)
    perc_cut = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2,
        default=0.00)
    flat_cut = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2,
        default=0.00)
    max_cut = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2,
        default=0.00)

    class Meta:
        app_label = 'lazypos'


class Seller(models.Model):
    sp_code = models.CharField(max_length=25, unique=True)
    name = models.CharField(max_length=25)
    city_code = models.CharField(max_length=25)
    address = models.CharField(max_length=55, null=True, blank=True)
    phone_number = models.CharField(max_length=55, unique=True)
    username = models.CharField(max_length=25, null=True, blank=True)
    password = models.CharField(max_length=25)
    synced = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    revenue_model = models.ForeignKey(RevenueModel, to_field='name', null=True, blank=True)
    immediate_flag = models.BooleanField(default=False)

    class Meta:
        app_label = 'lazypos'


class User(models.Model):
    user_code = models.CharField(max_length=25, unique=True)
    user_name = models.CharField(max_length=25)
    user_email = models.CharField(max_length=25, null=True, blank=True)
    phone_number = models.CharField(max_length=25, unique=True)
    user_address = models.CharField(max_length=55, null=True, blank=True)
    user_city = models.CharField(max_length=25, null=True, blank=True)
    created_at = models.DateTimeField(editable=False, null=True, blank=True)
    last_edited_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        self.last_edited_at = datetime.datetime.now()
        super(User, self).save(*args, **kwargs)
        self.user_code = 'USR' + str(self.id)
        super(User, self).save(*args, **kwargs)


class Khata(models.Model):
    user_code = models.ForeignKey(User, to_field='user_code')
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    khata = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2,
        default=Decimal('0.00'))
    created_at = models.DateTimeField(editable=False)
    last_edited_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        app_label = 'lazypos'
        unique_together = (("user_code"), ("sp_code"))

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        self.last_edited_at = datetime.datetime.now()
        super(Khata, self).save(*args, **kwargs)


class Order(models.Model):
    order_code = models.CharField(max_length=25)
    order_number = models.CharField(max_length=25, unique=True)
    user_code = models.ForeignKey(User, to_field='user_code')
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    order_amount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    order_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    delivery_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    discount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    taxes = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    tax_percentage = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    offline_order = models.IntegerField(default=1)
    items = models.TextField(null=True, blank=True)
    khata = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    khata_type = models.CharField(choices=KHATA_TRANSACTION_TYPE,
                                  max_length=3, null=True, blank=True)
    order_creation_time = models.CharField(max_length=25)
    order_post_time = models.DateTimeField()

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.order_post_time = datetime.datetime.now()
        super(Order, self).save(*args, **kwargs)


class Item(models.Model):
    item_code = models.CharField(max_length=25, unique=True)
    st_code = models.CharField(max_length=25)
    sc_code = models.CharField(max_length=25)
    item_name = models.CharField(max_length=55)
    item_type_code = models.CharField(
        max_length=25, default='1', null=True, blank=True)
    item_unit = models.CharField(max_length= 25, null=True, blank=True)
    item_quantity = models.IntegerField(null=True, blank=True)
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    item_barcode = models.CharField(max_length=25, null=True, blank=True)
    curation_needed = models.BooleanField(default=True)
    item_short_desc = models.CharField(max_length=25, null=True, blank=True)
    item_desc = models.CharField(max_length=25, null=True, blank=True)
    barcode_verified = models.BooleanField(default=False)
    synced = models.BooleanField(default=False)
    default_seller = models.CharField(
        max_length=25, null=True, blank=True, default='Lazylad')
    created_at = models.DateTimeField(editable=False, null=True, blank=True)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        super(Item, self).save(*args, **kwargs)


class SellerItems(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    item_code = models.ForeignKey(Item, to_field='item_code')
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    item_quantity = models.IntegerField(null=True, blank=True, default=1)
    status = models.CharField(
        choices=ITEM_STATUS_CHOICES, default='AVA', max_length=3)
    last_updated_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        app_label = 'lazypos'
        unique_together = (("item_code"), ("sp_code"))

    def save(self, *args, **kwargs):
        self.last_updated_at = datetime.datetime.now()
        super(SellerItems, self).save(*args, **kwargs)


class KhataLogs(models.Model):
    trans_type = models.CharField(
        choices=KHATA_TRANSACTION_TYPE, max_length=3)
    log_time = models.DateTimeField()
    user_code = models.ForeignKey(User, to_field='user_code')
    amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    reference = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.log_time = datetime.datetime.now() +\
                        datetime.timedelta(hours=5, minutes=30)
        super(KhataLogs, self).save(*args, **kwargs)


class OrderItems(models.Model):
    item_code = models.ForeignKey(Item, to_field='item_code')
    order_number = models.ForeignKey(Order, to_field='order_number')
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    quantity = models.IntegerField(default=1)

    class Meta:
        app_label = 'lazypos'


class RemoteWalletTransactions(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    wallet_name = models.CharField(max_length=25)
    log_time = models.DateTimeField()
    amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    trans_type = models.CharField(
        choices=REMOTE_WALLET_TRANSACTION_TYPE, max_length=3)
    reference = models.CharField(max_length=25, null=True, blank=True)
    remote_transaction_id = models.CharField(
        max_length=25, null=True, blank=True)
    reference_type = models.CharField(max_length=25, default='Order')

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.log_time = datetime.datetime.now()
        super(RemoteWalletTransactions, self).save(*args, **kwargs)


class OrderLogs(models.Model):
    created_at = models.DateTimeField(editable=False)
    request_json = models.TextField(null=True, blank=True)
    successful = models.BooleanField(default=0)
    response_json = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        super(OrderLogs, self).save(*args, **kwargs)


class SellerInventory(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    upload_time = models.DateTimeField(editable=False)
    inventory_image = models.ImageField(upload_to=upload_path_handler)
    image_processed = models.BooleanField(default=False)
    last_edited_at = models.DateTimeField()

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.upload_time = datetime.datetime.now()
        self.last_edited_at = datetime.datetime.now()
        super(SellerInventory, self).save(*args, **kwargs)


class RunningAccountWallet(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    unsettled_amount = models.DecimalField(
        max_digits=20, decimal_places=2, default=0.00)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.last_edited_at = timezone.now()
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountWallet, self).save(*args, **kwargs)


class SettleUpAccountWallet(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    settle_up_amount = models.DecimalField(max_digits=20, decimal_places=2)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.to_time = timezone.now()
        super(SettleUpAccountWallet, self).save(*args, **kwargs)


class SellerSMSLog(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    no_of_messages = models.IntegerField(default=0)
    time_of_messages = models.DateTimeField(editable=False)
    recipients = models.TextField()
    message = models.TextField()
    amount = models.DecimalField(
        max_digits=20, decimal_places=2, default=0.00)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.time_of_messages = timezone.now()
        super(SellerSMSLog, self).save(*args, **kwargs)


class RunningAccountSMS(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    unsettled_amount = models.DecimalField(
        max_digits=20, decimal_places=2, default=0.00)
    last_settled_at = models.DateTimeField(null=True, blank=True)
    last_edited_at = models.DateTimeField()
    no_of_orders_from_last_settlement = models.IntegerField(default=0)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.last_edited_at = timezone.now()
        self.no_of_orders_from_last_settlement += 1
        super(RunningAccountSMS, self).save(*args, **kwargs)


class SettleUpAccountSMS(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    settle_up_amount = models.DecimalField(max_digits=20, decimal_places=2)

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.to_time = timezone.now()
        super(SettleUpAccountSMS, self).save(*args, **kwargs)


class SellerDailyReport(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    report_date = models.DateField(editable=False)
    no_of_orders = models.IntegerField(default=0)
    cash_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    khata_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    online_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    total_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    no_of_new_customers = models.IntegerField(default=0)
    top_10_items_of_today = models.TextField()
    last_updated_at = models.DateField()

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.report_date = timezone.now().date()
        self.last_updated_at = timezone.now().date()
        super(SellerDailyReport, self).save(*args, **kwargs)


class SellerMonthlyReport(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    report_date = models.DateField(editable=False)
    month = models.IntegerField(editable=False)
    year = models.IntegerField(editable=False)
    no_of_orders = models.IntegerField(default=0)
    cash_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    khata_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    online_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    total_amount = models.DecimalField(
        default=0.00, max_digits=20, decimal_places=2)
    no_of_new_customers = models.IntegerField(default=0)
    top_10_items_of_today = models.TextField()
    last_updated_at = models.DateField()

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.report_date = (timezone.now()).date()
        self.last_updated_at = (timezone.now()).date()
        super(SellerMonthlyReport, self).save(*args, **kwargs)


class SellerPermissions(models.Model):
    sp_code = models.ForeignKey(Seller, to_field='sp_code')
    lazypos_wallet = models.BooleanField(default=False)
    lazylad_wallet = models.BooleanField(default=False)
    lazyprime = models.BooleanField(default=True)
    wallet_incentive_model = models.BooleanField(default=False)
    wallet_loan_model = models.BooleanField(default=False)
    sms = models.BooleanField(default=False)
    report = models.BooleanField(default=False)
    app_version = models.CharField(max_length=25)
    last_updated_at = models.DateField()

    class Meta:
        app_label = 'lazypos'

    def save(self, *args, **kwargs):
        self.last_updated_at = datetime.datetime.now()
        super(SellerPermissions, self).save(*args, **kwargs)
