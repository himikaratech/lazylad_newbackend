
ORDER_STATUS = (('Pending', 'Pending'),
                ('Confirmed', 'Confirmed'),
                ('Cancelled', 'Cancelled'),
                ('Delivered', 'Delivered'))


COUPON_TYPES = (('FLAT', 'Flat Type'),
                ('PERCENTAGE', 'Percentage Type'))
