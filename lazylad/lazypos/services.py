
import requests
import urllib
import json
from .api_settings import lazylad_old_server_api,\
    lazylad_new_server_api, headers


def post_online_order_to_lazylad(post_dict):
    try:
        res = requests.post(lazylad_old_server_api +
                            '/postOrderToServerFinal_v4',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return {'error': 1,
                'message': 'Order Successfully placed',
                'order_code': json.loads(res.text).get('order_code')}
    else:
        return {'error': 0, 'message': 'Order/s Not Placed'}


def use_lazywallet(post_dict):
    try:
        res = requests.post(lazylad_new_server_api +
                            '/account/makeTransaction',
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'success': False, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return {'success': 1,
                'message': ' Successful Transaction',
                'transaction_id': json.loads(res.text).get('transaction_id')}
    else:
        return {'success': False, 'message': 'Order/s Not Placed'}
