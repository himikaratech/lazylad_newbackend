from .utils import *
import LazyladConnection
import math
import itertools

EARTH_RADIUS_IN_M = 6371000


# This Should Be A Model Class Instead
class GeoFence(object) :
    TYPE_CIRCULAR = 0
    TYPE_POLYGON = 1

    def __init__(self, type, polygon=list(), centre=tuple(), radius=0.0):
        self.type = type
        self.polygon = polygon
        self.centre = centre
        self.radius = radius

    def inside(self, point):
        if self.TYPE_CIRCULAR is self.type :
            return self.great_circle_distance(self.centre, point) <= self.radius * 1000
        elif self.TYPE_POLYGON is self.type :
            return self.point_inside_polygon(point[0], point[1], self.polygon)
        else :
            return False

    # determine if a point is inside a given polygon or not
    # Polygon is a list of (x,y) pairs.
    @staticmethod
    def point_inside_polygon(x, y,poly):

        n = len(poly)
        inside =False

        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y

        return inside

    @staticmethod
    def great_circle_distance(latlong_a, latlong_b):
        """
        >>> coord_pairs = [
        ...     # between eighth and 31st and eighth and 30th
        ...     [(40.750307,-73.994819), (40.749641,-73.99527)],
        ...     # sanfran to NYC ~2568 miles
        ...     [(37.784750,-122.421180), (40.714585,-74.007202)],
        ...     # about 10 feet apart
        ...     [(40.714732,-74.008091), (40.714753,-74.008074)],
        ...     # inches apart
        ...     [(40.754850,-73.975560), (40.754851,-73.975561)],
        ... ]

        >>> for pair in coord_pairs:
        ...     great_circle_distance(pair[0], pair[1]) # doctest: +ELLIPSIS
        83.325362855055...
        4133342.6554530...
        2.7426970360283...
        0.1396525521278...
        """
        lat1, lon1 = latlong_a
        lat2, lon2 = latlong_b

        dLat = math.radians(lat2 - lat1)
        dLon = math.radians(lon2 - lon1)
        a = (math.sin(dLat / 2) * math.sin(dLat / 2) +
                math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
                math.sin(dLon / 2) * math.sin(dLon / 2))
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = EARTH_RADIUS_IN_M * c

        return d

class GroupedGeoFence(object):
    WHITE_GEOFENCES = "white_geofences"
    BLACK_GEOFENCES = "black_geofences"

    def __init__(self, geofence_data= dict()):
        self.geofence_data = geofence_data

    def add(self, key, white_geofences, black_geofences = list()):
        if key in self.geofence_data:
            self.geofence_data[key][self.WHITE_GEOFENCES].extend(white_geofences)
        else:
            self.geofence_data[key] = {self.WHITE_GEOFENCES : white_geofences, self.BLACK_GEOFENCES : black_geofences}

    def getValidKeys(self, point):
        valid_keys = list()
        for key, geofences_dict in self.geofence_data.items():
            point_in_key = False

            white_geofences = geofences_dict[self.WHITE_GEOFENCES]
            black_geofences = geofences_dict[self.BLACK_GEOFENCES]

            for geofence in white_geofences:
                if isinstance(geofence, GeoFence) and geofence.inside(point):
                    point_in_key = True

            if point_in_key:
                valid_keys.append(key)

        return valid_keys


def getSellersDeliveryInfo(city_code, service_category):
    connection = LazyladConnection.LazyladConnection()
    seller_delivery_data = connection.get_sellers_delivery_details(city_code=city_code, st_code=service_category)
    return seller_delivery_data

def getLazyladSellersList(city_code, service_category):
    connection = LazyladConnection.LazyladConnection()
    seller_list = connection.get_lazylad_sellers_details(city_code=city_code, st_code=service_category)
    return seller_list

def get_sellers_geofencedict(seller_delivery_data):

    geofence_dict = dict()
    for sp_code, lat, long, radius in seller_delivery_data:

        radial_geofence = GeoFence(type=GeoFence.TYPE_CIRCULAR, centre=(lat,long), radius=radius)
        white_geofences = list()
        white_geofences.append(radial_geofence)

        geofence_dict[sp_code] = {GroupedGeoFence.WHITE_GEOFENCES : white_geofences, GroupedGeoFence.BLACK_GEOFENCES : list()}
    return geofence_dict

def prepareGroupedGeoFenceforSellers(seller_delivery_data):
    geofence_dict = get_sellers_geofencedict(seller_delivery_data)
    return GroupedGeoFence(geofence_data=geofence_dict)

