from django.db import connections


class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def get_sellers_delivery_details(self, city_code, st_code):
        query = "select sp_code, sp_latitude, sp_longitude, delivery_radius from service_providers where sp_service_type_code = %s " \
                " and sp_city_code = %s and lazylad_store = 0 and (sp_latitude is not null and sp_longitude is not null) "
        self.cursor.execute(query, [st_code, city_code])
        seller_delivery_data = self.cursor.fetchall()
        return seller_delivery_data

    def get_lazylad_sellers_details(self, city_code, st_code):
        query = "select sp_code from service_providers where sp_service_type_code = %s " \
                " and sp_city_code = %s and lazylad_store = 1 "
        self.cursor.execute(query, [st_code, city_code])
        sellers_list = self.cursor.fetchall()
        return [seller[0] for seller in sellers_list]

    def updateUserDeliveryLocation(self, user_code, lat, long, city_code):
        query = "update user_details set user_delivery_lat = %s, user_delivery_long = %s, user_delivery_city = %s \
                where user_code = %s "
        self.cursor.execute(query, [lat, long, city_code, user_code])

    def updateSellerShopCordinates(self, sp_code, lat, longi, city_code):
        query = "update service_providers set sp_latitude = %s, sp_longitude = %s, sp_city_code = %s  \
                where sp_code = %s "
        self.cursor.execute(query, [lat, longi, city_code, sp_code])

    def getSellerShopLocation(self, sp_code):
        query = "select sp_latitude, sp_longitude, sp_city_code from service_providers where sp_code = %s "
        self.cursor.execute(query, [sp_code])
        location = self.cursor.fetchone()
        if location:
            location_dict = {\
                                "lat":location[0],\
                                "long":location[1],\
                                "city_code":location[2]\
                            }
            shop_location = {"shop_location": location_dict}

        else:
            shop_location = None

        return shop_location

