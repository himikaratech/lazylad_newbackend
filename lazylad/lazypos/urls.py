from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import POS, Services

api_urlpatterns = patterns(
    'lazypos.apiviews',
    url(
        r'^fetch_items',
        POS.as_view({'get': 'fetch_items'})),
    url(
        r'^send_orders',
        POS.as_view({'post': 'send_orders'})),
    url(
        r'^update_inventory',
        POS.as_view({'post': 'update_inventory'})),
    url(
        r'^add_users',
        POS.as_view({'post': 'add_users'})),
    url(
        r'^edit_users',
        POS.as_view({'post': 'edit_users'})),
    url(
        r'^use_khata',
        POS.as_view({'post': 'use_khata'})),
    url(
        r'^fetch_khata',
        POS.as_view({'get': 'fetch_khata'})),
    url(
        r'^fetch_all_khate',
        POS.as_view({'get': 'fetch_all_khate'})),
    url(
        r'^add_seller',
        POS.as_view({'post': 'add_seller'})),
    url(
        r'^edit_seller',
        POS.as_view({'post': 'edit_seller'})),
    url(
        r'^user_orders',
        POS.as_view({'get': 'user_orders'})),
    url(
        r'^sync_items',
        POS.as_view({'get': 'sync_items'})),
    url(
        r'^initial_sync_items',
        POS.as_view({'get': 'initial_sync_items'})),
    url(
        r'^initial_sync_seller',
        POS.as_view({'get': 'initial_sync_seller'})),
    url(
        r'^seller_orders',
        POS.as_view({'post': 'seller_orders'})),
    url(
        r'^seller_login',
        POS.as_view({'post': 'seller_login'})),
    url(
        r'^fetch_seller_inventory',
        POS.as_view({'post': 'fetch_seller_inventory'})),
    url(
        r'^seller_populate_items',
        POS.as_view({'post': 'seller_populate_items'})),
    url(
        r'^pos_config',
        POS.as_view({'post': 'pos_config'})),
    url(
        r'^inventory_image',
        POS.as_view({'post': 'inventory_image'})),
    url(
        r'^seller_sms',
        POS.as_view({'post': 'seller_sms'})),
    url(
        r'^seller_daily_report',
        POS.as_view({'post': 'seller_daily_report'})),
    url(
        r'^seller_monthly_report',
        POS.as_view({'post': 'seller_monthly_report'})),
    url(
        r'^seller_report',
        POS.as_view({'post': 'seller_report'})),
    url(
        r'^seller_unsettled_amount',
        POS.as_view({'get': 'seller_unsettled_amount'})),
    url(
        r'^email_bill',
        Services.as_view({'get': 'email_bill'})),
    url(
        r'^generate_orders',
        Services.as_view({'get': 'generate_orders'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)
