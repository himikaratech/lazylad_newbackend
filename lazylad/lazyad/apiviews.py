
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from django.core.exceptions import ValidationError
from .LazyadConnections import Lazyad, Lazylad
from .serializers import *


class Customer(ViewSet):

    def get_all_coupon_codes(self, request):
        """
        API To Fetch All Available Coupons Of A User

        :param request:
        user_code (req)
        area_code
        city_code

        :return:
        coupons :[
            {
            coupon_code
            name
            img_flag
            img_address
            short_desc
            },
            {
            coupon_code
            name
            img_flag
            img_address
            short_desc
            },
        success
        error_code
        ]

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GetAllCouponsSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.get_all_coupons(
                    data.get('user_code'), data.get('area_code'),
                    data.get('city_code'))
                if resp.get('success'):
                    response['coupons'] = resp.get('coupons')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_cities(self, request):
        """
        API To Fetch All Cities

        :param request:
        user_code

        :return:
        cities :[
            {
            id
            name
            country
            },
            {
            id
            name
            country
            },
        ]
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = UserSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.get_cities()
                if resp.get('success'):
                    response['cities'] = resp.get('cities')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def coupons_clicks(self, request):
        """
        API To Track Coupon Clicks

        :param request:
        log_list : [
            {
                coupon_id
                user_code
                counter
            },
        ]

        :return:
        success
        error_code


        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SyncSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.coupon_click_log(
                    data.get('log_list'))
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def coupons_views(self, request):
        """
        API To Track Coupon Views

        :param request:
        log_list : [
            {
                coupon_id
                user_code
                counter
            },
        ]

        :return:
        success
        error_code


        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SyncSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.coupon_view_log(
                    data.get('log_list'))
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def user_signup(self, request):
        """
        API To Add New User

        :param request:
        username
        password
        phone_number (reqd)

        :return:
        user_code
        number_verified
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = UserSignUpSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.user_signup(
                    data.get('username'), data.get('password'), data.get('phone_number'))
                if resp.get('success'):
                    response['user_code'] = resp.get('user_code')
                    response['number_verified'] = resp.get('number_verified')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def user_signin(self, request):
        """
        API For User Signin

        :param request:
        username
        password
        phone_number (reqd)

        :return:
        user_code
        number_verified
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = UserSignInSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.user_signin(
                    data.get('phone_number'))
                if resp.get('success'):
                    response['user_code'] = resp.get('user_code')
                    response['number_verified'] = resp.get('number_verified')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def user_login(self, request):
        """
        API For User Login  POST

        :param request:
        username
        password
        phone_number (reqd)

        :return:
        user_code
        number_verified
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = UserSignUpSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.get_or_create_user(
                    data.get('phone_number'),
                    data.get('username'),
                    data.get('password'))
                if resp.get('success'):
                    response['user_code'] = resp.get('user_code')
                    response['number_verified'] = resp.get('number_verified')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_coupon_details(self, request):
        """
        API To Fetch Coupon Details

        :param request:
        coupon_id

        :return:
        coupon_details : {}
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = CouponDetailsSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.get_coupon_details(
                    data.get('coupon_id'))
                if resp.get('success'):
                    response['coupon_details'] = resp.get('coupon_details')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_coupon_code_details(self, request):
        """
        API To Fetch Coupon Code Details

        :param request:
        coupon_code

        :return:
        coupon_details : {}
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = CouponCodeDetailsSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.get_coupon_code_details(
                    data.get('coupon_code'))
                if resp.get('success'):
                    response['coupon_code_details'] = resp.get(
                        'coupon_code_details')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def buy_coupon(self, request):
        """
        API To Buy Coupons

        :param request:
        coupon_code  reqd
        user_code    reqd
        city_id
        area_code

        :return:
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = BuyCouponsSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.buy_coupon(
                    data.get('coupon_id'), data.get('user_code'),
                    data.get('city_id'), data.get('area_code'))
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
                    response['coupon_code'] = resp.get('coupon_code')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def get_coupons(self, request):
        """
        API To Get All Available Coupons

        :param request:
        user_code    reqd

        :return:
        success
        error_code

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GetAllCouponsSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.get_coupons(
                    data.get('user_code'), data.get('city_code'))
                if resp.get('success'):
                    response['coupons'] = resp.get('coupons')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)


class Merchant(ViewSet):

    def redeem_coupon(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RedeemCouponSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazyad()
        try:
            if _serlz.is_valid():
                resp = connection.redeem_coupon(
                    data.get('coupon_code'), data.get('merchant_code'))
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def sign_in_merchant(self, request):
        """
        API TO SIGN IN MERCHANT
        :param request:
        {"phone_number":"9999999999", "password":"**********"}
        :return:
        {"success":True,
        "merchant_name":"xxxxxx",
        "merchant_authenticated":True,
        "merchant_exist":True}
        """
        response = dict()
        connection = Lazyad()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = MerchantSignIn(data=data)
            if _serlz.is_valid():
                response = connection.sign_in_merchant(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def merchant_coupon_verification(self, request):
        """
        API TO VERIFY MERCHANT COUPONS
        :param request:
        {"coupon_code":"abc"
        "merchant_code":"abc"}
        :return:
        {"success":True, "coupon_valid":True}
        """
        response = dict()
        connection = Lazyad()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = MerchantCouponVerification(data=data)
            if _serlz.is_valid():
                response = connection.merchant_coupon_verification(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def merchant_coupons(self, request):
        """
        API TO FETCH ALL MERCHANT COUPONS
        :param request:
        {"merchant_code":"88888"}
        :return:
        {"success":True,"coupons":[]}
        """
        response = dict()
        connection = Lazyad()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = MerchantCoupons(data=data)
            if _serlz.is_valid():
                response = connection.merchant_coupons(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def merchant_coupon_details(self, request):
        """
        API TO FETCH MERCHANT COUPON DETAILS
        :param request:
        {"merchant_code":"8888","coupon_code":"xxxxx"}
        :return:
        {"success":True,"coupon_details": {} }
        """
        response = dict()
        connection = Lazyad()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = MerchantCouponDetails(data=data)
            if _serlz.is_valid():
                response = connection.merchant_coupon_details(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def merchant_coupon_analytics(self, request):
        """
        API TO SEND COUPON ANALYTICS
        :param request:
        {"merchant_code":"8888","coupon_name":"xyz"}
        :return:
        {"success":True,"coupon_analytics": {} }
        """
        response = dict()
        connection = Lazyad()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = MerchantCouponAnalytics(data=data)
            if _serlz.is_valid():
                response = connection.merchant_coupon_analytics(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def lazylad_coupon_analytics(self, request):
        """
        API TO SEND COUPON ANALYTICS
        :param request:
        {"merchant_code":"8888","coupon_name":"xyz"}
        :return:
        {"success":True,"coupon_analytics": {} }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = MerchantCouponAnalytics(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazylad()
        try:
            if _serlz.is_valid():
                resp = connection.get_coupon_analytcs(
                    data.get('merchant_code'), data.get('coupon_name'))
                if resp.get('success'):
                    response['coupon_analytics'] = resp.get('coupon_analytics')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def lazylad_item_coupon_analytics(self, request):
        """
        API TO SEND COUPON ANALYTICS
        :param request:
        {"merchant_code":"8888","coupon_name":"xyz"}
        :return:
        {"success":True,"coupon_analytics": {} }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = MerchantCouponAnalytics(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = Lazylad()
        try:
            if _serlz.is_valid():
                resp = connection.lazylad_item_coupon_analytics(
                    data.get('merchant_code'), data.get('coupon_name'))
                if resp.get('success'):
                    response['coupon_analytics'] = resp.get('coupon_analytics')
                    response['message'] = resp.get('message')
                    response['error_code'] = 1
                    response['success'] = True
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)
