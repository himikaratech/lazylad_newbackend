
from rest_framework import fields, serializers
from .app_settings import URID_TYPE_CHOICES,\
    TRANSACTION_TYPE_CHOICES, OWNER_TYPE_CHOICES


class CashBackSerializer(serializers.Serializer):
    amount = serializers.FloatField(required=True)
    owner_type = serializers.ChoiceField(
        required=True, choices=OWNER_TYPE_CHOICES)
    user_code = serializers.CharField(required=True)
    urid = serializers.CharField(required=True)
    urid_type = serializers.ChoiceField(
        required=True, choices=URID_TYPE_CHOICES)
    t_type = serializers.ChoiceField(
        required=True, choices=TRANSACTION_TYPE_CHOICES)
