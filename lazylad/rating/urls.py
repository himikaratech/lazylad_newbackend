from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import Rating

api_urlpatterns = patterns(
    'rating.apiviews',
    url(
        r'^saverating',
        Rating.as_view({'post': 'save_merchant_rating'}),
        name='save_merchant_rating'),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)