
from django.db import connections
import datetime
import json
import csv

from decimal import Decimal
from datetime import date
from random import randint
from django.db.models import Sum, Count

from django.core.exceptions import ValidationError, MultipleObjectsReturned
from django.db import IntegrityError
from decimal import Decimal

from .app_settings import SMS_CHARGE

from .models import User, Khata, Item, Seller, SellerItems, KhataLogs,\
    Order, OrderItems, RevenueModel, RemoteWalletTransactions, \
    SellerInventory, OrderLogs, RunningAccountWallet, SettleUpAccountWallet,\
    SellerSMSLog, SellerDailyReport, RunningAccountSMS, SettleUpAccountSMS,\
    SellerMonthlyReport, SellerPermissions

from .utils import post_order_items_to_server, generate_item_code,\
    generate_barcode, generate_order_code, calculate_revenue, \
    print_exception, email_customer

from .services import use_lazywallet

from .app_settings import LAZYPOS_CITY_MAPPING

from oms.services import send_sms


def dictfetchall(cursor):
    " Return all rows from a cursor as a dict "
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def fetch_items(self, st_code):
        response = {
            'success': False,
            'items': None}
        try:
            query = 'select * from item_details where st_code='+st_code
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)
            response['items'] = data
            response['success'] = True
        except:
            pass
        return response

    def post_items(self, order_dict, sp_code):
        response = {
            'success': False,
            'message': None
        }
        try:
            query = 'insert into order_item_served_by_sp (order_code,' \
                    ' user_code, sp_code, item_code, item_cost, item_quantity)' \
                    ' values ( %d, %s, %d, %d, %d , %d)'
            self.cursor.execute(query, [order_dict.get('order_code'),
                                        'local_user',
                                        sp_code,
                                        order_dict.get('item_code'),
                                        order_dict.get('item_cost'),
                                        order_dict.get('item_quantity')
                                        ])
            response['success'] = True
        except:
            response['message'] = 'Error In Insertion'
        return response

    def post_order(self, order_dict, sp_code):
        response = {
            'success': False,
            'message': None
        }
        try:
            query = 'insert into order_item_served_by_sp (order_code,' \
                    ' user_code, sp_code, item_code, item_cost, item_quantity)' \
                    ' values ( %d, %s, %d, %d, %d , %d)'
            self.cursor.execute(query, [order_dict.get('order_code'),
                                        'local_user',
                                        sp_code,
                                        order_dict.get('item_code'),
                                        order_dict.get('item_cost'),
                                        order_dict.get('item_quantity')
                                        ])
            response['success'] = True
        except:
            response['message'] = 'Error In Insertion'
        return response

    def generate_item_code(self, item, sp_code):
        response = {
            'success': False,
            'message': None
        }
        try:
            query1 = 'select item_code from' \
                    '  "items_offered_by_service_provider"' \
                    ' order by id desc limit 1 where sp_code = %s'
            self.cursor.execute(query1, [sp_code])
            data = self.cursor.fetchone()
            item_code = int(data[0]) + 1
            query2 = 'insert into items_offered_by_service_provider ' \
                     '(item_code, sp_code, item_type_code, item_name,' \
                     ' item_unit, item_quantity, item_cost, item_img_flag,' \
                     ' item_img_add, item_short_desc, item_desc, item_status,' \
                     ' sc_code) values (%d, %d, %d, %s, %d, %d, %d, %d, %s,' \
                     ' %s, %s, %d, %d)'
            self.cursor.execute(query2, [
                item.get('item_code'),
                sp_code,
                item.get('item_type_code'),
                item.get('item_name'),
                item.get('item_unit'),
                item.get('item_quantity'),
                item.get('item_cost'),
                item.get('item_img_flag'),
                item.get('item_img_add'),
                item.get('item_short_desc'),
                item.get('item_desc'),
                item.get('item_status'),
                item.get('sc_code'),
            ])
            query3 = 'insert into item_details (item_code, st_code,' \
                     ' item_name, item_type_code, item_unit, item_quantity,' \
                     ' item_cost, item_img_flag, item_img_add, item_short_desc,' \
                     ' item_desc, availability_class) values (%d, %d, %s, %d,' \
                     ' %d, %d, %d, %d, %s, %s, %s, %d)'
            self.cursor.execute(query3, [
                item.get('item_code'),
                item.get('st_code'),
                item.get('item_name'),
                item.get('item_type_code'),
                item.get('item_unit'),
                item.get('item_quantity'),
                item.get('item_cost'),
                item.get('item_img_flag'),
                item.get('item_img_add'),
                item.get('item_short_desc'),
                item.get('item_desc'),
                item.get('availability_class'),
            ])
            response['item_code'] = item_code
            response['success'] = True
        except:
            response['message'] = 'Error In Insertion'
        return response

    def generate_barcode(self, item):
        response = {
            'success': False,
            'message': None
        }
        try:
            query = 'select barcode_value_integer from barcode_config where' \
                    ' barcode_field = "BARCODE_COUNT"'
            self.cursor.execute(query)
            data = self.cursor.fetchone()
            query = 'update barcode_config set barcode_value_integer = ' \
                    'barcode_value_integer + 1 where barcode_field = ' \
                    '"BARCODE_COUNT"'
            self.cursor.execute(query)
            query2 = 'update item_details set barcode = %d where item_code = %s'
            self.cursor.execute(query2,[
                int(data[0]) + 1,
                item.get('item_code')
            ])
            response['barcode'] = int(data[0]) + 1
            response['success'] = True
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    # def add_user(self, user, sp_code):
    #     response = {
    #         'success': False,
    #         'message': None
    #     }
    #     try:
    #         query = 'insert into user_details (user_code, phone_number,' \
    #                 ' created) values %s, %s, %s'
    #         self.cursor.execute(query, [user.get('user_code'),
    #                                     user.get('phone'),
    #                                     datetime.datetime.now()])
    #         query = 'insert into user_address_details (user_code, user_flat,' \
    #                 ' user_subarea, user_area, user_city, user_name,' \
    #                 ' user_email_id, user_phone_number,' \
    #                 ' sp_code) values %d, %s, %s, %s, %d, %s, %s, %s, %s'
    #         self.cursor.execute(query, [user.get('user_code'),
    #                                     user.get('flat'),
    #                                     user.get('subarea'),
    #                                     user.get('area'),
    #                                     user.get('city'),
    #                                     user.get('name'),
    #                                     user.get('email'),
    #                                     user.get('phone'),
    #                                     sp_code])
    #     except:
    #         response['message'] = 'Error In Database Transaction'
    #     return response


    # def edit_user(self, user, sp_code):
    #     response = {
    #         'success': False,
    #         'message': None
    #     }
    #     try:
    #         query = 'insert into user_details (user_code, phone_number,' \
    #                 ' created) values %s, %s, %s'
    #         self.cursor.execute(query, [user.get('user_code'),
    #                                     user.get('phone'),
    #                                     datetime.datetime.now()])
    #         query = 'insert into user_address_details (user_code, user_flat,' \
    #                 ' user_subarea, user_area, user_city, user_name,' \
    #                 ' user_email_id, user_phone_number,' \
    #                 ' sp_code) values %d, %s, %s, %s, %d, %s, %s, %s, %s'
    #         self.cursor.execute(query, [user.get('user_code'),
    #                                     user.get('flat'),
    #                                     user.get('subarea'),
    #                                     user.get('area'),
    #                                     user.get('city'),
    #                                     user.get('name'),
    #                                     user.get('email'),
    #                                     user.get('phone'),
    #                                     sp_code])
    #     except:
    #         response['message'] = 'Error In Database Transaction'
    #     return response

    def initial_sync_items(self):
        """
        Function To Sync Lazypos DB with Lazylad DB
        All Items From Lazylad Will Be Updated in Lazypos
        """
        response = {
            'success': False,
            'message': None
        }
        try:

            query = 'select * from item_details order by id'
            self.cursor.execute(query)
            item_list = dictfetchall(self.cursor)
            for item_dict in item_list:
                try:
                    item_saved = Item.objects.get(
                        item_code=item_dict.get('item_code'))
                except Item.DoesNotExist:
                    try:
                        item = Item(
                            item_code=item_dict.get('item_code'),
                            st_code=item_dict.get('st_code'),
                            sc_code=item_dict.get('sc_code'),
                            item_name=item_dict.get('item_name'),
                            item_type_code=item_dict.get('item_type_code'),
                            item_unit=1,
                            item_quantity=1,
                            item_cost=Decimal(item_dict.get('item_cost')),
                            item_barcode=item_dict.get('item_barcode'),
                            curation_needed=False,
                            item_short_desc=item_dict.get('item_short_desc'),
                            item_desc=item_dict.get('item_desc'),
                            synced=True,
                        )
                        if item_dict.get('barcode'):
                            item.barcode_verified = True
                        item.save()
                    except ValidationError:
                        pass
                    except IntegrityError:
                        pass
            response['success'] = True
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def sync_items(self):
        response = {
            'success': False,
            'message': None
        }
        try:
            items_list = Item.objects.filter(synced=False).values()
            query = 'insert into item_details (item_code, st_code, item_name,' \
                    ' item_type_code, item_unit, item_quantity, item_cost,' \
                    ' item_img_flag, item_img_add, item_short_desc,' \
                    ' item_desc, sc_code) values ('
            for item_dict in items_list:
                query += item_dict.get('item_code', '') + ', ' + \
                         item_dict.get('st_code', '') + ', ' + \
                         item_dict.get('item_name', '') + ', ' + \
                         item_dict.get('item_type_code', '') + ', ' + \
                         item_dict.get('item_unit', '') + ', ' + \
                         item_dict.get('item_quantity', '') + ', ' + \
                         item_dict.get('item_cost', '') + ', ' + \
                         item_dict.get('item_img_flag', '') + ', ' + \
                         item_dict.get('item_img_add', '') + ', ' + \
                         item_dict.get('item_short_desc', '') + ', ' + \
                         item_dict.get('item_desc', '') + ', ' + \
                         item_dict.get('sc_code', '') + ');'
                self.cursor.execute(query)
            response['success'] = True
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def initial_sync_sellers(self):
        """
        Function To Sync Lazypos DB with Lazylad DB
        All Sellers From Lazylad Will Be Updated in Lazypos
        """
        response = {
            'success': False,
            'message': None
        }
        try:
            query = 'select * from service_providers order by id'
            self.cursor.execute(query)
            seller_list = dictfetchall(self.cursor)
            for seller_dict in seller_list:
                try:
                    seller_saved = Seller.objects.get(
                        sp_code=seller_dict.get('sp_code')
                    )
                except Seller.DoesNotExist:
                    try:
                        seller = Seller(
                            sp_code=seller_dict.get('sp_code'),
                            name=seller_dict.get('sp_name'),
                            city_code=seller_dict.get('sp_city_code'),
                            address=seller_dict.get('shop_address'),
                            phone_number=seller_dict.get('sp_number'),
                            username=seller_dict.get('sp_email'),
                            password=seller_dict.get('sp_password'),
                            synced=True,
                        )
                        seller.save()
                    except ValidationError:
                        pass
                    except IntegrityError:
                        pass
            response['success'] = True
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def update_items(self, sp_code, lazy_update_list):
        response = {
            'success': False,
            'message': None
        }
        try:
            for item in lazy_update_list:
                query = 'update items_offered_by_service_providers set ' \
                        'item_cost = ' + item[0] + ', item_status =' + \
                        item[1] + ' where item_code = ' + item[2] + \
                        ' and sp_code = ' + sp_code + ';'
                self.cursor.execute(query)
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def lazylad_wallet(self, sp_code, amount, order_code):
        response = {
            'success': False,
            'message': None
        }
        post_dict = dict()
        try:
            post_dict.update({
                't_type': 'LAZYPOS_CHARGE',
                'owner_code': sp_code,
                'owner_type': '1',
                'CorD': '1' if amount > 0 else '0',
                'amount': str(amount),
                'reference': str(order_code),
            })
            resp = use_lazywallet(post_dict)
            if resp.get('success'):
                response['success'] = True
                response['message'] = resp.get('message')
                response['transaction_id'] = resp.get('transaction_id')
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def lazylad_wallet_sms(self, sp_code, amount, order_code):
        response = {
            'success': False,
            'message': None
        }
        post_dict = dict()
        try:
            post_dict.update({
                't_type': 'LAZYPOS_SMS_CHARGE',
                'owner_code': sp_code,
                'owner_type': '1',
                'CorD': '1' if amount > 0 else '0',
                'amount': str(amount),
                'reference': str(order_code),
            })
            resp = use_lazywallet(post_dict)
            if resp.get('success'):
                response['success'] = True
                response['message'] = resp.get('message')
                response['transaction_id'] = resp.get('transaction_id')
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def lazylad_wallet_redeem(self, sp_code, amount, order_code):
        response = {
            'success': False,
            'message': None
        }
        post_dict = dict()
        try:
            post_dict.update({
                't_type': 'LAZYPOS_REDEEM',
                'owner_code': sp_code,
                'owner_type': '1',
                'CorD': '1',
                'amount': str(amount),
                'reference': order_code,
            })
            resp = use_lazywallet(post_dict)
            if resp.get('success'):
                response['success'] = True
                response['message'] = resp.get('message')
                response['transaction_id'] = resp.get('transaction_id')
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def populate_seller_items(self, sp_code):
        response = {
            'success': False,
            'message': None
        }
        try:
            query = 'select item_code, item_cost, item_status from ' \
                    'items_offered_by_service_provider' \
                    ' where sp_code = ' + sp_code
            self.cursor.execute(query)
            item_list = dictfetchall(self.cursor)
            for item_dict in item_list:
                try:
                    seller = Seller.objects.get(sp_code=sp_code)
                    item_present = Item.objects.get(
                        item_code=item_dict.get('item_code'))
                    seller_item_present = SellerItems.objects.get(
                        sp_code=seller, item_code=item_present)
                except SellerItems.DoesNotExist:
                    seller_item = SellerItems(
                        sp_code=seller,
                        item_code=item_present,
                        item_cost=Decimal(item_dict.get('item_cost')),
                        status='AVA' if item_dict.get('item_status') == 1 else 'OUT',
                    )
                    seller_item.save()
                except Seller.DoesNotExist:
                    response['message'] = 'Seller Does Not Exists'
                    return response
                except Item.DoesNotExist:
                    pass
                except (IntegrityError, ValidationError):
                    pass
            response['success'] = True
        except:
            response['message'] = 'Error In Database Transaction'
        return response

class LazyPOS:

    def __init__(self):
        pass

    def fetch_items(self, st_code, prev_item_code):
        response = {
            'success': False,
            'message': None
        }
        try:
            if prev_item_code:
                try:
                    prev_item_id = Item.objects.get(
                        item_code=prev_item_code).id
                    items_list = list(Item.objects.filter(
                        st_code=st_code,
                        curation_needed=False,
                        id__gt=prev_item_id).order_by('id')[:2000].values())
                except:
                    items_list = list(Item.objects.filter(
                        st_code=st_code,
                        curation_needed=False).order_by('id')[:2000].values())
            else:
                items_list = list(Item.objects.filter(
                    st_code=st_code,
                    curation_needed=False).order_by('id')[:2000].values())
            for item_dict in items_list:
                try:
                    item_dict['item_barcode'] = int(
                        item_dict.get('item_barcode'))

                except:
                    pass
            if prev_item_code == '0':
                response['item_total_count'] = Item.objects.filter(
                    curation_needed=False).count()
            response['items_list'] = items_list
            response['success'] = True
        except:
            pass
        return response

    def add_items(self, items_list):
        failed_list = []
        for item_dict in items_list:
            try:
                item = Item(
                    item_code=item_dict.get('item_code'),
                    st_code=item_dict.get('st_code'),
                    sc_code=item_dict.get('sc_code'),
                    item_name=item_dict.get('item_name'),
                    item_type_code=item_dict.get('item_type_code'),
                    item_unit=item_dict.get('item_unit'),
                    item_quantity=item_dict.get('item_quantity'),
                    item_cost=item_dict.get('item_cost'),
                    item_barcode=item_dict.get('item_barcode'),
                    curation_needed=True,
                    item_short_desc=item_dict.get('item_short_desc'),
                    item_desc=item_dict.get('item_desc')
                )
                item.save()
            except ValidationError:
                failed_list.append(item_dict.get('item_code'))
        return failed_list

    def add_new_item(self, item_dict, barcode_verified, sp_code):
        if barcode_verified == 1:
            items = Item.objects.filter(
                item_barcode=item_dict.get('item_barcode'),
                curation_needed=True)
            if items.count() > 0:
                return items[0].item_code, items[0].item_barcode

        item_code = generate_item_code()
        barcode = item_dict.get('item_barcode')\
            if barcode_verified == 1 else generate_barcode()
        try:
            item = Item(
                item_code=item_code,
                st_code=item_dict.get('st_code', '1'),
                sc_code=item_dict.get('sc_code', '1'),
                item_name=item_dict.get('item_name'),
                item_type_code=item_dict.get('item_type_code'),
                item_unit=item_dict.get('item_unit'),
                item_quantity=item_dict.get('item_quantity'),
                item_cost=item_dict.get('item_cost'),
                item_barcode=barcode,
                curation_needed=True,
                item_short_desc=item_dict.get('item_short_desc'),
                item_desc=item_dict.get('item_desc'),
                barcode_verified=True if barcode_verified == 1 else False,
                default_seller=sp_code if sp_code else 'Lazylad'
            )
            item.save()
            return item_code, barcode
        except ValidationError:
            return None, None

    def add_seller_items(self, sp_code, items_list):
        failed_list = []
        for item_dict in items_list:
            try:
                item = SellerItems(
                    item_code=item_dict.get('item_code'),
                    sp_code=sp_code,
                    item_cost=item_dict.get('item_cost'),
                )
                item.save()
            except ValidationError:
                failed_list.append(item_dict.get('item_code'))
        return failed_list

    def add_orders(self, orders_list):
        failed_list = []
        for order_dict in orders_list:
            try:
                post_order_items_to_server(
                    order_dict.get('order_code'),
                    order_dict.get('items_list'))
            except:
                failed_list.append(order_dict.get('order_code'))
        return failed_list

    def add_user(self, user_dict, sp_code):
        response = {
            'success': False,
            'message': None
        }
        user_present = None
        try:
            user_present = User.objects.get(
                user_name=user_dict.get('user_name').upper(),
                phone_number=user_dict.get('phone_number'))
            khata = Khata.objects.get(
                user_code=user_present,
                sp_code=Seller.objects.get(sp_code=sp_code))
            if user_present:
                response['message'] = 'User Already Exist'
        except Khata.DoesNotExist:
            try:
                khata = Khata(user_code=user_present.id,
                              sp_code=sp_code)
                khata.save()
            except ValidationError:
                response['message'] = 'Khata Details Incorrect'
            except IntegrityError:
                response['message'] = 'Khata Already Exist'
        except User.DoesNotExist:
            try:
                user = User(user_name=user_dict.get('user_name').upper(),
                            phone_number=user_dict.get('phone_number'),
                            user_address=user_dict.get('user_address'),
                            user_city=user_dict.get('user_city'))
                user.save()
                khata = Khata(user_code=user,
                              sp_code=Seller.objects.get(sp_code=sp_code))
                khata.save()
                response['success'] = True
            except ValidationError:
                response['message'] = 'User Details Incorrect'
            except IntegrityError:
                response['message'] = 'User Already Exist'
        return response

    def edit_user(self, user_dict, sp_code):
        response = {
            'success': False,
            'message': None
        }
        try:
            user = User.objects.get(
                user_name=user_dict.get('user_name').upper(),
                phone_number=user_dict.get('phone_number')
            )
            if user_dict.get('user_address'):
                user.user_address = user_dict.get('user_address')
            if user_dict.get('user_city'):
                user.user_city = user_dict.get('user_city')
            user.save()
            response['success'] = True
        except User.DoesNotExist:
            response['message'] = 'User Doesnot Exist'
        except ValidationError:
            response['message'] = 'Invalid Details'
        return response

    def khata_transaction(self, user, trans_type, amount, sp_code, reference=None):
        response = {
            'success': False,
            'message': None
        }
        try:
            khata = Khata.objects.get(user_code=user,
                                      sp_code=Seller.objects.get(
                                          sp_code=sp_code))
            if trans_type == 'CRE':
                khata.khata = khata.khata + Decimal(amount)
            if trans_type == 'DEB':
                khata.khata = khata.khata - Decimal(amount)
            khata.save()
            khatalog = KhataLogs(trans_type=trans_type,
                                 amount=amount,
                                 user_code=user,
                                 sp_code=Seller.objects.get(sp_code=sp_code),
                                 reference=reference)
            khatalog.save()
            response['khata_log'] = {
                'new_balance': khata.khata,
                'khata_trans_global_id': khatalog.id,
                'type': trans_type,
                'amount': amount}
            response['success'] = True
        except ValidationError:
            response['message'] = 'Invalid Details'
        return response

    def get_or_create_user(self, user_dict, sp_code):
        response = {
            'success': False,
            'message': None
        }
        user_present = None
        return_user = None
        try:
            user_present = User.objects.get(
                phone_number=user_dict.get('phone_number'))
            khata = Khata.objects.get(user_code=user_present,
                                      sp_code=sp_code)
            if user_present:
                response['message'] = 'User Already Exist'
                if not user_present.user_name == user_dict.get('user_name'):
                    user_present.user_name = user_dict.get('user_name')
                    user_present.save()
                return_user = user_present
                response['success'] = True
        except Khata.DoesNotExist:
            try:
                khata = Khata(user_code=user_present,
                              sp_code=Seller.objects.get(sp_code=sp_code))
                khata.save()
                return_user = user_present
                response['success'] = True
            except ValidationError:
                response['message'] = 'Khata Details Incorrect'
            except IntegrityError:
                response['message'] = 'Khata Already Exist'
        except User.DoesNotExist:
            try:
                user = User(user_name=user_dict.get('user_name').upper(),
                            phone_number=user_dict.get('phone_number'),
                            user_address=user_dict.get('user_address'),
                            user_city=user_dict.get('user_city'),
                            user_email=user_dict.get('user_email'))
                user.save()
                khata = Khata(user_code=user,
                              sp_code=Seller.objects.get(sp_code=sp_code))
                khata.save()
                response['success'] = True
                return_user = user
            except ValidationError:
                response['message'] = 'User Details Incorrect'
            except IntegrityError:
                response['message'] = 'User Already Exist'
        response['user'] = return_user
        return response

    def add_seller(self, sellers_list):
        failed_list = []
        for seller_dict in sellers_list:
            try:
                sp_code = 0
                seller = Seller(
                    sp_code=sp_code,
                    name=seller_dict.get('name'),
                    city_code=seller_dict.get('city_code'),
                    address=seller_dict.get('address'),
                    username=seller_dict.get('username'),
                    password=seller_dict.get('password')
                )
                seller.save()
            except (ValidationError, IntegrityError):
                failed_list.append(seller_dict.get('name'))
        return failed_list

    def khata(self, user, sp_code, trans_type, amount):
        response = {
            'success': False,
            'message': None
        }
        try:
            khata = Khata.objects.get(user_code=user.user_code,
                                      sp_code=sp_code)
            if trans_type == 'CRE':
                khata.amount += amount
            elif trans_type == 'DEB':
                khata.amount -= amount
            khata.save()
            response['success'] = True
            response['amount'] = khata.amount
        except:
            pass
        return response

    def all_khate(self, sp_code):
        response = {
            'success': False,
            'message': None,
            'khata_list': None
        }
        khata_list = list()
        try:
            khate = Khata.objects.filter(sp_code=sp_code)
            for khata in khate:
                user = User.objects.get(
                    id=khata.user_code.id)
                khata_list.append({
                    'user_name': user.user_name,
                    'phone_number': user.phone_number,
                    'balance': khata.khata,
                    'address': user.user_address
                })
            response['success'] = True
            response['khata_list'] = khata_list
        except:
            pass
        return response

    def get_khata(self, sp_code, user_dict):
        response = {
            'success': False,
            'message': None,
            'khata_logs': []
        }
        try:
            user = User.objects.get(
                user_name=user_dict.get('user_name'),
                phone_number=user_dict.get('phone_number')
            )
            user_code = user.user_code
            khata = Khata.objects.get(
                sp_code=sp_code,
                user_code=user_code
            )
            khata_logs = KhataLogs.objects.filter(
                user_code=user_code,
                sp_code=sp_code
            ).values()
            response['success'] = True
            response['balance'] = khata.amount
            response['khata_logs'] = khata_logs
            response['khata_id'] = khata.id
        except User.DoesNotExist:
            response['message'] = 'User Not Found In Database'
        except Khata.DoesNotExist:
            response['message'] = 'Given User Is Not A Customer Of Given Shop'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def all_orders(self, sp_code, prev_order_no=None):
        response = {
            'success': False,
            'message': None,
            'orders': None
        }
        prev_order_id = None
        try:
            if prev_order_no:
                try:
                    prev_order = Order.objects.get(order_number=prev_order_no)
                    prev_order_id = prev_order.id
                except Order.DoesNotExist:
                    pass
            if prev_order_id:
                orders = Order.objects.filter(
                    sp_code=sp_code, id__gte=prev_order_id).values()
            else:
                orders = Order.objects.filter(sp_code=sp_code).values()
            for order in orders:
                order['items'] = json.loads(order.get('items'))
            response['success'] = True
            response['orders'] = orders
        except:
            response['message'] = 'Unexpected Error'
        return response

    def seller_inventory(self, sp_code, prev_item_code=None):
        response = {
            'success': False,
            'message': None,
            'orders': None
        }
        prev_item_id = None
        try:
            if prev_item_code:
                prev_item = Item.objects.get(item_code=prev_item_code)
                prev_item_id = prev_item.id
            seller_items_list = SellerItems.objects.filter(
                sp_code=sp_code)
            item_code_dict = dict()
            for seller_items in seller_items_list:
                # item_code_dict[seller_items.item_code.item_code] = \
                #     seller_items.item_cost
                item_code_dict[seller_items.item_code.item_code] = {
                    'item_cost': seller_items.item_cost,
                    'item_quantity': seller_items.item_quantity,
                }
            if prev_item_id:
                inventory = Item.objects.filter(
                    item_code__in=item_code_dict.keys(),
                    id__gte=prev_item_id).values()
            else:
                inventory = Item.objects.filter(
                    item_code__in=item_code_dict.keys()).values()
            for item_dict in inventory:
                item_dict['item_cost'] = item_code_dict.get(
                    item_dict.get('item_code')).get('item_cost')
                quantity = item_code_dict.get(
                    item_dict.get('item_code')).get('item_quantity')
                item_dict['item_quantity'] = quantity if quantity > 0 else 0

                try:
                    item_dict['item_barcode'] = int(
                        item_dict.get('item_barcode'))
                except:
                    pass
            response['success'] = True
            response['inventory'] = inventory
        except:
            response['message'] = 'Unexpected Error'
        return response

    def send_orders_to_POS(self, orders_list, sp_code):
        connect = LazyladConnection()
        item_update_dict = list()
        unsuccessful_orders = list()
        successful_orders = list()
        lazy_update_list = list()
        khata_trans_list = list()
        order_item_code_list = list()
        for order_dict in orders_list:
            try:
                order_items_list = []
                new_order_code = generate_order_code()
                items_list = order_dict.get('items')
                seller = Seller.objects.get(sp_code=sp_code)
                for item in items_list:
                    if item.get('local_flag') == 1:
                        barcode_verified = item.get(
                            'barcode_verified')
                        item_code, item_barcode =\
                            self.add_new_item(
                                item, barcode_verified, seller.sp_code)
                        item_update_dict.append({
                                    'item_code_old': item.get('item_code'),
                                    'item_code': item_code,
                                    'barcode': item_barcode,
                                    'barcode_old': item.get('item_barcode'),
                                })
                        # seller_items = SellerItems(
                        #     sp_code=seller,
                        #     item_code=Item.objects.get(item_code=item_code),
                        #     item_cost=item.get('item_cost')
                        #     )
                        # seller_items.save()
                    else:
                        item_code = item.get('item_code')
                        item_barcode = item.get('barcode')
                        # try:
                        #     seller_items = SellerItems.objects.get(
                        #             sp_code=sp_code, item_code=item_code)
                        # except SellerItems.DoesNotExist:
                        #     seller_items = SellerItems(
                        #         sp_code=seller,
                        #         item_code=Item.objects.get(
                        # item_code=item_code),
                        #         item_cost=item.get('item_cost')
                        #     )
                        #     seller_items.save()
                        # seller_items.item_cost = item.get('item_cost')
                        # seller_items.status = 'AVA'
                        # seller_items.save()
                    order_item_code_list.append(
                        [item_code, item.get('item_cost')])
                    order_items_list.append({
                        'item_code': item_code,
                        'item_cost': item.get('item_cost'),
                        'item_quantity': item.get('item_quantity'),
                    })
                    lazy_update_list.append(
                            [item.get('item_cost'),
                             1 if item.get('status') == 'AVA' else 3,
                             item_code])
                # connect.update_items(sp_code, lazy_update_list)
                if order_dict.get('user').get('user_name') and\
                        order_dict.get('user').get('phone_number'):
                    user = self.get_or_create_user(
                        order_dict.get('user'), sp_code).get('user')
                else:
                    try:
                        user = User.objects.get(user_name='Not Registerd')
                    except User.DoesNotExist:
                        user = self.get_or_create_user({
                            'user_name': 'Not Registerd',
                            'phone_number': 'Not Registerd',
                            'user_address': 'Not Registerd',
                            'user_city': 'Not Registerd'},
                            sp_code).get('user')
                khata_amount = 0.00
                khata_type = None
                if order_dict.get('khata_amount') and user:
                    khata_resp = self.khata_transaction(
                        user,
                        order_dict.get('khata_type', 'CRE'),
                        order_dict.get('khata_amount'),
                        sp_code,
                        new_order_code,
                    )
                    if khata_resp.get('success'):
                        khata_resp.get('khata_log').update(
                                {'local_trans_id': order_dict.get(
                                    'local_trans_id')})
                        khata_trans_list.append(khata_resp.get('khata_log'))
                        khata_amount = khata_resp.get('khata_log').get('amount')
                        khata_type = khata_resp.get('khata_log').get('type')

                revenue_model = None
                try:
                    revenue_model = seller.revenue_model
                except AttributeError:
                    pass
                if not revenue_model:
                    revenue_model = RevenueModel.objects.first()

                revenue = calculate_revenue(
                    revenue_model, order_dict.get('order_cost'))
                if revenue:
                    remote_wallet_transaction = connect.lazylad_wallet(
                        seller.sp_code, revenue, new_order_code)
                    if remote_wallet_transaction.get('success'):
                        self.track_remote_transactions(
                            seller,
                            revenue,
                            'LazyWallet',
                            'DEB',
                            new_order_code,
                            remote_wallet_transaction.get('transaction_id'),
                        )
                        self.update_running_account(seller, revenue)
                order = Order(
                    order_number=new_order_code,
                    order_code=order_dict.get('order_code'),
                    user_code=user,
                    sp_code=seller,
                    order_amount=order_dict.get('order_amount'),
                    order_cost=order_dict.get('order_cost'),
                    delivery_cost=order_dict.get('delivery_cost'),
                    discount=order_dict.get('discount'),
                    taxes=order_dict.get('taxes'),
                    tax_percentage=order_dict.get('tax_percentage'),
                    offline_order=order_dict.get('offline_order'),
                    order_creation_time=order_dict.get('order_creation_time'),
                    khata=Decimal(khata_amount),
                    khata_type=khata_type,
                    items=json.dumps(order_items_list))
                order.save()
                for od_list in order_items_list:
                    try:
                        order_item = OrderItems(
                                item_code=Item.objects.get(item_code=od_list.get('item_code')),
                                order_number=order,
                                item_cost=Decimal(od_list.get('item_cost')),
                                quantity=int(od_list.get('quantity', 1))
                            )
                        order_item.save()
                    except:
                        pass
                self.update_seller_items(sp_code, order_items_list)
                successful_orders.append(
                    {
                        'new_order_code': new_order_code,
                        'old_order_code': order_dict.get('order_code'),
                    }
                )

            except:
                unsuccessful_orders.append(
                    order_dict.get('order_code'))
        return item_update_dict, unsuccessful_orders,\
               khata_trans_list, successful_orders

    def update_running_account(self, seller, revenue):
        response = {
            'success': False,
            'message': None,
        }
        try:
            running_account = RunningAccountWallet.objects.get(sp_code=seller)
            running_account.unsettled_amount += Decimal(revenue)
            running_account.save()
            response['success'] = True
        except RunningAccountWallet.DoesNotExist:
            running_account = RunningAccountWallet(
                sp_code=seller,
                unsettled_amount=Decimal(revenue)
            )
            running_account.save()
            response['success'] = True
        except:
            response['message'] = print_exception()
        return response

    def settle_up(self, sp_code, settle_up_payment):
        response = {
            'success': False,
            'message': None,
            'unsettled_amount': None,
        }
        try:
            settle_up_account = SettleUpAccountWallet(
                sp_code=Seller.objects.get(sp_code=sp_code),
                settle_up_amount=settle_up_payment
            )
            running_account = RunningAccountWallet.objects.get(
                sp_code=Seller.objects.get(sp_code=sp_code))
            running_account.unsettled_amount -= Decimal(settle_up_payment)
            settle_up_account.from_time = running_account.last_settled_at
            running_account.last_settled_at = datetime.datetime.now()
            running_account.save()
            settle_up_account.save()
            response['unsettled_amount'] = running_account.unsettled_amount
            response['success'] = True
        except RunningAccountWallet.DoesNotExist:
            running_account = RunningAccountWallet(
                sp_code=Seller.objects.get(sp_code=sp_code),
            )
            running_account.save()
            response['message'] = 'Running Account Was Not Present'
        except:
            response['message'] = print_exception()
        return response

    def seller_login(self, username, password):
        response = {
            'success': False,
            'message': None,
            'sp_code': None,
        }
        try:
            seller = Seller.objects.get(phone_number=username)
            if seller.password == password:
                response['success'] = True
                response['message'] = 'Login Successful'
                response['sp_code'] = seller.sp_code
                response['address'] = seller.address
                response['city'] = dict(
                    LAZYPOS_CITY_MAPPING).get(seller.city_code)
                seller.active = True
                seller.save()
            else:
                response['message'] = 'Incorrect Password'
        except Seller.DoesNotExist:
            response['message'] = 'Invalid Username'
        return response

    def track_remote_transactions(
            self, seller, amount, wallet_name, trans_type, reference,
            trans_id=None, reference_type='Order'):

        response = {
            'success': False,
            'message': None,
            'sp_code': None,
        }
        try:
            remote_transaction = RemoteWalletTransactions(
                sp_code=seller,
                wallet_name=wallet_name,
                amount=amount,
                trans_type=trans_type,
                reference=reference,
                remote_transaction_id=trans_id,
                reference_type=reference_type
            )
            remote_transaction.save()
            response['success'] = True
            response['message'] = 'Successful Transaction'
        except:
            response['message'] = 'Unsuccessful Transaction'
        return response

    def config(self, sp_code):
        response = {
            'success': False,
            'message': None,
            'sp_code': None,
        }
        try:
            seller = Seller.objects.get(sp_code=sp_code)
            perm = SellerPermissions.objects.get(sp_code=seller)
            response['permission'] = (
                dict((name, getattr(perm, name))
                     for name in perm.__dict__ if not name.startswith('_')))
            response['city'] = dict(LAZYPOS_CITY_MAPPING).get(
                seller.city_code, 'New City')
            response['name'] = seller.name
            response['address'] = seller.address
            response['phone_number'] = seller.phone_number
            response['welcome_text'] = 'Thank You For Visiting.' \
                                       ' Looking Forward To Serve You Again'
            response['success'] = True
            response['message'] = 'Successful Transaction'
        except Seller.DoesNotExist:
            response['message'] = 'Invalid SP Code'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def update_seller_items(self, sp_code, order_items_list):
        response = {
            'success': False,
            'message': None,
            'sp_code': None,
        }
        for item in order_items_list:
            try:
                seller_item = SellerItems.objects.get(
                    sp_code=sp_code,
                    item_code=Item.objects.get(
                        item_code=item.get('item_code')))
                seller_item.item_cost = item.get('item_cost')
                seller_item.item_quantity -= int(item.get('item_quantity'))
                seller_item.save()
                response['success'] = True
            except SellerItems.DoesNotExist:
                seller_item = SellerItems(
                    sp_code=Seller.objects.get(sp_code=sp_code),
                    item_code=Item.objects.get(
                        item_code=item.get('item_code')),
                    item_cost=Decimal(item.get('item_cost')),
                    item_quantity=1,
                )
                seller_item.save()
            except:
                response['message'] = 'Unexpected Error'
        return True

    def seller_unsettled_amount(self, sp_code):
        response = {
            'success': False,
            'message': None,
            'running_account': None,
        }
        try:
            seller = Seller.objects.get(sp_code=sp_code)
            running_wallet = RunningAccountWallet.objects.get(
                sp_code=seller)
            response['running_account'] = {
                'sp_code': sp_code,
                'unsettled_amount': running_wallet.unsettled_amount,
                'last_settled_at': running_wallet.last_settled_at,
                'last_edited_at': running_wallet.last_edited_at,
                'no_of_orders_from_last_settlement':
                    running_wallet.no_of_orders_from_last_settlement
            }
            response['success'] = True
        except Seller.DoesNotExist:
            response['message'] = 'Invalid Seller'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def image_inventory(self, sp_code, inventory_image):
        response = {
            'success': False,
            'message': None,
            'sp_code': None,
        }
        try:
            seller = Seller.objects.get(sp_code=sp_code)
            s_i = SellerInventory(
                sp_code=seller,
                inventory_image=inventory_image,
            )
            s_i.save()
            response['success'] = True
            response['message'] = 'Successful Added Image'
        except Seller.DoesNotExist:
            response['message'] = 'Invalid SP Code'
        except:
            response['message'] = print_exception()
        return response

    def seller_daily_report(self, sp_code, day, month, year):
        response = {
            'success': False,
            'message': None,
            'report': None,
        }
        today = datetime.datetime.now().day
        mon = datetime.datetime.now().month
        yr = datetime.datetime.now().year
        today_date = datetime.date(
            int(year) if year else yr,
            int(month) if month else mon,
            int(day) if day else today)
        # today_date = datetime.datetime.now().date()
        try:
            seller = Seller.objects.get(sp_code=sp_code)
            no_of_orders = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__contains=today_date
            ).count()
            cash_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__contains=today_date,
                offline_order=1
            ).aggregate(Sum('order_amount')).get('order_amount__sum')
            if not cash_amount:
                cash_amount = Decimal(0.0)
            khata_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__contains=today_date,
                offline_order=1
            ).aggregate(Sum('order_amount')).get('khata__sum')
            if not khata_amount:
                khata_amount = Decimal(0.00)
            online_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__contains=today_date,
                offline_order=0
            ).aggregate(Sum('order_amount')).get('khata__sum')
            if not online_amount:
                online_amount = Decimal(0.00)
            total_amount = cash_amount + khata_amount + online_amount
            new_customers = Khata.objects.filter(
                sp_code__sp_code=sp_code,
                created_at__contains=today_date
            ).count()
            top_10_items = OrderItems.objects.filter(
                order_number__order_post_time__contains=today_date,
                order_number__sp_code__sp_code=sp_code
            ).select_related('item_code').values(
                'item_code__item_name',
                'item_code__item_cost'
            ).annotate(
                item_count=Count('item_code__item_code')
            ).order_by('-item_count')[:10]
            seller_report = SellerDailyReport.objects.get(
                sp_code__sp_code=sp_code,
                report_date=today_date)
            seller_report.no_of_orders = no_of_orders
            seller_report.cash_amount = cash_amount
            seller_report.khata_amount = khata_amount
            seller_report.online_amount = online_amount
            seller_report.total_amount = total_amount
            seller_report.no_of_new_customers = new_customers
            seller_report.top_10_items_of_today = top_10_items
            seller_report.save()

        except SellerDailyReport.DoesNotExist:
            seller_report = SellerDailyReport(
                no_of_orders=no_of_orders,
                cash_amount=cash_amount,
                khata_amount=khata_amount,
                online_amount=online_amount,
                total_amount=total_amount,
                no_of_new_customers=new_customers,
                top_10_items_of_today=top_10_items,
                sp_code=seller
            )
            seller_report.save()
        except (ValidationError, IntegrityError):
            response['message'] = 'Invalid Daily Report'
        except Seller.DoesNotExist:
            response['message'] = 'Invalid SP Code'
        except:
            response['message'] = print_exception()
        if seller_report:
            response['success'] = True
            response['report'] = dict(
                (name, getattr(seller_report, name))
                for name in seller_report.__dict__
                if not name.startswith('_'))
        return response

    def seller_monthly_report(self, sp_code, month, year):
        response = {
            'success': False,
            'message': None,
            'report': None,
        }
        try:
            seller = Seller.objects.get(sp_code=sp_code)
            no_of_orders = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__month=month,
                order_post_time__year=year,
            ).count()
            cash_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__month=month,
                order_post_time__year=year,
                offline_order=1
            ).aggregate(Sum('order_amount')).get('order_amount__sum')
            if not cash_amount:
                cash_amount = Decimal(0.0)
            khata_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__month=month,
                order_post_time__year=year,
                offline_order=1
            ).aggregate(Sum('order_amount')).get('khata__sum')
            if not khata_amount:
                khata_amount = Decimal(0.00)
            online_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__month=month,
                order_post_time__year=year,
                offline_order=0
            ).aggregate(Sum('order_amount')).get('khata__sum')
            if not online_amount:
                online_amount = Decimal(0.00)
            total_amount = cash_amount + khata_amount + online_amount
            new_customers = Khata.objects.filter(
                sp_code__sp_code=sp_code,
                created_at__month=month,
                created_at__year=year,
            ).count()
            top_10_items = OrderItems.objects.filter(
                order_number__sp_code__sp_code=sp_code,
                order_number__order_post_time__month=month,
                order_number__order_post_time__year=year,
            ).select_related('item_code').values(
                'item_code__item_name',
                'item_code__item_cost').annotate(
                item_count=Count('item_code__item_code')
            ).order_by('-item_count')[:10]
            seller_report = SellerMonthlyReport.objects.get(
                sp_code__sp_code=sp_code,
                month=month, year=year)
            seller_report.no_of_orders = no_of_orders
            seller_report.cash_amount = cash_amount
            seller_report.khata_amount = khata_amount
            seller_report.online_amount = online_amount
            seller_report.total_amount = total_amount
            seller_report.no_of_new_customers = new_customers
            seller_report.top_10_items_of_today = top_10_items
            seller_report.save()

        except SellerMonthlyReport.DoesNotExist:
            seller_report = SellerMonthlyReport(
                no_of_orders=no_of_orders,
                cash_amount=cash_amount,
                khata_amount=khata_amount,
                online_amount=online_amount,
                total_amount=total_amount,
                no_of_new_customers=new_customers,
                top_10_items_of_today=top_10_items,
                sp_code=seller,
                month=month,
                year=year
            )
            seller_report.save()
        except (ValidationError, IntegrityError):
            response['message'] = 'Invalid Monthly Report'
        except Seller.DoesNotExist:
            response['message'] = 'Invalid SP Code'
        except:
            response['message'] = print_exception()
        if seller_report:
            response['success'] = True
            response['report'] = dict(
                (name, getattr(seller_report, name))
                for name in seller_report.__dict__
                if not name.startswith('_'))
        return response

    def seller_report(self, sp_code, from_day, from_month, from_year, to_day,
                      to_month, to_year):
        response = {
            'success': False,
            'message': None,
            'report': None,
        }
        today = datetime.datetime.now().day
        mon = datetime.datetime.now().month
        yr = datetime.datetime.now().year
        from_date = datetime.date(int(from_year) if from_year else yr,
                                  int(from_month) if from_month else mon,
                                  int(from_day) if from_day else today)
        to_date = datetime.date(int(to_year) if to_year else yr,
                                int(to_month) if from_month else mon,
                                int(to_day) if to_day else today)
        try:
            seller = Seller.objects.get(sp_code=sp_code)
            no_of_orders = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__gte=from_date,
                order_post_time__lte=to_date,
            ).count()
            cash_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__gte=from_date,
                order_post_time__lte=to_date,
                offline_order=1
            ).aggregate(Sum('order_amount')).get('order_amount__sum')
            if not cash_amount:
                cash_amount = Decimal(0.0)
            khata_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__gte=from_date,
                order_post_time__lte=to_date,
                offline_order=1
            ).aggregate(Sum('order_amount')).get('khata__sum')
            if not khata_amount:
                khata_amount = Decimal(0.00)
            online_amount = Order.objects.filter(
                sp_code__sp_code=sp_code,
                order_post_time__gte=from_date,
                order_post_time__lte=to_date,
                offline_order=0
            ).aggregate(Sum('order_amount')).get('khata__sum')
            if not online_amount:
                online_amount = Decimal(0.00)
            total_amount = cash_amount + khata_amount + online_amount
            new_customers = Khata.objects.filter(
                sp_code__sp_code=sp_code,
                created_at__gte=to_date,
                created_at__lte=from_date,
            ).count()
            top_10_items = OrderItems.objects.filter(
                order_number__sp_code__sp_code=sp_code,
                order_number__order_post_time__gte=from_date,
                order_number__order_post_time__lte=to_date,
            ).select_related('item_code').values(
                'item_code__item_name',
                'item_code__item_cost').annotate(
                item_count=Count('item_code__item_code')
            ).order_by('-item_count')[:10]
            report_dict = {
                'no_of_orders': no_of_orders,
                'cash_amount': cash_amount,
                'khata_amount': khata_amount,
                'online_amount': online_amount,
                'total_amount': total_amount,
                'no_of_new_customers': new_customers,
                'top_10_items_of_today': top_10_items,
            }
            response['report'] = report_dict
            response['success'] = True
        except Seller.DoesNotExist:
            response['message'] = 'Invalid SP Code'
        except:
            response['message'] = print_exception()

        return response

    def send_email(self, order_code):
        response = {
            'success': False,
            'message': None,
        }
        try:
            order = Order.objects.get(order_number=order_code)
            send_email = order.user_code.user_email

            if send_email:
                order_items = OrderItems.objects.filter(
                    order_number__order_number=order_code).values_list(
                    'item_cost', 'quantity', 'item_code__item_name')
                order_items_msg = ''
                for item in order_items:
                    temp_msg = '\t  Name : {0}, Quantity : {1}, Cost : {2} \n'.format(
                        item[2],
                        item[1],
                        item[0],
                    )
                    order_items_msg += (temp_msg + '\n ')
                message = 'Dear {0}, \n ' \
                    '\t Order Code : {1} \n ' \
                    '\t Order Cost : {2} \n ' \
                    '\t Discount : {3} \n ' \
                    '\t Taxes : {4} \n ' \
                    '\t Order Amount : {5} \n ' \
                    '\t Time : {6} \n ' \
                    '\t Items : {7} \n ' \
                    '\n Retailer : {8} \n '.format(
                        order.user_code.user_name,
                        order_code,
                        order.order_cost,
                        order.discount,
                        order.taxes,
                        order.order_amount,
                        order.order_creation_time,
                        order_items_msg,
                        order.sp_code.name
                    )
                email_customer(message, send_email)
                response['success'] = True

            else:
                response['message'] = 'Email Address Not Found'

        except Order.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except AttributeError:
            response['message'] = 'Email Address Not Found'
        except:
            response['message'] = print_exception()

        return response


def order_logs(request_json, response_json, successful):
    order_log = OrderLogs(request_json=request_json,
                          response_json=response_json,
                          successful=successful)
    order_log.save()
    return True


def clean_lazypos_db():
    connection = LazyladConnection()
    with open('lazypos_order.csv', 'rU') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in reader:
            delete_order_items(row[0])
            print row
    return 1


def delete_order_items(order_number):
    order = Order.objects.get(order_number=order_number)
    seller = order.sp_code
    order_items = OrderItems.objects.filter(order_number=order)
    order_items.delete()
    transaction = RemoteWalletTransactions.objects.get(reference=order_number)
    connect = LazyladConnection()
    remote_wallet_transaction = connect.lazylad_wallet_redeem(
        seller.sp_code, transaction.amount, order_number)
    connection = LazyPOS()
    connection.track_remote_transactions(
                            seller,
                            transaction.amount,
                            'LazyWallet Refund',
                            'CRE',
                            order_number,
                            remote_wallet_transaction.get('transaction_id'))
    order.delete()
    return True


def seller_sms(sp_code, recipients_list, message):
    response = {
            'success': False,
            'message': None,
        }
    seller = None
    send_to = str()
    sms_charge = 0.00
    connection = LazyPOS()
    connect = LazyladConnection()
    try:
        seller = Seller.objects.get(sp_code=sp_code)
        for recipient in recipients_list:
            send_to += recipient
            send_to += ','
        resp = send_sms(send_to, message)
        if resp.text.split(' | ')[0] == 'success':
            sms_charge = SMS_CHARGE*len(
                recipients_list)*(len(message)/160 + 1)
            sms_log = SellerSMSLog(
                sp_code=seller,
                no_of_messages=len(recipients_list),
                recipients=json.dumps(recipients_list),
                message=message,
                amount=Decimal(sms_charge)
            )
            sms_log.save()
            running_account = RunningAccountSMS.objects.get(sp_code=seller)
            running_account.unsettled_amount += Decimal(sms_charge)
            running_account.save()
            remote_wallet_transaction = connect.lazylad_wallet_sms(
                        seller.sp_code, sms_charge, sms_log.id)
            connection.track_remote_transactions(
                seller,
                sms_charge,
                'LazyWallet',
                'DEB',
                sms_log.id,
                remote_wallet_transaction.get('transaction_id'),
                'SMS'
            )
            response['success'] = True
    except Seller.DoesNotExist:
        response['message'] = 'Invalid SP Code'
    except RunningAccountSMS.DoesNotExist:
        running_account = RunningAccountSMS(
            sp_code=seller,
            unsettled_amount=Decimal(sms_charge)
        )
        running_account.save()
        response['success'] = True
    except AttributeError:
        response['message'] = 'Error From 3rd Party Messaging Service'
    except:
        response['message'] = print_exception()
    return response


def delete_order(order_number):
    order = Order.objects.get(order_number=order_number)
    items = OrderItems.objects.filter(order_number=order)
    items.delete()
    order.delete()


def create_dummy_json():
    sp_code = generate_sp_code()
    dummy_json = {"sp_code": sp_code,
                  "orders_list": []}
    no_of_orders = randint(1, 20)
    order_count = 0
    while order_count < no_of_orders:
        dummy_json["orders_list"].append({
            "delivery_cost": 0.0,
            "discount": 0.0,
            "khata_amount": 0.0,
            "khata_type": "DEB",
            "local_trans_id": "",
            "order_amount": 0.0,
            "order_code": "10000001",
            "order_cost": 0.0,
            "order_creation_time": generate_creation_time(),
            "sp_code": sp_code,
            "taxes": 0.0,
            "user": {
                  "phone_number": "",
                  "user_address": "",
                  "user_city": "",
                  "user_name": ""},
            "offline_order": 1,
            "items": []})
        no_of_items = randint(1, 12)
        item_count = 0
        while item_count < no_of_items:
            item_dict = generate_item(sp_code)
            dummy_json["orders_list"][order_count]['items'].append(
               item_dict)
            item_count += 1
            dummy_json["orders_list"][order_count]['order_amount'] += \
                item_dict.get('item_cost')
            dummy_json["orders_list"][order_count]['order_cost'] += \
                item_dict.get('item_cost')
            item_count += 1
        order_count += 1
    return dummy_json, sp_code, no_of_orders


def generate_creation_time():
    return "2016-{0}-{1} {2}:{3}:{4}".format(
        str(randint(3, 4)).zfill(2),
        str(randint(1, 30)).zfill(2),
        str(randint(9, 21)).zfill(2),
        str(randint(1, 60)).zfill(2),
        str(randint(1, 60)).zfill(2),
    )


def generate_sp_code():
    seller = Seller.objects.filter(immediate_flag=True)
    count = seller.count()
    return seller[randint(0, count-1)].sp_code


def generate_item(sp_code):
    seller_items = SellerItems.objects.filter(sp_code__sp_code=sp_code)
    count = seller_items.count()
    item = seller_items[randint(0, count-1)].item_code
    return {
        "item_barcode": item.item_barcode,
        "item_code": item.item_code,
        "item_cost": float(item.item_cost),
        "item_desc": item.item_desc,
        "item_name": item.item_name,
        "item_quantity": randint(1, 5),
        "item_unit": item.item_unit,
        "sc_code": item.sc_code,
        "st_code": item.st_code,
        "barcode_verified": 1,
        "item_img_flag": 0,
        "local_flag": 0}


def generate_orders(no_of_times):
    response = {
        "success": False,
        "message": None,
        "orders_posted": 0}
    count = 0
    connect = LazyPOS()
    try:
        while count < no_of_times:
            dummy_json, sp_code, no_of_orders = create_dummy_json()
            connect.send_orders_to_POS(dummy_json.get('orders_list'), sp_code)
            count += 1
            response["orders_posted"] += no_of_orders
        response['success'] = True
    except:
        response["message"] = print_exception()
    return response



