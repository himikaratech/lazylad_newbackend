
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from serializers import *
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from LazyladConnection import *
from models import RiderProfile, Order, RiderLogging, Logs
from utils import authenticate_user
from app_settings import ORDER_BY_STATUS_MAPPING as OBSM
from functions import order_details_get, get_orders_list


class Rider(ViewSet):

    def add_rider(self, request):
        """

        Description:
            Api to add new riders

        :param request:
        {
            rider_name :
            phone_no :
            password :
            license_no :
            city_code = :
            checkin_time = :
            duration_time = :
            account_name :
            account_number :
            ifsc_code :
        }

        :return:
        {
            success:
            error_code:
            error_details
            error
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddRiderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        user = None
        try:
            if _serlz.is_valid():
                user = User.objects.create_user(
                    username=data.get('rider_name'), password=data.get('password'))
                user.save()
                create_dict = {}
                for key, value in data.items():
                    if value and key not in ['rider_name', 'password']:
                        create_dict[key] = value
                create_dict['user'] = user
                rider = RiderProfile.objects.create(**create_dict)
                rider.save()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except ValidationError:
            if user:
                user.delete()
            response['error_details'] = 'Rider With Following Details ' \
                                        'Cannot be added'
        except IntegrityError:
            response['error_details'] = 'User Already Exists'

        return Response(response, api_response_status)

    def get_riders_list(self, request):
        """

        Description:
            Api to Get List Of All Checked-In Rider

        :param request:
        {
            order_code :
            city_code :

        }

        :return:
        {
            success :
            error_code :
            error_details :
            error
            city_code : 'city_1'
            riders_assigned : '{'rider_name' : 'rider_name1', 'phone': '999999999'}'
            riders_availabe : '{
                                    'city_1': [{'rider_1': '9999999999'}, {'rider_2': '8888888888'}],
                                    'city_2': [{'rider_1': '9999999999'}, {'rider_2': '8888888888'}],
                                }'

        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        rider_dict = dict()
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GetRidersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                riders_list = RiderProfile.objects.filter(logging='IN')
                if riders_list:
                    for rider in riders_list:
                        city_code = rider.city_code
                        if rider.city_code in rider_dict.keys():
                            rider_dict[city_code].append({
                                'rider_name': rider.user.username,
                                'rider_phone': rider.phone_no})
                        else:
                            rider_dict[city_code] = [{
                                'rider_name': rider.user.username,
                                'rider_phone': rider.phone_no}]

                    if order:
                        response['city_code'] = connection.get_city_code(
                            data.get('order_code')).get('city_code')
                        if order.rider:
                            response['rider_assigned'] = {
                                'rider_name': order.rider.user.username,
                                'rider_phone': order.rider.phone_no}
                    response['success'] = True
                    response['error_code'] = 1
                    response['riders_available'] = rider_dict
                else:
                    response['error_details'] = 'No Riders Available'
                api_response_status = status.HTTP_200_OK
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            response['error_details'] = 'City Not found For Given Order'
        except Order.DoesNotExist:
            api_response_status = status.HTTP_200_OK
            response['error_details'] = 'Order Code Invalid/ Not Found'
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def rider_login(self, request):
        """

        Description:
            Api for rider_login

        :param request:
        {
            phone_no :
            password :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error
            rider_id:

        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RiderLoginSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                rider = RiderProfile.objects.get(
                    phone_no=data.get('phone_no'))
                username = rider.user.username
                auth_dict = authenticate_user(
                    username, password=data.get('password'))
                if auth_dict.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                    response['rider_id'] = rider.id
                    rider.logging = 'IN'
                else:
                    response['error_details'] = auth_dict['message']
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Incorrect username/password'
            return Response(response, api_response_status)
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def rider_checkin(self, request):
        """

        Description:
            Api for rider check-in

        :param request:
        {
            rider_id :
            bike_reading :
            bike_number :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RiderCheckInOutSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                rider = RiderProfile.objects.get(
                    id=data.get('rider_id'))
                rider.logging = 'IN'
                rider.save()
                update_dict = data.copy()
                update_dict.pop('rider_id')
                rider_log = RiderLogging(
                    rider=rider, log_type='IN',
                    bike_reading=data.get('bike_reading'),
                    bike_number=data.get('bike_number'),
                    latitude=data.get('latitude'),
                    longitude=data.get('longitude'))
                rider_log.save()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def rider_checkout(self, request):
        """

        Description:
            Api for rider check-out

        :param request:
        {
            rider_id :
            bike_reading :
            bike_number :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RiderCheckInOutSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                rider = RiderProfile.objects.get(
                    id=data.get('rider_id'))
                rider.logging = 'OUT'
                rider.save()
                update_dict = data.copy()
                update_dict.pop('rider_id')
                rider_log = RiderLogging(
                    rider=rider, log_type='OUT',
                    bike_reading=data.get('bike_reading'),
                    bike_number=data.get('bike_number'),
                    latitude=data.get('latitude'),
                    longitude=data.get('longitude'))
                rider_log.save()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def rider_edit(self, request):
        """

        Description:
            Api to edit exixting riders

        :param request:
        {
            rider_id :
            rider_name :
            password :
            license_no :
            city_code = :
            checkin_time = :
            duration_time = :
            account_name :
            account_number :
            ifsc_code :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RiderEditSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        update_dict = dict()
        try:
            if _serlz.is_valid():
                rider = RiderProfile.objects.get(
                    id=data.get('rider_id'))
                for key, value in data.items():
                    if value and key not in ['rider_id', 'rider_name']:
                        update_dict[key] = value
                RiderProfile.objects.filter(
                    id=data.get('rider_id')).update(**update_dict)
                if data.get('rider_name'):
                    rider.user.username = data['rider_name']
                    rider.save()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except ValidationError:
            response['error_details'] = 'Rider With Following Details ' \
                                        'Cannot be Editedd'

        return Response(response, api_response_status)

    def rider_details(self, request):
        """
        Description:
            Api to add new riders

        :param request:
        {
            rider_id :
        }

        :return:
        {
            rider_name :
            phone_no :
            password :
            license_no :
            city_code = :
            checkin_time = :
            duration_time = :
            account_name :
            account_number :
            ifsc_code :
            success:
            error_code:
            error_details
            error:
        }

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RiderDetailSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        rider_details = dict()
        try:
            if _serlz.is_valid():
                rider = RiderProfile.objects.get(
                    id=data.get('rider_id'))
                rider_fields = RiderProfile._meta.get_all_field_names()
                rider_fields.remove('logs')
                rider_fields.remove('order')
                rider_fields.remove('user')
                for field in rider_fields:
                    rider_details[field] = getattr(rider, field)
                response['rider_details'] = rider_details
                response['rider_details']['rider_name'] = rider.user.username
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK

        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'

        return Response(response, api_response_status)


class Orders(ViewSet):

    def create_order(self, request):
        """

        Description:
            Api to add new orders in LOGISTICS

        :param request:
        {
            order_code :
        }

        :return:
        {
            success:
            error_code:
            error_details
            error
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderCreateSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                if data.get('city_code'):
                    city_code = data.get('city_code')
                else:
                    city_code = connection.get_city_code(
                        data.get('order_code')).get('city_code')
                if not city_code:
                    response['error_details'] = 'City Code Not Found/Exists'
                    return Response(response, api_response_status)
                order = Order(
                    order_code=data.get('order_code'),
                    city_code=city_code)
                order.save()
                connection.update_logistics_used(data.get('order_code'))
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except ValidationError:
            response['error_details'] = 'Order With Following Details ' \
                                        'Cannot be added'
        except IntegrityError:
            response['error_details'] = 'Order With Following Details ' \
                                        'Already Exists'

        return Response(response, api_response_status)

    def order_details(self, request):
        """

        Description:
            Api to view order details

        :param request:
        {
            order_code :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error
            order_details: '{}'
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    'order_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order_detail = order_details_get(data.get('order_code'))
                if order_detail:
                    response['success'] = True
                    response['error_code'] = 1
                    response.update(**order_detail)
                else:
                    response['error_details'] = 'Order Details Not Found/Exists'
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def order_specs(self, request):
        """

        Description:
            Api to view order specifications

        :param request:
        {
            order_code :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error
            order_specs: '{}'
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        order_specs = dict()
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                order_fields = Order._meta.get_all_field_names()
                for field in order_fields:
                    order_specs[field] = getattr(order, field)
                    response['order_specs'] = order_specs
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Code Does Not Exist'

        return Response(response, api_response_status)

    def get_order_status(self, request):
        """

        Description:
            Api to retrieve order status

        :param request:
        {
            order_code :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
            order_status:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                response['order_status'] = order.get_status_display()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def get_orders_of_status(self, request):
        """

        Description:
            Api to retrieve all orders of given status

        :param request:
        {
            status :
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
            order_list : '[]'
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderStatusSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order_by_field = dict(OBSM).get(data.get('status'))
                orders = Order.objects.filter(
                    status=data.get('status')).order_by(
                    order_by_field).values_list('order_code')
                page_no = data.get('page_no')
                paginate_by = data.get('paginate_by')
                if page_no:
                    paginator = Paginator(orders, paginate_by)
                    try:
                        order_list = list(paginator.page(page_no))
                    except PageNotAnInteger:
                        order_list = list(paginator.page(1))
                else:
                    order_list = list(orders)

                resp = get_orders_list([o[0] for o in order_list])
                if resp:
                    response.update(**resp)
                    response['success'] = True
                    response['error_code'] = 1
                else:
                    response['error_details'] = 'Order List Not Found/Exists'
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def get_all_undelivered_orders(self, request):
        """

        Description:
            Api to retrieve all undelivered orders

        :param request:
        {
            None
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
            order_list : '[['order_1', 'UNA', '2'], ['order_2', 'ASS', '2']]'
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            status_list = ['UNA', 'ASS', 'TBP', 'PIK']
            orders = Order.objects.filter(
                status__in=status_list).values_list(
                'order_code', 'status', 'rider_id')
            response['order_list'] = list(orders)
            response['success'] = True
            response['error_code'] = 1
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def assign_rider(self, request):
        """

        Description:
            Api to assign order to a rider

        :param request:
        {
            rider_uid
            order_code
            latitude
            longitude
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        connection = LazyladConnection()
        if not data:
            data = self.request.data.copy()
        _serlz = AssignRiderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                rider = RiderProfile.objects.get(phone_no=int(data.get('rider_uid')))
                order.status = 'ASS'
                order.rider = rider
                order.assigned_at = datetime.now()
                update = connection.assign_rider(order.order_code)
                if not update:
                    response['error_details'] = 'Error In Updating Status'
                    return Response(response, api_response_status)
                order.save()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except ValidationError:
            response['error_details'] = 'Provided Data Incorrect'
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def accept_reject_order(self, request):
        """

        Description:
            Api for rider to accept/reject an order

        :param request:
        {
            rider_id
            order_code
            latitude
            longitude
            action : ACCEPT/REJECT
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        connection = LazyladConnection()
        if not data:
            data = self.request.data.copy()
        _serlz = AcceptRejectOrdersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                rider = RiderProfile.objects.get(id=int(data.get('rider_uid')))
                action = data.get('action')
                if action == 'ACCEPT':
                    order.status = 'TBP'
                    order.rider = rider
                    order.to_be_picked_at = datetime.now()
                    update = connection.rider_accept(order.order_code)
                    if not update:
                        response['error'] = 'Error In Updating Status'
                        return Response(response, api_response_status)
                    order.save()
                else:
                    order.status = 'UNA'
                    order.rider = None
                    order.assigned_at = None
                    update = connection.rider_reject(order.order_code)
                    if not update:
                        response['error'] = 'Error In Updating Status'
                        return Response(response, api_response_status)
                    order.save()
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except ValidationError:
            response['error_details'] = 'Provided Data Incorrect'
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def order_picked(self, request):
        """

        Description:
            Api to record order picked from seller

        :param request:
        {
            order_code
            latitude
            longitude
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                rider = RiderProfile.objects.get(id=int(data.get('rider_id')))
                if data.get('longitude'):
                    order.longitude = data.get('longitude')
                if data.get('latitude'):
                    order.latitude = data.get('latitude')
                order.status = 'PIK'
                order.rider = rider
                order.picked_at = datetime.now()
                response['success'] = True
                response['error_code'] = 1
                log = Logs(
                    rider=rider, latitude=data.get('latitude'),
                    longitude=data.get('longitude'), log_type='PIK')
                order.save()
                log.save()
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except ValidationError:
            response['error_details'] = 'Provided Data Incorrect'
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def rider_orders(self, request):
        """

        Description:
            Api to return all current orders of a rider

        :param request:
        {
            rider_id
            latitude
            longitude
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
            order_list : '[['order_1', 'UNA'], ['order_12', 'PIK']]'
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = RiderDetailSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        order_list = []
        try:
            if _serlz.is_valid():
                if data.get('status'):
                    orders = Order.objects.filter(
                     rider__id=int(data.get('rider_id')),
                     status=data.get('status')).values_list(
                     'order_code', 'status')
                else:
                    orders = Order.objects.filter(
                        rider__id=int(data.get('rider_id'))).values_list(
                        'order_code', 'status')
                for order in orders:
                    order_list.append([
                        order[0], order[1], order_details_get(order[0])])
                response['order_list'] = order_list
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)

    def order_delivered(self, request):
        """

        Description:
            Api to record order delivered to customer

        :param request:
        {
            order_code
            latitude
            longitude
        }

        :return:
        {
            success:
            error_code:
            error_details:
            error:
        }
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        connection = LazyladConnection()
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderDeliveredSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                order = Order.objects.get(order_code=data.get('order_code'))
                rider = RiderProfile.objects.get(id=int(data.get('rider_id')))
                order.status = 'DLV'
                order.rider = rider
                order.delivered_at = datetime.now()
                order.amount_collected = data.get('amount_collected')
                update = connection.update_order_delivered(order.order_code)
                if data.get('longitude'):
                    order.longitude = data.get('longitude')
                if data.get('latitude'):
                    order.latitude = data.get('latitude')
                if not update:
                    response['error_details'] = 'Error In Updating Status'
                    return Response(response, api_response_status)
                response['success'] = True
                response['error_code'] = 1
                log = Logs(
                    rider=rider, latitude=data.get('latitude'),
                    longitude=data.get('longitude'), log_type='DLV')
                order.save()
                log.save()
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except Order.DoesNotExist:
            response['error_details'] = \
                'Order With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except RiderProfile.DoesNotExist:
            response['error_details'] = \
                'Rider With Given Credentials Does Not Exists'
            return Response(response, api_response_status)
        except ValidationError:
            response['error_details'] = 'Provided Data Incorrect'
        except:
            response['error_details'] = 'Unexpected Error'

        return Response(response, api_response_status)


