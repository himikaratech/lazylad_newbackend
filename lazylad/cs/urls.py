from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import CouponSystem

api_urlpatterns = patterns(
    'cs.apiviews',
    url(
        r'^save_coupon_clicks',
        CouponSystem.as_view({'post': 'save_coupon_clicks'})),
    url(
        r'^save_coupon_views',
        CouponSystem.as_view({'post': 'save_coupon_views'})),
    url(
        r'^get_all_coupons',
        CouponSystem.as_view({'get': 'get_all_coupons'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)