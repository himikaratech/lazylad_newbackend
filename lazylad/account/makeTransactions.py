import json
import decimal
from revenue.functions import RunningSellerTransactions
from django.http.response import HttpResponse

from account.constant import PAYMENT_MODE
from database.LazyladConnection import LazyladConnection
from database.models import Wallet, Transactions, PaymentDetails, OrderCompletionDetails, WalletCashbackCoupons,\
    WalletCashbackCouponDetails
from constant import ErrorCode, ResponseConstant, RequestConstant, TransactionType, STATUS, ACCOUNTING_TYPE, \
    RelationColumnName
import helper
import CustomExceptions
from ims.LazyladConnections import Items
from .utils import sp_code_from_order_code


def order_pay_wallet_customer(data):
    ret = dict()
    try:
        owner_code = data[RequestConstant.USER_CODE]
        owner_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        urid_type = data[RequestConstant.URID_TYPE]
        p_mode = data[RequestConstant.PAYMENT_MODE]
        amount = data[RequestConstant.AMOUNT]
        accounting = data[RequestConstant.ACCOUNTING]
    except:
        ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connection = LazyladConnection()
    try:
        wallet = connection.get_wallet_from_user_code(owner_code, owner_type)
        wallet.w_id
    except CustomExceptions.WalletNotFoundException as e:
        ret[ResponseConstant.MESSAGE] = str(e)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    #amount = connection.get_order_wallet_component(urid)
    urid_visible = connection.get_urid_visible_mapping(urid, urid_type)
    transaction = Transactions(w_id=wallet, t_type=TransactionType.ORDER_PAY_WALLET_CUSTOMER, urid=urid,
                               urid_type=urid_type, description="Wallet Pay for Order no " + str(urid_visible), CorD=int(accounting), amount=amount,
                               status=STATUS.PENDING)
    transaction.save()
    payment_detail = PaymentDetails(order_code=urid, p_mode=p_mode, amount=amount, p_status=STATUS.PENDING,
                                    p_tid=transaction)
    payment_detail.save()

    if int(accounting) == ACCOUNTING_TYPE.DEBIT:
        if helper.pay_amount_from_wallet(wallet, amount):
            transaction.status = STATUS.SUCCESS
            payment_detail.p_status = STATUS.SUCCESS
            transaction.save()
            payment_detail.save()
            ret[ResponseConstant.TRANSACTION_ID] = transaction.t_id
            ret[ResponseConstant.MESSAGE] = 'Amount Paid'
            ret[ResponseConstant.SUCCESS] = True
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            transaction.status = STATUS.FAILED
            payment_detail.p_status = STATUS.FAILED
            transaction.save()
            payment_detail.save()
            ret[ResponseConstant.MESSAGE] = 'Insufficient Amount'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INSUFFICIENT_AMOUNT
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
    elif int(accounting) == ACCOUNTING_TYPE.CREDIT:
        if helper.pay_amount_from_wallet(wallet, amount * -1):
            transaction.status = STATUS.SUCCESS
            payment_detail.p_status = STATUS.SUCCESS
            transaction.save()
            payment_detail.save()
            ret[ResponseConstant.MESSAGE] = 'Amount Paid'
            ret[ResponseConstant.SUCCESS] = True
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            transaction.status = STATUS.FAILED
            payment_detail.p_status = STATUS.FAILED
            transaction.save()
            payment_detail.save()
            ret[ResponseConstant.MESSAGE] = 'Insufficient Amount'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INSUFFICIENT_AMOUNT
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
    else:
        ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def order_pay_online_customer(data):
    ret = dict()
    try:
        owner_code = data[RequestConstant.USER_CODE]
        owner_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        amount = data[RequestConstant.AMOUNT]
        urid_type = data[RequestConstant.URID_TYPE]
        p_mode = data[RequestConstant.PAYMENT_MODE]
        accounting = data[RequestConstant.ACCOUNTING]
    except:
        ret[ResponseConstant.MESSAGE] = 'Attributes missing in post requests'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")
    connection = LazyladConnection()
    try:
        wallet = connection.get_wallet_from_user_code(owner_code, owner_type)
        wallet.w_id
    except:
        ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    urid_visible = connection.get_urid_visible_mapping(urid, urid_type)
    transaction = Transactions(w_id=wallet, t_type=TransactionType.ORDER_PAY_ONLINE_CUSTOMER, urid=urid,
                               urid_type=urid_type, description="Online Pay for Order no" + str(urid_visible), CorD=int(accounting), amount=amount,
                               status=STATUS.PENDING)
    transaction.save()
    payment_detail = PaymentDetails(order_code=urid, p_mode=p_mode, amount=amount, p_status=STATUS.PENDING,
                                    p_tid=transaction)
    payment_detail.save()
    ret[ResponseConstant.TRANSACTION_ID] = transaction.t_id
    ret[ResponseConstant.MESSAGE] = 'Transaction id generated'
    ret[ResponseConstant.SUCCESS] = True
    connection.close_connection()
    return HttpResponse(json.dumps(ret), content_type="application/json")


def order_cancel_customer(data):
    ret = dict()
    try:
        owner_code = data[RequestConstant.USER_CODE]
        owner_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        urid_type = data[RequestConstant.URID_TYPE]
    except:
        ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connection = LazyladConnection()
    try:
        wallet = connection.get_wallet_from_user_code(owner_code, owner_type)
        wallet.w_id
    except:
        ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")
    order_status = connection.get_order_status(urid)
    if order_status == STATUS.CANCELLED:
        cancelTransactionSet = Transactions.objects.filter(w_id=wallet, t_type=TransactionType.ORDER_CANCEL_CUSTOMER, urid=urid,urid_type=urid_type, status=STATUS.SUCCESS)
        if 0 != len(cancelTransactionSet):
            ret[ResponseConstant.MESSAGE] = 'Cancel Transaction already occurred for this wallet'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.REPEAT_OPERATION_REQUESTED
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            pass

        payments_made = PaymentDetails.objects.all().filter( order_code=urid, p_status=STATUS.SUCCESS)
        amount = 0
        for payment in payments_made:
            amount = amount + payment.amount

        if amount == 0:
            ret[ResponseConstant.MESSAGE] = 'Amount not need to be returned'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
            ret[ResponseConstant.SUCCESS] = True
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        urid_visible = connection.get_urid_visible_mapping(urid, urid_type)
        return_transaction = Transactions(w_id=wallet, t_type=TransactionType.ORDER_CANCEL_CUSTOMER, urid=urid,
                                          urid_type=urid_type, description="Amount Returned on Cancellation for order " + str(urid_visible),
                                          CorD=ACCOUNTING_TYPE.CREDIT, amount=amount,
                                          status=STATUS.PENDING)
        return_transaction.save()

        helper.pay_amount_from_wallet(wallet, -1 * amount)
        return_transaction.status = STATUS.SUCCESS
        return_transaction.save()

        ret[ResponseConstant.MESSAGE] = 'Amount Refunded'
        ret[ResponseConstant.TRANSACTION_ID] = return_transaction.t_id
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")
    else:
        ret[ResponseConstant.MESSAGE] = 'Order can not be cancelled.'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
        ret[ResponseConstant.SUCCESS] = False
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def order_cancel_seller(data):
    ret = dict()
    try:
        owner_code = data[RequestConstant.USER_CODE]
        owner_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        urid_type = data[RequestConstant.URID_TYPE]

    except CustomExceptions.ObjectNotFoundException as o:
        ret[ResponseConstant.MESSAGE] = str(o)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connection = LazyladConnection()
    try:
        wallet = connection.get_wallet_from_user_code(owner_code, owner_type)
        wallet.w_id
    except CustomExceptions.WalletNotFoundException as e:
        ret[ResponseConstant.MESSAGE] = str(e)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    order_status = connection.get_order_status(urid)
    if order_status == STATUS.PENDING:
        transaction_set = Transactions.objects.all().filter(w_id=wallet, urid=urid, urid_type=urid_type)
        amount = 0
        if len(transaction_set) == 0:
            ret[ResponseConstant.MESSAGE] = 'Order can not be cancelled.'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        for transaction in transaction_set:
            if transaction.status == STATUS.SUCCESS:
                amount = amount + transaction.amount
            elif transaction.status == STATUS.REFUNDED:
                ret[ResponseConstant.MESSAGE] = 'Amount has been already refunded.'
                ret[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
                ret[ResponseConstant.SUCCESS] = False
                connection.close_connection()
                return HttpResponse(json.dumps(ret), content_type="application/json")
        refund_transaction = Transactions(w_id=wallet, t_type=TransactionType.ORDER_CANCEL_SELLER, urid=urid,
                                          urid_type=urid_type, description="Amount Refunded for order " + str(urid),
                                          CorD=ACCOUNTING_TYPE.CREDIT, amount=amount,
                                          status=STATUS.PENDING)
        refund_transaction.save()
        payment_detail = PaymentDetails(order_code=urid, p_mode="wallet_refund", amount=amount, p_status=STATUS.PENDING,
                                        p_tid=refund_transaction)
        payment_detail.save()
        helper.pay_amount_from_seller_wallet(wallet, -1 * amount)
        refund_transaction.status = STATUS.REFUNDED
        refund_transaction.save()
        payment_detail.p_status = STATUS.REFUNDED
        payment_detail.save()
        ret[ResponseConstant.MESSAGE] = 'Amount Refunded'
        ret[ResponseConstant.TRANSACTION_ID] = refund_transaction.t_id
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")
    else:
        ret[ResponseConstant.MESSAGE] = 'Order can not be cancelled.'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
        ret[ResponseConstant.SUCCESS] = False
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def order_finish_customer(data):
    ret = dict()
    try:
        owner_code = data[RequestConstant.USER_CODE]
        owner_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        urid_type = data[RequestConstant.URID_TYPE]
    except CustomExceptions.ObjectNotFoundException as o:
        ret[ResponseConstant.MESSAGE] = str(o)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connection = LazyladConnection()
    try:
        wallet = connection.get_wallet_from_user_code(owner_code, owner_type)
        wallet.w_id
    except CustomExceptions.WalletNotFoundException as e:
        ret[ResponseConstant.MESSAGE] = str(e)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    order_status = connection.get_order_status(urid)
    if order_status == STATUS.DELIVERED:

        deliveredTransactionSet = Transactions.objects.filter(w_id=wallet, t_type=TransactionType.ORDER_FINISH_CUSTOMER, urid=urid,urid_type=urid_type, status=STATUS.SUCCESS)
        if 0 != len(deliveredTransactionSet):
            ret[ResponseConstant.MESSAGE] = 'Delivered Transaction already occurred for this wallet'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.REPEAT_OPERATION_REQUESTED
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            pass

        cost_detail_array = connection.get_order_cost_details(urid)
        total_amount = cost_detail_array[0]

        payments = PaymentDetails.objects.filter(order_code=urid)

        try:
            for payment in payments:
                if payment.p_mode == 'ONLINE':
                    sp_code = sp_code_from_order_code(urid)
                    seller_wallet = connection.get_wallet_from_user_code(sp_code, '1')
                    urid_visible = connection.get_urid_visible_mapping(urid, urid_type)
                    helper.pay_amount_from_seller_wallet(seller_wallet, -1 * float(payment.amount))
                    run_log = RunningSellerTransactions()
                    run_log.running_wallet_charges_logs(payment.amount, sp_code)
                    transaction = Transactions(
                        w_id=seller_wallet,
                        t_type=TransactionType.ORDER_FINISH_SELLER,
                        urid=urid,
                        urid_type=urid_type,
                        description="Online Pay for Order- " + str(urid_visible),
                        CorD=1,
                        amount=payment.amount,
                        status=STATUS.SUCCESS)
                    transaction.save()
        except:
            pass

        payments_made = PaymentDetails.objects.all().filter(order_code=urid, p_status=STATUS.SUCCESS)
        paid_amount = 0
        for payment in payments_made:
            paid_amount = paid_amount + float(payment.amount)

        return_amount = paid_amount - total_amount

        if return_amount > 0:
            urid_visible = connection.get_urid_visible_mapping(urid, urid_type)
            refund_transaction = Transactions(w_id=wallet, t_type=TransactionType.ORDER_FINISH_CUSTOMER, urid=urid,
                                              urid_type=urid_type, description="Amount Returned on Delievered for order " + str(urid_visible),
                                              CorD=ACCOUNTING_TYPE.CREDIT, amount=return_amount,
                                              status=STATUS.PENDING)
            refund_transaction.save()
            helper.pay_amount_from_wallet(wallet, -1 * return_amount)
            refund_transaction.status = STATUS.SUCCESS
            refund_transaction.save()
            ret[ResponseConstant.MESSAGE] = 'Amount successfully refunded.'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
            ret[ResponseConstant.SUCCESS] = True
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            ret[ResponseConstant.MESSAGE] = 'No amount to refund.'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
            ret[ResponseConstant.SUCCESS] = True
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
    else:
        ret[ResponseConstant.MESSAGE] = 'Invalid Information.'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
        ret[ResponseConstant.SUCCESS] = False
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def remote_wallet_transaction(data):
    return_dict = {
        'success': False,
        'message': None
    }
    try:
        connection = LazyladConnection()
        wallet = connection.get_wallet_from_user_code(
            data.get('owner_code'), data.get('owner_type'))
        amount = decimal.Decimal(data.get('amount'))
        seller_transaction = Transactions(
            w_id=wallet,
            t_type=data.get('t_type') if data.get(
                't_type') else TransactionType.LAZYPOS_CHARGE,
            urid=data.get('reference'),
            urid_type='LAZYPOS',
            description="Lazypos Charge For Seller" if amount > 0 else
            "Lazypos Incentive For Seller",
            CorD=data.get('CorD', ACCOUNTING_TYPE.DEBIT),
            amount=amount,
            status=STATUS.SUCCESS)
        seller_transaction.save()
        wallet.balance -= decimal.Decimal(data.get('amount'))
        wallet.save()
        return_dict['success'] = True
        return_dict['transaction_id'] = seller_transaction.t_id
    except:
        return_dict['message'] = 'Unexpected Error'
    return HttpResponse(
        json.dumps(return_dict), content_type="application/json")


def remote_wallet_redeem_transaction(data):
    return_dict = {
        'success': False,
        'message': None
    }
    try:
        connection = LazyladConnection()
        wallet = connection.get_wallet_from_user_code(
            data.get('owner_code'), data.get('owner_type'))
        seller_transaction = Transactions(
            w_id=wallet,
            t_type='LAZYPOS REFUND',
            urid=data.get('reference'),
            urid_type='LAZYPOS',
            description="Lazypos Redeem For Seller " + str(
                data.get('reference')),
            CorD=data.get('CorD', ACCOUNTING_TYPE.CREDIT),
            amount=decimal.Decimal(data.get('amount')),
            status=STATUS.SUCCESS)
        seller_transaction.save()
        wallet.balance += decimal.Decimal(data.get('amount'))
        wallet.save()
        return_dict['success'] = True
        return_dict['transaction_id'] = seller_transaction.t_id
    except:
        return_dict['message'] = 'Unexpected Error'
    return HttpResponse(
        json.dumps(return_dict), content_type="application/json")


def remote_wallet_sms_transaction(data):
    return_dict = {
        'success': False,
        'message': None
    }
    try:
        connection = LazyladConnection()
        wallet = connection.get_wallet_from_user_code(
            data.get('owner_code'), data.get('owner_type'))
        seller_transaction = Transactions(
            w_id=wallet,
            t_type='LAZYPOS SMS',
            urid=data.get('reference'),
            urid_type='LAZYPOS',
            description="Lazypos SMS Charge",
            CorD=data.get('CorD', ACCOUNTING_TYPE.DEBIT),
            amount=decimal.Decimal(data.get('amount')),
            status=STATUS.SUCCESS)
        seller_transaction.save()
        wallet.balance += decimal.Decimal(data.get('amount'))
        wallet.save()
        return_dict['success'] = True
        return_dict['transaction_id'] = seller_transaction.t_id
    except:
        return_dict['message'] = 'Unexpected Error'
    return HttpResponse(
        json.dumps(return_dict), content_type="application/json")


def order_finish_seller(data):
    ret = dict()
    try:
        owner_code = data[RequestConstant.USER_CODE]
        owner_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        urid_type = data[RequestConstant.URID_TYPE]
    except CustomExceptions.ObjectNotFoundException as o:
        ret[ResponseConstant.MESSAGE] = str(o)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connection = LazyladConnection()
    try:
        wallet = connection.get_wallet_from_user_code(owner_code, owner_type)
        wallet.w_id
    except CustomExceptions.WalletNotFoundException as e:
        ret[ResponseConstant.MESSAGE] = str(e)
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    order_status = connection.get_order_status(urid)

    if order_status == STATUS.DELIVERED:
        deliveredTransactionSet = Transactions.objects.filter(
            w_id=wallet, t_type=TransactionType.ORDER_FINISH_SELLER,
            urid=urid,urid_type=urid_type, status=STATUS.SUCCESS)
        if 0 != len(deliveredTransactionSet):
            ret[ResponseConstant.MESSAGE] = 'Delivered Transaction already occurred for this wallet'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.REPEAT_OPERATION_REQUESTED
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            pass

    cost_detail_array = connection.get_order_cost_details(urid)
    total_amount = cost_detail_array[0]
    delivery_cost = cost_detail_array[1]
    discount_cost = cost_detail_array[2]
    total_items_amount = cost_detail_array[3]

    urid_visible = connection.get_urid_visible_mapping(urid, urid_type)

    price_difference_cost = connection.get_price_difference(urid)

    running_logs = RunningSellerTransactions()
    offer_price_dict = connection.get_offer_price_difference(urid)
    if offer_price_dict.get('success') and \
            offer_price_dict.get('offer_price_list'):
        offer_amount = connection.get_item_offer_amount(
            urid).get('amount')
        trans = Transactions(
            w_id=wallet,
            t_type='ITEM_OFFER_PRICE',
            urid=urid,
            urid_type=urid_type,
            description="Item Offer for Order  " + str(urid_visible),
            CorD=0,
            amount=offer_amount,
            status=STATUS.SUCCESS
        )
        trans.save()
        helper.pay_amount_from_seller_wallet(
                wallet, -1 * offer_amount)
        rid = running_logs.running_item_offer_logs(offer_amount, owner_code)
        for price_diff_dict in offer_price_dict.get('offer_price_list'):
            it = Items()
            it.offer_item({
                'sp_code': owner_code,
                'user_code': price_diff_dict.get('user_code'),
                'order_code': urid,
                'item_code': price_diff_dict.get('item_code'),
                'item_quantity': price_diff_dict.get('item_quantity'),
                'item_cost': price_diff_dict.get('item_cost'),
                'offer_cost': price_diff_dict.get('offer_cost'),
                'amount': price_diff_dict.get('amount'),
                'running_account_id': rid,
                'lazy_wallet_trans_id': str(trans.t_id),
            })
            # trans = Transactions(w_id=wallet,
            #                      t_type='ITEM_OFFER_PRICE',
            #                      urid=urid,
            #                      urid_type=urid_type,
            #                      description="Price Diff For Item " +
            #                                  price_diff_dict.get(
            #                                      'item_name'),
            #                      CorD=0,
            #                      amount=abs(price_diff_dict.get(
            #                          'price_diff_net')),
            #                      status=STATUS.SUCCESS)
            # trans.save()
            # helper.pay_amount_from_seller_wallet(
            #     wallet, -1 * price_diff_dict.get('price_diff_net'))
            # running_logs.running_item_offer_logs(price_diff_dict.get(
            #                          'price_diff_net'), owner_code)

    seller_chargable_earning = total_items_amount + price_difference_cost

    order_logistics_flag = connection.get_logistics_order(urid)
    seller_charge_array = connection.get_seller_charge_parameter(owner_code)
    logistic_services_flag = seller_charge_array[0]
    seller_delivery_charge = seller_charge_array[1]
    # price_difference_flag = seller_charge_array[2]

    lazylad_charge = connection.get_lazylad_charge_for_seller(
        owner_code, seller_chargable_earning, order_logistics_flag)
    # if logistic_services_flag == 0 and order_logistics_flag == 1:
    #     connection.update_delivery_charge(urid, lazylad_charge)

    payment_detail_array = PaymentDetails.objects.all().filter(
        order_code=urid, p_status=STATUS.SUCCESS)
    amount_paid_user = 0
    for payment_detail in payment_detail_array:
        if payment_detail.p_mode == PAYMENT_MODE.ONLINE or\
                        payment_detail.p_mode == PAYMENT_MODE.WALLET:
            amount_paid_user = amount_paid_user + float(payment_detail.amount)

    order_amount_to_seller = min(total_amount, amount_paid_user)

    seller_amount = order_amount_to_seller - delivery_cost +\
                    discount_cost - lazylad_charge + price_difference_cost
    running_logs.running_coupon_charges_logs(discount_cost, owner_code)
    running_logs.running_lazylad_charges_logs(-1 * lazylad_charge, owner_code)
    running_logs.running_price_diff_logs(price_difference_cost, owner_code)
    running_logs.running_wallet_charges_logs(amount_paid_user, owner_code)

    if logistic_services_flag == 0:
        seller_amount = seller_amount + delivery_cost

    # Seller Incentive per Order
    seller_amount += connection.get_lazylad_incentive_for_seller(owner_code)

    if seller_amount > 0:
        CorD = ACCOUNTING_TYPE.CREDIT
    else :
        CorD = ACCOUNTING_TYPE.DEBIT
    if seller_amount != 0 :
        seller_transaction = Transactions(
            w_id=wallet, t_type=TransactionType.ORDER_FINISH_SELLER, urid=urid,
            urid_type=urid_type, description="Online Amount Settlement for order " + str(urid_visible),
            CorD=CorD, amount=abs(seller_amount),
            status=STATUS.PENDING)
        seller_transaction.save()
        helper.pay_amount_from_seller_wallet(wallet, -1 * seller_amount)
        seller_transaction.status = STATUS.SUCCESS
        seller_transaction.save()
        order_log = OrderCompletionDetails(
            lazy_lad_charges=lazylad_charge,
            price_difference=price_difference_cost,
            order_code=str(urid),
            lazylad_charge=delivery_cost if logistic_services_flag == 1 else 0.00
        )
        order_log.save()

        ret[ResponseConstant.MESSAGE] = 'Online amount settled successfully for the order.'
        ret[ResponseConstant.TRANSACTION_ID] = seller_transaction.t_id
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
        ret[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")
    else:
        ret[ResponseConstant.MESSAGE] = 'Invalid Information.'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
        ret[ResponseConstant.SUCCESS] = False
        connection.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def redeem_cash_coupon(data):
    try:
        user_code = data[RequestConstant.USER_CODE]
        o_type = data[RequestConstant.OWNER_TYPE]
        coupon_code = data[RequestConstant.COUPON_CODE]

    except Exception, e:
        print e
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connect = LazyladConnection()
    owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
    if owner_id is None:
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    try:
        walletdetails = connect.create_and_get_wallet(owner_id=owner_id, owner_type=o_type)
        w_id = walletdetails.w_id
        try:
            connection = LazyladConnection()
            coupon = connection.get_coupon_details()
            if coupon.applied == True:
                ret = dict()
                ret[ResponseConstant.MESSAGE] = ErrorCode.USED_COUPON
                ret[ResponseConstant.ERROR_CODE] = ErrorCode.USED_COUPON_MESSAGE
                ret[ResponseConstant.SUCCESS] = False
                connection.close_connection()
                return HttpResponse(json.dumps(ret), content_type="application/json")
            else:
                try:
                    balance = walletdetails.balance
                    balance = balance + coupon.amount
                    Wallet.objects.filter(w_id=w_id).update(balance=balance)
                    ret = dict()
                    ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
                    ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
                    ret[ResponseConstant.SUCCESS] = True
                    connection.close_connection()
                    return HttpResponse(json.dumps(ret), content_type="application/json")
                except Exception, e:
                    print str(e)
                    ret = dict()
                    ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
                    ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
                    ret[ResponseConstant.SUCCESS] = False
                    connection.close_connection()
                    return HttpResponse(json.dumps(ret), content_type="application/json")

        except Exception.e:
            print str(e)
      
            ret = dict()
            ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

    except Wallet.DoesNotExist:
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def recharge(data):
    try:
        amount = data[RequestConstant.AMOUNT]
        user_code = data[RequestConstant.USER_CODE]
        o_type = data[RequestConstant.OWNER_TYPE]
        urid = data[RequestConstant.URID]
        urid_type = data[RequestConstant.URID_TYPE]
        status = data[RequestConstant.STATUS]
        if RequestConstant.DESCRIPTION in data:
            description = data[RequestConstant.DESCRIPTION]
        else:
            description = 'recharge transaction'
    except Exception, e:
        print e
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    connect = LazyladConnection()
    owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
    if owner_id is None:
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
        ret[ResponseConstant.SUCCESS] = False
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    try:
        walletdetails = connect.create_and_get_wallet(owner_id=owner_id, owner_type=o_type)
        w_id = walletdetails.w_id
    except Wallet.DoesNotExist:
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
        ret[ResponseConstant.SUCCESS] = True
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")

    try:
        t_details = Transactions(w_id=w_id,
                                 t_type=TransactionType.RECHARGE, urid=urid, urid_type=urid_type,
                                 description=description,
                                 CorD=ACCOUNTING_TYPE.CREDIT, amount=amount, status=status)
        try:
            walletdetails = Wallet.objects.get(w_id)
            if walletdetails.balance + walletdetails.max_neg_balance >= amount:
                ret = dict()
                balance = walletdetails.balance - amount
                t_details.save()
                Wallet.objects.filter(w_id=w_id).update(balance=balance)
                ret[ResponseConstant.MESSAGE] = 'Transaction successfull'
                ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
                ret[RelationColumnName.TRANSACTION_ID] = t_details.t_id
                ret[RelationColumnName.TRANSACTION_AMOUNT] = float(amount)
                ret[ResponseConstant.SUCCESS] = True
                connect.close_connection()
                return HttpResponse(json.dumps(ret), content_type="application/json")
            else:
                ret = dict()
                t_details.status = 'Failed'
                t_details.save()
                ret[ResponseConstant.MESSAGE] = ErrorCode.INSUFICIENT_BALANCE_MESSAGE
                ret[ResponseConstant.ERROR_CODE] = ErrorCode.INSUFICIENT_BALANCE
                ret[RelationColumnName.TRANSACTION_ID] = t_details.t_id
                ret[RelationColumnName.TRANSACTION_AMOUNT] = float(amount)
                ret[ResponseConstant.SUCCESS] = False
                connect.close_connection()
                return HttpResponse(json.dumps(ret), content_type="application/json")

        except Exception, e:
            print e
            ret = dict()
            ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
            ret[ResponseConstant.SUCCESS] = False
            connect.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

    except Exception, e:
        print e
        ret = dict()
        ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
        ret[ResponseConstant.SUCCESS] = False
        connect.close_connection()
        return HttpResponse(json.dumps(ret), content_type="application/json")


def settle_up(data):
    # try:
    #     amount = decimal.Decimal(data[RequestConstant.AMOUNT])
    #     user_code = data[RequestConstant.USER_CODE]
    #     o_type = 1  # seller's owner_type
    #     if RequestConstant.DESCRIPTION in data:
    #         description = data[RequestConstant.DESCRIPTION]
    #     else:
    #         description = 'Settle Up'
    # except Exception, e:
    #     print e
    #     ret = dict()
    #     ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
    #     ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
    #     ret[ResponseConstant.SUCCESS] = False
    #     return HttpResponse(json.dumps(ret), content_type="application/json")
    #
    # connect = LazyladConnection()
    # owner_id = connect.get_owner_id_from_user_code(user_code, o_type)
    # if owner_id is None:
    #     ret = dict()
    #     ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
    #     ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
    #     ret[ResponseConstant.SUCCESS] = False
    #     connect.close_connection()
    #     return HttpResponse(json.dumps(ret), content_type="application/json")
    #
    # try:
    #     walletdetails = connect.create_and_get_wallet(owner_id=owner_id, owner_type=o_type)
    #     w_id = walletdetails.w_id
    #     balance = walletdetails.balance
    # except Wallet.DoesNotExist:
    #     ret = dict()
    #     ret[ResponseConstant.MESSAGE] = 'Wallet does not exists'
    #     ret[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_WALLET_DETAIL
    #     ret[ResponseConstant.SUCCESS] = False
    #     connect.close_connection()
    #     return HttpResponse(json.dumps(ret), content_type="application/json")
    #
    # try:
    #     if (balance <= amount):
    #         ret = dict()
    #         ret[ResponseConstant.MESSAGE] = ErrorCode.INSUFICIENT_BALANCE_MESSAGE
    #         ret[ResponseConstant.ERROR_CODE] = ErrorCode.INSUFICIENT_BALANCE
    #         ret[ResponseConstant.SUCCESS] = False
    #         connect.close_connection()
    #         return HttpResponse(json.dumps(ret), content_type="application/json")
    #     else:
    #         balance = balance - amount
    #         t_details = Transactions(w_id=walletdetails,
    #                                  t_type=TransactionType.SETTLE_UP,
    #                                  description=description,
    #                                  CorD=ACCOUNTING_TYPE.DEBIT, amount=balance, status=STATUS.SUCCESS)
    #
    #         try:
    #             try:
    #                 t_details.save()  # bug here
    #                 Wallet.objects.filter(w_id=str(w_id)).update(balance=amount)
    #             except Exception, e:
    #                 print e
    #                 ret = dict()
    #                 ret[ResponseConstant.SUCCESS] = False
    #                 ret[ResponseConstant.MESSAGE] = 'Transaction' + STATUS.FAILED
    #                 connect.close_connection()
    #                 return HttpResponse(json.dumps(ret), content_type="application/json")
    #             ret = dict()
    #             ret[ResponseConstant.SUCCESS] = True
    #             ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
    #             ret[ResponseConstant.TRANSACTION_ID] = t_details.t_id
    #             ret[RelationColumnName.TRANSACTION_AMOUNT] = float(balance)
    #             connect.close_connection()
    #             return HttpResponse(json.dumps(ret), content_type="application/json")
    #         except Exception, e:
    #             print e
    #             ret = dict()
    #             ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
    #             ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
    #             ret[ResponseConstant.SUCCESS] = False
    #             connect.close_connection()
    #             return HttpResponse(json.dumps(ret), content_type="application/json")

    # except CustomExceptions.InternalErrorMessage as e:
    #     print str(e)
    #     ret = dict()
    #     ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
    #     ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
    #     ret[ResponseConstant.SUCCESS] = False
    #     connect.close_connection()
    #     return HttpResponse(json.dumps(ret), content_type="application/json")
    ret = dict()
    ret[ResponseConstant.MESSAGE] = 'Settel Up Functionality Temporarily Down'
    ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
    ret[ResponseConstant.SUCCESS] = False
    return HttpResponse(json.dumps(ret), content_type="application/json")


def redeem_wallet_cashback_coupon(data):
    try:
        connect = LazyladConnection()
        owner_id = connect.get_owner_id_from_user_code(data.get('user_code'), 0)
        if owner_id:
            wallet = connect.create_and_get_wallet(owner_id=owner_id, owner_type=0)
            Coupon = WalletCashbackCoupons.objects.filter(coupon_name=data.get('coupon_code'))
            if len(Coupon):
                Coupon = Coupon.first()
                if Coupon.coupons_used < Coupon.total_coupons:
                    amount = decimal.Decimal(Coupon.coupon_cash)
                    w_c_details = WalletCashbackCouponDetails(
                        coupon_user_code=data.get('user_code'),
                        coupon_used=data.get('coupon_code'),
                    )
                    w_c_details.save()
                    t_details = Transactions(w_id=wallet,
                                             t_type='CASHBACK', urid=w_c_details.id,
                                             urid_type='WALLET_CASHBACK_COUPON',
                                             description="Cashback coupon used " + str(data.get('coupon_code')),
                                             CorD=1, amount=amount, status='Pending')
                    t_details.save()
                    wallet.balance += amount
                    wallet.save()
                    t_details.status = 'Success'
                    t_details.save()
                    Coupon.coupons_used += 1
                    Coupon.save()
                    return {'success': True, 'amount_credited': round(amount), 'wallet_balance': round(wallet.balance),
                            'error': 0}
                else:
                    return {'success': False, 'message': 'Coupon usage maximum limit reached', 'error': 1}
            else:
                return {'success': False, 'message': 'No Such Coupon Exists', 'error': 1}
        else:
            return {'success': False, 'message': 'Wallet Does Not Exist', 'error': 1}
    except Exception as inst:
        print inst
        return {'success': False, 'message': inst}


# Not implementing for now
def refferal_bonus(data):
    print 'refferal_bonus'


# Not implementing for now
def order_return_customer(data):
    print 'order_return_customer'


# Not Implementing for now
def order_return_seller(data):
    print 'order_return_seller'


'''             #========if transaction has to be made=============
    try:
        t_details = Transactions(w_id=w_id,
                                 t_type=TransactionType.RECHARGE, urid=urid, urid_type=urid_type,
                                 description=description,
                                 CorD=1, amount=amount, status=status)
        t_details.save()
        ret = dict()
        ret[ResponseConstant.MESSAGE] = 'Transaction successfull'
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.SUCCESS
        ret[RelationColumnName.TRANSACTION_ID] = t_details.t_id
        ret[RelationColumnName.TRANSACTION_AMOUNT] = str(amount)
        ret[ResponseConstant.SUCCESS] = True
        return HttpResponse(json.dumps(ret), content_type="application/json")
    except Exception, e:
        print e
        ret = dict()
        ret[ResponseConstant.MESSAGE] = ErrorCode.INTERNAL_ERROR_MESSAGE
        ret[ResponseConstant.ERROR_CODE] = ErrorCode.INTERNAL_ERROR
        ret[ResponseConstant.SUCCESS] = False
        return HttpResponse(json.dumps(ret), content_type="application/json")

    print 'redeem_cash_coupon'
'''
