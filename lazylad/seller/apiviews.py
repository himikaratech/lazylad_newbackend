
import json
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from .serializers import SellerProfileSerializer, ConfirmItemSerial
from LazyladConnection import LazyladConnection
from app_settings import ACCOUNT_EDITABLE_FLAGS

from location import function as location_function


class Seller(ViewSet):

    def get_seller_profile(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.GET.copy()
        _serlz = SellerProfileSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        account_is_updatable = dict(ACCOUNT_EDITABLE_FLAGS)
        try:
            if _serlz.is_valid():
                seller_id = data.get('seller_id')
                seller_name, seller_number = connection.get_seller_details(seller_id)
                if not seller_name:
                    response['error_details'] = 'Seller Does Not' \
                                                ' Exists/Not Found'

                shop_location = location_function.getSellerShopLocation(sp_code=seller_id)
                if shop_location:
                    response['success'] = True
                    response.update(shop_location)

                resp = connection.get_seller_account_details(seller_id)
                if resp:
                    response['seller_name'] = seller_name
                    response['seller_number'] = seller_number
                    response['account_details'] = resp
                    response['success'] = True
                    response['error_code'] = None
                    response['editable_flags'] = {
                        'seller_name': False,
                        'account_details': account_is_updatable,
                    }
                else:
                    response['account_details'] = 'Account Details Does Not' \
                                                 ' Exists/Not Found'
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)

    def update_seller_profile(self, request):
        response = {'success': False,
                    'error_code': 1,
                    'message': 'Error in Processing',
                    }
        update_dict = {}
        is_updatable = dict(ACCOUNT_EDITABLE_FLAGS)
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerProfileSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            to_update = False

            if not _serlz.is_valid():
                response['message'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
                return Response(response, api_response_status)
            response['error_details'] = dict()
            seller_id = data.get('seller_id')
            seller_details = connection.get_seller_details(seller_id)
            if not seller_details:
                response['message'] = 'Seller Not Registered'
                api_response_status = status.HTTP_200_OK
                return Response(response, api_response_status)

            if data.get('shop_location') is not None:
                to_update = True

                location = data.get('shop_location')
                lat = location.get('lat')
                long = location.get('long')
                city_code = location.get('city_code')
                location_function.updateSellerShopCordinates(seller_id,lat,long,city_code)

                response['success'] = True
                response['error_code'] = None
                response['message'] = 'Successfully Updated'


            if data.get('account_name') is not None:
                if is_updatable.get('account_name'):
                    update_dict['account_name'] = data.get('account_name')
                else:
                    response['error_details']['account_name'] = \
                        'This field cannot be updated'
            if data.get('account_number') is not None:
                if is_updatable.get('account_number'):
                    update_dict['account_number'] = data.get('account_number')
                else:
                    response['error_details']['account_number'] =\
                        'This field cannot be updated'
            if data.get('ifsc_code'):
                if is_updatable.get('ifsc_code') is not None:
                    update_dict['ifsc_code'] = data.get('ifsc_code')
                else:
                    response['error_details']['ifsc_code'] =\
                        'This field cannot be updated'
            if data.get('branch_details') is not None:
                if is_updatable.get('branch_details'):
                    update_dict['branch_details'] = data.get('branch_details')
                else:
                    response['error_details']['branch_details'] = \
                        'This field cannot be updated'
            if update_dict:
                to_update = True
                resp = connection.create_or_update_seller_account_details(
                    seller_id, update_dict)
                if resp:
                    response['message'] = "Successfully Updated"
                    response['success'] = True
                    response['error_code'] = None
                else:
                    response['error_details']['error'] = 'Error Occurred ' \
                                                         'while Updating'

            if not to_update:
                response['error_details'] = json.dumps(response['error_details'])
                response['message'] = "Nothing to Update"
                response['success'] = True
                response['error_code'] = None
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)

    def confirm_items_service_provider_with_category(self, request):
        response = dict()
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            data["JsonDataArray"] = json.loads(data["JsonDataArray"])
            _serlz = ConfirmItemSerial(data=data)
            if _serlz.is_valid():
                response = connection.confirm_items_service_provider(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def seller_call_us(self, request):
        response = dict()
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            if "sp_code" in data:
                response = connection.seller_call_us(data["sp_code"])
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": "sp_code not provided"})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)
