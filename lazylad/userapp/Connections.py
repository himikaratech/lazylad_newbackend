from django.db import connections
from dateutil import parser

from oms.utils import print_exception
from .utils import getOrderCodeFromOrderString, dictfetchall
from database.models import PaymentDetails
from services.models import Order


class LazyService():
    def __init__(self):
        pass

    def get_previous_orders(self, user_code, prev_order_string, page_number):
        prev_order = None
        lazy_con = LazyladConnection()
        phone_number = lazy_con.get_phone_number_from_user_code(user_code)
        if prev_order_string:
            try:
                prev_order = Order.objects.get(order_code=prev_order_string)
            except Order.DoesNotExist:
                pass
        if prev_order:
            prev_orders = Order.objects.filter(
                phone_number=phone_number,
                id__lt=prev_order.id).values(
                'order_code',
                'user_code',
                'order_cost',
                'created_at',
                'status',
                'delivery_time',
                'created_at',
                'phone_number',
            )[:10*page_number]
        else:
            prev_orders = Order.objects.filter(
                phone_number=phone_number,
            ).values(
                'order_code',
                'user_code',
                'order_cost',
                'created_at',
                'status',
                'delivery_time',
                'created_at',
                'phone_number'
            )[:10*page_number]
        return prev_orders

    def process_prev_orders(self, prev_orders_list):
        return_list = list()
        try:
            for order in prev_orders_list:
                return_list.append({
                    'status': order.get('status'),
                    # 'to_be_paid': order.get('order_cost'),
                    'to_be_paid': 0,
                    'order_string': order.get('order_code'),
                    'order_delivery_cost': 0,
                    'sp_name': 'Pick My Laundry',
                    'paid': 0,
                    'expected_del_time': convert_to_datetime(
                        order.get('delivery_time')),
                    'order_date': convert_to_datetime(
                        order.get('created_at')),
                    'order_type': 'Lazylad Service'
                })
        except:
            pass
        return return_list


def convert_to_datetime(date_string):
    return_string = 'Default'
    try:
        return_string = date_string.strftime("%a %b %d, %y %H:%M")
    except:
        pass
    return return_string


def sort_orders_list(order_list_1, order_list_2, page_number):
    return_list = list()
    c1 = 0
    c2 = 0
    list_1 = True
    list_2 = True
    try:
        while len(return_list) < 10 * page_number:
            try:
                order_list_1[c1]
            except IndexError:
                list_1 = False
            try:
                order_list_2[c2]
            except IndexError:
                list_2 = False
            if not list_1 and not list_2:
                break
            if not list_1:
                return_list.append(order_list_2[c2])
                c2 += 1
                continue
            if not list_2:
                return_list.append(order_list_1[c1])
                c1 += 1
                continue
            if compare_dates(
                    order_list_1[c1].get('order_date'),
                    order_list_2[c2].get('order_date')):
                return_list.append(order_list_1[c1])
                c1 += 1
            else:
                return_list.append(order_list_2[c2])
                c2 += 1
        return_list = return_list[(page_number-1)*10:page_number*10]
    except:
        pass
    return return_list


def compare_dates(date1, date2):
    try:
        return parser.parse(date1) > parser.parse(date2)
    except:
        return True


class LazyladConnection():

    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def get_user_previous_orders(self, user_code, prev_order_string):
        response = dict()
        service_conn = LazyService()
        try:
            no_prev_order = False
            prev_order_code = None
            if prev_order_string:
                if prev_order_string == '0':
                    no_prev_order = True
                else:
                    prev_order_code = getOrderCodeFromOrderString(prev_order_string)
            order_count_to_send = 10
            query = "SELECT os.order_string, od.order_date, od.order_tot_amount," \
                    " od.order_discount_cost, od.order_code, od.order_total_cost" \
                    ", oin.status, sp.sp_name, od.order_delivery_cost, od.expected_del_time" \
                    " FROM user_details ud, " \
                    "order_in_progress oin, order_details od, " \
                    "service_providers sp, order_strings os where " \
                    "os.o_id = od.id and od.user_code=ud.user_code and " \
                    "(ud.user_code = "+str(user_code)+" or ud.phone_number = " \
                    "(select phone_number from user_details where user_code = "+\
                    str(user_code)+" )) " \
                    "and od.order_code=oin.order_code and sp.sp_code=oin.sp_code"

            if prev_order_code:
                order_condition = " and CAST(od.order_code as UNSIGNED) < " + \
                                  str(prev_order_code)
                sort_condition = " order by CAST(od.order_code as UNSIGNED) desc"
                limit_condition = " limit " + str(order_count_to_send)
                query += str(order_condition) + str(sort_condition) + str(limit_condition)
            else:
                if no_prev_order:
                    limit_condition = " limit " + str(order_count_to_send)
                else:
                    limit_condition = " limit " + str(order_count_to_send)
                sort_condition = " order by CAST(od.order_code as UNSIGNED) desc"
                query += str(sort_condition) + str(limit_condition)
            rows_fetched = self.cursor.execute(query)
            if rows_fetched:
                orders = dictfetchall(self.cursor)
                for order in orders:
                    try:
                        payment_details = PaymentDetails.objects.get(order_code=order['order_code'])
                        wallet_amount = float(payment_details.amount)
                        if payment_details.p_mode == 'ONLINE':
                            order['paid'] = wallet_amount
                            order['to_be_paid'] = order['order_total_cost'] - order['paid']\
                                          + order['order_delivery_cost']
                            order['order_type'] = 'Lazylad Order'
                            order['expected_del_time'] = str(order['expected_del_time'])
                            order.pop('order_tot_amount')
                            order.pop('order_discount_cost')
                            order.pop('order_total_cost')
                            order.pop('order_code')
                            continue
                    except (PaymentDetails.DoesNotExist, AttributeError, IndexError, KeyError):
                        wallet_amount = float(0.0)
                    order['expected_del_time'] = str(order['expected_del_time'])
                    order['order_type'] = 'Lazylad Order'
                    order['paid'] = wallet_amount + order['order_discount_cost']
                    order['to_be_paid'] = order['order_total_cost'] - order['paid']\
                                          + order['order_delivery_cost']
                    order.pop('order_tot_amount')
                    order.pop('order_discount_cost')
                    order.pop('order_total_cost')
                    order.pop('order_code')

                response["previous_orders_details"] = orders
                response["success"] = True
            else:
                response["previous_orders_details"] = list()
            response["success"] = True
        except:
            response["error"] = 1
            response["message"] = print_exception()
            response["success"] = False
        return response

    def get_user_previous_orders_v2(self, user_code, prev_order_string, page_number=1):
        response = dict()
        service_conn = LazyService()
        try:
            no_prev_order = False
            prev_order_code = None
            if prev_order_string:
                if prev_order_string == '0':
                    no_prev_order = True
                else:
                    prev_order_code = getOrderCodeFromOrderString(prev_order_string)
            order_count_to_send = 10*page_number
            query = "SELECT os.order_string, od.order_date, od.order_tot_amount," \
                    " od.order_discount_cost, od.order_code, od.order_total_cost" \
                    ", oin.status, sp.sp_name, od.order_delivery_cost, od.expected_del_time" \
                    " FROM user_details ud, " \
                    "order_in_progress oin, order_details od, " \
                    "service_providers sp, order_strings os where " \
                    "os.o_id = od.id and od.user_code=ud.user_code and " \
                    "(ud.user_code = "+str(user_code)+" or ud.phone_number = " \
                    "(select phone_number from user_details where user_code = "+\
                    str(user_code)+" )) " \
                    "and od.order_code=oin.order_code and sp.sp_code=oin.sp_code"

            # if prev_order_code:
            #     order_condition = " and CAST(od.order_code as UNSIGNED) < " + \
            #                       str(prev_order_code)
            #     sort_condition = " order by CAST(od.order_code as UNSIGNED) desc"
            #     limit_condition = " limit " + str(order_count_to_send)
            #     query += str(order_condition) + str(sort_condition) + str(limit_condition)
            # else:
            #     if no_prev_order:
            #         limit_condition = " limit " + str(order_count_to_send)
            #     else:
            #         limit_condition = " limit " + str(order_count_to_send)
            #     sort_condition = " order by CAST(od.order_code as UNSIGNED) desc"
            #     query += str(sort_condition) + str(limit_condition)

            limit_condition = " limit " + str(order_count_to_send)
            sort_condition = " order by CAST(od.order_code as UNSIGNED) desc"
            query += str(sort_condition) + str(limit_condition)
            rows_fetched = self.cursor.execute(query)
            if rows_fetched:
                orders = dictfetchall(self.cursor)
                for order in orders:
                    try:
                        payment_details = PaymentDetails.objects.get(order_code=order['order_code'])
                        wallet_amount = float(payment_details.amount)
                        if payment_details.p_mode == 'ONLINE':
                            order['paid'] = wallet_amount
                            order['to_be_paid'] = order['order_total_cost'] - order['paid']\
                                          + order['order_delivery_cost']
                            order['order_type'] = 'Lazylad Order'
                            order['expected_del_time'] = str(order['expected_del_time'])
                            order.pop('order_tot_amount')
                            order.pop('order_discount_cost')
                            order.pop('order_total_cost')
                            order.pop('order_code')
                            continue
                    except (PaymentDetails.DoesNotExist, AttributeError, IndexError, KeyError):
                        wallet_amount = float(0.0)
                    order['expected_del_time'] = str(order['expected_del_time'])
                    order['order_type'] = 'Lazylad Order'
                    order['paid'] = wallet_amount + order['order_discount_cost']
                    order['to_be_paid'] = order['order_total_cost'] - order['paid']\
                                          + order['order_delivery_cost']
                    order.pop('order_tot_amount')
                    order.pop('order_discount_cost')
                    order.pop('order_total_cost')
                    order.pop('order_code')

                service_orders = service_conn.get_previous_orders(
                    user_code, prev_order_string, page_number)
                processed_orders = service_conn.process_prev_orders(
                    service_orders)
                response["previous_orders_details"] = sort_orders_list(
                    orders, processed_orders, page_number)
                response["success"] = True
            else:
                response["previous_orders_details"] = list()
            response["success"] = True
        except:
            response["error"] = 1
            response["message"] = print_exception()
            response["success"] = False
        return response

    def get_order_details(self, user_code, order_string):
        response = dict()
        try:
            order_code = getOrderCodeFromOrderString(order_string)
            query = "select uad.user_name, uad.user_phone_number, uad.user_address," \
                    " od.coupon_code, od.order_tot_amount, od.order_discount_cost," \
                    " od.order_total_cost, od.order_delivery_cost from " \
                    "user_address_details uad, " \
                    "order_details od where od.del_address_code = uad.user_address_code and " \
                    "od.del_address_user_code = uad.user_code and od.order_code = '"\
                    +str(order_code)+"' and" \
                    " od.user_code in (select ud1.user_code from user_details ud1, " \
                    "user_details ud2 where ud1.phone_number = ud2.phone_number and " \
                    "ud2.user_code = '"+str(user_code)+"')"
            rows_fetched = self.cursor.execute(query)
            if rows_fetched:
                order_details = dictfetchall(self.cursor)
                delivery_details = dict()
                delivery_details['user_name'] = order_details[0]['user_name']
                delivery_details['user_phone_number'] = order_details[0]['user_phone_number']
                delivery_details['user_address'] = order_details[0]['user_address']
                response['order_delivery_details'] = delivery_details
                response['order_code'] = str(order_string)
                order_payment_details = dict()
                order_payment_details['coupon_code'] = order_details[0]['coupon_code']
                order_payment_details['order_delivery_cost'] = order_details[0]['order_delivery_cost']
                order_payment_details['discount_amount'] = order_details[0]['order_discount_cost']
                try:
                    payment_details = PaymentDetails.objects.get(order_code=order_code)
                    wallet_amount = float(payment_details.amount)
                    if payment_details.p_mode == 'ONLINE':
                        order_payment_details['paid'] = order_details[0]['order_tot_amount']
                        order_payment_details['to_be_paid'] = 0.0
                        order_payment_details['wallet_paid'] = float(payment_details.amount) - (
                            order_details[0]['order_delivery_cost'] + order_details[0]['order_total_cost'])
                        response['order_payment_details'] = order_payment_details
                        response['success'] = True
                        return response
                except (PaymentDetails.DoesNotExist, AttributeError):
                    wallet_amount = float(0.0)
                paid = order_details[0]['order_discount_cost'] + wallet_amount
                to_be_paid = order_details[0]['order_total_cost'] - paid \
                             + order_details[0]['order_delivery_cost']
                order_payment_details['paid'] = paid
                order_payment_details['to_be_paid'] = to_be_paid
                order_payment_details['wallet_paid'] = wallet_amount
                order_payment_details['special_offer'] = \
                    self.get_special_offer_price(order_code)
                response['order_payment_details'] = order_payment_details
            else:
                response['order_payment_details'] = dict()
                response['order_delivery_details'] = dict()
            response['success'] = True
        except:
            response["error"] = 1
            response["message"] = print_exception()
            response["success"] = False
        return response

    def save_user_config(self, data):
        response = dict()
        try:
            user_code = data.get('user_code')
            lazylad_version_code = data.get('lazylad_version_code', None)
            area_code_selected = data.get('area_code_selected', None)
            city_code_selected = data.get('city_code_selected', None)
            imei_number = data.get('imei_number', None)
            mac_address = data.get('mac_address', None)
            os_type = data.get('os_type', None)
            model_name = data.get('model_name', None)
            phone_manufacturer = data.get('phone_manufacturer', None)
            query = " update user_details set "
            query_list = list()
            if lazylad_version_code:
                query_list.append("lazylad_version_code = " + str(lazylad_version_code))
            if city_code_selected:
                query_list.append(" user_current_city = '" + str(city_code_selected) + "'")
            if area_code_selected:
                query_list.append(" user_current_area = '" + str(area_code_selected) + "'")
            if imei_number:
                query_list.append(" imei_number = '" + str(imei_number) + "'")
            if mac_address:
                query_list.append(" mac_address = '" + str(mac_address) + "'")
            if os_type:
                query_list.append(" os_type = '" + str(os_type) + "'")
            if model_name:
                query_list.append(" model_name = '" + str(model_name) + "'")
            if phone_manufacturer:
                query_list.append(" phone_manufacturer = '" + str(phone_manufacturer) + "'")
            final_query_segment = ', '.join([str(x) for x in query_list])
            if len(query_list):
                query += final_query_segment + " WHERE user_code = " + str(user_code)
                rows_affected = self.cursor.execute(query)
                if rows_affected:
                    query = "SELECT config_value_integer, config_value_string, config_name " \
                            "FROM lazylad_config WHERE config_name =  'shall_number_verify_customer' " \
                            "OR config_name =  'call_us_number'"
                    rows_fetched = self.cursor.execute(query)
                    if rows_fetched:
                        config_names = dictfetchall(self.cursor)
                        for config_name in config_names:
                            if config_name['config_name'] == 'shall_number_verify_customer':
                                response['number_verification'] = {'shall_number_verify': config_name['config_value_integer']}
                            elif config_name['config_name'] == 'call_us_number':
                                response['lazylad_contact'] = {'call_us_number': config_name["config_value_string"]}
                        response['number_verification']
                    else:
                        response['number_verification'] = {'shall_number_verify': 1}
                    response['success'] = True
                else:
                    response = dict()
                    response['success'] = False
                    response['message'] = 'Database Not Accessible'
            else:
                response['success'] = False
                response['message'] = 'No parameters received for updation'
        except:

            response["error"] = 1
            response["message"] = print_exception()
            response["success"] = False

        return response

    def get_special_offer_price(self, order_code):
        try:
            query = 'select offer_diff from order_item_special_offer_details' \
                    ' where order_code = {0}'.format(order_code)
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)
            offer_diff = data[0]
        except:
            offer_diff = 0
        return offer_diff

    def get_phone_number_from_user_code(self, user_code):
        try:
            query = 'select phone_number from user_details' \
                    ' where user_code = {0}'.format(user_code)
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)
            phone_number = data[0].get('phone_number')
        except:
            phone_number = None
        return phone_number
