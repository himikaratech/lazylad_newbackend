from django.db import models
from django.contrib.auth.models import User
from .app_settings import LOGISTICS_ORDER_STATUS, DRIVER_LOGGING,\
    DRIVER_STATUS


class RiderProfile(models.Model):
    user = models.OneToOneField(User)
    phone_no = models.CharField(max_length=13, unique=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    license_no = models.CharField(max_length=30)
    logging = models.CharField(choices=DRIVER_LOGGING, default='OUT',
                               max_length=3)
    status = models.CharField(choices=DRIVER_STATUS, default='0',
                              max_length=1)
    city_code = models.CharField(max_length=4)
    latitude = models.CharField(max_length=20, null=True, blank=True)
    longitude = models.CharField(max_length=20, null=True, blank=True)
    checkin_time = models.TimeField(null=True, blank=True)
    duration_time = models.TimeField(null=True, blank=True)
    account_name = models.CharField(max_length=30,  null=True, blank=True)
    account_no = models.CharField(max_length=30, null=True, blank=True)
    ifsc_code = models.CharField(max_length=30, null=True, blank=True)
    bike_number = models.CharField(max_length=30, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.phone_no


class Order(models.Model):
    order_code = models.CharField(max_length=30, unique=True)
    status = models.CharField(choices=LOGISTICS_ORDER_STATUS, default='UNA',
                              max_length=3)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    assigned_at = models.DateTimeField(
        editable=False, null=True, blank=True)
    to_be_picked_at = models.DateTimeField(
        editable=False, null=True, blank=True)
    picked_at = models.DateTimeField(editable=False, null=True, blank=True)
    delivered_at = models.DateTimeField(editable=False, null=True, blank=True)
    rider = models.ForeignKey(RiderProfile, null=True, blank=True)
    amount_collected = models.CharField(max_length=20, default='0')
    city_code = models.CharField(max_length=4)


class Logs(models.Model):
    rider = models.ForeignKey(RiderProfile)
    log_type = models.CharField(choices=DRIVER_LOGGING, max_length=3)
    log_time = models.TimeField(auto_now_add=True, editable=False)
    log_date = models.DateField(auto_now_add=True, editable=False)
    latitude = models.CharField(max_length=20, null=True, blank=True)
    longitude = models.CharField(max_length=20, null=True, blank=True)


class RiderLogging(models.Model):
    bike_reading = models.IntegerField(default=0)
    bike_number = models.CharField(max_length=30, null=True, blank=True)
