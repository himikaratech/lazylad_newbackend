import sys
import linecache


def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    line_no = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, line_no, f.f_globals)
    return str('EXCEPTION IN ({}, LINE {} :: "{}"): {}'.format(filename, line_no, line.strip(), exc_obj))