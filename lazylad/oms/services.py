
import requests
import urllib
import json
from .app_settings import GUPSHUP_USER_ID, GUSSHUP_PASSWORD
from .api_settings import lazylad_old_server_api, headers


def send_sms(send_to, message):
    return_dict = dict()
    return_dict['method'] = 'sendMessage'
    return_dict['send_to'] = str(send_to)
    return_dict['msg'] = message
    return_dict['msg_type'] = 'Text'
    return_dict['userid'] = GUPSHUP_USER_ID
    return_dict['password'] = GUSSHUP_PASSWORD
    return_dict['v'] = '1.1'
    url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?'
    response = requests.get(url, params=urllib.urlencode(return_dict))
    return response


def post_single_order(post_dict, v5_flag):
    if v5_flag:
        api_name = '/postOrderToServerFinal_v5'
    else:
        api_name = '/postOrderToServerFinal_v4'
    try:
        res = requests.post(lazylad_old_server_api +
                            api_name,
                            data=json.dumps(post_dict), headers=headers)
    except requests.exceptions.RequestException as e:
        return{'error': 0, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return {'error': 1,
                'message': 'Order Successfully placed',
                'order_code': json.loads(res.text).get('order_code')}
    else:
        return {'error': 0, 'message': 'Order/s Not Placed'}


def cancel_single_order(post_dict):
    r = requests.post(lazylad_old_server_api +
                      '/cancelUsersPreviousOrder',
                      data=json.dumps(post_dict), headers=headers)
    if r.status_code == 200:
        return True
    else:
        return False
