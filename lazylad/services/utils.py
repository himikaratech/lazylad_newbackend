
import json
import string
import random
import linecache
import sys
import time
from decimal import Decimal
from django.core.exceptions import MultipleObjectsReturned
from django.core.mail import send_mail

from lazylad.settings import PICK_MY_LAUNDRY_EMAIL, EMAIL_HOST_USER


from .models import Order


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def epoch_to_datetime(epoch_time):
    try:
        return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch_time/1000))
    except:
        return None


def datetime_to_epoch(date_time):
    try:
        return int(time.mktime(date_time.timetuple())*1000)
    except:
        return None


def generate_order_code():
    new_order_code = None
    while 1:
        try:
            new_order_code = id_generator()
            order = Order.objects.get(order_code=new_order_code)
        except Order.DoesNotExist:
            return new_order_code


def send_email(message):
    try:
        send_mail('New Order From Lazylad', message,
                  EMAIL_HOST_USER, PICK_MY_LAUNDRY_EMAIL, fail_silently=False)
        return True
    except:
        return False


