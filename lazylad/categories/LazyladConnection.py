__author__ = 'AnkitAtreja'
from django.db import connections


class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def getServiceTypesforCityforBuyers(self, city_code):
        query = "SELECT  st.st_code, st.st_name, st.st_img_flag, st.st_img_add, \
            sts.seller_automated, st.items_common FROM service_types st, service_type_status sts \
            where st.st_code = sts.st_code and sts.city_code = %s and \
            sts.launched_for_buyer = TRUE  and st.is_service = FALSE order by st.sort_order "
        self.cursor.execute(query, [city_code])

        response = {"service_types":[]}
        id = 1
        for row in self.cursor:
            (stcode, stname, st_img_flag, st_img_add, seller_automated, items_common) = row
            tmp = dict()
            tmp["id"] = id
            tmp["st_code"] = stcode
            tmp["st_name"] = stname
            tmp["st_img_flag"] = st_img_flag
            tmp["st_img_add"] = st_img_add
            tmp["seller_automated"] = seller_automated
            tmp["items_common"] = items_common

            response["service_types"].append(tmp)
            id = id +1
        return response

    def getServiceTypesforCityforBuyers_v2(self, city_code):
        query = "SELECT  st.st_code, st.st_name, st.st_img_flag, st.st_img_add, \
            sts.seller_automated, st.items_common, st.is_service FROM service_types st, service_type_status sts \
            where st.st_code = sts.st_code and sts.city_code = %s and \
            sts.launched_for_buyer = TRUE order by st.sort_order "
        self.cursor.execute(query, [city_code])

        response = {"service_types":[]}
        id = 1
        for row in self.cursor:
            (stcode, stname, st_img_flag, st_img_add, seller_automated, items_common, is_service) = row
            tmp = dict()
            tmp["id"] = id
            tmp["st_code"] = stcode
            tmp["st_name"] = stname
            tmp["st_img_flag"] = st_img_flag
            tmp["st_img_add"] = st_img_add
            tmp["seller_automated"] = seller_automated
            tmp["items_common"] = items_common
            tmp['is_service'] = is_service

            response["service_types"].append(tmp)
            id = id +1
        return response

    def getServiceTypesforCityforSeller(self, city_code):
        query = "SELECT st.id, st.st_code, st.st_name FROM service_types st, service_type_status sts \
        where st.st_code = sts.st_code and sts.city_code = %s and sts.launched_for_seller = TRUE"
        self.cursor.execute(query, [city_code])

        response = {"service_types":[]}

        for row in self.cursor:
            (id, stcode, stname) = row
            tmp = dict()
            tmp["id"] = id
            tmp["st_code"] = stcode
            tmp["st_name"] = stname

            response["service_types"].append(tmp)

        return response
