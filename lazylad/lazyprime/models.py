from django.db import models
from decimal import Decimal
from .app_settings import USER_ORIGIN, ORDER_STATUS


class UserCity(models.Model):
    name = models.CharField(max_length=25, unique=True, default='Gurgaon')
    country = models.CharField(max_length=25, default='India')

    class Meta:
        app_label = 'lazyprime'


class User(models.Model):
    user_code = models.CharField(max_length=25, unique=True)
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=25)
    phone_number = models.CharField(max_length=25, unique=True)
    city = models.ForeignKey(UserCity, to_field='name')
    origin = models.CharField(
        max_length=25, choices=USER_ORIGIN, default='DEF')
    origin_ref = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        app_label = 'lazyprime'


class Addresses(models.Model):
    city = models.ForeignKey(UserCity, to_field='name')
    user_code = models.CharField(max_length=25)
    key = models.PositiveIntegerField(default=1)
    flat = models.CharField(max_length=45, null=True, blank=True)
    subarea = models.CharField(max_length=45, null=True, blank=True)
    area = models.CharField(max_length=45, null=True, blank=True)
    pincode = models.CharField(max_length=10, null=True, blank=True)
    latitude = models.CharField(max_length=45, null=True, blank=True)
    longitude = models.CharField(max_length=45, null=True, blank=True)

    class Meta:
        unique_together = ('user_code', 'key')
        app_label = 'lazyprime'


class Order(models.Model):
    order_code = models.CharField(max_length=25, unique=True)
    user_code = models.ForeignKey(User, to_field='user_code')
    address = models.ForeignKey(Addresses)
    items_amount = models.DecimalField(max_digits=20, decimal_places=2)
    amount_payable = models.DecimalField( max_digits=20, decimal_places=2)
    delivery_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    discount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    taxes = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    tax_percentage = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    coupon_code = models.CharField(max_length=25, null=True, blank=True)
    status = models.CharField(
        max_length=10, choices=ORDER_STATUS, default='PEN')
    post_time = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
         app_label = 'lazyprime'


class Brand(models.Model):
    brand_name = models.CharField(max_length=25, unique=True)
    image_address = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        app_label = 'lazyprime'


class Vendor(models.Model):
    vendor_name = models.CharField(max_length=25, unique=True)
    phone_number = models.CharField(max_length=25, null=True, blank=True)
    brand = models.ForeignKey(Brand, to_field='brand_name')
    city = models.ForeignKey(UserCity, to_field='name')
    email = models.CharField(max_length=25, null=True, blank=True)
    password = models.CharField(max_length=25, null=True, blank=True)
    address = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'lazyprime'


class Item(models.Model):
    item_code = models.CharField(max_length=25, unique=True)
    st_code = models.CharField(max_length=25)
    sc_code = models.CharField(max_length=25)
    item_name = models.CharField(max_length=55)
    item_type_code = models.CharField(
        max_length=25, default='1', null=True, blank=True)
    item_unit = models.CharField(max_length=25, null=True, blank=True)
    item_quantity = models.IntegerField(null=True, blank=True, default=1)
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    item_cost_bag_one = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    item_barcode = models.CharField(max_length=25, null=True, blank=True)
    item_short_desc = models.CharField(max_length=25, null=True, blank=True)
    item_desc = models.CharField(max_length=25, null=True, blank=True)
    vendor_name = models.CharField(max_length=25, default='V1')
    brand_name = models.CharField(max_length=25, default='ITC')

    class Meta:
        app_label = 'lazyprime'


class OrderItems(models.Model):
    item_code = models.ForeignKey(Item, to_field='item_code')
    order_number = models.ForeignKey(Order, to_field='order_code')
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    item_quantity = models.IntegerField(null=True, blank=True)

    class Meta:
        app_label = 'lazyprime'


class VendorLogistics(models.Model):
    name = models.CharField(max_length=25)
    phone_no = models.CharField(max_length=13)
    address = models.TextField(null=True, blank=True)
    email = models.CharField(max_length=25, null=True, blank=True)
    city = models.ForeignKey(UserCity)

    class Meta:
        app_label = 'lazyprime'


class Orderline(models.Model):
    order_line = models.CharField(max_length=25, unique=True)
    order = models.ForeignKey(Order, to_field='order_code')
    vendor = models.ForeignKey(Vendor, to_field='vendor_name')
    items_amount = models.DecimalField(max_digits=20, decimal_places=2)
    delivery_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2, default=0)
    discount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2, default=0)
    taxes = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2, default=0)
    tax_percentage = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2, default=0)
    amount_payable = models.DecimalField(max_digits=20, decimal_places=2)
    status = models.CharField(
        max_length=10, choices=ORDER_STATUS, default='PEN')
    payment_status = models.CharField(
        max_length=10, choices=ORDER_STATUS, default='PEN')
    post_time = models.DateTimeField(auto_now_add=True, editable=False)
    logistics_support = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        app_label = 'lazyprime'


class OrderLineItems(models.Model):
    order_line = models.ForeignKey(Orderline)
    item_code = models.ForeignKey(Item, to_field='item_code')
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    item_quantity = models.IntegerField(null=True, blank=True)

    class Meta:
        app_label = 'lazyprime'
