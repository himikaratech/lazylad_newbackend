from django.db import models
import datetime

from .app_settings import ORDER_STATUS, COUPON_TYPES


class Service(models.Model):
    st_code = models.CharField(max_length=30)
    st_name = models.CharField("Root Service", max_length=30)
    service_code = models.PositiveIntegerField()
    service_name = models.CharField(
        "Service Name", max_length=25, unique=True)
    score = models.PositiveIntegerField(default=0)
    desc = models.TextField(
        "Service Description", blank=True, null=True)
    open_time = models.IntegerField()
    close_time = models.IntegerField()
    gap_time = models.IntegerField()
    interval = models.IntegerField()
    image_url = models.CharField(max_length=255, null=True, blank=True)
    image_flag = models.BooleanField(default=True)

    class Meta:
        app_label = 'services'
        unique_together = (("st_code"), ("service_code"))

    def __str__(self):              # __unicode__ on Python 2
        return self.st_name


class ServiceCategory(models.Model):
    category_name = models.CharField("Category Name", max_length=30)
    root_service = models.ForeignKey(Service, to_field='service_name')
    category_code = models.CharField(max_length=30)
    category_desc = models.TextField(
        "Category Description", blank=True, null=True)

    class Meta:
        app_label = 'services'

    def __str__(self):              # __unicode__ on Python 2
        return self.cat_name


class CategoryItems(models.Model):
    item_name = models.CharField("Item Name", max_length=30)
    item_category = models.ForeignKey(ServiceCategory)
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=10, decimal_places=2, default=0)
    item_img = models.CharField(max_length=100)
    item_desc = models.CharField(max_length=55, null=True, blank=True)
    group = models.PositiveIntegerField(default=1)
    unit = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        app_label = 'services'

    def __str__(self):              # __unicode__ on Python 2
        return self.item_name

    class Meta:
        app_label = 'services'


class Coupon(models.Model):
    coupon_code = models.CharField(
        "Coupon code", max_length=25, unique=True, null=False, blank=False)
    name = models.CharField(max_length=25, null=False, blank=False)
    description = models.CharField(null=True, blank=True, max_length=25)
    coupon_type = models.CharField(max_length=15, choices=COUPON_TYPES)
    discount_amount = models.DecimalField(
        null=False, max_digits=20, decimal_places=2)
    max_discount = models.DecimalField(
        default=0, max_digits=20, decimal_places=2)
    min_order_amount = models.DecimalField(
        default=0, max_digits=20, decimal_places=2)
    total_coupons = models.IntegerField(default=0)
    coupons_applied = models.IntegerField(default=0)
    coupons_per_user = models.IntegerField(default=0)
    active = models.BooleanField(default=True)
    expiry_date = models.DateField(null=True, blank=True)

    class Meta:
        app_label = 'services'

    def __str__(self):
        return self.name


class Order(models.Model):
    order_code = models.CharField(max_length=25, unique=True)
    user_code = models.CharField(max_length=25)
    status = models.CharField(
        max_length=25, choices=ORDER_STATUS, default='Pending')
    phone_number = models.CharField(null=True, blank=True, max_length=25)
    address_code = models.CharField(max_length=25)
    address_user_code = models.CharField(max_length=25)
    pickup_time = models.DateTimeField(null=True, blank=True)
    delivery_time = models.DateTimeField(null=True, blank=True)
    order_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2, default=0)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    coupon_code = models.CharField(null=True, blank=True, max_length=25)
    coupon_type = models.CharField(null=True, blank=True, max_length=25)
    coupon_discount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    coupon_max_discount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)

    class Meta:
        app_label = 'services'

    def __str__(self):              # __unicode__ on Python 2
        return self.order_code

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        super(Order, self).save(*args, **kwargs)


class OrderServices(models.Model):
    order = models.ForeignKey(Order, to_field='order_code')
    service = models.ForeignKey(Service, to_field='service_name')
    pick_up_time = models.DateTimeField()
    delivery_time = models.DateTimeField()
    order_cost = models.DecimalField(
        null=True, blank=True, default=0, decimal_places=2, max_digits=20)
    del_address_code = models.IntegerField(null=True, blank=True)
    del_address_user_code = models.IntegerField(null=True, blank=True)
    partial_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)

    class Meta:
        app_label = 'services'


class EstimateCart(models.Model):
    user_code = models.CharField(max_length=25)
    estimated_cost = models.DecimalField(
        null=True, blank=True, max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        super(EstimateCart, self).save(*args, **kwargs)

    class Meta:
        app_label = 'services'


class CartItems(models.Model):
    cart = models.ForeignKey(EstimateCart)
    item = models.ForeignKey(CategoryItems)
    quantity = models.PositiveIntegerField(default=1)

    class Meta:
        app_label = 'services'


class OrderLogs(models.Model):
    created_at = models.DateTimeField(editable=False)
    request_json = models.TextField(null=True, blank=True)
    successful = models.BooleanField(default=0)
    response_json = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'services'

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        super(OrderLogs, self).save(*args, **kwargs)
