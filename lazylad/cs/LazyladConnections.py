from django.db import connections
from cs.utils import print_exception


class Coupons:

    def __init__(self):
        self.ncomdb = connections['lazylad_server'].cursor()

    def save_coupon_clicks(self, data):
        response = dict()
        try:
            clicks = data['click_logs']
            unsuccessful_saves = list()
            successful_saves = list()
            for click in clicks:
                query = "select COUNT(*) from cs_coupons " \
                        "where coupon_id = " + str(click["coupon_id"])
                self.ncomdb.execute(query)
                if int(self.ncomdb.fetchone()[0]) > 0:
                    query = "select COUNT(*) from cs_coupon_analytics " \
                        "where coupon_id = " + str(click["coupon_id"])
                    self.ncomdb.execute(query)
                    if int(self.ncomdb.fetchone()[0]) > 0:
                        query = "update cs_coupon_analytics " \
                                "set clicks = clicks + "+str(click["counters"])\
                                + " where coupon_id = " + str(click["coupon_id"])
                        self.ncomdb.execute(query)
                        query = "insert into cs_coupon_clicks_log " \
                                "(coupon_id, user_code, clicks) VALUES " \
                                "("+str(click["coupon_id"])+"," +\
                                str(click["user_code"])+"," +\
                                str(click["counters"]) + ")"
                        self.ncomdb.execute(query)
                    else:
                        query = "insert into cs_coupon_analytics " \
                                "(coupon_id, clicks)VALUES("+str(click["coupon_id"]) +\
                                ", "+str(click["counters"])+")"
                        self.ncomdb.execute(query)
                        query = "insert into cs_coupon_clicks_log " \
                                "(coupon_id, user_code, clicks) VALUES " \
                                "("+str(click["coupon_id"])+", " +\
                                str(click["user_code"])+", " +\
                                str(click["counters"])+")"
                        self.ncomdb.execute(query)
                    successful_saves.append(click)
                else:
                    unsuccessful_saves.append(click)
            response["success"] = True
            response["successful_saves"] = successful_saves
            response["unsuccessful_saves"] = unsuccessful_saves
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def save_coupon_views(self, data):
        response = dict()
        try:
            views = data['view_logs']
            unsuccessful_saves = list()
            successful_saves = list()
            for view in views:
                query = "select COUNT(*) from cs_coupons " \
                        "where coupon_id = " + str(view["coupon_id"])
                self.ncomdb.execute(query)
                if int(self.ncomdb.fetchone()[0]) > 0:
                    query = "select COUNT(*) from cs_coupon_analytics " \
                        "where coupon_id = " + str(view["coupon_id"])
                    self.ncomdb.execute(query)
                    if int(self.ncomdb.fetchone()[0]) > 0:
                        query = "update cs_coupon_analytics " \
                                "set views = views + "+str(view["counters"])\
                                + " where coupon_id = " + str(view["coupon_id"])
                        self.ncomdb.execute(query)
                        query = "insert into cs_coupon_views_log " \
                                "(coupon_id, user_code, views) VALUES " \
                                "("+str(view["coupon_id"])+"," +\
                                str(view["user_code"])+"," +\
                                str(view["counters"])+")"
                        self.ncomdb.execute(query)
                    else:
                        query = "insert into cs_coupon_analytics " \
                                "(coupon_id, views) VALUES ("+str(view["coupon_id"]) +\
                                ","+str(view["counters"])+")"
                        self.ncomdb.execute(query)
                        query = "insert into cs_coupon_views_log " \
                                "(coupon_id, user_code, views) VALUES " \
                                "("+str(view["coupon_id"])+"," +\
                                str(view["user_code"])+"," +\
                                str(view["counters"])+")"
                        self.ncomdb.execute(query)
                    successful_saves.append(view)
                else:
                    unsuccessful_saves.append(view)
            response["success"] = True
            response["successful_saves"] = successful_saves
            response["unsuccessful_saves"] = unsuccessful_saves

        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def get_all_coupons(self, city_code):
        response = dict()
        try:
            if bool(city_code):
                query = "SELECT coupon_id FROM cs_coupon_city" \
                        " WHERE city_code = " + str(city_code)
                self.ncomdb.execute(query)
                coupon_ids_list = [str(id[0]) for id in self.ncomdb.fetchall()]
                if len(coupon_ids_list) > 0:
                    coupon_ids = ",".join(coupon_ids_list)
                    query = "SELECT * FROM  cs_coupons WHERE coupon_id in" \
                            " ("+str(coupon_ids)+") AND active = 1"
                    self.ncomdb.execute(query)
                    columns = [column[0] for column in self.ncomdb.description]
                    rows = self.ncomdb.fetchall()
                    all_coupons = []
                    for row in rows:
                        all_coupons.append(dict(zip(columns, row)))
                    response["success"] = True
                    response["coupons"] = all_coupons
                    response["coupon_exist_in_city"] = True
                else:
                    query = "select image_flag, image_address " \
                            "from cs_default_img_city " \
                            "where city_code = " + str(city_code)
                    self.ncomdb.execute(query)
                    row = self.ncomdb.fetchone()
                    if not row:
                        row = ["1", "DEFAULT IMAGE FOR ALL CITIES"]
                    response["success"] = True
                    response["coupons"] = [{"coupon_name":
                                                "Default Coupon",
                                            "coupon_img_add":
                                                row[1],
                                            "coupon_img_flag": int(row[0])
                                            }]
                    response["coupon_exist_in_city"] = False
            else:
                query = "SELECT * FROM  cs_coupons WHERE active = 1"
                self.ncomdb.execute(query)
                columns = [column[0] for column in self.ncomdb.description]
                rows = self.ncomdb.fetchall()
                all_coupons = []
                for row in rows:
                    all_coupons.append(dict(zip(columns, row)))
                response["success"] = True
                response["coupons"] = all_coupons
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response
