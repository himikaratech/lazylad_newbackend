__author__ = 'AnkitAtreja'
import utils
import operator

def getSellersListinLocation(lat, long, city_code, service_category):
    seller_delivery_data = utils.getSellersDeliveryInfo(city_code, service_category)
    groupGeofence = utils.prepareGroupedGeoFenceforSellers(seller_delivery_data)

    sellersList = groupGeofence.getValidKeys(point=(lat,long))
    if len(sellersList) == 0:
        sellersList = utils.getLazyladSellersList(city_code, service_category)
        return sellersList

    sortedSellersList = sortSellerBasedonDistance(sellersList, seller_delivery_data, lat, long)
    return sortedSellersList

def sortSellerBasedonDistance(sellersList, seller_delivery_data, lat, long):
    sellersListwithDistance = [(sel[0],\
                                utils.GeoFence.great_circle_distance(latlong_a=(sel[1],sel[2]), latlong_b=(lat,long)))\
                               for sel in seller_delivery_data if sel[0] in sellersList]
    sortedsellersListwithDistance = sorted(sellersListwithDistance, key=operator.itemgetter(1))

    sortedSellersList = [sel[0] for sel in sortedsellersListwithDistance]

    return sortedSellersList
