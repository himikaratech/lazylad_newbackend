from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from .apiviews import SendSMS, BulkOrder, orderDetails, \
    orderDetailsbyCode, changeSeller, Coupon, Coupons, LazyPrime
from . import apiviews

api_urlpatterns = patterns(
    'oms.apiviews',
    url(
        r'^send_sms',
        SendSMS.as_view({'post': 'send_sms'}),
        name='send-sms'),
    url(
        r'^placeBulkOrderFromCart',
        BulkOrder.as_view({'post': 'place_bulk_order'}),
        name='bulk_order'),
    url(
        r'^update_logistics',
        BulkOrder.as_view({'post': 'update_logistics'}),
        name='update_logistics'),
    url(
        r'^order_offer_diff',
        BulkOrder.as_view({'post': 'order_offer_diff'}),
        name='order_offer_diff'),
    url(
        r'^checkCouponCodeValidityFinal',
        Coupon.as_view({'post': 'check_coupon_code_validity_final'}),
        name='check_coupon_code_validity_final'),
    url(
        r'^getOrderDetails$',
        orderDetails.as_view({'get': 'get_order_details'}),
        name='get_order_details'),
    url(
        r'^getOrderDetailsByOrderCode$',
        orderDetailsbyCode.as_view({'get': 'get_order_details_by_order_code'}),
        name='get_order_details_by_order_code'),
    url(
        r'^changeSellerOfOrder$',
        changeSeller.as_view({'get': 'change_seller_of_order_get', 'post':'change_seller_of_order_post'}),
        name='change_seller_of_order'),
    url(
        r'^createCoupons$',
        Coupons.as_view({'get': 'get_create_coupons', 'post': 'post_created_coupons'}),
        name='Coupons_urls'),
    url(
        r'^get_prime_orders',
        LazyPrime.as_view({'get': 'lazyprime_orders'}),
        name='lazy_prime_orders'),
    url(
        r'^lpm_orders',
        LazyPrime.as_view({'post': 'lpm_orders'}),
        name='lpm_orders')

)

urlpatterns = format_suffix_patterns(api_urlpatterns)

urlpatterns.append(
    url(r'^checkSellerCompatibility$', apiviews.check_seller_compatibility, name='check_seller_compatibility'),
)
urlpatterns.append(
    url(r'^add_item_to_seller$', apiviews.add_item_to_seller, name='add_item_to_seller'),
)

