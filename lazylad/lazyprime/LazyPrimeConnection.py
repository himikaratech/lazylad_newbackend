
from django.db import connections
import datetime
import json
from decimal import Decimal
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from decimal import Decimal
from .models import *
from .utils import generate_user_code, generate_order_code,\
    generate_order_line, email_vendor, print_exception
from django.db.models import F


class LazyPrime:

    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()
        pass

    def add_user(self, user_dict):
        response = {
            'success': False,
            'message': None
        }
        try:
            user_code = generate_user_code()
            user = User(
                username=user_dict.get('username'),
                password=user_dict.get('password'),
                phone_number=user_dict.get('phone_number'),
                user_code=user_code,
                city=UserCity.objects.get(id=1)
            )
            if user_dict.get('origin_ref'):
                user.origin_ref = user_dict.get('origin_ref')
            if user_dict.get('origin'):
                user.origin = user_dict.get('origin')
            user.save()
            response['message'] = 'User Successfully Added'
            response['success'] = True
        except ValidationError:
            response['message'] = 'User Cant be added with these fields'
        except IntegrityError:
            response['message'] = 'User Already Exists'
        except:
            response['message'] = 'Unexpected Error In database transaction'
        return response

    def user_login(self, user_dict):
        response = {
            'success': False,
            'message': None
        }
        try:
            user = User.objects.get(
                username=user_dict.get('username')
            )
            if user_dict.get('password') == user.password:
                response['success'] = True
                response['message'] = 'Login Successful'
                response['user_code'] = user.user_code
            else:
                response['message'] = 'Incorrect Password'
                response['success'] = True
        except User.DoesNotExist:
            response['message'] = 'Invalid Username'
        except:
            response['message'] = 'Unexpected Error In database transaction'
        return response

    def get_items(self, data):
        response = {
            'success': False,
            'message': None
        }
        items_list = None
        try:
            if data.get('prev_item_code') and \
                    (not data.get('prev_item_code') == '0'):
                prev_item_id = Item.objects.get(
                   item_code=data.get('prev_item_code')).id
                items_list = Item.objects.filter(
                    id__gte=prev_item_id,
                    st_code=data.get('st_code'),
                    sc_code=data.get('sc_code')).order_by('id')[:20].values()
            else:
                items_list = Item.objects.filter(
                    st_code=data.get('st_code'),
                    sc_code=data.get('sc_code')).order_by('id')[:20].values()
            response['success'] = True
            response['items_list'] = items_list
        except (Item.DoesNotExist, AttributeError):
            items_list = Item.objects.filter(
                st_code=data.get('st_code')).values()
        response['items_list'] = items_list
        return response

    def post_order_POS(self, data):
        response = {
            'success': False,
            'message': None
        }
        user = None
        city = None
        try:
            city = UserCity.objects.get(
                name=data.get('city_name'))
            user = User.objects.get(
                origin='LPOS', origin_ref=data.get('sp_code'))
        except UserCity.DoesNotExist:
            response['message'] = 'Invalid City Name'
            return response
        except User.DoesNotExist:
            res = self.add_user({
                'username': data.get('name'),
                'password': data.get('name'),
                'phone_number': data.get('phone_number'),
                'address': data.get('address'),
                'origin_ref': data.get('sp_code'),
                'city_id': city.id,
                'origin': 'LPOS'
            })
            self.add_address({}, res.get('user_code'), city.id)
            user = User.objects.get(
                origin='LPOS', origin_ref=data.get('sp_code'))
        except:
            response['message'] = print_exception()
            return response
        data['city_code'] = city.id
        data['user_code'] = user.user_code
        data['address_key'] = 1
        resp = self.post_order(data)
        if resp.get('success'):
            response['success'] = True
            response['order_code'] = resp.get('order_code')
            response['total_amount'] = resp.get('total_amount')
            response['address'] = resp.get('address')
        else:
            resp['message'] = resp.get('message')
        return response

    def post_order(self, data):
        response = {
            'success': False,
            'message': None
        }
        vendor = PrimeVendor()
        order_lines = list()
        order_dict = data.get('order')
        items_list = data.get('items_list')
        user_code = data.get('user_code')
        city_code = data.get('city_code')
        orders_list = data.get('orders_list')
        if not city_code:
            city_code = 1
        try:
            user = User.objects.get(user_code=user_code)
            city = UserCity.objects.get(id=city_code)
            address = Addresses.objects.get(
                user_code=user_code,
                key=data.get('address_key')
            )

            order_code = generate_order_code()
            order = Order(
                order_code=order_code,
                user_code=user,
                items_amount=Decimal(order_dict.get(
                    'items_amount', '0.00')),
                amount_payable=Decimal(order_dict.get(
                    'amount_payable', '0.00')),
                delivery_cost=Decimal(order_dict.get(
                    'delivery_cost', '0.00')),
                discount=Decimal(order_dict.get('discount', '0.00')),
                taxes=Decimal(order_dict.get('taxes', '0.00')),
                tax_percentage=Decimal(order_dict.get(
                    'tax_percentage', '0.00')),
                coupon_code=order_dict.get('coupon_code'),
                address=address
            )
            order.save()

            for item_dict in items_list:
                try:
                    item = Item.objects.get(
                        item_code=item_dict.get('item_code')
                    )
                    order_items = OrderItems(
                        item_code=item,
                        order_number=order,
                        item_cost=Decimal(item_dict.get('item_cost')),
                        item_quantity=int(item_dict.get('item_quantity')),
                    )
                    order_items.save()
                except Item.DoesNotExist:
                    pass
                except (ValidationError, IntegrityError, AttributeError):
                    pass

            ol = None
            for ordr in orders_list:
                ol = vendor.post_order_line(ordr, order, address)
                if ol.get('success'):
                    order_lines.append({
                        'vendor': ol.get('vendor'),
                        'total_amount': ol.get('total_amount'),
                        'order_code': ol.get('order_line')
                    })

            response['order_code'] = order_code
            response['address'] = address.key
            response['sub_orders'] = ol
            response['total_amount'] = order_dict.get(
                'amount_payable', '0.00')
            response['success'] = True
        except (ValidationError, IntegrityError):
            response['message'] = 'Error In Processing Address'
            return response

        except User.DoesNotExist:
            response['message'] = 'Invalid User'
        except Addresses.DoesNotExist:
            response['message'] = 'Invalid Address'
        return response

    def get_service_categories(self):
        try:
            response = dict()
            st_code = int(6)
            query = "SELECT st.st_name, stc.sc_name," \
                    " stc.sc_code FROM service_types st,\
                    service_type_categories stc WHERE" \
                    " st.st_code = "+str(st_code)+\
                    " AND stc.st_code = st.st_code"
            rows_fetched = self.cursor.execute(query)
            if rows_fetched:
                rows = self.cursor.fetchall()
                service_categories = list()
                for row in rows:
                    service_category = dict()
                    service_category['sc_code'] = row[2]
                    service_category['sc_name'] = row[1]
                    st_name = str(row[0])
                    service_categories.append(service_category)
            types = list()
            service_type = dict()
            service_type['st_name'] = st_name
            service_type['st_code'] = int(6)
            service_type['categories'] = service_categories
            types.append(service_type)
            response["success"] = True
            response["types"] = types
        except TypeError:
            response["error"] = 1
            response["message"] = "Type Error : " + print_exception()
            response["success"] = False
        except Exception:
            response["error"] = 1
            response["message"] = "Exception Error : " + print_exception()
            response["success"] = False
        except:
            response["error"] = 1
            response["message"] = print_exception()
            response["success"] = False
        return response

    def get_service_types_categories(self):
        response = {
            'success': False,
            'message': None,
            'return_list': list(),
        }
        connect = LazyladConnection()
        try:
            st_codes = Item.objects.all().values_list(
                'st_code', flat=True).distinct()
            for st_code in st_codes:
                temp_dict = dict()
                temp_dict['st_code'] = st_code
                temp_dict['st_name'] = connect.get_service_type_name(st_code)
                temp_dict['categories'] = list()
                sc_codes = Item.objects.filter(st_code=st_code).values_list(
                    'sc_code', flat=True).distinct()
                for sc_code in sc_codes:
                    temp2_dict = dict()
                    temp2_dict['sc_code'] = sc_code
                    temp2_dict['sc_name'] = connect.get_service_category_name(
                        st_code, sc_code)
                    temp_dict['categories'].append(temp2_dict)
                response['return_list'].append(temp_dict)
                response['success'] = True
        except:
            response["message"] = print_exception()
        return response

    def add_address(self, address_dict, user_code, city_id):
        response = {
            'success': False,
            'message': None,
            'address_key': None,
        }
        try:
            user = User.objects.get(
                user_code=user_code
            )
            key = Addresses.objects.filter(user_code=user_code).count() + 1
            try:
                city = UserCity.objects.get(id=city_id)
            except UserCity.DoesNotExist:
                city = UserCity.objects.first()
            address = Addresses(
                city=city,
                user_code=user_code,
                key=key,
                flat=address_dict.get('flat'),
                subarea=address_dict.get('subarea'),
                area=address_dict.get('area'),
                pincode=address_dict.get('pincode',''),
                latitude=address_dict.get('latitude'),
                longitude=address_dict.get('longitude'),
            )
            address.save()
            response['address_key'] = key
            response['address_id'] = address.id
            response['user_code'] = user_code
            response['success'] = True
        except User.DoesNotExist:
            response['message'] = 'Invalid Username'
        except (IntegrityError, ValidationError):
            response['message'] = 'Address Fields Incorrect'
        except:
            response['message'] = 'Unexpected Error In database transaction'
        return response

    def get_addresses(self, user_code):
        response = {
            'success': False,
            'message': None,
            'addresses': None,
        }
        try:
            addresses = Addresses.objects.filter(
                user_code=user_code).select_related(
                'city', 'user_code').values(
                'city', 'user_code',
                'key', 'flat', 'subarea', 'area',
                'pincode', 'latitude', 'longitude'
            )
            if addresses.count() > 0:
                response['success'] = True
                response['addresses'] = addresses

        except:
            response['message'] = 'Unexpected Error'
        return response

    def get_address(self, user_code, address_id):
        response = {
            'success': False,
            'message': None,
            'addresses': None,
        }
        try:
            addresses = Addresses.objects.filter(
                user_code=user_code.user_code,
                key=int(address_id)).select_related(
                'city').values(
                'city_id', 'key', 'flat',
                'subarea', 'pincode',
                'latitude', 'longitude')
            response['addresses'] = addresses[0]
        except:
            response['message'] = 'Unexpected Error'
        return response

    def confirm_order(self, order_code):
        response = {
            'success': False,
            'message': None,
        }
        try:
            order = Order.objects.get(order_code=order_code)
            if order.status == 'PEN':
                order.status = 'CNF'
                order.save()
                response['success'] = True
            else:
                response['message'] = 'Already Confirmed'
        except Order.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def view_order(self, order_code):
        response = {
            'success': False,
            'message': None,
            'order': None
        }
        order_dict = dict()
        try:
            order = Order.objects.get(order_code=order_code)
            order_dict.update(dict(
                (name, getattr(
                    order, name)) for name in
                order.__dict__ if not name.startswith('_')))
            order_items = OrderItems.objects.filter(
                order_number__order_code=order_code).annotate(
                St_Code=F('item_code__st_code'),
                Sc_Code=F('item_code__sc_code'),
                Item_Name=F('item_code__item_name'),
                Item_Type_Code=F('item_code__item_type_code'),
                Item_Unit=F('item_code__item_unit'),
                Item_Barcode=F('item_code__item_barcode'),
                Short_Description=F('item_code__item_short_desc'),
                Description=F('item_code__item_desc'),
                Item_Code=F('item_code'),
                ).select_related(
                'item_code', 'order_number').values(
                'Item_Code', 'item_cost', 'item_quantity',
                'St_Code', 'Sc_Code',
                'Item_Name', 'Item_Type_Code',
                'Item_Unit', 'Item_Barcode',
                'Short_Description', 'Description',
            )
            order_dict.update({'items': order_items})
            response['order'] = order_dict
            response['success'] = True
        except Order.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def vendor_accounts(self, data):
        response = {
            'success': False,
            'message': None,
            'order': None
        }
        try:
            orders = Orderline.objects.filter(
                order__user_code__user_code=data.get('user_code'),
                payment_status=data.get('status')
            ).values('amount_payable', 'vendor__vendor_name')
            response['orders'] = orders
            response['success'] = True
        except Order.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except:
            response['message'] = 'Unexpected Error'
        return response


class PrimeVendor:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()
        pass

    def vendor_login(self, data):
        response = {
            'success': False,
            'message': None
        }
        try:
            vendor = Vendor.objects.get(vendor_name=data.get('username'))
            if data.get('password') == vendor.password:
                response['success'] = True
                response['vendor_id'] = vendor.id
                response['message'] = 'Login Successful'
            else:
                response['message'] = 'Incorrect Password'
        except Vendor.DoesNotExist:
            response['message'] = 'Invalid Username'
        except AttributeError:
            response['success'] = True
        except:
            response['message'] = print_exception()
        return response

    def view_order(self, data):
        response = {
            'success': False,
            'message': None,
            'order': None
        }
        order_dict = dict()
        try:
            order = Orderline.objects.get(order_line=data.get('order_code'))
            order_dict.update(dict(
                (name, getattr(
                    order, name)) for name in
                order.__dict__ if not name.startswith('_')))
            order_items = OrderLineItems.objects.filter(
                order_line__order_line=data.get('order_code')).annotate(
                st_code=F('item_code__st_code'),
                sc_code=F('item_code__sc_code'),
                item_name=F('item_code__item_name'),
                item_type_code=F('item_code__item_type_code'),
                item_unit=F('item_code__item_unit'),
                item_barcode=F('item_code__item_barcode'),
                short_description=F('item_code__item_short_desc'),
                description=F('item_code__item_desc'),
                ).select_related(
                'item_code', 'order_number').values(
                'item_code', 'item_cost', 'item_quantity',
                'st_code', 'sc_code',
                'item_name', 'item_type_code',
                'item_unit', 'item_barcode',
                'short_description', 'description',
            )
            logistics = VendorLogistics.objects.filter(
                city__id=order.order.user_code.city.id
            ).values('name', 'phone_no', 'address', 'email', 'id')
            order_dict.update({'items': order_items})
            order_dict.update({'logistics': logistics})
            order_dict.update({'user_address': self.get_order_line_address(
                data.get('order_code'))})
            response['order'] = order_dict
            response['success'] = True
        except Order.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def get_order_line_address(self, order_code):
        try:
            user_code = Orderline.objects.get(
                order_line=order_code).order.user_code.user_code
            if user_code:
                address = Addresses.objects.filter(
                    user_code=user_code).values()[0]
                user = User.objects.get(user_code=user_code)
                address.update({
                    'username': user.username,
                    'user_phone': user.phone_number,
                })
                return address
        except:
            pass
        return 'Address Not Found'

    def choose_logistics(self, city):
        try:
            logistics = VendorLogistics.objects.filter(city=city)
            return logistics[0]
        except:
            pass
        return VendorLogistics.objects.first()

    def get_orders(self, data):
        response = {
            'success': False,
            'message': None,
            'order_list': None
        }
        user_code = None
        if not data.get('status'):
            data['status'] = 'PEN'
        try:
            if data.get('prev_order_code') and \
                    (not data.get('prev_order_code') == '0'):
                prev_order_id = Orderline.objects.get(
                   orderline=data.get('prev_order_code')).id
                order_list = Orderline.objects.filter(
                    id__gte=prev_order_id,
                    status=data.get('status'),
                    vendor__vendor_id=int(data.get('vendor_id'))
                ).order_by('id').extra(
                    select={'date_str': 'CAST(post_time AS CHAR)'}
                )[:20].values(
                    'order_line', 'order__order_code',
                    'items_amount', 'delivery_cost',
                    'discount', 'taxes', 'amount_payable',
                    'status', 'date_str'
                )
            else:
                order_list = Orderline.objects.filter(
                    vendor__id=int(data.get('vendor_id')),
                    status=data.get('status'),
                ).order_by('id').extra(
                    select={'date_str': 'CAST(post_time AS CHAR)'}
                )[:20].values(
                    'order_line', 'order__order_code',
                    'items_amount', 'delivery_cost',
                    'discount', 'taxes', 'amount_payable',
                    'status', 'date_str'
                )
            response['success'] = True
            response['order_list'] = order_list
        except (Item.DoesNotExist, AttributeError):
            order_list = Item.objects.filter(
                st_code=data.get('st_code')).values()
        response['order_list'] = order_list
        return response

    def get_brands(self, data):
        response = {
            'success': False,
            'message': None,
            'brands': None
        }
        try:
            brands = Brand.objects.all().values(
                'brand_name', 'image_address'
            )
            response['brands'] = brands
            response['success'] = True
        except:
            response['message'] = print_exception()
        return response

    def get_items(self, data):
        response = {
            'success': False,
            'message': None,
            'items_list': None
        }
        items_list = None
        try:
            if data.get('prev_item_code') and \
                    (not data.get('prev_item_code') == '0'):
                prev_item_id = Item.objects.get(
                   item_code=data.get('prev_item_code')).id
                items_list = Item.objects.filter(
                    id__gte=prev_item_id,
                    brand_name=data.get('brand_name')
                ).order_by('id')[:20].values()
            else:
                items_list = Item.objects.filter(
                   brand_name=data.get('brand_name')).order_by('id')[:20].values()
            response['success'] = True
            response['items_list'] = items_list
        except (Item.DoesNotExist, AttributeError):
            items_list = Item.objects.filter(
                 brand_name=data.get('brand_name')).values()
        response['items_list'] = items_list
        return response

    def post_order_line(self, data, order, address):
        response = {
            'success': False,
            'message': None
        }
        order_dict = data.get('order')
        items_list = data.get('items_list')
        try:
            vendor = Vendor.objects.get(brand__brand_name=data.get('brand_name'))
            order_line_code = generate_order_line()
            order_line = Orderline(
                order_line=order_line_code,
                order=order,
                vendor=vendor,
                items_amount=Decimal(order_dict.get(
                    'items_amount', '0.00')),
                amount_payable=Decimal(order_dict.get(
                    'amount_payable', '0.00')),
                delivery_cost=Decimal(order_dict.get(
                    'delivery_cost', '0.00')),
                discount=Decimal(order_dict.get('discount', '0.00')),
                taxes=Decimal(order_dict.get('taxes', '0.00')),
                tax_percentage=Decimal(order_dict.get(
                    'tax_percentage', '0.00')),
            )
            order_line.save()

            for item_dict in items_list:
                try:
                    item = Item.objects.get(
                        item_code=item_dict.get('item_code')
                    )
                    order_items = OrderLineItems(
                        item_code=item,
                        order_line=order_line,
                        item_cost=Decimal(item_dict.get('item_cost')),
                        item_quantity=int(item_dict.get('item_quantity')),
                    )
                    order_items.save()
                except Item.DoesNotExist:
                    pass
                except (ValidationError, IntegrityError, AttributeError):
                    pass

            response['order_line'] = order_line_code
            response['vendor'] = vendor.vendor_name
            response['total_amount'] = order_dict.get(
                'amount_payable', '0.00')
            response['success'] = True
        except (ValidationError, IntegrityError):
            response['message'] = 'Error In Processing Address'
            return response

        except User.DoesNotExist:
            response['message'] = 'Invalid User'
        except Addresses.DoesNotExist:
            response['message'] = 'Invalid Address'
        return response

    def cnf_order(self, data):
        response = {
            'success': False,
            'message': None,
        }
        try:
            order = Orderline.objects.get(order_line=data.get('order_code'))
            vendor = order.vendor
            address = order.order.address
            logistics = VendorLogistics.objects.get(id=data.get('logistics_id'))
            order.logistics_support = logistics.id
            order.save()
            try:
                to_email = VendorLogistics.objects.get(
                    id=order.logistics_support).email
            except:
                to_email = 'support@lazylad.com'

            if order.status == 'PEN':
                order.status = 'CNF'
                order.save()
                response['success'] = True
                try:
                    email_text = 'New Pickup From Lazylad \n\n' \
                                 'Order Code : {0} \n\n' \
                                 'Payment : {1} \n\n' \
                                 'Pickup : {2} \n\n' \
                                 'Drop: {3} \n\n'.format(
                                    order.order_line, order.amount_payable,
                                    str(vendor.vendor_name) + '\n' +
                                    '\t' + str(vendor.address) + '\n' +
                                    '\t' + str(vendor.phone_number),
                                    str(order.user_code.username) +'\n' +
                                    '\t' + str(address.flat) +'\n' +
                                    '\t' + str(address.area) +'\n' +
                                    '\t' + str(address.subarea) +'\n' +
                                    '\t' + str(address.pincode) +'\n' +
                                    '\t' + str(order.user_code.city.name) + '\n' +
                                    '\t' + str(order.user_code.phone_number)
                                )
                    email_vendor(email_text, to_email)
                except:
                    pass
            else:
                response['message'] = 'Already Confirmed'
        except Orderline.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except:
            response['message'] = 'Unexpected Error'
        return response

    def pay_order(self, data):
        response = {
            'success': False,
            'message': None,
        }
        try:
            order = Orderline.objects.get(order_line=data.get('order_code'))

            if order.payment_status == 'PEN':
                order.payment_status = 'CNF'
                order.save()
                response['success'] = True
            else:
                response['message'] = 'Already Confirmed'
        except Orderline.DoesNotExist:
            response['message'] = 'Invalid Order Code'
        except:
            response['message'] = 'Unexpected Error'
        return response


def dictfetchall(cursor):
    " Return all rows from a cursor as a dict "
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def initial_sync_items(self):
        """
        Function To Sync Lazypos DB with Lazylad DB
        All Items From Lazylad Will Be Updated in Lazypos
        """
        response = {
            'success': False,
            'message': None
        }
        try:
            query = 'select * from item_details where st_code=6 order by id'
            self.cursor.execute(query)
            item_list = dictfetchall(self.cursor)
            for item_dict in item_list:
                try:
                    item_saved = Item.objects.get(
                        item_code=item_dict.get('item_code'))
                except Item.DoesNotExist:
                    try:
                        item = Item(
                            item_code=item_dict.get('item_code'),
                            st_code=item_dict.get('st_code'),
                            sc_code=item_dict.get('sc_code'),
                            item_name=item_dict.get('item_name'),
                            item_type_code=item_dict.get('item_type_code'),
                            item_unit=1,
                            item_quantity=1,
                            item_cost=Decimal(item_dict.get('item_cost')),
                            item_barcode=item_dict.get('item_barcode'),
                            item_short_desc=item_dict.get('item_short_desc'),
                            item_desc=item_dict.get('item_desc'),
                        )
                        if item_dict.get('barcode'):
                            item.barcode_verified = True
                        item.save()
                    except ValidationError:
                        pass
                    except IntegrityError:
                        pass
            response['success'] = True
        except:
            response['message'] = 'Error In Database Transaction'
        return response

    def get_service_type_name(self, st_code):
        st_name = None
        try:
            query = 'select st_name from service_types where st_code='+str(st_code)
            self.cursor.execute(query)
            st_name = self.cursor.fetchone()[0]
        except:
            pass
        return st_name

    def get_service_category_name(self, st_code, sc_code):
        sc_name = None
        try:
            query = 'select sc_name from service_type_categories where st_code=' \
                    ''+str(st_code)+' and sc_code='+str(sc_code)
            self.cursor.execute(query)
            sc_name = self.cursor.fetchone()[0]
        except:
            pass
        return sc_name

