from django.db import connections


def getOrderCodeFromOrderString(order_string):
    order_code = None
    try:
        cursor = connections['lazylad_server'].cursor()
        query = "select od.order_code from order_strings os, order_details od " \
                "where os.o_id = od.id and os.order_string = '" \
                + str(order_string) + "'"
        rows_fetched = cursor.execute(query)
        if rows_fetched:
            order_code = cursor.fetchone()[0]
    except:
        pass
    return order_code


def dictfetchall(cursor):
    " Return all rows from a cursor as a dict "
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]