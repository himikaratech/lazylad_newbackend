
USER_SOURCE = (('AD', 'lazyad'),
               ('LAD', 'lazylad'))

REDEEM_TRANSACTION_TYPE = (('EXP', 'Coupon Expired'),
                           ('AR', 'Already Redeemed'),
                           ('SUC', 'Success'))
