from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import Rider, Orders

api_urlpatterns = patterns(
    'logistics.apiviews',
    url(
        r'^add_rider',
        Rider.as_view({'post': 'add_rider'}),
        name='add_rider'),
    url(
        r'^get_riders_list',
        Rider.as_view({'get': 'get_riders_list'}),
        name='get_riders_list'),
    url(
        r'^rider_login',
        Rider.as_view({'post': 'rider_login'}),
        name='rider_login'),
    url(
        r'^rider_checkin',
        Rider.as_view({'get': 'rider_checkin'}),
        name='rider_checkin'),
    url(
        r'^rider_checkout',
        Rider.as_view({'get': 'rider_checkout'}),
        name='rider_checkout'),
    url(
        r'^rider_edit',
        Rider.as_view({'post': 'rider_edit'}),
        name='rider_edit'),
     url(
        r'^rider_details',
        Rider.as_view({'get': 'rider_details'}),
        name='rider_details'),
    url(
        r'^create_order',
        Orders.as_view({'post': 'create_order'}),
        name='create_order'),
    url(
        r'^order_details',
        Orders.as_view({'get': 'order_details'}),
        name='order_details'),
    url(
        r'^order_specs',
        Orders.as_view({'get': 'order_specs'}),
        name='order_specs'),
    url(
        r'^get_order_status',
        Orders.as_view({'get': 'get_order_status'}),
        name='get_order_status'),
    url(
        r'^get_orders_of_status',
        Orders.as_view({'get': 'get_orders_of_status'}),
        name='get_orders_of_status'),
    url(
        r'^get_all_undelivered_orders',
        Orders.as_view({'get': 'get_all_undelivered_orders'}),
        name='get_all_assigned_orders'),
    url(
        r'^assign_rider',
        Orders.as_view({'post': 'assign_rider'}),
        name='assign_rider'),
    url(
        r'^accept_reject_order',
        Orders.as_view({'post': 'accept_reject_order'}),
        name='accept_reject_order'),
    url(
        r'^order_picked',
        Orders.as_view({'post': 'order_picked'}),
        name='order_picked'),
    url(
        r'^rider_orders',
        Orders.as_view({'get': 'rider_orders'}),
        name='rider_orders'),
    url(
        r'^order_delivered',
        Orders.as_view({'post': 'order_delivered'}),
        name='order_delivered'),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)
