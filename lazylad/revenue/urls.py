from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from .apiviews import *

api_urlpatterns = patterns(
    'revenue.apiviews',
    url(
        r'^merchantDashboardHome$',
        MDashboard.as_view({'get': 'merchant_dashboard_home'}),
        name='merchant_dashboard_home'),
    url(
        r'^allMerchantWallet$',
        MDashboard.as_view({'get': 'all_merchant_wallet'}),
        name='all_merchant_wallet'),
    url(
        r'^settlementRequested$',
        MDashboard.as_view({'get': 'settlement_requested'}),
        name='settlement_requested'),
    url(
        r'^allLazyladWallet$',
        MDashboard.as_view({'get': 'all_lazylad_wallet'}),
        name='all_lazylad_wallet'),
    url(
        r'^settleUpWithMerchant$',
        Settlement.as_view({'get': 'settle_up_merchant'}),
        name='settle_up_merchant'),
    url(
        r'^RequestSettlement$',
        Settlement.as_view({'post': 'request_settlement'}),
        name='request_settlement'),
    url(
        r'^RequestBalance$',
        Settlement.as_view({'post': 'request_balance'}),
        name='request_settlement'),
    url(
        r'^cut_settlement_for_all_merchant$',
        Settlement.as_view({'post': 'settle_cut_merchants'}),
        name='cut_settlement_for_all_merchant$'),
    url(
        r'^item_offer_balance',
        Settlement.as_view({'get': 'item_offer_balance'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)
