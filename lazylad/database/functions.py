__author__ = 'AnkitAtreja'

import LazyladConnection

def get_urid_list_original_mapping(urid_visible_list, urid_type):
    connection = LazyladConnection.LazyladConnection()
    urid_list = connection.get_urid_list_original_mapping(
        urid_visible_list, urid_type)
    return urid_list

def get_urid_list_visible_mapping(urid__list, urid_type):
    connection = LazyladConnection.LazyladConnection()
    urid_visible_list = connection.get_urid_list_visible_mapping(
        urid__list, urid_type)
    return urid_visible_list

def get_urid_original_mapping(urid_visible, urid_type):
    connection = LazyladConnection.LazyladConnection()
    urid_list = connection.get_urid_original_mapping(
        urid_visible, urid_type)
    return urid_list

def get_urid_visible_mapping(urid, urid_type):
    connection = LazyladConnection.LazyladConnection()
    urid_visible = connection.get_urid_visible_mapping(
        urid, urid_type)
    return urid_visible