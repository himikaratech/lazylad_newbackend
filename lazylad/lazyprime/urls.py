from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import Prime, Vendor

api_urlpatterns = patterns(
    'lazyprime.apiviews',
    url(
        r'^user_login',
        Prime.as_view({'post': 'user_login'})),
    url(
        r'^add_user',
        Prime.as_view({'post': 'add_user'})),
    url(
        r'^add_address',
        Prime.as_view({'post': 'add_address'})),
    url(
        r'^get_items',
        Prime.as_view({'get': 'get_items'})),
    url(
        r'^get_addresses',
        Prime.as_view({'get': 'get_addresses'})),
    url(
        r'^place_order',
        Prime.as_view({'post': 'place_order'})),
    url(
        r'^pos_to_prime',
        Prime.as_view({'post': 'pos_to_prime'})),
    url(
        r'^initial_sync_items',
        Prime.as_view({'get': 'initial_sync_items'})),
    url(
        r'service_categories',
        Prime.as_view({'post': 'get_service_categories'})),
    url(
        r'^confirm_order',
        Prime.as_view({'post': 'confirm_order'})),
    url(
        r'^vendor_account',
        Prime.as_view({'get': 'vendor_account'})),
    url(
        r'vendor/vendor_login',
        Vendor.as_view({'post': 'vendor_login'})),
    url(
        r'^vendor/get_orders',
        Vendor.as_view({'get': 'get_orders'})),
    url(
        r'^vendor/view_order',
        Vendor.as_view({'get': 'view_order'})),
    url(
        r'^vendor/get_brands',
        Vendor.as_view({'get': 'get_brands'})),
    url(
        r'^vendor/get_items',
        Vendor.as_view({'get': 'get_items'})),
    url(
        r'^vendor/cnf_order',
        Vendor.as_view({'post': 'cnf_order'})),
    url(
        r'^vendor/pay_order',
        Vendor.as_view({'post': 'pay_order'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)
