
import json
import string
import random
from decimal import Decimal
import sys
import linecache

from django.core.mail import send_mail
from lazylad.settings import EMAIL_HOST_USER

from .models import User, Order, Orderline


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_user_code():
    new_user_code = None
    while 1:
        try:
            new_user_code = id_generator()
            user = User.objects.get(user_code=new_user_code)
        except User.DoesNotExist:
            return new_user_code


def generate_order_code():
    new_order_code = None
    while 1:
        try:
            new_order_code = id_generator()
            order = Order.objects.get(user_code=new_order_code)
        except Order.DoesNotExist:
            return new_order_code


def generate_order_line():
    new_order_line = None
    while 1:
        try:
            new_order_line = id_generator()
            order_line = Orderline.objects.get(order_line=new_order_line)
        except Orderline.DoesNotExist:
            return new_order_line


def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    line_no = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, line_no, f.f_globals)
    return str('EXCEPTION IN ({}, LINE {} :: "{}"): {}'.format(
        filename, line_no, line.strip(), exc_obj))


def email_vendor(message, to_email):
    try:
        send_mail('New Order From Lazylad', message,
                  EMAIL_HOST_USER, [to_email], fail_silently=False)
        return True
    except:
        return False