from rest_framework import status
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.viewsets import ViewSet
from django.views.decorators.csrf import csrf_exempt
from lazylad.auth.decorators import *
from oms.utils import print_exception
from django.forms.models import model_to_dict
import database.models as dbmodels
import revenue.models as revenue_models
from django.db import connections
from collections import OrderedDict
from decimal import Decimal
from .serializers import *
from .functions import settle_up_with_merchant, merchant_request_settlement, \
    merchant_request_balance, merchant_cut_settlement, item_offer_balance


class MDashboard(ViewSet):

    def __init__(self):
        self.ncom_db = connections["lazylad_server"].cursor()

    @login_required_vs
    def merchant_dashboard_home(self, request):
        try:
            return render(request, "merchant_home.html")
        except:
            print print_exception()

    @login_required_vs
    def all_merchant_wallet(self, request):
        try:
            wallets = dict((o.owner_id, model_to_dict(o))
                           for o in dbmodels.Wallet.objects.filter(
                            owner_type=1).order_by("owner_id"))
            sp_codes = [sp_code for sp_code in wallets.keys()]
            seller_codes = ",".join(sp_code for sp_code in sp_codes)
            query = "select *,(select city_name from city_details " \
                    "where city_code = sp.sp_city_code) as city_name " \
                    "from service_providers sp where sp_code in (" +\
                    str(seller_codes)+")"
            rows_fetched = self.ncom_db.execute(query)
            if rows_fetched:
                columns = [column[0] for column in self.ncom_db.description]
                rows = self.ncom_db.fetchall()
                for row in rows:
                    temp = dict(zip(columns, row))
                    wallets[temp['sp_code']].update(temp)
            wallets = OrderedDict(sorted(wallets.items(),
                key=lambda t: int(t[1]['balance']), reverse=True))
            context = {
                "wallets": wallets
            }
            return render(request, "merchant_wallets.html", context)
        except:
            print print_exception()

    @login_required_vs
    def settlement_requested(self, request):
        try:
            coupons_requests = revenue_models.RunningAccountCoupons.objects.filter(settlement_requested=1)
            pricediff_requests = revenue_models.RunningAccountPriceDiff.objects.filter(settlement_requested=1)
            wallet_requests = revenue_models.RunningAccountWallet.objects.filter(settlement_requested=1)
            merchants = [str(req.sp_code) for req in coupons_requests] + \
                        [str(req.sp_code) for req in pricediff_requests] + \
                        [str(req.sp_code) for req in wallet_requests]
            merchants_requested = list(set(merchants))
            if len(merchants_requested):
                merchant_codes = ",".join(str(merchant_requested) for merchant_requested in merchants_requested)
                query = "SELECT * , (SELECT city_name FROM city_details " \
                        "WHERE city_code = sp.sp_city_code) AS city_name, " \
                        "ad.account_name, ad.account_number, ad.ifsc_code " \
                        "FROM service_providers sp, account_details ad WHERE " \
                        "sp.sp_code IN ( "+str(merchant_codes)+" ) AND " \
                        "ad.owner_id = sp.sp_code"
                rows_fetched = self.ncom_db.execute(query)
                merchant_details = dict()
                if rows_fetched:
                    columns = [column[0] for column in self.ncom_db.description]
                    rows = self.ncom_db.fetchall()
                    for row in rows:
                        temp = dict(zip(columns, row))
                        merchant_details[temp['sp_code']] = temp
            settlements = dict()
            if len(merchants_requested):
                settlements["settlements_requested"] = True
                settlements["merchants"] = dict()
                for merchant_requested in merchants_requested:
                    settlements["merchants"][merchant_requested] = dict()
                    settlements["merchants"][merchant_requested]["merchant_details"] \
                        = merchant_details[merchant_requested] if \
                        merchant_requested in merchant_details else {}
                    try:
                        coupons_request = coupons_requests.get(sp_code=merchant_requested)
                        temp = dict()
                        temp["coupons_settlement_requested"] = True
                        temp["coupons_settlement_details"] = model_to_dict(coupons_request)
                        settlements["merchants"][merchant_requested].update(temp)
                    except:
                        temp = dict()
                        temp["coupon_settlement_requested"] = False
                        settlements["merchants"][merchant_requested].update(temp)
                        pass
                    try:
                        pricediff_request = pricediff_requests.get(sp_code=merchant_requested)
                        temp = dict()
                        temp["pricediff_settlement_requested"] = True
                        temp["pricediff_settlement_details"] = model_to_dict(pricediff_request)
                        settlements["merchants"][merchant_requested].update()
                    except:
                        temp = dict()
                        temp["pricediff_settlement_requested"] = False
                        settlements["merchants"][merchant_requested].update(temp)
                        pass
                    try:
                        wallet_request = wallet_requests.get(sp_code=merchant_requested)
                        temp = dict()
                        temp["wallet_settlement_requested"] = True
                        temp["wallet_settlement_details"] = model_to_dict(wallet_request)
                        settlements["merchants"][merchant_requested].update(temp)
                    except:
                        temp = dict()
                        temp["wallet_settlement_requested"] = False
                        settlements["merchants"][merchant_requested].update(temp)
                        pass
            else:
                settlements["settlements_requested"] = False
            context = {
                "settlements": settlements
            }
            return render(request, 'requested_settlements.html', context)
        except:
            print print_exception()

    @login_required_vs
    def all_lazylad_wallet(self, request):
        try:
            coupons_accounts = revenue_models.RunningAccountCoupons.objects.all()
            pricediff_accounts = revenue_models.RunningAccountPriceDiff.objects.all()
            wallet_accounts = revenue_models.RunningAccountWallet.objects.all()
            coupons_accounts = dict((coupons_account.sp_code,
                                     model_to_dict(coupons_account))
                                    for coupons_account in coupons_accounts)
            pricediff_accounts = dict((pricediff_account.sp_code,
                                       model_to_dict(pricediff_account))
                                      for pricediff_account in pricediff_accounts)
            wallet_accounts = dict((wallet_account.sp_code,
                                    model_to_dict(wallet_account))
                                   for wallet_account in wallet_accounts)
            merchants = [str(sp_code) for sp_code in coupons_accounts.keys()] +\
                [str(sp_code) for sp_code in pricediff_accounts.keys()] + \
                [str(sp_code) for sp_code in wallet_accounts.keys()]
            all_merchants = list(set(merchants))
            merchant_codes = ",".join(str(merchant) for merchant in all_merchants)
            query = "SELECT * , (SELECT city_name FROM city_details " \
                    "WHERE city_code = sp.sp_city_code) AS city_name, " \
                    "ad.account_name, ad.account_number, ad.ifsc_code " \
                    "FROM service_providers sp, account_details ad WHERE " \
                    "sp.sp_code IN ( "+str(merchant_codes)+" ) AND " \
                    "ad.owner_id = sp.sp_code"
            rows_fetched = self.ncom_db.execute(query)
            if rows_fetched:
                merchant_details = dict()
                columns = [column[0] for column in self.ncom_db.description]
                rows = self.ncom_db.fetchall()
                for row in rows:
                    temp = dict(zip(columns, row))
                    merchant_details[temp['sp_code']] = temp
            wallets = dict()
            for merchant in all_merchants:
                wallets[merchant] = dict()
                wallets[merchant]["balance"] = Decimal(0.00)
                if merchant in merchant_details:
                    wallets[merchant]["merchant_details"] = merchant_details[merchant]
                try:
                    coupons_account = coupons_accounts[merchant]
                    wallets[merchant]["balance"] += coupons_account["unsettled_amount"]
                    wallets[merchant]["coupons_account"] = coupons_account
                except:
                    wallets[merchant]["coupons_account"] = {}
                    pass
                try:
                    pricediff_account = pricediff_accounts[merchant]
                    wallets[merchant]["balance"] += pricediff_account["unsettled_amount"]
                    wallets[merchant]["pricediff_account"] = pricediff_account
                except:
                    wallets[merchant]["pricediff_account"] = {}
                    pass
                try:
                    wallet_account = wallet_accounts[merchant]
                    wallets[merchant]["balance"] += wallet_account["unsettled_amount"]
                    wallets[merchant]["wallet_account"] = wallet_account
                except:
                    wallets[merchant]["coupons_account"] = {}
                    pass
            context = {
                "wallets": wallets
            }
            return render(request, "lazylad_merchants_wallets.html", context)
        except:
            print print_exception()


class Settlement(ViewSet):

    @login_required_vs
    def settle_up_merchant(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        serializer = SettleUpSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if serializer.is_valid():
                response = settle_up_with_merchant(data, request.user.username)
            else:
                response['error'] = 'Bad Input'
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    @login_required_vs
    def settle_cut_merchants(self, request):
        """
        API to settle all lazylad charges account
        :param request:
        :return:
        """
        response = dict()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            if 'QueryDict' in type(data).__name__:
                data = data.dict()
            if data["pass_code"] == '42':
                response = merchant_cut_settlement(request.user.username)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": "Invalid Passcode"})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def request_settlement(self, request):
        """
        API TO REQUEST SETTLEMENT
        :param request:
        {"sp_code":"8888","wallet":"1","coupons":"1","pricediff":"1"}
        :return:
        {"success":True,"wallet_amount":"","coupons_amount":"","pricediff_amount":""}
        """
        response = dict()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = RequestSettlementSerializer(data=data)
            if _serlz.is_valid():
                response = merchant_request_settlement(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def request_balance(self,request):
        """
        API TO REQUEST SETTLEMENT
        :param request:
        {"sp_code":"8888","wallet":"1","coupons":"1","pricediff":"1"}
        :return:
        {"success":True,"wallet_amount":"","coupons_amount":"","pricediff_amount":""}
        """
        response = dict()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = RequestSettlementSerializer(data=data)
            if _serlz.is_valid():
                response = merchant_request_balance(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def item_offer_balance(self,request):
        """
        API TO REQUEST SETTLEMENT
        :param request:
        {"sp_code":"8888","wallet":"1","coupons":"1","pricediff":"1"}
        :return:
        {"success":True,"wallet_amount":"","coupons_amount":"","pricediff_amount":""}
        """
        response = dict()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.GET.copy()
            _serlz = SellerSerializer(data=data)
            if _serlz.is_valid():
                resp = item_offer_balance(data.get('sp_code'))
                if resp :
                    resp['unsettled_amount'] = resp
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)
