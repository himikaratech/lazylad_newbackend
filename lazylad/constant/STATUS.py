SUCCESS = "Success"
PENDING = "Pending"
FAILED = "Failed"

PAYMENT_FAILED = "Payment_Failed"
CANCELLED = "Cancelled"
CONFIRMED = "Confirmed"
DISPATCHED = "Dispatched"
DELIVERED = "Delivered"

REFUNDED = "Refund"
