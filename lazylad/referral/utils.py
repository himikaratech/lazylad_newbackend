
from database.LazyladConnection import LazyladConnection


def referral_referee_description(referrer_type, referrer_id):
    description = 'Referral Bonus'
    referrer_type = int(referrer_type)
    try:
        if referrer_type == 0:
            description += ', Referred by Customer ' + referrer_id
        elif referrer_type == 1:
            description += ', Referred by Seller '
            connection = LazyladConnection()
            seller = connection.get_seller_name(seller_id=referrer_id)
            description += seller
            connection.close_connection()
        elif referrer_type == 2:
            description += ', Referred By Lazylad Sales Executive'
        elif referrer_type == 3:
            description += ', Referred By Lazylad Business Executive'
        return description
    except:
        return description


def referral_referrer_description(referee_type, referee_id):
    description = 'Referral Bonus'
    referee_type = int(referee_type)
    try:
        if referee_type == 0:
            description += ', Referred to Customer ' + referee_id
        else:
            description += ', Referred to Seller '
            connection = LazyladConnection()
            seller = connection.get_seller_name(seller_id=referee_id)
            description += seller
            connection.close_connection()
        return description
    except:
        return description

