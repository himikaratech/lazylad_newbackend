
from rest_framework import fields, serializers


class GetAllCouponsSerializer(serializers.Serializer):
    area_code = serializers.CharField(required=False)
    city_code = serializers.CharField(required=False)
    user_code = serializers.CharField(required=True)


class UserSerializer(serializers.Serializer):
    user_code = serializers.CharField(required=True)


class SyncSerializer(serializers.Serializer):
    log_list = serializers.ListField(
        required=True, child=serializers.DictField())


class UserSignUpSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=True)


class UserSignInSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=True)


class CouponDetailsSerializer(serializers.Serializer):
    coupon_id = serializers.CharField(required=True)


class BuyCouponsSerializer(serializers.Serializer):
    coupon_id = serializers.CharField(required=True)
    user_code = serializers.CharField(required=True)
    city_id = serializers.CharField(required=False)
    area_code = serializers.CharField(required=False)


class CouponCodeDetailsSerializer(serializers.Serializer):
    coupon_code = serializers.CharField(required=True)


class RedeemCouponSerializer(serializers.Serializer):
    coupon_code = serializers.CharField(required=True)
    merchant_code = serializers.CharField(required=True)


class MerchantSignIn(serializers.Serializer):
    phone_number = serializers.CharField(required=True)
    password = serializers.CharField(required=True)


class MerchantCouponVerification(serializers.Serializer):
    coupon_code = serializers.CharField(required=True)
    merchant_code = serializers.CharField(required=True)


class MerchantCoupons(serializers.Serializer):
    merchant_code = serializers.CharField(required=True)


class MerchantCouponDetails(serializers.Serializer):
    coupon_name = serializers.CharField(required=True)
    merchant_code = serializers.CharField(required=True)
    coupon_type = serializers.CharField(required=True)


class MerchantCouponAnalytics(serializers.Serializer):
    coupon_name = serializers.CharField(required=True)
    merchant_code = serializers.CharField(required=True)
