from django.db import models


class RatingModel(models.Model):
    rating_id = models.AutoField(primary_key=True)
    feedback = models.CharField(max_length=200)
    urid = models.CharField(max_length=20)
    urid_type = models.CharField(max_length=20)
    score = models.IntegerField(null=True, blank=True, default=None)
    out_of = models.IntegerField(null=True, blank=True, default=None)
    rating_type = models.CharField(max_length=50)
