from django.db import connections
from cs.utils import print_exception
from datetime import datetime
from .models import OfferItems
from decimal import Decimal


class Items:

    def __init__(self):
        self.ncomdb = connections['lazylad_server'].cursor()

    def save_item_clicks(self, data):

        response = dict()
        try:
            clicks = data['click_logs']
            unsuccessful_saves = list()
            successful_saves = list()
            for click in clicks:
                query_cs_map = "select cs_coupon_id from cs_item_coupons_mapping where item_code = " +\
                                str(click["item_code"])
                self.ncomdb.execute(query_cs_map)
                coupon_id = self.ncomdb.fetchone()

                if int(self.ncomdb.rowcount) > 0:
                    query = "select active from cs_coupons where coupon_id = " + str(coupon_id[0])
                    self.ncomdb.execute(query)
                    active = int(self.ncomdb.fetchone()[0])
                    if active == 1:
                        query = "update cs_item_coupons_mapping set clicks = clicks + " +\
                                str(click["counters"]) + " where item_code = " + str(click["item_code"])
                        self.ncomdb.execute(query)

                query = "select COUNT(*) from item_details " +\
                        "where item_code = " + str(click["item_code"])
                self.ncomdb.execute(query)
                if int(self.ncomdb.fetchone()[0]) == 0:
                    unsuccessful_saves.append(click)
                    continue

                query = "select COUNT(*) from ims_item_analytics " +\
                        "where item_code = " + str(click["item_code"])
                self.ncomdb.execute(query)

                if int(self.ncomdb.fetchone()[0]) > 0:
                    query = "update ims_item_analytics set clicks = clicks + " + str(click["counters"]) +\
                            " where item_code = " + str(click["item_code"])
                    self.ncomdb.execute(query)
                    query = "select COUNT(*) from ims_sp_item_analytics " +\
                        "where item_code = " + str(click["item_code"]) +\
                        " and sp_code = " + str(click["sp_code"])
                    self.ncomdb.execute(query)

                    if int(self.ncomdb.fetchone()[0]) > 0:
                        query = "update ims_sp_item_analytics " \
                                "set clicks = clicks + "+str(click["counters"])\
                                + " where item_code = " + str(click["item_code"])\
                                + " and sp_code = " + str(click["sp_code"])
                        self.ncomdb.execute(query)

                        query = "insert into ims_item_clicks_log " +\
                                "(item_code, user_code, sp_code, clicks, timestamp) VALUES (" \
                                + str(click["item_code"])+"," +\
                                str(click["user_code"])+"," +\
                                str(click["sp_code"]) + "," +\
                                str(click["counters"]) + ",'" +\
                                str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + "')"
                        # import pdb;pdb.set_trace()
                        self.ncomdb.execute(query)

                    else:
                        query = "insert into ims_sp_item_analytics " \
                                "(item_code, sp_code, clicks)VALUES("+str(click["item_code"]) +\
                                ", "+str(click["sp_code"])+","+str(click["counters"])+")"
                        self.ncomdb.execute(query)

                        query = "insert into ims_item_clicks_log " \
                                "(item_code, user_code, sp_code, clicks, timestamp) VALUES (" \
                                + str(click["item_code"])+", " +\
                                str(click["user_code"])+", " +\
                                str(click["sp_code"])+", " +\
                                str(click["counters"])+", '" +\
                                str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + "')"
                        self.ncomdb.execute(query)

                    successful_saves.append(click)
                else:
                    query = "insert into ims_item_analytics (item_code, clicks) " +\
                            "VALUES("+str(click["item_code"]) +\
                            ", "+str(click["counters"])+")"
                    self.ncomdb.execute(query)
                    query = "insert into ims_sp_item_analytics (item_code, sp_code, clicks) " +\
                            "VALUES("+str(click["item_code"])+", "+str(click["sp_code"]) +\
                            ", "+str(click["counters"])+")"
                    self.ncomdb.execute(query)
                    query = "insert into ims_item_clicks_log (item_code, user_code, sp_code, clicks, timestamp)" +\
                            " VALUES("+str(click["item_code"])+", "+str(click["user_code"])+", " +\
                            str(click["sp_code"])+", "+str(click["counters"])+",'"+str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"')"
                    self.ncomdb.execute(query)
                    successful_saves.append(click)

            response["success"] = True
            response["successful_saves"] = successful_saves
            response["unsuccessful_saves"] = unsuccessful_saves
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def save_item_views(self, data):
        response = dict()
        try:
            views = data['view_logs']
            unsuccessful_saves = list()
            successful_saves = list()
            for view in views:

                query_cs_map = "select cs_coupon_id from cs_item_coupons_mapping where item_code = " +\
                                str(view["item_code"])
                self.ncomdb.execute(query_cs_map)
                coupon_id = self.ncomdb.fetchone()

                if int(self.ncomdb.rowcount) > 0:
                    query = "select active from cs_coupons where coupon_id = " + str(coupon_id[0])
                    self.ncomdb.execute(query)
                    active = int(self.ncomdb.fetchone()[0])
                    if active == 1:
                        query = "update cs_item_coupons_mapping set views = views + " +\
                                str(view["counters"]) + " where item_code = " + str(view["item_code"])
                        self.ncomdb.execute(query)

                query = "select COUNT(*) from item_details " +\
                        "where item_code = " + str(view["item_code"])
                self.ncomdb.execute(query)
                if not int(self.ncomdb.fetchone()[0]) > 0:
                    unsuccessful_saves.append(view)
                    continue

                query = "select COUNT(*) from ims_item_analytics " +\
                        "where item_code = " + str(view["item_code"])
                self.ncomdb.execute(query)

                if int(self.ncomdb.fetchone()[0]) > 0:
                    query = "update ims_item_analytics set views = views + " + str(view["counters"]) +\
                            " where item_code = " + str(view["item_code"])
                    self.ncomdb.execute(query)
                    query = "select COUNT(*) from ims_sp_item_analytics " +\
                        "where item_code = " + str(view["item_code"]) +\
                        " and sp_code = " + str(view["sp_code"])
                    self.ncomdb.execute(query)

                    if int(self.ncomdb.fetchone()[0]) > 0:
                        query = "update ims_sp_item_analytics " \
                                "set views = views + "+str(view["counters"])\
                                + " where item_code = " + str(view["item_code"])\
                                + " and sp_code = " + str(view["sp_code"])
                        self.ncomdb.execute(query)

                        query = "insert into ims_item_views_log " +\
                                "(item_code, user_code, sp_code, views, timestamp) VALUES (" \
                                + str(view["item_code"])+"," +\
                                str(view["user_code"])+"," +\
                                str(view["sp_code"]) + "," +\
                                str(view["counters"]) + ",'" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + "')"
                        self.ncomdb.execute(query)

                    else:
                        query = "insert into ims_sp_item_analytics " \
                                "(item_code, sp_code, views)VALUES("+str(view["item_code"]) +\
                                ", "+str(view["sp_code"])+","+str(view["counters"])+")"
                        self.ncomdb.execute(query)

                        query = "insert into ims_item_views_log " \
                                "(item_code, user_code, sp_code, views, timestamp) VALUES (" \
                                + str(view["item_code"])+", " +\
                                str(view["user_code"])+", " +\
                                str(view["sp_code"])+", " +\
                                str(view["counters"])+", '"+str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"')"
                        self.ncomdb.execute(query)
                    successful_saves.append(view)
                else:
                    query = "insert into ims_item_analytics (item_code, views) " +\
                            "VALUES("+str(view["item_code"]) +\
                            ", "+str(view["counters"])+")"
                    self.ncomdb.execute(query)
                    query = "insert into ims_sp_item_analytics (item_code, sp_code, views) " +\
                            "VALUES("+str(view["item_code"])+", "+str(view["sp_code"]) +\
                            ", "+str(view["counters"])+")"
                    self.ncomdb.execute(query)
                    query = "insert into ims_item_views_log (item_code, user_code, sp_code, views, timestamp)" +\
                            " VALUES("+str(view["item_code"])+", "+str(view["user_code"])+", " +\
                            str(view["sp_code"])+", "+str(view["counters"])+",'"+str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+"')"
                    self.ncomdb.execute(query)
                    successful_saves.append(view)

            response["success"] = True
            response["successful_saves"] = successful_saves
            response["unsuccessful_saves"] = unsuccessful_saves
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def offer_item(self, item_dict):
        try:
            offer_item = OfferItems(
                sp_code=item_dict.get('sp_code'),
                user_code=item_dict.get('user_code'),
                order_code=item_dict.get('order_code'),
                item_code=item_dict.get('item_code'),
                item_quantity=int(item_dict.get('item_quantity')),
                item_cost=Decimal(item_dict.get('item_cost')),
                offer_cost=Decimal(item_dict.get('offer_cost')),
                # amount=Decimal(item_dict.get('amount')),
                running_account_id=item_dict.get('running_account_id', 'None'),
                lazy_wallet_trans_id=item_dict.get('lazy_wallet_trans_id'),
            )
            offer_item.amount = Decimal(item_dict.get('amount'))
            offer_item.save()
        except:
            return False
        return True
