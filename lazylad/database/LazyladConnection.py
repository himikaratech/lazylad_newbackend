from django.db import connections

from account.constant import UserType
from models import Wallet

from decimal import Decimal

class LazyladConnection:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def get_owner_id_from_user_code(self, user_code, user_type):
        owner_id = None
        if int(user_type) == UserType.CUSTOMER:
            query = "select phone_number from user_details where user_code=%s"
            self.cursor.execute(query, [user_code])
            data = self.cursor.fetchone()
            owner_id = data[0]
        elif int(user_type) == UserType.SELLER:
            owner_id = user_code
        return owner_id

    def create_and_get_wallet(self, owner_id, owner_type,
                        balance=0, neg_allowed=0,
                        max_neg_balance=0):
        wallet = None
        walletSet = Wallet.objects.filter(owner_id=owner_id, owner_type=owner_type)
        if walletSet.count() > 0:
            wallet = walletSet.first()
        else:
            if int(owner_type) == UserType.CUSTOMER:
                wallet = Wallet(owner_id=owner_id, owner_type=owner_type,
                                balance=balance, neg_allowed=0,
                                max_neg_balance=max_neg_balance)
                wallet.save()
            elif int(owner_type) == UserType.SELLER:
                wallet = Wallet(owner_id=owner_id, owner_type=owner_type,
                                balance=balance, neg_allowed=1,
                                max_neg_balance=max_neg_balance)
                wallet.save()
        if not wallet:
            raise Wallet.DoesNotExist

        return wallet

    def get_wallet_from_user_code(self, user_code, user_type):
        wallet = None
        owner_id = self.get_owner_id_from_user_code(user_code, user_type)
        if owner_id is not None:
            wallet = self.create_and_get_wallet(owner_id=owner_id, owner_type=user_type)

        return wallet

    def get_order_wallet_component(self, order_id):
        query = "select order_tot_amount from order_details where order_code=%s"
        self.cursor.execute(query, [order_id])
        data = self.cursor.fetchone()
        return data[0]

    def get_coupon_details(self, order_id):
        query = "select order_tot_amount from order_details where order_code=%s"
        self.cursor.execute(query, [order_id])
        data = self.cursor.fetchone()
        return data[0]

    def get_order_status(self, order_id):
        query = "select status from order_in_progress where order_code=%s"
        self.cursor.execute(query, [order_id])
        data = self.cursor.fetchone()
        return data[0]

    def get_urid_visible_mapping(self, urid, urid_type):
        if urid_type == "ORDER" :
            query = 'select os.order_string as order_string from order_strings os, order_details od '  \
                    'where os.o_id = od.id and od.order_code = %s'
            self.cursor.execute(query, [urid])
            data = self.cursor.fetchone()
            if data:
                return data[0]
            else:
                return None
        else:
            return urid

    def get_urid_original_mapping(self, urid_visible, urid_type):
        if urid_type == "ORDER" :
            query = 'select od.order_code as order_code from order_strings os, order_details od '  \
                    'where os.o_id = od.id and os.order_string = %s'
            self.cursor.execute(query, [urid_visible])
            data = self.cursor.fetchone()
            if data:
                return data[0]
            else:
                return None
        else:
            return urid_visible

    def get_logistics_order(self, urid):
        try:
            query = 'select logistics_services_used from order_details where order_code = %s'
            self.cursor.execute(query, [urid])
            data = self.cursor.fetchone()
            return data[0]
        except (IndexError, ValueError):
            pass
        return 0

    def update_delivery_charge(self, urid, delivery_charges):
        try:
            query = 'update order_details set order_delivery_cost = '\
                    + str(delivery_charges) + 'where order_code = %s'
            self.cursor.execute(query, [urid])
        except (IndexError, ValueError):
            pass
        return True

    def get_urid_list_visible_mapping(self, urid_list, urid_type):
        if urid_type == "ORDER" :
            if urid_list:
                list_specifier = ','.join(['%s']*len(urid_list))
                query = 'select os.order_string as order_string from order_strings os, order_details od '  \
                        'where os.o_id = od.id and od.order_code in ('+list_specifier+') '
                self.cursor.execute(query, urid_list)
                data = self.cursor.fetchall()
                if data:
                    return [row[0] for row in data]

            return None

        else:
            return urid_list

    def get_urid_list_original_mapping(self, urid_visible_list, urid_type):
        if urid_type == "ORDER" :
            if urid_visible_list:
                list_specifier = ','.join(['%s']*len(urid_visible_list))
                query = 'select od.order_code as order_code from order_strings os, order_details od '  \
                        'where os.o_id = od.id and os.order_string in ('+list_specifier+') '
                self.cursor.execute(query, urid_visible_list)
                data = self.cursor.fetchall()
                if data:
                    return [row[0] for row in data]

            return None

        else:
            return urid_visible_list

    def get_order_cost_details(self, order_id):
        query = 'select order_tot_amount, order_delivery_cost, order_discount_cost, order_total_cost ' \
                'from order_details where order_code=%s'
        self.cursor.execute(query, [order_id])
        data = self.cursor.fetchone()
        return data

    def get_seller_charge_parameter(self, seller_id):
        query = 'select logistics_services from service_providers where sp_code=%s'
        self.cursor.execute(query, [seller_id])
        data = self.cursor.fetchone()
        data = list(data)
        data.extend([0, 1])   # 20 is seller delievery, 1 is price difference
        return data

    def get_seller_name(self, seller_id):
        query = 'select sp_name from service_providers where sp_code=%s'
        self.cursor.execute(query, [seller_id])
        data = self.cursor.fetchone()
        data = data[0]
        return data

    def get_price_difference(self, order_id):
        try:
            query = 'select sum(user_cost*quantity), sum(seller_cost*quantity)' \
                    ' from (select order_item_details.item_cost as user_cost,' \
                    ' order_item_details.item_quantity AS quantity,' \
                    ' order_item_served_by_sp.item_cost as seller_cost' \
                    ' from order_item_details, order_item_served_by_sp where' \
                    ' order_item_details.order_code =' \
                    ' order_item_served_by_sp.order_code' \
                    ' and order_item_details.order_code = %s and' \
                    ' order_item_details.item_code =' \
                    ' order_item_served_by_sp.item_code ) as price_sp_cp '
            self.cursor.execute(query, [order_id])
            data = self.cursor.fetchone()
            user_cost = data[0]
            seller_cost = data[1]
            price_difference = seller_cost - user_cost
            return price_difference
        except:
            return 0

    def get_offer_price_difference(self, order_code):
        response = {
            'success': False,
            'offer_price_list': None
        }
        try:
            return_list = list()
            query = 'select * from order_item_details where order_code' \
                    ' = %s and item_offer_flag = 1'
            self.cursor.execute(query, [order_code])
            item_list = dictfetchall(self.cursor)
            for item_dict in item_list:
                return_list.append({
                    'item_code': item_dict.get('item_code'),
                    'item_quantity': int(item_dict.get('item_quantity')),
                    'item_cost': item_dict.get('item_cost'),
                    'offer_cost': item_dict.get('item_offer_price'),
                    'amount': int(item_dict.get('item_quantity'))*(
                        (item_dict.get('item_cost')) - float(item_dict.get(
                            'item_offer_price'))),
                    'user_code': item_dict.get('user_code'),
                })
            response['offer_price_list'] = return_list
            response['success'] = True
        except:
            pass
        return response
        #     return_list = list()
        #     query = 'select item_code, item_cost, sp_code from' \
        #             ' order_item_served_by_sp where order_code = %s'
        #     self.cursor.execute(query, [order_id])
        #     item_list = dictfetchall(self.cursor)
        #     item_codes_string = ''
        #     cost_dict = dict()
        #     for item in item_list:
        #         item_codes_string += item.get('item_code')
        #         item_codes_string += ' ,'
        #         cost_dict[item.get('item_code')] = item.get('item_cost')
        #     query = 'select item_code, item_cost, item_quantity from' \
        #             ' order_item_details where item_code in ('\
        #             + item_codes_string[:-1] + ' ) and order_code = ' +\
        #             order_id + ' and b_in_offer = 1'
        #     self.cursor.execute(query)
        #     price_list = dictfetchall(self.cursor)
        #     query = 'select item_code, item_name from item_details where' \
        #             ' item_code in ( ' + item_codes_string[:-1] + ' )'
        #     self.cursor.execute(query)
        #     item_name_list = dictfetchall(self.cursor)
        #     item_name_dict = dict()
        #     for item in item_name_list:
        #         item_name_dict[item.get('item_code')] = item.get('item_name')
        #     quant_dict = dict()
        #     for quant in price_list:
        #         quant_dict[quant.get('item_code')] = [quant.get(
        #             'item_quantity'), quant.get('item_cost')]
        #     for price_dict in price_list:
        #         item_code_t = price_dict.get('item_code')
        #         quantity = quant_dict[item_code_t][0]
        #         return_list.append({
        #             'item_code': str(item_code_t),
        #             'quantity': str(quantity),
        #             'price_diff_net': float(quantity) * (
        #                 price_dict['item_cost'] - quant_dict[item_code_t][1]),
        #             'item_name': item_name_dict.get(item_code_t)
        #         })
        #     response['offer_price_list'] = return_list
        #     response['success'] = True
        # except:
        #      pass
        # return response

    def get_item_offer_amount(self, order_code):
        response = {
            'success': False,
            'amount': None
        }
        try:
            query = 'select offer_diff from order_item_offer_details where order_code = %s'
            self.cursor.execute(query, [order_code])
            response['amount'] = Decimal(self.cursor.fetchone()[0])
            response['success'] = True
        except:
            pass
        return response

    def get_lazylad_charge_for_seller(self, seller_id, total_amount, logistic_services_flag):
        query = 'select lazy_del_flat_cut, lazy_del_perc_cut,  self_del_flat_cut, self_del_perc_cut, lazylad_charge_type from service_providers where sp_code = ' + seller_id
        self.cursor.execute(query)
        data = self.cursor.fetchone()

        lazy_flat = data[0]
        lazy_perc = data[1]
        self_flat = data[2]
        self_perc = data[3]
        charge_model = data[4]

        if 0 == logistic_services_flag:
            flat = self_flat
            perc = self_perc
        else:
            flat = lazy_flat
            perc = lazy_perc


        if 1 == charge_model:
            return flat
        elif 2 == charge_model:
            return (perc*.01) * total_amount
        else:
            return (flat + ((perc*.01) * total_amount))


    def get_lazylad_incentive_for_seller(self, seller_id):
        query = 'select incentive_model from service_providers where sp_code=%s'
        self.cursor.execute(query, [seller_id])
        data = self.cursor.fetchone()
        incentive_model = data[0]
        if 1 == incentive_model:
            return 0
        else:
            return 0

    def get_referral_prefix_from_owner(self, owner_id, user_type):
        referral_prefix = None
        if int(user_type) == UserType.CUSTOMER:
            query = "select first_name,last_name from user_profile where phone_number=%s"
            self.cursor.execute(query, [owner_id])
            try:
                data = self.cursor.fetchone()
                firstname = data[0]
                lastname=data[1]
                if firstname :
                    referral_prefix = firstname.lower()
                    if lastname:
                        referral_prefix = referral_prefix + lastname[0].lower()
                else:
                    pass
            except:
                pass
        elif int(user_type) == UserType.SELLER:
            query = "select  sp_name from service_providers where sp_code=%s"
            self.cursor.execute(query, [owner_id])
            data = self.cursor.fetchone()
            try:
                referral_prefix = data[0].split()[0].lower()
            except (IndexError, AttributeError):
                pass

        return referral_prefix

    def shall_referral_happen(self, owner_id_referee, user_type):
        if int(user_type) == UserType.CUSTOMER:
            query = "select user_code from user_details where phone_number=%s"
            self.cursor.execute(query, [owner_id_referee])
            num_locals = len(self.cursor.fetchall())
            if 1 < num_locals:
                return False

        return True

    def get_order_strings_by_phone(self, phone):
        query = "select user_code from user_details where phone_number=%s"
        self.cursor.execute(query, [phone])
        user_codes_tuples = self.cursor.fetchall()
        user_codes = []
        for user_codes_tuple in user_codes_tuples:
            user_codes.append(user_codes_tuple[0])
        if len(user_codes):
            query = "select order_details.id, order_details.order_date , order_strings.order_string, \
                    order_details.expected_del_time, order_details.order_tot_amount\
                    from order_details INNER JOIN order_strings ON order_details.id=order_strings.o_id \
                    where user_code IN %s ORDER BY str_to_date( order_details.order_date , %s) DESC"
            format = '%a %b %d, %Y %k:%i'
            self.cursor.execute(query, [user_codes, format])
            order_details_tuples = self.cursor.fetchall()
            rows = []
            columns = [column[0] for column in self.cursor.description]
            for order_details_tuple in order_details_tuples:
                rows.append(dict(zip(columns, order_details_tuple)))
            return rows
        else:
            return []


    def close_connection(self):
        self.cursor.close()


def dictfetchall(cursor):
    " Return all rows from a cursor as a dict "
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
