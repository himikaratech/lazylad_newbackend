
from django.contrib.auth import authenticate


def authenticate_user(username=None, password=None):
    return_dict = {
        'success': False,
        'message': None
    }
    user = authenticate(
        username=username, password=password)
    if user is not None:
        if user.is_active:
            return_dict['success'] = True
            return_dict['message'] = \
                "User is valid, active and authenticated"
        else:
            return_dict['message'] = \
                "The password is valid, but the account has been disabled!"
    else:
        return_dict['message'] = "The username and password were incorrect."
    return return_dict
