
from django.db import connections

from .models import *
from .utils import *
from django.core.exceptions import ValidationError

class Lazyad:

    def __init__(self):
        pass

    def get_cities(self):
        response = {
            'success': False,
            'message': None,
        }
        try:
            cities = City.objects.all().values()
            response['cities'] = cities
            response['message'] = 'Success'
            response['success'] = True
        except :
            response['message'] = print_exception()

        return response

    def coupon_click_log(self, click_list):
        response = {
            'success': False,
            'message': None,
        }
        try:
            for click in click_list:
                try:
                    user = User.objects.get(user_code=click.get('user_code'))
                    coupon = Coupon.objects.get(id=int(click.get('coupon_id')))
                    coupon.clicks += int(click.get('counter'))
                    coupon.save()
                    click_log = CouponClickLog(
                        user=user,
                        coupon=coupon,
                        counter=int(click.get('counter'))
                    )
                    click_log.save()
                except:
                    pass
            response['message'] = 'Success'
            response['success'] = True
        except :
            response['message'] = print_exception()

        return response

    def coupon_view_log(self, view_list):
        response = {
            'success': False,
            'message': None,
        }
        try:
            for view in view_list:
                try:
                    user = User.objects.get(user_code=view.get('user_code'))
                    coupon = Coupon.objects.get(id=int(view.get('coupon_id')))
                    coupon.views += int(view.get('counter'))
                    coupon.save()
                    view_log = CouponViewLog(
                        user=user,
                        coupon=coupon,
                        counter=int(view.get('counter'))
                    )
                    view_log.save()
                except:
                    pass
            response['message'] = 'Success'
            response['success'] = True
        except :
            response['message'] = print_exception()

        return response

    def get_or_create_user(self, phone_number, username, password):
        response = {
            'success': False,
            'message': None,
            'user_code': None,
            'number_verified': None
        }
        try:
            user = User.objects.get(phone_number=phone_number)
            response['user_code'] = user.user_code
            response['number_verified'] = user.number_verified
            response['message'] = 'Success'
            response['success'] = True
        except User.DoesNotExist:
            user = User(
                username=username,
                password=password,
                phone_number=phone_number,
                user_code='USR' + str(User.objects.last().id+1)
            )
            user.save()
            response['user_code'] = user.user_code
            response['number_verified'] = user.number_verified
            response['message'] = 'Success'
            response['success'] = True
        except ValidationError:
            response['message'] = 'Values Not Allowed'
        except:
            response['message'] = print_exception()

        return response

    def user_signup(self, username, password, phone_number):
        response = {
            'success': False,
            'message': None,
            'user_code': None,
        }
        try:
            new_user = User(
                username=username,
                password=password,
                phone_number=phone_number
            )
            new_user.user_code = 'USR' + str(
                User.objects.last().id+1)
            new_user.save()
            response['user_code'] = new_user.user_code
            response['number_verified'] = new_user.number_verified
            response['message'] = 'Success'
            response['success'] = True
        except :
            response['message'] = print_exception()

        return response

    def user_signin(self, phone_number):
        response = {
            'success': False,
            'message': None,
            'user_code': None,
        }
        try:
            user = User.objects.get(phone_number=phone_number)
            response['user_code'] = user.user_code
            response['number_verified'] = user.number_verified
            response['message'] = 'Success'
            response['success'] = True
        except User.DoesNotExist:
            response['message'] = 'Invalid User'
        except :
            response['message'] = print_exception()

        return response

    def get_all_coupons(self, user_code, area_code, city_code):
        response = {
            'success': False,
            'message': None,
            'coupons': None,
        }
        try:
            filter_dict = dict()
            coupons = list()
            if user_code:
                filter_dict['user__user_code'] = user_code
            # if area_code:
            #     filter_dict['area__area_code'] = area_code
            if city_code:
                filter_dict['city__id'] = city_code
            coupon_codes = CouponCodes.objects.filter(
                **filter_dict).select_related('coupon')
            for coupon_code in coupon_codes:
                try:
                    temp_dict = dict((name, getattr(coupon_code, name))
                                     for name in coupon_code.__dict__
                                     if not name.startswith('_'))
                    temp_dict['coupon'] = dict(
                        (name, getattr(coupon_code.coupon, name))
                        for name in coupon_code.coupon.__dict__
                        if not name.startswith('_'))
                    temp_dict['user'] = dict(
                        (name, getattr(coupon_code.user, name))
                        for name in coupon_code.user.__dict__
                        if not name.startswith('_'))
                    coupons.append(temp_dict)
                except:
                    pass
            response['coupons'] = coupons
            response['message'] = 'Success'
            response['success'] = True
        except:
            response['message'] = print_exception()

        return response

    def get_coupon_code_details(self, coupon_code):
        response = {
            'success': False,
            'message': None,
            'coupon_details': None,
        }
        coupon_dict = dict()
        try:
            coupon = CouponCodes.objects.get(coupon_code=coupon_code)
            coupon_dict.update(dict(
                (name, getattr(
                    coupon, name)) for name in
                coupon.__dict__ if not name.startswith('_')))
            coupon_dict['coupon'] = dict(
                (name, getattr(
                    coupon.coupon, name)) for name in
                coupon.coupon.__dict__ if not name.startswith('_'))
            coupon_dict['user'] = dict(
                (name, getattr(
                    coupon.user, name)) for name in
                coupon.user.__dict__ if not name.startswith('_'))
            response['coupon_code_details'] = coupon_dict
            response['message'] = 'Success'
            response['success'] = True
        except CouponCodes.DoesNotExist:
            response['message'] = 'Invalid Coupon Code'
        except:
            response['message'] = print_exception()

        return response

    def get_coupon_details(self, coupon_id):
        response = {
            'success': False,
            'message': None,
            'coupon_details': None,
        }
        coupon_dict = dict()
        try:
            coupon = Coupon.objects.get(id=int(coupon_id))
            coupon_dict.update(dict(
                (name, getattr(
                    coupon, name)) for name in
                coupon.__dict__ if not name.startswith('_')))
            response['coupon_details'] = coupon_dict
            response['message'] = 'Success'
            response['success'] = True
        except CouponCodes.DoesNotExist:
            response['message'] = 'Invalid Coupon Code'
        except:
            response['message'] = print_exception()

        return response

    def buy_coupon(self, coupon_id, user_code, city_id, area_code):
        response = {
            'success': False,
            'message': None,
        }
        try:
            coupon = Coupon.objects.get(id=coupon_id)
            user = User.objects.get(user_code=user_code)
            if coupon.conversions >= coupon.number_of_coupons or not coupon.active:
                response['message'] = 'Coupon Not Active'
                fail_log = CouponUseFailedLog(
                    user_code=user.user_code,
                    coupon=coupon
                )
                fail_log.save()
                return response

            coupon_code = CouponCodes(
                coupon_code=generate_coupon_code(),
                user=user,
                coupon=coupon
            )
            if city_id:
                try:
                    city = City.objects.get(id=city_id)
                    coupon_code.city = city
                except City.DoesNotExist:
                    pass
            # if area_code:
            #     try:
            #         area = Area.objects.get(area_code=area_code)
            #         coupon_code.area = area
            #     except City.DoesNotExist:
            #         pass

            coupon_code.save()
            coupon.conversions += 1
            # if coupon.conversions >= coupon.redeemable_coupons:
            #     coupon.active = False
            coupon.save()
            use_log = CouponUseLog(
                user_code=user.user_code,
                coupon_code=coupon_code
            )
            use_log.save()
            response['message'] = 'Success'
            response['success'] = True
            response['coupon_code'] = coupon_code.coupon_code
        except Coupon.DoesNotExist:
            response['message'] = 'Invalid Coupon Id'
        except User.DoesNotExist:
            response['message'] = 'Invalid User Code'

        return response

    def get_coupons(self, user_code, city_code):
        response = {
            'success': False,
            'message': None,
            'coupons': None,
        }
        try:
            all_coupons = Coupon.objects.filter(active=True, city__id=city_code).values()
            response['coupons'] = all_coupons
            response['message'] = 'Success'
            response['success'] = True
        except:
            response['message'] = print_exception()
        return response

    def redeem_coupon(self, coupon_code, merchant_code):
        response = {
            'success': False,
            'message': None,
        }
        coupon_passed = True
        try:
            coupon_code = CouponCodes.objects.get(coupon_code=coupon_code)
            if not coupon_code.coupon.merchant.merchant_code == merchant_code:
                coupon_passed = False
                response['message'] = 'Coupon Code Invalid For Merchant'
            if not coupon_code.coupon.coupons_redeemed < \
                    coupon_code.coupon.number_of_coupons:
                redeem_log = CouponRedeemLog(
                    user_code=coupon_code.user.user_code,
                    coupon_code=coupon_code,
                    trans_type='EXP'
                    )
                redeem_log.save()
                coupon_passed = False
                response['message'] = 'Coupon Expired'
            if coupon_code.redeem:
                redeem_log = CouponRedeemLog(
                    user_code=coupon_code.user.user_code,
                    coupon_code=coupon_code,
                    trans_type='AR'
                    )
                redeem_log.save()
                coupon_passed = False
                response['message'] = 'Coupon Already Redeemed'
            if coupon_passed:
                coupon_code.redeem = True
                coupon_code.coupon.coupons_redeemed += 1
                coupon_code.coupon.save()
                coupon_code.save()
                redeem_log = CouponRedeemLog(
                    user_code=coupon_code.user.user_code,
                    coupon_code=coupon_code,
                    trans_type='SUC'
                    )
                redeem_log.save()
                response['message'] = 'Success'
                response['success'] = True
            else:
                pass
        except CouponCodes.DoesNotExist:
            response['message'] = 'Invalid Coupon Code'

        return response


    def sign_in_merchant(self, data):
        response = dict()
        try:
            try:
                merchant = Merchant.objects.get(
                    phone_no=data['phone_number'])
                if data['password'] == merchant.password:
                    response["success"] = True
                    response["merchant_name"] = merchant.name
                    response["merchant_authenticated"] = True
                    response["merchant_exist"] = True
                    response["merchant_code"] = merchant.merchant_code
                else:
                    response["merchant_exist"] = True
                    response["success"] = False
                    response["merchant_authenticated"] = False
            except Merchant.DoesNotExist:
                response["success"] = False
                response["merchant_authenticated"] = False
                response["merchant_exist"] = False
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def merchant_coupon_verification(self, data):
        response = dict()
        try:
            merchant = Merchant.objects.get(
                merchant_code=data['merchant_code'])
            try:
                coupon_code = CouponCodes.objects.get(
                    coupon_code=data['coupon_code'])
                if coupon_code.coupon.merchant == merchant:
                    if coupon_code.coupon.number_of_coupons >\
                            coupon_code.coupon.coupons_redeemed:
                        response["success"] = True
                        response["coupon_valid"] = True
                        response["coupon_exist"] = True
                        response["details"] = "Coupon Valid."
                    else:
                        response["success"] = False
                        response["coupon_valid"] = False
                        response["coupon_exist"] = True
                        response["details"] = "Coupon " \
                            "usage maximum limit reached."
                else:
                    response["success"] = False
                    response["coupon_valid"] = False
                    response["coupon_exist"] = False
                    response["details"] = "Coupon code " \
                        "merchant does not matches."
            except CouponCodes.DoesNotExist:
                response["success"] = False
                response["coupon_valid"] = False
                response["coupon_exist"] = False
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def merchant_coupon_details(self, data):
        response = dict()
        try:
            try:
                if data["coupon_type"] == "lazyad":
                    coupon = Coupon.objects.get(
                        name=data['coupon_name'])
                    coupon_details = {"coupon_name": coupon.name,
                                      "number_of_coupons":
                                          coupon.number_of_coupons,
                                      "coupon_desc": coupon.desc,
                                      "coupon_short_desc":
                                          coupon.short_desc,
                                      "terms": coupon.terms,
                                      "img_flag": coupon.img_flag,
                                      "img_address": coupon.img_address
                                      }
                    response["success"] = True
                    response["coupon_details"] = coupon_details
                if data["coupon_type"] == "lazylad":
                    cursor = connections['lazylad_server'].cursor()
                    query = "select * from cs_coupons where client_code = '"\
                            + str(data["merchant_code"]) +\
                            "' AND coupon_name='" +\
                            str(data["coupon_name"]) +\
                            "'AND active = 1"
                    rows_fetched = cursor.execute(query)
                    if rows_fetched:
                        c = dictfetchall(cursor)[0]
                        coupon_details = {"coupon_name": c["coupon_name"],
                                          "number_of_coupons":
                                              c["num_coupons"],
                                          "coupon_desc": c["coupon_desc"],
                                          "coupon_short_desc":
                                              c["coupon_short_desc"],
                                          "terms": c["terms"],
                                          "img_flag": c["coupon_img_flag"],
                                          "img_address": c["coupon_img_add"]
                                          }
                        response["success"] = True
                        response["coupon_details"] = coupon_details
                    else:
                        response["success"] = False
                        response["details"] = "Coupon does not exist"
            except Coupon.DoesNotExist:
                response["success"] = False
                response["details"] = "Coupon does not exist"
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def merchant_coupons(self, data):
        response = dict()
        try:
            merchant = Merchant.objects.get(
                merchant_code=data["merchant_code"])
            merchant_coupons = Coupon.objects.filter(merchant=merchant)
            coupons = list()
            for c in merchant_coupons:
                coupon = {"coupon_name": c.name,
                          "img_flag": c.img_flag,
                          "img_address": c.img_address,
                          "coupon_short_desc": c.short_desc,
                          "coupon_type": "lazyad"
                          }
                coupons.append(coupon)
            cursor = connections['lazylad_server'].cursor()
            query = "select * from cs_coupons where client_code = '"\
                    + str(data["merchant_code"])+"' AND active = 1"
            rows_fetched = cursor.execute(query)
            if rows_fetched:
                lad_coupons = dictfetchall(cursor)
                for coupon in lad_coupons:
                    coupon.update({"coupon_type": "lazylad"})
                    coupon["coupon_img_flag"] = True \
                        if coupon["coupon_img_flag"] == 1 else False
                    coupons.append(coupon)
            response["success"] = True
            response["coupons"] = coupons
        except:
            response["message"] = print_exception()
            response["success"] = False
        return response

    def merchant_coupon_analytics(self, data):
        response = dict()
        try:
            try:
                coupon = Coupon.objects.get(
                    name=data['coupon_name'])
                coupon_analytics = {"name": coupon.name,
                                    "hits": coupon.clicks,
                                    "views": coupon.views,
                                    "conversions": coupon.conversions,
                                    "number_of_coupons":
                                        coupon.number_of_coupons,
                                    "coupons_redeemed":
                                        coupon.coupons_redeemed,
                                    "coupon_desc": coupon.desc,
                                    "coupon_short_desc":
                                        coupon.short_desc,
                                    "terms": coupon.terms,
                                    "img_flag": coupon.img_flag,
                                    "img_address": coupon.img_address}
                response["success"] = True
                response["coupon_analytics"] = coupon_analytics
            except Coupon.DoesNotExist:
                response["success"] = False
                response["details"] = "Coupon does not exist"

        except:
            response["message"] = print_exception()
            response["success"] = False
        return response


def dictfetchall(cursor):
    " Return all rows from a cursor as a dict "
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class Lazylad:
    def __init__(self):
        self.cursor = connections['lazylad_server'].cursor()

    def get_cursor(self):
        return self.cursor

    def get_coupon_analytcs(self, sp_code, coupon_name):
        response = {
            'success': False,
            'items': None}
        try:
            merchant = Merchant.objects.get(merchant_code=sp_code)
            query = 'select * from cs_coupons where coupon_name = "' + coupon_name + '"'
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)
            coupon_id = data[0].get('coupon_id')
            query = 'select * from cs_coupon_analytics where coupon_id = ' + str(coupon_id)
            self.cursor.execute(query)
            data[0].update(**dictfetchall(self.cursor)[0])
            response['coupon_analytics'] = data[0]
            response['success'] = True
        except Merchant.DoesNotExist:
            response['message'] = 'Invalid Merchant Code'
        except IndexError:
            response['message'] = 'Invalid Coupon Name'
        except:
            response["message"] = print_exception()
        return response

    def lazylad_item_coupon_analytics(self, merchant_code, coupon_name):
        response = {
            'success': False,
            'items': None}
        try:
            query = "SELECT cc.* , cicm.*, id.* FROM cs_coupons cc," \
                    " cs_item_coupons_mapping cicm, item_details id WHERE " \
                    "cc.client_code='"+str(merchant_code)+"' " \
                    "AND cc.coupon_name =  '"+str(coupon_name)+"' AND " \
                    "cicm.cs_coupon_id = cc.coupon_id AND " \
                    "id.item_code=cicm.item_code"
            self.cursor.execute(query)
            data = dictfetchall(self.cursor)
            response["message"] = "No item mapped with this coupon!!!" \
                if len(data) == 0 else \
                (str(len(data))+" item mapped with this coupons")
            response['coupon_analytics'] = data
            response['success'] = True
        except:
            response["message"] = print_exception()
        return response
