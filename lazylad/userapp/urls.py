from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import *

api_urlpatterns = patterns(
    'userapp.apiviews',
    url(
        r'^getUserPreviousOrdersGSONLimited/(?P<user_code>\w+)$',
        UserOrders.as_view({'get': 'get_user_previous_orders'})),
    url(
        r'^getUserPreviousOrdersGSONLimited_v2/(?P<user_code>\w+)/(?P<page_number>\w+)$',
        UserOrders.as_view({'get': 'get_user_previous_orders_v2'})),
    url(
        r'^getOrderDetailsUser',
        UserOrders.as_view({'post': 'get_order_details_user'})),

    url(r'^configDetails_v2',
        Config.as_view({'post': 'get_user_config'})),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)
