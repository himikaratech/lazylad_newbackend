from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import Seller

api_urlpatterns = patterns(
    'seller.apiviews',
    url(
        r'^seller_profile',
        Seller.as_view({'get': 'get_seller_profile', 'post': 'update_seller_profile'}),
        name='seller-profile'),
    url(
        r'^confirm_items_service_provider_with_category',
        Seller.as_view({'post': 'confirm_items_service_provider_with_category'}),
        name='confirm_items_service_provider_with_category'),
    url(
        r'^seller_call_us',
        Seller.as_view({'post': 'seller_call_us'}),
        name='seller_call_us'),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)
