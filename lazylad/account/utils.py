from database.models import PaymentDetails, WalletUsageCapsForCities
from constant import ResponseConstant
from database.LazyladConnection import LazyladConnection
from django.db import connections
from .app_settings import ORDER_USAGE_ENTRY
from database.models import PaymentDetails


def return_wallet_details(owner_id, owner_type):
    wallet_usage = list()
    cursor = connections['lazylad_server'].cursor()
    if int(owner_type) == 0:
        query = "SELECT user_delivery_city FROM user_details WHERE phone_number = "\
                + str(owner_id) + " ORDER BY  created DESC LIMIT 1"
        rows_fetched = cursor.execute(query)
        city_code = cursor.fetchone()[0] if int(rows_fetched) else 0
        if not int(city_code):
            query = 'select cd.city_code from user_address_details uad, city_details cd where uad.user_phone_number = %s and \
            cd.city_name = uad.user_city ORDER BY user_address_code DESC LIMIT 1'
            rows_fetched = cursor.execute(query, [owner_id])
            if rows_fetched:
                city = cursor.fetchone()[0]
                usage_entry = WalletUsageCapsForCities.objects.filter(city_id=int(city))[0]
                ORDER_USAGE_ENTRY = (('urid_type', str(usage_entry.urid_type)),
                           ('usage_type', int(usage_entry.usage_type)),
                           ('usage_number', int(usage_entry.usage_number)),
                           ('usage_cap', int(usage_entry.usage_cap)))
            else:
                ORDER_USAGE_ENTRY = (('urid_type', 'Order'),
                       ('usage_type', 1),
                       ('usage_number', 0),
                       ('usage_cap', 0))
        else:
            usage_entry = WalletUsageCapsForCities.objects.filter(city_id=int(city_code))[0]
            ORDER_USAGE_ENTRY = (('urid_type', str(usage_entry.urid_type)),
                       ('usage_type', int(usage_entry.usage_type)),
                       ('usage_number', int(usage_entry.usage_number)),
                       ('usage_cap', int(usage_entry.usage_cap)))
    else:
        query = 'select sp_city_code from service_providers where sp_code = ' + str(owner_id)
        rows_fetched = cursor.execute(query)
        if rows_fetched:
            city = cursor.fetchone()[0]
            usage_entry = WalletUsageCapsForCities.objects.filter(city_id=int(city))[0]
            ORDER_USAGE_ENTRY = (('urid_type', str(usage_entry.urid_type)),
                       ('usage_type', int(usage_entry.usage_type)),
                       ('usage_number', int(usage_entry.usage_number)),
                       ('usage_cap', int(usage_entry.usage_cap)))
        else:
            ORDER_USAGE_ENTRY = (('urid_type', 'Order'),
                   ('usage_type', 1),
                   ('usage_number', 0),
                   ('usage_cap', 0))
    usage_entry = dict(ORDER_USAGE_ENTRY)
    wallet_usage.append(usage_entry)
    cursor.close()
    return wallet_usage


def get_order_payment_details(order_code):
    payments_made = PaymentDetails.objects.all().filter(order_code=order_code)
    pay_return = list()
    for payment in payments_made:
        pay = dict()

        pay[ResponseConstant.PAYMENT_MODE] = payment.p_mode
        pay[ResponseConstant.AMOUNT] = float(payment.amount)
        pay[ResponseConstant.PAYMENT_STATUS] = payment.p_status

        pay_return.append(pay)

    return {ResponseConstant.ORDER_PAYMENTS : pay_return}


def sp_code_from_order_code(order_code):
    try:
        cursor = connections['lazylad_server'].cursor()
        query = 'select sp_code from order_in_progress where order_code = ' + str(order_code)
        rows_fetched = cursor.execute(query)
        sp_code = cursor.fetchone()
        return sp_code[0]
    except:
        return None
