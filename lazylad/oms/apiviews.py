import json
from lazyprime.LazyPrimeConnection import LazyPrime as Prime
from .helper_functions import get_order_city_code, does_order_exists
from django.db import connections
from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from serializers import SendSMSSerializer, OfferDiffSerializer
from .services import send_sms, post_single_order, cancel_single_order
from .utils import group_list_by_sp_code, compute_total_items_amount,\
    sort_cart_by_amount, process_discount, group_by_sp_code
from database.models import *
from .LazyladConnection import LazyladConnection
from .utils import check_coupon_code_validity,get_lpm_orders
from functions import get_order_details
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from lazylad.auth.decorators import *
import lazyprime.models as lpm


class SendSMS(ViewSet):

    def send_sms(self, request):
        response = {'success': False,
                    'error_code': 1,
                    'message': "Unexpected Error Occurred"}
        data = request.data.copy()
        _serlz = SendSMSSerializer(data=data)
        try:
            if _serlz.is_valid():
                message = request.data.get('message')
                send_to = request.data.get('send_to')
                resp = send_sms(send_to, message)
                api_response_status = resp.status_code
                response['message'] = resp.content
                if resp.content.split(' ')[0] == 'success':
                    response['success'] = True
                    response['error_code'] = None
            else:
                response['message'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
                api_response_status = status.HTTP_200_OK
        except AttributeError:
            return Response(response, status.HTTP_200_OK)

        return Response(response, api_response_status)


class BulkOrder(ViewSet):

    def place_bulk_order(self, request):
        response = {'success': False,
                    'error_code': 1,
                    'message': 'Error in Processing',
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        api_response_status = status.HTTP_400_BAD_REQUEST

        shopping_cart = json.loads(data.get('JsonDataArray'))
        seller_details = json.loads(data.get('seller_details'))
        order_payment_details = json.loads(data.get('order_payment_details'))

        user_code = data.get('user_code', '')
        address_code = data.get('address_code', '')
        address_user_code = data.get('address_user_code', '')
        coupon_code = data.get('couponCode', '')
        expected_delivery_time = data.get('user_exp_del_time', '')
        st_code = data.get('st_code', '')

        total_amount = data.get('tot_amount')
        discount_amount = float(data.get('discount_amount', '0.0'))

        grouped_shopping_cart, v5_flag  = group_list_by_sp_code(unsorted_list=shopping_cart)
        grouped_seller_details = group_by_sp_code(unsorted_list=seller_details)
        grand_total_items_amount, processed_shopping_cart = \
            compute_total_items_amount(
                grouped_shopping_cart, grouped_seller_details)
        if not processed_shopping_cart:
            api_response_status = 200
            response['message'] = 'Values Missing In Post Request'
            return Response(response, api_response_status)
        sorted_orders_by_amount = sort_cart_by_amount(processed_shopping_cart)
        processed_retained_cart = process_discount(
            user_code,
            sorted_list= sorted_orders_by_amount,
            order_payment_details= order_payment_details,
            processed_shopping_cart= processed_shopping_cart,
            discount_amount=discount_amount)

        if not processed_retained_cart:
            response['message'] = 'Error while Capping wallet'
            api_response_status = status.HTTP_200_OK
            return Response(response, api_response_status)

        update_dict = {
            'user_code': user_code,
            'address_code': address_code,
            'address_user_code': address_user_code,
            'couponCode': coupon_code,
            'user_exp_del_time': expected_delivery_time,
            'st_code': st_code,
        }

        total_amount_after_discount = float(0)
        return_list = []
        orders_posted = []
        placed_orders = list()
        unplaced_orders = list()
        all_orders = list()
        all_sellers = str('[')
        for sp_code, details in processed_retained_cart.items():
            all_sellers += str(details['sp_code'])+','
            details.update(update_dict)
            all_orders.append(details)
            resp = post_single_order(post_dict=details, v5_flag=v5_flag)
            if resp.get('error', 0) != 1:
                details.update({'order_code': ''})
                details['reason'] = resp.get('message')
                unplaced_orders.append(details)
            else:
                order_code = resp.get('order_code')
                orders_posted.append(order_code)
                return_list.append(
                    {
                        'sp_code': sp_code,
                        'tot_amount': round(details['tot_amount']),
                        'order_code': order_code,
                    }
                )
                details.update({'order_code': order_code})
                placed_orders.append(details)
                total_amount_after_discount += details['tot_amount']

        all_sellers = all_sellers[0:-1:] + ']'
        bulk_order_details = BulkOrderDetails(
            user_code=user_code,
            bulk_order_details_json=json.dumps(all_orders),
            order_payment_details=json.dumps(order_payment_details),
            discount=discount_amount,
            coupon_code=coupon_code,
            sellers=all_sellers,
            order_amount=total_amount_after_discount,
        )
        bulk_order_details.save()
        for placed_order in placed_orders:
            bulk_order_single_order = BulkOrderSingleOrder(
                order_code=placed_order['order_code'],
                order_successfully_placed=True,
                sp_code=placed_order['sp_code'],
                order_json=json.dumps(placed_order),
            )
            bulk_order_single_order.save()
            bulk_order_single_order_mapping = BulkOrderSingleOrderMapping(
                bulk_order=bulk_order_details,
                bulk_order_single_order=bulk_order_single_order,
            )
            bulk_order_single_order_mapping.save()

        response['unplaced_order_details'] = list()
        for unplaced_order in unplaced_orders:
            response['unplaced_order_details'].append(unplaced_order)
            bulk_order_single_order = BulkOrderSingleOrder(
                order_code=unplaced_order['order_code'],
                order_successfully_placed=False,
                sp_code=unplaced_order['sp_code'],
                order_json=json.dumps(unplaced_order),
            )
            bulk_order_single_order.save()
            bulk_order_single_order_mapping = BulkOrderSingleOrderMapping(
                bulk_order=bulk_order_details,
                bulk_order_single_order=bulk_order_single_order,
            )
            bulk_order_single_order_mapping.save()

        response['error_code'] = 0
        response['message'] = 'Orders Successfully Created'
        response['success'] = True
        response['orders_details'] = return_list
        response['user_exp_del_time'] = expected_delivery_time
        response['tot_amount'] = round(total_amount_after_discount)

        api_response_status = status.HTTP_200_OK
        return Response(response, api_response_status)

    def update_logistics(self, request):
        response = {'success': False,
                    'error_code': 1,
                    'message': 'Error in Processing',
                    }
        data = self.request.data.copy()

        if not data:
            data = self.request.POST.copy()
        api_response_status = status.HTTP_400_BAD_REQUEST

        order_code = data.get('order_code')
        sp_code = data.get('sp_code')
        connection = LazyladConnection()
        resp = connection.update_logistics(sp_code, order_code)
        if resp:
            response['success'] = True
            response['message'] = 'Successfully Updated'
        api_response_status = status.HTTP_200_OK
        return Response(response, api_response_status)

    def order_offer_diff(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    'offer_diff': 0,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OfferDiffSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                resp = connection.get_offer_diff(
                    data.get('order_code'))
                response['error_code'] = 1
                response['success'] = True
                response['offer_diff'] = resp
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)


class Coupon(ViewSet):
    
    def check_coupon_code_validity_final(self, request):
        try:
            api_response_status = status.HTTP_400_BAD_REQUEST
            data = request.data.copy()
            if len(str(data['user_code'])) and len(str(data['coupon_code'])) and len(str(data['total_amount'])):
                response = check_coupon_code_validity(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    return Response(response, api_response_status)
                return Response(response, api_response_status)
            else:
                return Response({'success': False, 'error': 1, 'message': 'Bad Request'}, api_response_status)
        except Exception as exp:
            print exp


class orderDetailsbyCode(ViewSet):

    @login_required_vs
    def get_order_details_by_order_code(self, request):
        try:
            return render(request,"order_details_by_code.html")
        except Exception as inst:
            print inst


class orderDetails(ViewSet):

    @login_required_vs
    def get_order_details(self, request):
        try:
            o_str = request.GET.get('o_str')
            flag = does_order_exists(o_str)
            if flag:
                o_details = get_order_details(o_str, 6)
                city_code = get_order_city_code(o_str)
            else:
                o_details = {}
                city_code = ""
            return render(request, "order_string_details.html",
                          {'o_details': o_details, 'o_str': o_str,
                           'city_code': city_code, "order_exist": flag})
        except Exception as inst:
            print inst


class changeSeller(ViewSet):

    @login_required_vs
    def change_seller_of_order_get(self, request):
        try:
            order_string = request.GET.get('order_code')
            cursor = connections['lazylad_server'].cursor()
            query = "select oip.sp_code, oip.order_code, sp.sp_city_code, sp.sp_name from order_strings os, order_details od,\
             order_in_progress oip, service_providers sp where os.order_string = %s and od.id = os.o_id and \
             oip.order_code = od.order_code and sp.sp_code = oip.sp_code"
            cursor.execute(query, [order_string])
            data = cursor.fetchone()
            o_sp_code = data[0]
            o_code = data[1]
            sp_city_code = data[2]
            sp_name = data[3]
            query = "select sp.sp_code, sp.sp_name, ad.area_name \
            from service_providers sp, area_details ad where sp_city_code=%s and\
            ad.area_code=sp.sp_area_code and sp.shop_approval_status='SHOP_APPROVED'"
            cursor.execute(query, [sp_city_code])
            columns = [column[0] for column in cursor.description]
            sellers = cursor.fetchall()
            area_code_wise_sorted_sellers = dict()
            for seller in sellers:
                area_code_wise_sorted_sellers[str(seller[2])] = list()
            for seller in sellers:
                area_code_wise_sorted_sellers[str(seller[2])].append(dict({"sp_code":seller[0], "sp_name": seller[1]}))
            context = {
                "o_sp_code": o_sp_code,
                "o_code": o_code,
                "o_sp_name": sp_name,
                "sorted_sellers": area_code_wise_sorted_sellers,
            }
            cursor.close()
            return render(request, "change_seller.html", context)
        except Exception as inst:
            print inst

    @login_required_vs
    def change_seller_of_order_post(self, request):
        try:
            cursor = connections['lazylad_server'].cursor()
            new_seller_code = request.data.get('new_seller', '')
            order_code = request.data.get('order_code', '')
            query = 'select logistics_services from service_providers WHERE sp_code=%s'
            cursor.execute(query, [str(new_seller_code)])
            flag = cursor.fetchone()[0]
            query = 'UPDATE order_details SET logistics_services_used=%s WHERE order_code=%s'
            cursor.execute(query, [str(flag), order_code])
            query = 'UPDATE order_in_progress SET sp_code = %s WHERE order_code=%s'
            cursor.execute(query, [new_seller_code, order_code])
            query = 'select os.order_string from order_strings os, order_details od \
            where od.order_code = %s and os.o_id=od.id'
            cursor.execute(query, [order_code])
            order_string = cursor.fetchone()[0]
            return HttpResponseRedirect("./getOrderDetails?o_str="+str(order_string))
        except Exception as inst:
            print inst


@csrf_exempt
def add_item_to_seller(request):
    try:
        if request.method == 'GET':
            sp_code = request.GET.get('sp_code', '')
            item_code = request.GET.get('item_code', '')
            item_cost = request.GET.get('item_cost', '')
            if len(sp_code) and len(item_code) and len(item_cost):
                cursor = connections['lazylad_server'].cursor()
                query = "select Count(*) from items_offered_by_service_provider" \
                        " where item_code= "+str(item_code)+" and " \
                        "sp_code= "+str(sp_code)
                cursor.execute(query)
                count = cursor.fetchone()[0]
                if not count:
                    query = "insert into items_offered_by_service_provider" \
                            " (item_code, sp_code, item_cost, item_status) " \
                            "values ('"+str(item_code)+"', '" + \
                            str(sp_code)+"', '" + str(item_cost)+"','1')"
                    rows_affected = cursor.execute(query)
                    if rows_affected:
                        response = {'success': True}
                    else:
                        response = {'success': False}
                else:
                    response = {'success': True}
            else:
                response = {'success': False}
            return HttpResponse(json.dumps(response), content_type="application/json")
    except Exception as inst:
        print inst


class Coupons(ViewSet):

    @login_required_vs
    def get_create_coupons(self,request):
        try:
            return render(request, "create_coupons.html")
        except Exception as exp:
            print exp

    @login_required_vs
    def post_created_coupons(self, request):
        try:
            form = dict(request.POST.iterlists())
            cursor = connections['lazylad_server'].cursor()
            query = "SELECT coupon_code FROM coupons_by_lazylad ORDER BY id DESC LIMIT 1"
            cursor.execute(query)
            coupon_code = int(cursor.fetchone()[0]) + 1
            coupon_type_id = 3
            coupon_name = form['coupon_name'][0]
            discount_amount = form['discount_amount'][0]
            number_of_coupons = form['number_of_coupons'][0]
            minimum_order_amount = form['minimum_order_amount'][0]
            coupons_applied = 0
            apply_type = form['apply_type'][0]
            max_discount_amount = form['max_discount_amount'][0]
            num_coupons_per_user = form['num_coupons_per_user'][0]
            coupon_active = 1
            query = "SELECT * FROM coupons_by_lazylad WHERE coupon_name = '"+ str(coupon_name) +"'"
            rows_fetched = cursor.execute(query)
            if rows_fetched:
                return render(request, "create_coupons.html", {"msg": "Coupon Already Exist", "success": 0})
            else:
                query = "INSERT INTO coupons_by_lazylad (coupon_code, coupon_type_id, coupon_name, discount_amount,\
                        number_of_coupons, minimum_order_amount, coupons_applied, apply_type, max_discount_amount, \
                        num_coupons_per_user, coupon_active) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                cursor.execute(query, [coupon_code, coupon_type_id, coupon_name, discount_amount,
                                       number_of_coupons, minimum_order_amount, coupons_applied, apply_type,
                                       max_discount_amount, num_coupons_per_user, coupon_active])
                return render(request, "create_coupons.html", {"msg": "Coupon Created", "success": 1})
        except KeyError as ke:
            print ke
        except Exception as exp:
            print exp


@csrf_exempt
def check_seller_compatibility(request):
    try:
        if request.method == 'GET':
            order_code = request.GET.get('order_code', '')
            new_seller_code = request.GET.get('new_seller_code', '')
            cursor = connections['lazylad_server'].cursor()
            query = "SELECT item_code from order_item_details where \
                     order_code= %s and item_code not in  (SELECT item_code FROM items_offered_by_service_provider\
                     WHERE  sp_code = %s and item_code in (select item_code from order_item_details\
                     where order_code= %s))"
            cursor.execute(query, [order_code, new_seller_code, order_code])
            not_available_item_codes = ",".join("'"+str(item_code[0])+"'" for item_code in cursor.fetchall())
            if len(not_available_item_codes) == 0:
                not_available_item_codes = "'aaaa'"
            query = "select item_code, item_name,item_quantity, item_unit, item_cost, item_img_add, item_desc from item_details \
                    where item_code in (" + not_available_item_codes + ")"
            cursor.execute(query)
            columns = [column[0] for column in cursor.description]
            rows = cursor.fetchall()
            not_available_items = []
            for row in rows:
                not_available_items.append(dict(zip(columns, row)))
            query = "select item_name,item_quantity, item_unit, item_cost, item_img_add, item_desc from item_details \
                    where item_code in (select item_code from order_item_details where order_code= %s)"
            cursor.execute(query, [order_code])
            columns = [column[0] for column in cursor.description]
            rows = cursor.fetchall()
            ordered_items = []
            for row in rows:
                ordered_items.append(dict(zip(columns, row)))
            response = {
                'ordered_items': ordered_items,
                'not_available_items': not_available_items,
            }
            return HttpResponse(json.dumps(response), content_type="application/json")
    except Exception as inst:
        print inst


class LazyPrime(ViewSet):

    # @login_required_vs
    def lazyprime_orders(self, request):
        try:
            pen_orders = lpm.Order.objects.filter(status='PEN').order_by("-post_time")[0:5]
            con_orders = lpm.Order.objects.filter(status='CNF').order_by("-post_time")[0:5]
            conn = Prime()
            pen_orders_list = list()
            con_orders_list = list()
            for order in pen_orders:
                order_dict = conn.view_order(order.order_code)
                user_add = conn.get_address(order.user_code, order.address_id)
                order_dict.update(user_add)
                pen_orders_list.append(order_dict)
            for order in con_orders:
                order_dict = conn.view_order(order.order_code)
                user_add = conn.get_address(order.user_code, order.address_id)
                order_dict.update(user_add)
                con_orders_list.append(order_dict)
            context = {
                "pending": True if len(pen_orders_list) else False,
                "confirmed": True if len(con_orders_list) else False,
                "pen_orders": pen_orders_list,
                "con_orders": con_orders_list
            }
            return render(request, "lazyprime_orders.html", context)
        except Exception as exp:
            print exp

    def lpm_orders(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    'offer_diff': 0,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if 'con_last_order' and 'pen_last_order' in data:
                resp = get_lpm_orders(data['pen_last_order'],
                    data['con_last_order'])
                response = resp
            else:
                response['error'] = 'Bad Input'
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)
