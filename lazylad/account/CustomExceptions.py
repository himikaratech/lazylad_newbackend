__author__ = 'aman'

class BadRequestException(Exception):

    def __init__(self, value='Bad request'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class PostNotFoundException(Exception):

    def __init__(self, value='Post not found.'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ObjectNotFoundException(Exception):

    def __init__(self, value='Object not found.'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ObjectAlreadyExists(Exception):

    def __init__(self, value='Cant create because already exists'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class InvalidTypeException(Exception):

    def __init__(self, value='Invalid URL.'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class WalletNotFoundException(Exception):

    def __init__(self, value='Wallet not found.'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class WalletAlreadyExists(Exception):

    def __init__(self, value='Cant create because already exists'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class InsufficientAmount(Exception):

    def __init__(self, value='Not sufficient Amount'):
        self.value = value

    def __str__(self):
        return repr(self.value)

class InternalErrorMessage(Exception):

    def __init__(self, value='INTERNAL ERROR MESSAGE'):
        self.value = value

    def __str__(self):
        return repr(self.value)
