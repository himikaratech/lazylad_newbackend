
from django.db import models
import datetime


class SuccessLog(models.Model):
    created_at = models.DateTimeField(editable=False)
    request_json = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'ims'

    def save(self, *args, **kwargs):
        self.created_at = datetime.datetime.now()
        super(SuccessLog, self).save(*args, **kwargs)