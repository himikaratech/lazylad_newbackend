
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from django.core.exceptions import ValidationError
from .LazyladConnections import Coupons
from .serializers import *


class CouponSystem(ViewSet):

    def save_coupon_clicks(self, request):
        """
        API TO SEND
        :param request:
        {"click_logs":[{},{},{}......]}
        :return:
        {"success":True }
        """
        response = dict()
        connection = Coupons()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = CouponClicksSerializer(data=data)
            if _serlz.is_valid():
                response = connection.save_coupon_clicks(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def save_coupon_views(self, request):
        """
        API TO SEND
        :param request:
        {"view_logs":[{},{},{}......]}
        :return:
        {"success":True}
        """
        response = dict()
        connection = Coupons()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = CouponViewsSerializer(data=data)
            if _serlz.is_valid():
                response = connection.save_coupon_views(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def get_all_coupons(self, request):
        """
        API TO SEND ALL COUPONS

        """
        response = dict()
        connection = Coupons()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = request.GET.copy()
            if not data:
                data = request.data.copy()
            city_code = data.get('city_code', '')
            response = connection.get_all_coupons(city_code)
            if response['success']:
                api_response_status = status.HTTP_200_OK
                response.update({'error': 0})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)
