
from django.db import models
from decimal import Decimal
import datetime
from django.utils import timezone


class OfferItems(models.Model):
    sp_code = models.CharField(max_length=25)
    user_code = models.CharField(max_length=25)
    order_code = models.CharField(max_length=25)
    item_code = models.CharField(max_length=25)
    item_quantity = models.IntegerField()
    item_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    offer_cost = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    amount = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2)
    running_account_id = models.CharField(max_length=25)
    lazy_wallet_trans_id = models.CharField(max_length=25)
    created_at = models.DateTimeField(editable=False)

    class Meta:
        app_label = 'ims'

    def save(self, *args, **kwargs):
        super(OfferItems, self).save(*args, **kwargs)
