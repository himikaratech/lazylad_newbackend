from rest_framework import fields, serializers


class OrderDetailsSerlz(serializers.Serializer):
    order_code = serializers.CharField(required=True, allow_blank=False)
    user_code = serializers.CharField(required=True, allow_blank=False)


class ConfigAPISerlz(serializers.Serializer):
    area_code_selected = serializers.CharField(required=True)
    city_code_selected = serializers.CharField(required=True)
    imei_number = serializers.CharField(required=False, allow_blank=True)
    lazylad_version_code = serializers.IntegerField(required=True, max_value=None, min_value=None)
    os_type = serializers.IntegerField(required=True, max_value=None, min_value=None)
    mac_address = serializers.CharField(required=False, allow_blank=True)
    model_name = serializers.CharField(required=False, allow_blank=True)
    phone_manufacturer = serializers.CharField(required=False, allow_blank=True)
    user_code = serializers.CharField(required=True)

