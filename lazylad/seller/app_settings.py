
ACCOUNT_EDITABLE_FLAGS = (
    ('account_name', True),
    ('account_number', True),
    ('ifsc_code', True),
    ('branch_details', True),
)