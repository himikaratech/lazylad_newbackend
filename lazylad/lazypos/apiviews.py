
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from django.core.exceptions import ValidationError
from .serializers import FetchAllItemsSerializer, SendOrdersSerializer,\
    UpdateInventorySerializer, AddUsersSerializer, UserSerializer,\
    SyncSerializer, KhataSerializer, SellerSerializer, \
    ViewKhataSerializer, SellerLoginSerializer, AddSellersSerializer,\
    ImageInventorySerializer, SettleUpSerializer, SellerSMSSerializers, \
    SellerMonthlySerializer, SellerReportSerializer,\
    SellerDailyReportSerializer, OrderSerializer, GenerateOrderSerializer
from .LazyladConnection import order_logs, seller_sms, generate_orders
from .LazyladConnection import LazyladConnection, LazyPOS
from .models import *
from .app_settings import SYNC_PASSWORD
import json


class POS(ViewSet):

    def fetch_items(self, request):
        """

        API to Fetch All Available Items For POS

        request:
            st_code -

        :return:
            success
            error_code
            error_details
            items      [
                        {
                            'item_code'
                            'st_code'
                            'sc_code'
                            'item_name'
                            'item_type_code'
                            'item_unit'
                            'item_quantity'
                            'item_cost'
                            'item_barcode'
                            'curation_needed'
                            'item_short_desc'
                            'item_dsc'
                            'barcode_verified'
                            'synced'
                        },
                        {item2 as dict}
                        ]

        """

        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = FetchAllItemsSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.fetch_items(
                    data.get('st_code'), data.get('prev_item_code'))
                if resp.get('success'):
                    response['items'] = resp.get('items_list')
                    response['item_total_count'] = resp.get('item_total_count')
                    response['error_code'] = 1
                else:
                    response['error_details'] = 'Items Not Fetched From db'
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def send_orders(self, request):
        """

        API to Add Orders From POS

        request:
            orders_list - List of Orders
                            [
                                {
                                    'order_code':
                                    'khata_type': 'CRE'/'DEB'
                                    'khata_amount':
                                    'local_trans_id':
                                    'order_creation_time'
                                    'sp_code':
                                    'created_at':
                                    'order_amount':
                                    'order_cost':
                                    'delivery_cost':
                                    'discount':
                                    'taxes':
                                    'tax_percentage':
                                    'offline_order':
                                    'local_order':
                                    'items': []
                                    'user':{
                                            'user_name':
                                            'phone_number':
                                            'user_address':
                                            'user_city':
                                            }
                                },

                            ]
            sp_code -

        :return:
            success
            error_code
            error_details
            items_updates - {
                                   'order_1': [{
                                                    'item_code'
                                                    'item_barcode'
                                                },
                                                {},
                                              ]
                                }
            unsuccessful_orders - [
                                    'order1', 'order2'
                                    ]

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SendOrdersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                orders_list = data.get('orders_list')
                sp_code = data.get('sp_code')
                item_update_dict, unsuccessful_orders, khata_trans_list, \
                    successful_orders = connection.send_orders_to_POS(
                        orders_list, sp_code)
                response['item_update_list'] = item_update_dict
                response['unsuccessful_orders'] = unsuccessful_orders
                response['khata_trans_list'] = khata_trans_list
                response['successful_orders'] = successful_orders
                response['error_code'] = 1
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        order_logs(data, response, response['success'])
        return Response(response, api_response_status)

    def update_inventory(self, request):
        """

        API to Update Inventory Of A Seller From POS

        request:
            items_list - [
                            {
                            'item_code'
                            'st_code'
                            'sc_code'
                            'item_name'
                            'item_type_code'
                            'item_unit'
                            'item_quantity'
                            'item_cost'
                            'item_barcode'
                            'curation_needed'
                            'item_short_desc'
                            'item_dsc'
                            'barcode_verified'
                            'status' - 'AVA'/'OUT'
                            'local_flag'
                            }
                        ]
            sp_code -

        :return:
            success
            error_code
            error_details
            items_update_list - [{}, {}]

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()

        if not data:
            data = self.request.data.copy()
        _serlz = UpdateInventorySerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        connect = LazyladConnection()
        lazy_update_list = list()
        try:
            if _serlz.is_valid():
                items_list = data.get('items_list')
                sp_code = data.get('sp_code')
                if not items_list:
                    response['error_details'] = 'No Items To Update'
                    api_response_status = status.HTTP_200_OK
                    return response
                item_update_list = list()
                for item in items_list:
                    if item.get('local_flag') == 1:
                        barcode_verified = item.get('barcode_verified')
                        item_code, item_barcode = connection.add_new_item(
                            item, barcode_verified, sp_code)
                        item_update_list.append({
                                    'item_code': item_code,
                                    'barcode': item_barcode,
                                    'item_code_old': item.get('item_code'),
                                    'barcode_old': item.get('item_barcode'),
                                })
                        seller_items = SellerItems(
                                    sp_code=Seller.objects.get(
                                        sp_code=sp_code),
                                    item_code=Item.objects.get(
                                        item_code=item_code),
                                    item_cost=item.get('item_cost'),
                                    status=item.get('status', 'AVA'),
                                )
                        seller_items.save()
                    else:
                        item_code = item.get('item_code')
                        item_barcode = item.get('item_barcode')
                        try:
                            seller_items = SellerItems.objects.get(
                                    sp_code=sp_code, item_code=item_code)
                        except SellerItems.DoesNotExist:
                            seller_items = SellerItems(
                                sp_code=Seller.objects.get(sp_code=sp_code),
                                item_code=Item.objects.get(item_code=item_code),
                                item_cost=item.get('item_cost')
                            )
                            seller_items.save()
                        except Item.DoesNotExist:
                            pass
                        seller_items.item_cost = item.get('item_cost')
                        seller_items.status = item.get('status', 'AVA')
                        seller_items.item_quantity = int(item.get('item_quantity'))
                        seller_items.save()

                    lazy_update_list.append(
                        [item.get('item_cost'),
                         1 if item.get('status') == 'AVA' else 3,
                         item_code])
                # connect.update_items(sp_code, lazy_update_list)
                response['item_update_list'] = item_update_list
                response['error_code'] = 1
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def add_users(self, request):
        """

        API to Add New Users From POS

        request:
            users_list - [
                            {
                                'user_name'
                                'phone_number'
                                'user_address'
                                'user_city'
                            },
                        ]
            sp_code -

        :return:
            success
            error_code
            error_details
            failed_list  - [{'user_name': 'USER1', 'phone_number': '9999999999'}, ]
            successful_list - [{'user_name': 'USER1', 'phone_number': '9999999999'}, ]
            
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddUsersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        failed_list = []
        successful_list = []
        try:
            if _serlz.is_valid():
                users_list = data.get('users_list')
                sp_code = data.get('sp_code')
                if not users_list:
                    response['error_details'] = 'No Users To Add'
                    api_response_status = status.HTTP_200_OK
                    return response
                for user in users_list:
                    resp = connection.get_or_create_user(user, sp_code)
                    if resp.get('success'):
                        successful_list.append(
                            {'user_name': user.get('user_name'),
                             'phone_number': user.get('phone_number')})
                    else:
                        failed_list.append(
                            {'user_name': user.get('user_name'),
                             'phone_number': user.get('phone_number')})
                response['failed_list'] = failed_list
                response['successful_list'] = successful_list
                response['error_code'] = 1
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def edit_users(self, request):
        """
        API to edit existing user

        request:
            users_list - [
                            {
                                'user_name'
                                'phone_number'
                                'user_address'
                                'user_city'
                            },
                        ]
            sp_code -


        :return:
            success
            error_code
            error_details
            failed_list  - [{'user_name': 'USER1', 'phone_number': '9999999999'}, ]
            successful_list - [{'user_name': 'USER1', 'phone_number': '9999999999'}, ]
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddUsersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        failed_list = []
        successful_list = []
        try:
            if _serlz.is_valid():
                users_list = data.get('users_list')
                sp_code = data.get('sp_code')
                if not users_list:
                    response['error_details'] = 'No Users To Add'
                    api_response_status = status.HTTP_200_OK
                    return response
                for user in users_list:
                    resp = connection.get_or_create_user(user, sp_code)
                    if resp.get('success'):
                        successful_list.append(
                            {'user_name': user.get('user_name'),
                             'phone_number': user.get('phone_number')})
                    else:
                        failed_list.append(
                            {'user_name': user.get('user_name'),
                             'phone_number': user.get('phone_number')})
                response['error_code'] = 1
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def use_khata(self, request):
        """
        API To Update Khata Of a User Explicitly
        This means khata editing is implemented
        independent of Order Placed
        request:
            sp_code
            user_name
            phone_number
            trans_type - CRE/DEB
            amount
        return:
            success
            error_code
            error_details
            new_balance
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = KhataSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        khata_trans_list = list()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                khata_transactions = data.get('khata_transactions')
                for trans_dict in khata_transactions:
                    user = connection.get_or_create_user(
                        {'user_name': trans_dict.get('user_name'),
                         'phone_number': trans_dict.get('phone_number')},
                        sp_code
                    )
                    khata_resp = connection.khata_transaction(
                        user.get('user'),
                        trans_dict.get('trans_type'),
                        trans_dict.get('amount'),
                        sp_code,
                        reference='Order Independent'
                    )
                    if khata_resp.get('success'):
                        khata_resp.get('khata_log').update({
                            'new_balance': khata_resp.get('khata_log').get('new_balance'),
                            'khata_trans_global_id': khata_resp.get('khata_log').get('khata_trans_global_id'),
                            'local_trans_id': trans_dict.get('local_trans_id'),
                        })
                        khata_trans_list.append(khata_resp.get('khata_log'))
                response['khata_transactions'] = khata_trans_list
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def fetch_khata(self, request):
        """
        API To fetch all khata related details of a user
        for a seller

        request:
            sp_code
            user_name
            phone_number

        return:
            success
            error_code
            error_details
            khata_logs - [
                            {
                                'sp_code'
                                'trans_type'
                                'user_code'
                                'amount'
                                'trans_type'
                            }
                        ]
            balance
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = ViewKhataSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                resp = connection.get_khata(
                    sp_code,
                    {'user_name': data.get('user_name'),
                     'phone_number': data.get('phone_number')}
                )
                if resp.get('success'):
                    response['success'] = True
                    response['khata_logs'] = resp.get('khata_logs')
                    response['balance'] = resp.get('balance')
                    response['khata_id'] = resp.get('khata_id')
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def fetch_all_khate(self, request):
        """
        API to fetch all khate of a user

        request:
            sp_code
        return:
            success
            error_code
            error_details
            khate - [
                        {
                            'user_name'
                            'phone_number'
                            'balance'
                        },
                        {
                        }
                    ]

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                resp = connection.all_khate(sp_code)
                if resp.get('success'):
                    response['success'] = True
                    response['khate'] = resp.get('khata_list')
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def add_seller(self, request):
        """
        API to add a new seller

        request:
            sp_code
            name
            city_code
            address
        return:
            success
            error_code
            error_details

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddSellersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                sellers_list = data.get('sellers_list')
                if not sellers_list:
                    response['error_details'] = 'No Sellers To Add'
                    api_response_status = status.HTTP_200_OK
                    return response
                resp = connection.add_seller(sellers_list)
                response['error_code'] = 1
                response['success'] = True
                response['failed_list'] = resp
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def edit_seller(self, request):
        """
        API to edit a seller

        request:
            sp_code
            name
            city_code
            address
        return:
            success
            error_code
            error_details

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = AddUsersSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                users_list = data.get('users_list')
                sp_code = data.get('sp_code')
                if not users_list:
                    response['error_details'] = 'No Users To Add'
                    api_response_status = status.HTTP_200_OK
                    return response
                for user in users_list:
                    connection.edit_user(user, sp_code)
                    response['error_code'] = 1
                    response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def user_orders(self, request):
        """
        API to view orders of a user at a seller

        request:
            sp_code
            user_name
            phone_number
            sp_code
        return:
            success
            error_code
            error_details
            orders_list

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = UserSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                user = User.objects.get(
                    user_name=data.get('user_name').upper(),
                    phone_number=data.get('phone_number'))
                orders = Order.objects.filter(
                    user_code=user,
                    sp_code__sp_code=data.get('sp_code')
                ).values()
                response['orders_list'] = orders
                response['success'] = True
                response['error_code'] = 1
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except User.DoesNotExists:
            response['error_details'] = 'User Not Found'
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def initial_sync_items(self, request):
        """
        API to sync items initially
        from lazylad to lazypos

        request:
            password
        return:
            success
            error_code
            error_details

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SyncSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                password = data.get('password')
                if password == SYNC_PASSWORD:
                    resp = connection.initial_sync_items()
                else:
                    response['error_message'] = 'Invalid Password'
                    return Response(response, api_response_status)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def sync_items(self, request):
        """
        API to sync items
        from lazypos to lazylad

        request:
            password
        return:
            success
            error_code
            error_details

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SyncSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                password = data.get('password')
                if password == SYNC_PASSWORD:
                    resp = connection.sync_items()
                else:
                    response['error_message'] = 'Invalid Password'
                    return Response(response, api_response_status)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def initial_sync_seller(self, request):
        """
        API to sync sellers initially
        from lazylad to lazypos

        request:
            password
        return:
            success
            error_code
            error_details

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SyncSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyladConnection()
        try:
            if _serlz.is_valid():
                password = data.get('password')
                if password == SYNC_PASSWORD:
                    resp = connection.initial_sync_sellers()
                else:
                    response['error_message'] = 'Invalid Password'
                    return Response(response, api_response_status)
                if resp.get('success'):
                    response['error_code'] = 1
                    response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_orders(self, request):
        """
        API to fetch all orders of a seller

        request:
            sp_code
            order_code
        return:
            success
            error_code
            error_details
            orders - [
                        {

                        },
                        {

                        },
                    ]

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                prev_order_no = data.get('order_code')
                resp = connection.all_orders(sp_code, prev_order_no)
                if resp.get('success'):
                    response['success'] = True
                    response['orders'] = resp.get('orders')
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def fetch_seller_inventory(self, request):
        """
        API to fetch inventory of a seller

        request:
            sp_code
            item_code
        return:
            success
            error_code
            error_details
            inventory - [
                        {

                        },
                        {

                        },
                    ]

        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                prev_item_code = data.get('item_code')
                resp = connection.seller_inventory(sp_code, prev_item_code)
                if resp.get('success'):
                    response['success'] = True
                    response['inventory'] = resp.get('inventory')
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_login(self, request):
        """

        :param request:
        :return:
        """
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerLoginSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                username = data.get('username')
                password = data.get('password')
                resp = connection.seller_login(username, password)
                if resp.get('success'):
                    response['success'] = True
                    response['sp_code'] = resp.get('sp_code')
                    response['message'] = resp.get('message')
                    response['city'] = resp.get('city')
                    response['address'] = resp.get('address')
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_populate_items(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        connect = LazyladConnection()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                resp = connect.populate_seller_items(sp_code)
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def pos_config(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                sp_code = data.get('sp_code')
                resp = connection.config(sp_code)
                if resp.get('success'):
                    response['success'] = True
                    response['name'] = resp.get('name')
                    response['address'] = resp.get('address')
                    response['permission'] = resp.get('permission')
                    response['phone_number'] = resp.get('phone_number')
                    response['welcome_text'] = resp.get('welcome_text')
                    response['error_code'] = 1
                    response['city'] = resp.get('city')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def inventory_image(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = ImageInventorySerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.image_inventory(
                    data.get('sp_code'), data.get('inventory_image'))
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def settle_up(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SettleUpSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.settle_up(
                    data.get('sp_code'), data.get('settle_up_payment'))
                if resp.get('success'):
                    response['success'] = True
                    response['unsettled_amount'] = resp.get('unsettled_amount')
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_sms(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSMSSerializers(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                resp = seller_sms(
                    data.get('sp_code'),
                    data.get('recipients_list'),
                    data.get('message')
                )
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_daily_report(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerDailyReportSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.seller_daily_report(
                    data.get('sp_code'),
                    data.get('day'),
                    data.get('month'),
                    data.get('year'))
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                    response['report'] = resp.get('report')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_monthly_report(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerMonthlySerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.seller_monthly_report(
                    data.get('sp_code'), data.get('month'), data.get('year'))
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                    response['report'] = resp.get('report')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_report(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.POST.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerReportSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.seller_report(
                    data.get('sp_code'), data.get('from_day'),
                    data.get('from_month'), data.get('from_year'),
                    data.get('to_day'), data.get('to_month'),
                    data.get('to_year'))
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                    response['report'] = resp.get('report')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def seller_unsettled_amount(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = SellerSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.seller_unsettled_amount(
                    data.get('sp_code'))
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                    response['running_account'] = resp.get('running_account')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)


class Services(ViewSet):

    def email_bill(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = OrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                resp = connection.send_email(data.get('order_code'))
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)

    def generate_orders(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        _serlz = GenerateOrderSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        connection = LazyPOS()
        try:
            if _serlz.is_valid():
                no_of_times = data.get('no_of_times', 1)
                resp = generate_orders(no_of_times)
                if resp.get('success'):
                    response['success'] = True
                    response['error_code'] = 1
                    response['orders_posted'] = resp.get('orders_posted')
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)