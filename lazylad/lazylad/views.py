from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from lazylad.auth.decorators import *

@csrf_exempt
def login(request):
    try:
        if not request.user.is_authenticated():
            return HttpResponseRedirect('/newserver/admin/login/?next=/newserver/home')
        else:
            return HttpResponseRedirect('/newserver/home')
    except Exception as inst:
        print inst

@csrf_exempt
@login_required
def home(request):
    try:
        return render(request, "home.html")
    except Exception as inst:
        print inst
