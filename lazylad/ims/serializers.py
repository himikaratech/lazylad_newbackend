
from rest_framework import fields, serializers


class ItemClicksSerializer(serializers.Serializer):
    click_logs = serializers.ListField(
         required=True, child=serializers.DictField())


class ItemViewsSerializer(serializers.Serializer):
    view_logs = serializers.ListField(
         required=True, child=serializers.DictField())
