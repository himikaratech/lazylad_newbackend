import json
import re
import string
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from account import helper
from account.constant import ResponseConstant, ErrorCode, RequestConstant, TransactionType, ACCOUNTING_TYPE, STATUS, UserType
from database.LazyladConnection import LazyladConnection
from database.models import RsReferralCodes, RsReferralLog, Transactions, RsReferralRules
from .utils import referral_referrer_description, referral_referee_description
from lazylad.auth.decorators import *

@csrf_exempt
def get_referral_code_for_owner(request):
    ret_method = dict()
    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                data = request.POST
            else:
                data = json.loads(request.body)
            user_code = data[RequestConstant.USER_CODE]
            owner_type = data[RequestConstant.OWNER_TYPE]
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connection = LazyladConnection()
        owner_id = connection.get_owner_id_from_user_code(user_code, owner_type)
        if owner_id is None:
            ret_method[ResponseConstant.MESSAGE] = 'Invalid Details'
            ret_method[ResponseConstant.SUCCESS] = False
            ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
            connection.close_connection()
            return HttpResponse(json.dumps(ret_method), content_type="application/json")
        referral_set = RsReferralCodes.objects.all().filter(owner_id=owner_id, owner_type=owner_type, active_flag=1)
        if len(referral_set) == 0:
            connection.close_connection()
            return create_referral_code_for_owner(request)
        ret_method[ResponseConstant.REFERRAL_CODE] = referral_set[0].referral_code
        ret_method[ResponseConstant.SUCCESS] = True
        connection.close_connection()
        return HttpResponse(json.dumps(ret_method), content_type="application/json")
    else:
        ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
        ret_method[ResponseConstant.SUCCESS] = False
        ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
        return HttpResponse(json.dumps(ret_method), content_type="application/json")

@csrf_exempt
def perform_referral(request):
    ret_method = dict()
    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                request = request.POST
            else:
                request = json.loads(request.body)
            user_code_referee = request[RequestConstant.USER_CODE]
            owner_type_referee = request[RequestConstant.OWNER_TYPE]
            referral_code = request[RequestConstant.REFERRAL_CODE]
            referral_code = referral_code.lower()
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")
        connection = LazyladConnection()
        owner_id_referee = connection.get_owner_id_from_user_code(user_code_referee, owner_type_referee)
        referral_set = RsReferralCodes.objects.all().filter(referral_code=referral_code)
        if len(referral_set) == 0:
            ret_method[ResponseConstant.MESSAGE] = 'Invalid Referral Code'
            ret_method[ResponseConstant.SUCCESS] = False
            ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
            connection.close_connection()
            return HttpResponse(json.dumps(ret_method), content_type="application/json")
        referral_detail = referral_set[0]

        try:
            referral_rule = RsReferralRules.objects.get(referrer_type = referral_detail.owner_type, referree_type = owner_type_referee)
        except RsReferralRules.DoesNotExist:
            ret_method[ResponseConstant.MESSAGE] = 'Referral Rule Not Exists'
            ret_method[ResponseConstant.SUCCESS] = False
            ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.REFFERAL_ERROR_CODE
            connection.close_connection()
            return HttpResponse(json.dumps(ret_method), content_type="application/json")

        referral_amount = referral_rule.bonus_referrer
        referee_amount = referral_rule.bonus_referee

        if not connection.shall_referral_happen(owner_id_referee, owner_type_referee):
            ret_method[ResponseConstant.MESSAGE] = 'Referral shall not done'
            ret_method[ResponseConstant.SUCCESS] = False
            ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.REFFERAL_ERROR_CODE
            connection.close_connection()
            return HttpResponse(json.dumps(ret_method), content_type="application/json")

        try:
            RsReferralLog.objects.get(referee_owner_id=owner_id_referee,referee_type=owner_type_referee)
            ret_method[ResponseConstant.MESSAGE] = 'Referral already used'
            ret_method[ResponseConstant.SUCCESS] = False
            ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.REFFERAL_ERROR_CODE
            connection.close_connection()
            return HttpResponse(json.dumps(ret_method), content_type="application/json")
        except RsReferralLog.DoesNotExist:
            pass

        log = RsReferralLog(referral_log="Referral", referrer_type=referral_detail.owner_type,
                            referrer_owner_id=referral_detail.owner_id, referee_type=owner_type_referee,
                            referee_owner_id=owner_id_referee, referral_code_used=referral_code)
        log.save()

        if int(owner_type_referee) in [UserType.CUSTOMER, UserType.SELLER]:
            referee_wallet = connection.create_and_get_wallet(owner_id_referee, owner_type_referee)
            if referee_wallet is None:
                ret_method[ResponseConstant.MESSAGE] = 'User wallet does not exits'
                ret_method[ResponseConstant.SUCCESS] = False
                ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
                connection.close_connection()
                return HttpResponse(json.dumps(ret_method), content_type="application/json")
            if int(referee_amount) > 0:
                referee_transaction = Transactions(
                    w_id=referee_wallet, t_type=TransactionType.REFFERAL_BONUS,
                    urid=log.id,
                    urid_type="Referral",
                    description=referral_referee_description(referral_detail.owner_type, referral_detail.owner_id),
                    CorD=ACCOUNTING_TYPE.CREDIT,
                    amount=referee_amount, status=STATUS.PENDING)
                referee_transaction.save()

                helper.pay_amount_from_wallet(referee_wallet, -1 * referee_amount)
                referee_transaction.status = STATUS.SUCCESS
                referee_transaction.save()
                log.referee_tid = referee_transaction

        if int(referral_detail.owner_type) in [UserType.CUSTOMER, UserType.SELLER]:
            referral_wallet = connection.create_and_get_wallet(referral_detail.owner_id, referral_detail.owner_type)

            if int(referral_amount) > 0:
                referrer_transaction = Transactions(
                    w_id=referral_wallet, t_type=TransactionType.REFFERAL_BONUS,
                    urid=log.id,
                    urid_type="Referral",
                    description=referral_referrer_description(owner_type_referee, owner_id_referee),
                    CorD=ACCOUNTING_TYPE.CREDIT,
                    amount=referral_amount, status=STATUS.PENDING)
                referrer_transaction.save()

                helper.pay_amount_from_wallet(referral_wallet, -1 * referral_amount)
                referrer_transaction.status = STATUS.SUCCESS
                referrer_transaction.save()

                log.referrer_tid = referrer_transaction

        log.save()

        ret_method[ResponseConstant.MESSAGE] = 'Code successfully applied.'
        ret_method[ResponseConstant.SUCCESS] = True
        ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.FAILURE
        connection.close_connection()
        return HttpResponse(json.dumps(ret_method), content_type="application/json")
    else:
        ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
        ret_method[ResponseConstant.SUCCESS] = False
        ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
        return HttpResponse(json.dumps(ret_method), content_type="application/json")

@csrf_exempt
def create_referral_code_for_owner(request):
    ret_method = dict()
    if request.method == 'POST':
        try:
            if RequestConstant.USER_CODE in request.POST:
                request = request.POST
            else:
                request = json.loads(request.body)
            user_code = request[RequestConstant.USER_CODE]
            user_type = request[RequestConstant.OWNER_TYPE]

            try:
                prefix = request[RequestConstant.REFERRAL_CODE_PREFIX]
                prefix = prefix.lower()
            except:
                prefix = None
            try:
                update_flag = request[RequestConstant.UPDATE_FLAG]
            except:
                update_flag = 0
        except:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Values missing in post requests'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        connection = LazyladConnection()
        owner_id = connection.get_owner_id_from_user_code(user_code, user_type)
        if owner_id is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Owner Id is NULL'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.MISSING_VALUE
            ret[ResponseConstant.SUCCESS] = False
            connection.close_connection()
            return HttpResponse(json.dumps(ret), content_type="application/json")

        if prefix is None:
            prefix = connection.get_referral_prefix_from_owner(owner_id, user_type)
        connection.close_connection()

        if prefix is None:
            ret = dict()
            ret[ResponseConstant.MESSAGE] = 'Refferal Code could not build'
            ret[ResponseConstant.ERROR_CODE] = ErrorCode.REFFERAL_PREFIX_CANT_BUILD
            ret[ResponseConstant.SUCCESS] = False
            return HttpResponse(json.dumps(ret), content_type="application/json")

        try :
            activeReferral = RsReferralCodes.objects.get(owner_id=owner_id, owner_type=user_type, active_flag=1)
        except RsReferralCodes.DoesNotExist:
            activeReferral = None

        if update_flag == 1 or not activeReferral:
            if activeReferral:
                activeReferral.active_flag = 0
                activeReferral.save()

            regexToMatchPrefix = r'^' +re.escape(prefix)+ r'[1-9][a-z]$'
            refferalWithSamePrefix = RsReferralCodes.objects.all().filter(referral_code__iregex = regexToMatchPrefix)
            existingPrefixCount = refferalWithSamePrefix.count()

            ch = string.letters[(existingPrefixCount) % 26]  # logic in doc is implemented in this way to create prefix
            num = int((existingPrefixCount + 1) / 26) + 1
            if num > 9 : #check for now that num should be single digit
                ret = dict()
                ret['success'] = False
                ret['message'] = ErrorCode.REFFERAL_ERROR
                ret['error'] = ErrorCode.REFFERAL_ERROR_CODE
                return HttpResponse(json.dumps(ret), content_type="application/json")

            ref_code = prefix + str(num) + str(ch)
            new_entry = RsReferralCodes(owner_id=owner_id, owner_type=user_type, active_flag=1, referral_code=ref_code)

            try:
                new_entry.save()
                ret = dict()
                ret['success'] = True
                ret[ResponseConstant.REFERRAL_CODE] = new_entry.referral_code
                ret['error'] = 00
                return HttpResponse(json.dumps(ret), content_type="application/json")

            except:
                ret = dict()
                ret['success'] = False
                ret['message'] = ErrorCode.REFFERAL_ERROR
                ret['error'] = ErrorCode.REFFERAL_ERROR_CODE
                return HttpResponse(json.dumps(ret), content_type="application/json")
        else:
            ret = dict()
            ret['success'] = True
            ret[ResponseConstant.REFERRAL_CODE] = activeReferral.referral_code
            ret['error'] = 00
            return HttpResponse(json.dumps(ret), content_type="application/json")

    else:
        ret_method[ResponseConstant.MESSAGE] = 'Method not allowed'
        ret_method[ResponseConstant.SUCCESS] = False
        ret_method[ResponseConstant.ERROR_CODE] = ErrorCode.INVALID_METHOD_CALL
        return HttpResponse(json.dumps(ret_method), content_type="application/json")

@csrf_exempt
@login_required
def search_referee_by_code(request):
    try:
        if request.method == 'GET':
            try:
                ctx = {
                    "data":False,
                }
                return render(request,"refree.html",ctx)
            except Exception as inst:
                print inst
    except Exception as inst:
        print inst

@csrf_exempt
@login_required
def search_referee_by_code_result(request):
    try:
        if request.method == 'GET':
            try:
                code = request.GET.get('ref_code')
                if len(code):
                    ref_objects = RsReferralLog.objects.filter(referral_code_used = code)
                    ctx = {
                        "data":True,
                        "ref_objects":ref_objects,
                    }
                    return render(request, "refree.html", ctx)
                else:
                    ref_objects = []
                    ctx = {
                        "data":True,
                        "ref_objects":ref_objects,
                    }
                    return render(request, "refree.html", ctx)
            except Exception as inst:
                print inst
    except Exception as inst:
        print inst


@csrf_exempt
@login_required
def search_order_details_by_phone(request):
    try:
        if request.method == 'GET':
            try:
                ctx = {
                    "data": False,
                }
                return render(request, "order_details.html", ctx)
            except Exception as inst:
                print inst
    except Exception as inst:
        print inst

@csrf_exempt
@login_required
def search_order_details_by_phone_result(request):
    try:
        if request.method == 'GET':
            try:
                phone = request.GET.get('phn_no')
                if len(phone):
                    connection = LazyladConnection()
                    orders = connection.get_order_strings_by_phone(phone)
                    ctx = {
                        "data": True,
                        "orders": orders,
                    }
                    connection.close_connection()
                    return render(request, "order_details.html", ctx)
                else:
                    strings = []
                    ctx = {
                        "data": True,
                        "strings": strings,
                    }
                    return render(request, "order_details.html", ctx)
            except Exception as inst:
                print inst
    except Exception as inst:
        print inst