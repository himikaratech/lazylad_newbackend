
from rest_framework import fields, serializers


class CouponClicksSerializer(serializers.Serializer):
    click_logs = serializers.ListField(
         required=True, child=serializers.DictField())


class CouponViewsSerializer(serializers.Serializer):
    view_logs = serializers.ListField(
         required=True, child=serializers.DictField())
