
from rest_framework import fields, serializers


class LocationSerializer(serializers.Serializer):
    lat = serializers.CharField(required=True)
    long = serializers.CharField(required=True)
    city_code = serializers.CharField(required=True)


class SellerListSerializer(LocationSerializer):
    st_code = serializers.CharField(required=True)


class SaveUserLocationSerializer(LocationSerializer):
    user_code = serializers.CharField(required=True)

class SaveSellerLocationSerializer(LocationSerializer):
    sp_code = serializers.CharField(required=True)
