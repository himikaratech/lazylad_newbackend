import datetime

from django.db import models
from django.utils import timezone


class CustomeModel(models.Model):
    created = models.DateTimeField(default=timezone.now, editable=False)
    modified = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        self.modified = timezone.now()
        return super(CustomeModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True

# Create your models here.


class Wallet(CustomeModel):
    w_id = models.AutoField(primary_key=True)
    owner_id = models.CharField(max_length=30)
    owner_type = models.IntegerField()
    balance = models.DecimalField(max_digits=18, decimal_places=2)
    neg_allowed = models.BooleanField()
    max_neg_balance = models.DecimalField(max_digits=18, decimal_places=2)


class Transactions(CustomeModel):
    t_id = models.AutoField(primary_key=True)
    w_id = models.ForeignKey(Wallet)
    t_type = models.CharField(max_length=60)
    urid = models.CharField(max_length=30)
    urid_type = models.CharField(default='', max_length=30)
    description = models.CharField(max_length=1000)
    CorD = models.BooleanField()
    amount = models.DecimalField(max_digits=18, decimal_places=2)
    status = models.CharField(max_length=60)


class UserCodeRelation(CustomeModel):
    id = models.CharField(primary_key=True, max_length=31, auto_created=False)
    wallet = models.ForeignKey(Wallet)


class PaymentDetails(CustomeModel):
    pid = models.AutoField(primary_key=True)
    order_code = models.CharField(max_length=30)
    p_mode = models.CharField(max_length=60)
    amount = models.DecimalField(max_digits=18, decimal_places=2)
    p_status = models.CharField(max_length=60)
    p_tid = models.ForeignKey(Transactions)


class RsReferralRules(CustomeModel):
    rule_id = models.AutoField(primary_key=True)
    referrer_type = models.CharField(max_length=30)
    referree_type = models.CharField(max_length=30)
    bonus_referrer = models.DecimalField(max_digits=18, decimal_places=2)
    bonus_referee = models.DecimalField(max_digits=18, decimal_places=2)


class RsReferralCodes(CustomeModel):
    r_id = models.AutoField(primary_key=True)
    referral_code = models.CharField(max_length=30)
    owner_id = models.CharField(max_length=30)
    owner_type = models.CharField(max_length=30)
    active_flag = models.BooleanField()


class RsReferralLog(CustomeModel):
    id = models.AutoField(primary_key=True)
    referral_log = models.CharField(max_length=30)
    referrer_type = models.CharField(max_length=30)
    referrer_owner_id = models.CharField(max_length=30)
    referee_type = models.CharField(max_length=30)
    referee_owner_id = models.CharField(max_length=30)
    referrer_tid = models.ForeignKey(Transactions, related_name="referrer_tid", default=None,blank=True, null=True, on_delete=models.SET_NULL)
    referee_tid = models.ForeignKey(Transactions, related_name="referee_tid", default=None,blank=True, null=True, on_delete=models.SET_NULL)
    referral_code_used = models.CharField(max_length=30)


class OrderCompletionDetails(CustomeModel):
    id = models.AutoField(primary_key=True)
    lazy_lad_charges = models.DecimalField(max_digits=18, decimal_places=2)
    price_difference = models.DecimalField(max_digits=18, decimal_places=2)
    order_code = models.CharField(max_length=30)
    lazylad_charge = models.DecimalField(
        null=True, blank=True, max_digits=20, decimal_places=2, default=0.00)


class WalletUsageCapsForCities(CustomeModel):
    city_id = models.IntegerField(primary_key=True)
    urid_type = models.CharField(max_length=30)
    usage_number = models.IntegerField()
    usage_cap = models.IntegerField()
    usage_type = models.IntegerField()

class BulkOrderSingleOrder(CustomeModel):
    bulk_order_single_order_id = models.AutoField(primary_key=True)
    order_code = models.CharField(max_length=30, blank=True, default=None, null=True)
    sp_code = models.CharField(max_length=30)
    order_json = models.CharField(max_length=20000)
    order_successfully_placed = models.BooleanField()


class BulkOrderDetails(CustomeModel):
    bulk_order_id = models.AutoField(primary_key=True)
    user_code = models.CharField(max_length=30)
    bulk_order_details_json = models.CharField(max_length=20000)
    order_payment_details = models.CharField(max_length=20000)
    discount = models.DecimalField(max_digits=18, decimal_places=2)
    coupon_code = models.CharField(max_length=30)
    orders = models.ManyToManyField(
        BulkOrderSingleOrder,
        through="BulkOrderSingleOrderMapping",
        related_name="orders"
    )
    sellers = models.CharField(max_length=1000)
    order_amount = models.DecimalField(max_digits=18, decimal_places=2)


class BulkOrderSingleOrderMapping(CustomeModel):
    bulk_order = models.ForeignKey(BulkOrderDetails)
    bulk_order_single_order = models.ForeignKey(BulkOrderSingleOrder)


class WalletCashbackCouponDetails(CustomeModel):
    id = models.AutoField(primary_key=True)
    coupon_user_code = models.CharField(max_length=30)
    coupon_used = models.CharField(max_length=30)


class WalletCashbackCoupons(CustomeModel):
    id = models.AutoField(primary_key=True)
    coupon_name = models.CharField(max_length=30)
    remarks = models.CharField(max_length=200)
    coupon_cash = models.DecimalField(max_digits=18, decimal_places=2)
    total_coupons = models.IntegerField()
    coupons_used = models.IntegerField(default=0)
