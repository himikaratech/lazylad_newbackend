
import requests
import urllib
import json
from .api_settings import lazylad_old_server_api, headers
from .app_settings import LOGISTICS_OWNER_CODE


def get_order_details(order_code):
    url = lazylad_old_server_api+'/'
    try:
        res = requests.post(
            url, data=json.dumps({'order_code': order_code,
                                  'owner_code': LOGISTICS_OWNER_CODE}),
            headers=headers)
    except requests.exceptions.RequestException as e:
        return{'success': False, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return {'success': True,
                'message': 'Order Details Recieved',
                'order_details': json.loads(res.text).get('order_details')}
    else:
        return {'success': False, 'message': 'Order Details Not Found'}


def get_orders_for_list(order_list):
    url = lazylad_old_server_api+'getOrdersforList/'
    try:
        res = requests.post(
            url, data=json.dumps({'order_list': order_list,
                                  'owner_code': LOGISTICS_OWNER_CODE}),
            headers=headers)
    except requests.exceptions.RequestException as e:
        return{'success': False, 'message': 'Connection Error Occured'}
    if res.status_code == 200:
        return {'success': True,
                'message': 'Order Details Recieved',
                'order_details': json.loads(res.text).get('orders_list')}
    else:
        return {'success': False, 'message': 'Order Details Not Found'}

