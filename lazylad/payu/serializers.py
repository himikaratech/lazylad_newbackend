
from rest_framework import fields, serializers


class HashCreateSerializer(serializers.Serializer):
    user_code = serializers.CharField(required=True)
    amount = serializers.CharField(required=True)
    prod_info = serializers.CharField(required=True)
    json_obj = serializers.CharField()


class PostOrderSerializer(serializers.Serializer):
    trans_id = serializers.CharField()

