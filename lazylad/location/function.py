from __future__ import absolute_import

from location import helper_functions
from seller import LazyladConnection as sellerConnection
from location import LazyladConnection
from categories import functions as category_functions
from constant import UserType


def getSellersdatainLocation(lat, long, city_code, service_category):
    sellersList = helper_functions.getSellersListinLocation(lat, long, city_code, service_category)

    connection = sellerConnection.LazyladConnection()

    sellersData = connection.getSellersdataforSpcodeList(sellersList)
    return sellersData

def updateUserDeliveryLocation(user_code, lat, long, city_code):
    connection = LazyladConnection.LazyladConnection()
    connection.updateUserDeliveryLocation(user_code, lat, long, city_code)

    response = dict()
    service_types = category_functions.getServiceTypesForLocation(UserType.CUSTOMER, city_code, lat, long)
    response.update(service_types)

    return response


def updateUserDeliveryLocation_v2(user_code, lat, long, city_code):
    connection = LazyladConnection.LazyladConnection()
    connection.updateUserDeliveryLocation(user_code, lat, long, city_code)

    response = dict()
    service_types = category_functions.getServiceTypesForLocation_v2(UserType.CUSTOMER, city_code, lat, long)
    response.update(service_types)

    return response

def updateSellerShopCordinates(sp_code,lat,longi, city_code):
    connection = LazyladConnection.LazyladConnection()
    connection.updateSellerShopCordinates(sp_code, lat, longi, city_code)

def getSellerShopLocation(sp_code):
    connection = LazyladConnection.LazyladConnection()
    shop_location = connection.getSellerShopLocation(sp_code)
    return shop_location