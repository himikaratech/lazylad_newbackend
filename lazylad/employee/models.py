
from django.db import models
from .app_settings import EMPLOYEE_CHOICES
from database.models import CustomeModel


class Employee(CustomeModel):
    emp_code = models.CharField(max_length=30, primary_key=True)
    emp_type = models.CharField(max_length=55, choices=EMPLOYEE_CHOICES)
    emp_name = models.CharField(max_length=55)
