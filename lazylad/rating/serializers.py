from rest_framework import fields, serializers

class MerchantRatingSerializer(serializers.Serializer):
    feedback = serializers.CharField(required=False, allow_blank=True)
    urid_type = serializers.CharField(required=True)
    urid = serializers.CharField(required=True)
    rating = serializers.DictField(required=True)