from __future__ import absolute_import

from constant import UserType
from categories import LazyladConnection

def getServiceTypesForLocation(owner_type, city_code, latitude, longitude):
    connection = LazyladConnection.LazyladConnection()
    if UserType.CUSTOMER == owner_type:
        return connection.getServiceTypesforCityforBuyers(city_code)
    elif UserType.SELLER == owner_type:
        return connection.getServiceTypesforCityforSeller(city_code)
    else:
        return {"service_types":[]}


def getServiceTypesForLocation_v2(owner_type, city_code, latitude, longitude):
    connection = LazyladConnection.LazyladConnection()
    if UserType.CUSTOMER == owner_type:
        return connection.getServiceTypesforCityforBuyers_v2(city_code)
    elif UserType.SELLER == owner_type:
        return connection.getServiceTypesforCityforSeller(city_code)
    else:
        return {"service_types":[]}
