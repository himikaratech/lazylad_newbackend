from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns

from .apiviews import CashBack, Payment

api_urlpatterns = patterns(
    'account2.apiviews',
    url(
        r'^cashback',
        CashBack.as_view({'post': 'cash_back'}),
        name='cash-back'),
    url(
        r'^order_finish_seller_payment_details',
        Payment.as_view({'get': 'order_payment_details'}),
        name='payment_details'),
)

urlpatterns = format_suffix_patterns(api_urlpatterns)

urlpatterns.append(
    url(r'^', include('account.urls'))
)