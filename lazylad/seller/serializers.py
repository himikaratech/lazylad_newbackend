
from rest_framework import fields, serializers
from location import serializers as location_serializers


class SellerProfileSerializer(serializers.Serializer):
    seller_id = serializers.CharField(required=True)
    name = serializers.CharField(required=False)
    account_name = serializers.CharField(required=False)
    account_number = serializers.CharField(required=False)
    ifsc_code = serializers.CharField(required=False)
    branch_details = serializers.CharField(required=False)


class ConfirmItemSerial(serializers.Serializer):
    sp_code = serializers.CharField(required=True)
    sc_code = serializers.CharField(required=True)
    JsonDataArray = serializers.ListField(required=True,
                                          child=serializers.DictField())


