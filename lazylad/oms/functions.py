__author__ = 'AnkitAtreja'

import LazyladConnection
# from database import functions as database_functions
from database.functions import get_urid_list_original_mapping, get_urid_original_mapping, get_urid_original_mapping
import helper_functions
import api_settings


def get_orders_for_list(order_strings, owner_type):
    order_codes = get_urid_list_original_mapping(
        order_strings,api_settings.ORDER_URID)
    connection = LazyladConnection.LazyladConnection()
    return connection.getOrdersforList(order_codes, owner_type)


def get_order_basic_details(order_string, owner_type):
    order_code = get_urid_original_mapping(
        order_string,api_settings.ORDER_URID)
    return helper_functions.get_order_basic_details(order_code, owner_type)


def get_order_details(order_string, owner_type):
    order_code = get_urid_original_mapping(
        order_string,api_settings.ORDER_URID)
    return helper_functions.get_order_details(order_code, owner_type)

