
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from serializers import SaveSellerLocationSerializer, SellerListSerializer, \
    SaveUserLocationSerializer
from LazyladConnection import *
import function


class SaveUser(ViewSet):
    def update_user_cordinates(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = SaveUserLocationSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                owner_id = data.get('user_code')
                lat = float(data.get('lat', '0.0'))
                long = float(data.get('long', '0.0'))

            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)

    def update_user_delivery_location(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = SaveUserLocationSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                owner_id = data.get('user_code')
                lat = float(data.get('lat', '0.0'))
                long = float(data.get('long', '0.0'))
                city_code = data.get('city_code')
                location_specific_data = function.updateUserDeliveryLocation(owner_id, lat, long, city_code)

                if not location_specific_data:
                    response['error'] = 'Bad Input'
                    response['error_code'] = 0
                    api_response_status = status.HTTP_200_OK
                    raise AttributeError

                response['success'] = True
                response.update(location_specific_data)
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)

    def update_user_delivery_location_v2(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = SaveUserLocationSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                owner_id = data.get('user_code')
                lat = float(data.get('lat', '0.0'))
                long = float(data.get('long', '0.0'))
                city_code = data.get('city_code')
                location_specific_data = function.updateUserDeliveryLocation_v2(owner_id, lat, long, city_code)

                if not location_specific_data:
                    response['error'] = 'Bad Input'
                    response['error_code'] = 0
                    api_response_status = status.HTTP_200_OK
                    raise AttributeError

                response['success'] = True
                response.update(location_specific_data)
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)


class SaveSeller(ViewSet):
    def update_seller_cordinates(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = SaveSellerLocationSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                owner_id = data.get('sp_code')
                lat = float(data.get('lat', '0.0'))
                long = float(data.get('long', '0.0'))
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)

    def update_seller_shop_cordinates(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }
        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = SaveSellerLocationSerializer(data=data)
        connection = LazyladConnection()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                owner_id = data.get('sp_code')
                lat = float(data.get('lat', '0.0'))
                long = float(data.get('long', '0.0'))
                city_code = data.get('city_code')
                function.updateSellerShopCordinates(owner_id,lat,long,city_code)
                response['success'] = True
            else:
                response['error'] = 'Bad Input'
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)


class SellerList(ViewSet):
    def get_seller_list(self, request):
        response = {'success': False,
                    'error_code': 1,
                    }

        data = self.request.data.copy()
        if not data:
            data = self.request.POST.copy()
        _serlz = SellerListSerializer(data=data)

        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                lat = float(data.get('lat', '0.0'))
                long = float(data.get('long', '0.0'))
                city_code = data.get('city_code')
                service_category = data.get('st_code')
                sellers_data = function.getSellersdatainLocation(lat, long, city_code, service_category)
                if not sellers_data:
                    response['error'] = 'Bad Input'
                    response['error_code'] = 0
                    api_response_status = status.HTTP_200_OK
                    raise AttributeError

                response['success'] = True
                response.update(sellers_data)
            else:
                response['error'] = 'Bad Input'
                response['error_code'] = 0
                response.update({'error_details': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except AttributeError:
            pass

        return Response(response, api_response_status)
