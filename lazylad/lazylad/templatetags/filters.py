from django import template

register = template.Library()


@register.filter(name='check_permission')
def check_permission(user, arg):
    return len(user.user_permissions.filter(codename=arg)) == 1
