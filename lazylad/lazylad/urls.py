"""lazylad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from rest_framework import routers
from account.utility import *
from database.models import *
from . import views



# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^account/', include('account2.urls')),
    url(r'^referral/', include('referral.urls')),
    url(r'^oms/', include('oms.urls')),
    url(r'^seller/', include('seller.urls')),
    url(r'^account2/', include('account2.urls')),
    url(r'^logistics/', include('logistics.urls')),
    url(r'^location/', include('location.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^rating/', include('rating.urls')),
    url(r'^lazypos/', include('lazypos.urls')),
    url(r'^lazyprime/', include('lazyprime.urls')),
    url(r'^lazyad/', include('lazyad.urls')),
    url(r'^userapp/', include('userapp.urls')),
    url(r'^coupon/', include('cs.urls')),
    url(r'^item/', include('ims.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^login', views.login, name='login'),
    url(r'^home', views.home, name='home'),
    url(r'^revenue/', include('revenue.urls')),
    url(r'^ims/', include('ims.urls')),
    url(r'^payu/', include('payu.urls')),
    url(r'^services/', include('services.urls')),
]
