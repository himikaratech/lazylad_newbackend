from django.contrib import admin
from .models import RiderProfile, Order


class RiderProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'phone_no', 'license_no', 'logging', 'status')
    list_select_related = ('user', )
    search_fields = ['phone_no', 'status',]


admin.site.register(RiderProfile, RiderProfileAdmin)