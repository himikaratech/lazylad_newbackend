
import json
from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from decimal import Decimal
from serializers import CashBackSerializer
from .utils import process_cashback, order_payment_details


class CashBack(ViewSet):

    def cash_back(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'message': "Unexpected Error Occurred"}
        data = request.POST.copy()
        if not data:
            data = json.loads(request.body).copy()
        _serlz = CashBackSerializer(data=data)
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if _serlz.is_valid():
                if Decimal(data.get('amount')) < 0:
                    response['message'] = 'Cashback cannot be negative'
                    api_response_status = status.HTTP_200_OK
                    return Response(response, api_response_status)
                resp = process_cashback(update_dict=data)
                if resp:
                    response['success'] = True
                    response['error_code'] = 1
                    response['message'] = 'Cashback Successfully Updated'
                else:
                    response['message'] = 'Cashback Failed'
            else:
                response['error'] = 'Bad Input'
                response.update({'message': _serlz.errors})
            api_response_status = status.HTTP_200_OK
        except:
            pass

        return HttpResponse(json.dumps(response), content_type="application/json")


class Payment(ViewSet):
    def order_payment_details(self, request):
        response = {'success': False,
                    'error_code': 0,
                    'error_details': None,
                    }
        data = self.request.GET.copy()
        if not data:
            data = self.request.data.copy()
        if 'QueryDict' in type(data).__name__:
            data = data.dict()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            if 't_id' in data:
                resp = order_payment_details(data['t_id'])
                if resp.get('success'):
                    response['transaction_details'] = \
                        resp.get('transaction_details')
                    response['message'] = resp.get('message')
                    response['error_code'] = 1
                    response['success'] = True
                    api_response_status = status.HTTP_200_OK
                else:
                    response['error_details'] = resp.get('message')
            else:
                response['error'] = 'Bad Input'
                response.update(
                    {'error_details': 'Transaction Id'
                        ' (t_id) not found in request params'}
                )
        except:
            response['error_details'] = 'Bad Request'

        return Response(response, api_response_status)
