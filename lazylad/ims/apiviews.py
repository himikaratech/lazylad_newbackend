
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from django.core.exceptions import ValidationError
from .LazyladConnections import Items
from .serializers import *


class ItemSystem(ViewSet):

    def save_item_clicks(self, request):
        """
        API TO SEND
        :param request:
        {"click_logs":[{},{},{}......]}
        :return:
        {"success":True }
        """
        response = dict()
        connection = Items()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = ItemClicksSerializer(data=data)
            if _serlz.is_valid():
                response = connection.save_item_clicks(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)

    def save_item_views(self, request):
        """
        API TO SEND
        :param request:
        {"view_logs":[{},{},{}......]}
        :return:
        {"success":True}
        """
        response = dict()
        connection = Items()
        api_response_status = status.HTTP_400_BAD_REQUEST
        try:
            data = self.request.data.copy()
            if not data:
                data = self.request.POST.copy()
            _serlz = ItemViewsSerializer(data=data)
            if _serlz.is_valid():
                response = connection.save_item_views(data)
                if response['success']:
                    api_response_status = status.HTTP_200_OK
                    response.update({'error': 0})
            else:
                response['error_details'] = 'Bad Input'
                response.update({"Serializer errors": _serlz.errors})
        except:
            response['error_details'] = 'Bad Request'
        return Response(response, api_response_status)
