
from oms import functions as oms_functions
from .app_settings import LOGISTICS_OWNER_CODE


def order_details_get(order_code):
    resp = oms_functions.get_order_details(
        order_code, LOGISTICS_OWNER_CODE)
    return resp


def get_orders_list(order_list):
    resp = oms_functions.get_orders_for_list(
        order_list, LOGISTICS_OWNER_CODE)
    return resp
